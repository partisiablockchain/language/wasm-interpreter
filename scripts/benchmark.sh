#!/usr/bin/env bash

WATPATH="src/test/resources/com/partisiablockchain/language/wasm/interpreter/benchmark.wat"
WASMPATH="$WATPATH.wasm"

echo "Translating .wat file to .wasm"
wat2wasm $WATPATH -o $WASMPATH

echo "Wasm3 (C, API in Python)"
python "scripts/benchmark_wasm_impl.py" "$WASMPATH" --runs 100 --runner wasm3

echo "Wasmtime (JIT, Rust)"
python "scripts/benchmark_wasm_impl.py" "$WASMPATH" --runs 100 --runner wasmtime

echo "Partisia WASM (Java)"
export WASM_BENCHMARK="true"
mvn test -Dtest=com.partisiablockchain.language.wasm.interpreter.BenchmarkTest#benchStuff

echo "Pywasm (Python)"
python "scripts/benchmark_wasm_impl.py" "$WASMPATH" --runs 3 --runner pywasm
