import argparse
import time

WASMPATH = None
NUM_RUNS = 10
NANOS_IN_MILLIS = 1000000
INSTANCE_DATA = None
RUNNER = None
ARGUMENT_SIZE_MULT = 1


def run_pywasm(func_str, arguments):
    return INSTANCE_DATA.exec(func_str, arguments)


def run_wasm3(func_str, arguments):
    func = INSTANCE_DATA.find_function(func_str)
    return func(arguments[0], arguments[1])


def run_wasmtime(func_str, arguments):
    process = subprocess.run(['wasmtime', WASMPATH, '--invoke', func_str] +
                             [str(x) for x in arguments],
                             shell=False,
                             capture_output=True)
    return int(process.stdout)


def bench(name, benchId, argument, expectedOutcome):
    times = []

    for i in range(NUM_RUNS):
        timeBefore = time.perf_counter_ns()
        outcome = RUNNER("run_program", [benchId, argument])
        timeAfter = time.perf_counter_ns()
        times.append((timeAfter - timeBefore) // NANOS_IN_MILLIS)
        if ARGUMENT_SIZE_MULT == 1:
            assert outcome == expectedOutcome, "{}: Expected {}, got {}".format(
                name, expectedOutcome, outcome)

    formatTimes(name, argument, times)


def formatTimes(name, argument, times):
    times.sort()
    timeMin = times[0]
    timeMax = times[len(times) - 1]
    timeMedian = times[len(times) // 2]
    time95th = times[(len(times) * 20 - len(times)) // 20]
    print(
        "{:<20} ({:7})\t{:d} runs\t{:6d}ms min\t{:6d}ms median\t{:6d}ms 95th%\t{:6d}ms max"
        .format(name, argument, len(times), timeMin, timeMedian, time95th,
                timeMax))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("wasmpath")
    parser.add_argument("--runs", default=10, type=int)
    parser.add_argument("--runner",
                        required=True,
                        choices={'pywasm', 'wasmtime', 'wasm3'})

    args = parser.parse_args()

    NUM_RUNS = args.runs
    WASMPATH = args.wasmpath

    if args.runner == 'pywasm':
        import pywasm
        RUNNER = run_pywasm
        timeBeforeLoad = time.perf_counter_ns()
        INSTANCE_DATA = pywasm.load(WASMPATH)
        timeAfterLoad = time.perf_counter_ns()
        print("Loads: {:8d}ms".format(
            (timeAfterLoad - timeBeforeLoad) // NANOS_IN_MILLIS))
    elif args.runner == 'wasmtime':
        import subprocess
        print(
            "NOTE: wasmtime is not run from a python interface, so these numbers"
        )
        print(
            "      include a lot of overhead, including startup, parsing, etc."
        )
        RUNNER = run_wasmtime
    elif args.runner == 'wasm3':
        import wasm3
        env = wasm3.Environment()
        INSTANCE_DATA = env.new_runtime(100000)
        with open(WASMPATH, 'rb') as f:
            mod = env.parse_module(f.read())
        INSTANCE_DATA.load(mod)
        del env
        RUNNER = run_wasm3

    assert RUNNER, args.runner

    bench("Prime Sieve        ", 1100, ARGUMENT_SIZE_MULT * 1000, 76127)
    bench("Prime List         ", 1101, ARGUMENT_SIZE_MULT * 1000, 76127)
    bench("Trampoline Odd     ", 1102, ARGUMENT_SIZE_MULT * 100000 + 1, 1)
    bench("Trampoline plusZero", 1103, ARGUMENT_SIZE_MULT * 100000 + 1, 100001)
    bench("Sort random list   ", 1104, ARGUMENT_SIZE_MULT * 4000, -1061197204)
    bench("NopMap insert      ", 2001, ARGUMENT_SIZE_MULT * 1000, 1)
    bench("NopMap search      ", 2002, ARGUMENT_SIZE_MULT * 100, 20000)
    bench("BTreeMap insert    ", 2011, ARGUMENT_SIZE_MULT * 1000, 1856145921)
    bench("BTreeMap search    ", 2012, ARGUMENT_SIZE_MULT * 100, 1410005648)
    bench("VecMap insert      ", 2021, ARGUMENT_SIZE_MULT * 1000, 1856145921)
    bench("VecMap search      ", 2022, ARGUMENT_SIZE_MULT * 100, 1410005648)
    bench("SortedVecMap insert", 2031, ARGUMENT_SIZE_MULT * 1000, 1856145921)
    bench("SortedVecMap search", 2032, ARGUMENT_SIZE_MULT * 100, 1410005648)
    bench("HashMap insert     ", 2041, ARGUMENT_SIZE_MULT * 1000, 1856145921)
    bench("HashMap search     ", 2042, ARGUMENT_SIZE_MULT * 100, 1410005648)
