#!/usr/bin/env bash
## Update the WASM Test Spec files from the official source
# Call from the base folder of privacyblockchain/language/wasm-interpreter

convert_wast() {
  file_name=$(basename -- "$1")
  test_name="${file_name%.*}"
  echo "Expanding Test Spec $test_name"
  mkdir -p "$test_name"
  pushd "$test_name" || exit
  cp "$1" .
  wast2json --no-check --debug-names "$file_name"
  popd || exit
}

interpreter_dir=$(realpath .)
tmp_dir=$(mktemp -d -t WASM_SPEC_TEST_XXXXXXXX)
cd "$tmp_dir" || exit
git clone --branch "w3c-1.0" --depth 1 https://github.com/WebAssembly/spec.git/ "spec-1-0"
cd "$interpreter_dir"/src/test/resources/com/partisiablockchain/language/wasm/spec/ || exit
for wast_file in "$tmp_dir"/spec-1-0/test/core/*.wast; do
  convert_wast "$wast_file"
done

# Get updated spec tests for I32/I64 containing tests for sign extension
cd "$tmp_dir" || exit
git clone --branch "opam-1.1.1" --depth 1 https://github.com/WebAssembly/spec.git/ "spec-1-1-1"
cd "$interpreter_dir"/src/test/resources/com/partisiablockchain/language/wasm/spec/ || exit

convert_wast "$tmp_dir"/spec-1-1-1/test/core/i32.wast
convert_wast "$tmp_dir"/spec-1-1-1/test/core/i64.wast
