
# Partisia WASM interpreter

WASM 1.0 interpreter written in Java.

## Dependencies

- Java SDK
- Maven

Nice to have:

- [Rust](https://www.rust-lang.org/): `cargo`, `rustup`, `rustc`, etc...
- [wabt](https://github.com/WebAssembly/wabt)

## Test

The full test suite can be run using with the `test` Maven command:

```sh
mvn test
```

## Benchmark

Basic benchmarking for the interpreter can be run with the following command:

```sh
export WASM_BENCHMARK="true"
mvn test -Dtest=com.partisiablockchain.language.wasm.interpreter.BenchmarkTest#heavyBenchmarkSuite
```

See the Rust sourcecode in the `src/test/rust/performance-tests/` crate for what each benchmark does.

### Comparison benchmarks

Benchmarks comparing this interpreter with certain others  are available in `scripts/benchmark.sh`, and can be run with:

```sh
bash benchmark.sh
```

Implementations compared with are:

- [wasm3](https://github.com/wasm3/wasm3)
- [wasmtime](https://wasmtime.dev/)
- [pywasm](https://github.com/mohanson/pywasm)

All of them must be installed. Check the links above for how to install them;
you can probably get them through your package manager.

### CI Benchmarks

Benchmarks are automatically run every now and then. If you have developer
access, these benchmarks can be triggered manually from [the Gitlab pipeline runner](https://gitlab.com/privacyblockchain/language/wasm-interpreter/-/pipelines/new), by setting the variable `WASM_BENCHMARK` to `true`

## Updating benchmarks

When modifying the `performance-tests` crate, you must compile for the `wasm32-unknown unknown` target with the `release` profile, and you must remember to update the file ran by the benchmarking suite. This can be accomplished with following lines from the Git root:

```sh
cargo build --manifest-path src/test/rust/performance-tests/Cargo.toml --release --target wasm32-unknown-unknown
wasm2wat src/test/rust/performance-tests/target/wasm32-unknown-unknown/release/partisia-wasm-performance-tests.wasm -o src/test/resources/com/partisiablockchain/language/wasm/interpreter/benchmark.wat
```
