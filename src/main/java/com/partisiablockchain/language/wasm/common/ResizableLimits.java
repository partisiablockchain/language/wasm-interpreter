package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmParseExceptionImpl;

/** The resizeable limits of memory or table. */
@Immutable
public final class ResizableLimits {

  private final Uint31 minimum;
  private final Uint31 maximum;

  /**
   * Construct a limits object.
   *
   * @param minimum the minimum in the limit
   * @param maximum the maximum in the limit
   */
  public ResizableLimits(Uint31 minimum, Uint31 maximum) {
    this.minimum = minimum;
    this.maximum = maximum;
  }

  /**
   * Gets the minimum.
   *
   * @return the minimum
   */
  public Uint31 getMinimum() {
    return minimum;
  }

  /**
   * Gets the maximum.
   *
   * @return the maximum
   */
  public Uint31 getMaximum() {
    return maximum;
  }

  static ResizableLimits read(WasmByteStream stream) {
    final int flag = stream.readUint8();
    if (flag == 0x00) {
      return new ResizableLimits(stream.readVarUint31(), Uint31.MAX_VALUE);
    } else if (flag == 0x01) {
      return new ResizableLimits(stream.readVarUint31(), stream.readVarUint31());
    } else {
      throw new WasmParseExceptionImpl("Invalid start byte for limits: 0x%02X", flag);
    }
  }
}
