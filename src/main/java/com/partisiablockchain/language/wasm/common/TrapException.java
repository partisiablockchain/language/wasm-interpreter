package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.FormatMethod;
import java.util.List;

/** Trap exception occurs on illegal operation (divide by zero or setting non-null to null). */
public final class TrapException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  /** WASM stack trace to append to message. */
  private List<String> wasmStackTrace = List.of();

  /**
   * Creates a new trap exception and formats the error message with {@link String#format}.
   *
   * @param message the message
   * @param args the parameters to format
   */
  @FormatMethod
  public TrapException(String message, Object... args) {
    super("Trap: " + String.format(message, args));
  }

  private TrapException(final Throwable cause, final String message) {
    super(message, cause);
  }

  /**
   * Overrides the WASM stack set in the current exception. The intention is that a trap can be
   * thrown deep in the execution model with no idea about the WASM stack, then caught in the
   * interpreter, and then rethrown with the WASM stack updated.
   *
   * @param wasmStackTrace new stack trace to use
   */
  public void setWasmStackTrace(final List<String> wasmStackTrace) {
    this.wasmStackTrace = wasmStackTrace;
  }

  @Override
  public String getMessage() {
    return super.getMessage()
        + (wasmStackTrace.isEmpty() ? "" : "\n  " + String.join("\n  ", wasmStackTrace));
  }

  /**
   * Creates a new trap exception with a cause and formats the error message with {@link
   * String#format}.
   *
   * @param cause reason the trap was thrown
   * @param message the message
   * @param args the parameters to format
   * @return new TrapException with a specific cause
   */
  @FormatMethod
  public static TrapException withCause(
      final Throwable cause, final String message, final Object... args) {
    return new TrapException(cause, "Trap: " + String.format(message, args));
  }
}
