package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Objects.requireNonNull;

import com.google.errorprone.annotations.Immutable;

/**
 * WASM implementation limitations for the Partisia Blockchain WASM interpreter.
 *
 * <h2>eagerValidation</h2>
 *
 * {@code eagerValidation} determines which validation mode to use.
 *
 * <p>While {@code true} validation errors will be thrown during instance initialization. This is
 * the default and preferred mode.
 *
 * <p>While {@code false} validation errors will still be detected during instance initialization,
 * but these will not be thrown. Rather, per function errors will be stored with the function, and
 * will be thrown if the function is ever called.
 *
 * <p>{@code false} mode will run a larger set of programs compared to {@code true} mode, but may
 * result in traps during execution, when calling into invalid functions. This is preferrable in
 * certain situations, mainly testing purposes.
 *
 * @see <a
 *     href="https://webassembly.github.io/JS-BigInt-integration/core/appendix/implementation.html">WASM
 *     spec</a>
 */
@Immutable
public record WasmLimitations(
    int maxStackFrameDepth, Uint31 maxMemoryPages, boolean eagerValidation) {

  /**
   * Constructor.
   *
   * @param maxStackFrameDepth The maximum number of frames allowed on the stack.
   * @param maxMemoryPages The maximum number of memory pages allowed. Each memory page is 64Kb, so
   *     the maximum memory allowed is MAX_MEMORY_PAGES * 64Kb.
   * @param eagerValidation determines which validation mode to use.
   */
  public WasmLimitations {
    if (maxStackFrameDepth < 0) {
      throw new RuntimeException("maxStackFrameDepth must be positive, was " + maxStackFrameDepth);
    }
    requireNonNull(maxMemoryPages);
  }

  /** Default limitations for WASM interpreter. */
  public static WasmLimitations DEFAULT = new WasmLimitations(300, new Uint31(256), true);

  /**
   * Returns new limitations object with same setting, except setting {@code eagerValidation} to
   * {@code false}.
   *
   * @return Newly created limitations.
   */
  public WasmLimitations withLazyValidation() {
    return new WasmLimitations(maxStackFrameDepth, maxMemoryPages, false);
  }
}
