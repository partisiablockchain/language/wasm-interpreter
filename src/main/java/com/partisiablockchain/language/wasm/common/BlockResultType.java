package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import java.util.List;

/**
 * Type of the result value that execution a control flow block produces. The control flow blocks
 * that produce a result are loop, block and if-else.
 */
@Immutable
public final class BlockResultType {

  /** Possibly null. */
  private final ValType wrappedType;

  private BlockResultType(ValType wrappedType) {
    this.wrappedType = wrappedType;
  }

  /**
   * Gets the arity.
   *
   * @return the arity
   */
  public Uint31 getArity() {
    return wrappedType == null ? Uint31.ZERO : Uint31.ONE;
  }

  /**
   * Gets the BlockResultType as a list of types.
   *
   * @return list of types
   */
  public List<ValType> asTypeList() {
    return wrappedType == null ? List.of() : List.of(wrappedType);
  }

  @Override
  public String toString() {
    return wrappedType == null ? "EMPTY" : wrappedType.toString();
  }

  @Override
  public boolean equals(Object other) {
    if (!(other instanceof BlockResultType)) {
      return false;
    }
    final ValType otherWrapped = ((BlockResultType) other).wrappedType;
    if (otherWrapped == null) {
      return wrappedType == null;
    } else if (wrappedType == null) {
      return false; // Cannot be equal, as otherType != null, but wrappedType == null.
    }
    return otherWrapped.equals(wrappedType);
  }

  @Override
  public int hashCode() {
    return wrappedType == null ? 0 : wrappedType.hashCode();
  }

  /** Constant value of an empty BlockResultType. */
  public static final BlockResultType EMPTY = new BlockResultType(null);

  /** Constant value of a BlockResultType wrapping an I32. */
  public static final BlockResultType I32 = new BlockResultType(ValType.I32);

  /** Constant value of a BlockResultType wrapping an I64. */
  public static final BlockResultType I64 = new BlockResultType(ValType.I64);

  /**
   * Gets block result type from a value.
   *
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#result-types%E2%91%A2">WASM Spec</a>
   * @param value the value
   * @return the corresponding block result type.
   */
  public static BlockResultType fromValue(int value) {
    if (value == 0x40) {
      return EMPTY;
    }
    return new BlockResultType(ValType.fromValue(value));
  }

  /**
   * Creates a new BlockResultType from a list of types.
   *
   * @param typeList list of types to infer BlockResultType from
   * @return newly created BlockResultType
   */
  public static BlockResultType of(List<ValType> typeList) {
    if (typeList.size() > 1) {
      throw new IllegalArgumentException("Generalized block result types not supported");
    }
    if (typeList.size() == 0) {
      return EMPTY;
    }
    return new BlockResultType(typeList.get(0));
  }
}
