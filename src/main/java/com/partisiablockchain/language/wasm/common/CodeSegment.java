package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.interpreter.Instruction;
import com.partisiablockchain.language.wasm.interpreter.InstructionParser;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmParseException;
import java.util.Arrays;
import java.util.List;

/**
 * Implementation of <a href="https://webassembly.github.io/spec/core/syntax/modules.html">code
 * segment </a>.
 */
public final class CodeSegment {

  private final List<ValType> localTypes;
  private final List<Instruction> code;
  private final WasmParseException parseException;

  CodeSegment(final List<ValType> localTypes, final List<Instruction> code) {
    this.localTypes = localTypes;
    this.code = code;
    this.parseException = null;
  }

  CodeSegment(final List<ValType> localTypes, final WasmParseException parseException) {
    this.localTypes = localTypes;
    this.code = null;
    this.parseException = parseException;
  }

  /**
   * Gets the local types for this code segment.
   *
   * @return the local types
   */
  public List<ValType> getLocalTypes() {
    return localTypes;
  }

  /**
   * Gets the code for this code segment.
   *
   * @return the code
   */
  public List<Instruction> getCode() {
    if (parseException != null) {
      throw getParseException();
    }
    return code;
  }

  /**
   * Gets the code for this code segment.
   *
   * @return the code
   */
  public WasmParseException getParseException() {
    return parseException;
  }

  /**
   * Reads a code segment from a wasm byte stream.
   *
   * @param stream the stream
   * @return the created code segment
   */
  public static CodeSegment read(WasmByteStream stream) {
    final Uint31 size = stream.readVarUint31();
    final Uint31 startPosition = stream.getPosition();
    final Uint31 expectedEndPosition = startPosition.add(size);

    List<Locals> locals = stream.parseVectorStream(Locals::read);
    byte[] rawCode = stream.readBytes(expectedEndPosition.minus(stream.getPosition()));

    Uint31 numLocalsInTotal = Uint31.ZERO;
    for (Locals local : locals) {
      numLocalsInTotal = numLocalsInTotal.add(local.getCount());
    }

    final ValType[] localTypes = new ValType[numLocalsInTotal.asInt()];
    Uint31 baseIndex = Uint31.ZERO;
    for (Locals local : locals) {
      final Uint31 numLocalsOfThisType = local.getCount();
      final Uint31 newBaseIndex = baseIndex.add(numLocalsOfThisType);
      Arrays.fill(localTypes, baseIndex.asInt(), newBaseIndex.asInt(), local.getType());
      baseIndex = newBaseIndex;
    }

    final InstructionParser parser = new InstructionParser(new WasmByteStream(rawCode));
    try {
      final List<Instruction> instructions = parser.parseFunction();
      return new CodeSegment(Arrays.asList(localTypes), instructions);
    } catch (WasmParseException e) {
      return new CodeSegment(Arrays.asList(localTypes), e);
    }
  }
}
