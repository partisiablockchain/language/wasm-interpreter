package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmParseExceptionImpl;

/**
 * Implementation of an <a
 * href="https://webassembly.github.io/spec/core/syntax/modules.html#imports">import construct </a>.
 */
@Immutable
public final class Import<T extends ImportEntryType> {

  private final String moduleName;
  private final String importedName;
  private final T importDescriptor;

  Import(String moduleName, String importedName, T importDescriptor) {
    this.moduleName = moduleName;
    this.importedName = importedName;
    this.importDescriptor = importDescriptor;
  }

  /**
   * Name of module imported from.
   *
   * @return module name
   */
  public String getModuleName() {
    return moduleName;
  }

  /**
   * Name import.
   *
   * @return imported name
   */
  public String getImportedName() {
    return importedName;
  }

  /**
   * Import descriptor.
   *
   * @return import descriptor
   */
  public T getImportDescriptor() {
    return importDescriptor;
  }

  /**
   * Copies current import and replaces descriptor with a new one. Used to get around Java's
   * restrictions on type casting.
   *
   * @param <R> type of new descriptor
   * @param otherDescriptor new descriptor
   * @return new import
   */
  public <R extends ImportEntryType> Import<R> copyWithDescriptor(final R otherDescriptor) {
    return new Import<R>(this.moduleName, this.importedName, otherDescriptor);
  }

  /**
   * Parse Import from {@link WasmByteStream}.
   *
   * @param stream byte stream to parse from
   * @return parsed import
   */
  public static Import<ImportEntryType> read(WasmByteStream stream) {
    return new Import<ImportEntryType>(
        stream.readStringBytes(), stream.readStringBytes(), parseEntry(stream));
  }

  static ImportEntryType parseEntry(WasmByteStream stream) {
    int typeFlag = stream.readUint8();
    Type type = Type.safeFromOrdinal(typeFlag);
    if (type == Type.TYPE) {
      return new TypeIndex(stream.readVarUint31());
    } else if (type == Type.TABLE) {
      return TableType.read(stream);
    } else if (type == Type.MEMORY) {
      return MemoryType.read(stream);
    } else if (type == Type.GLOBAL) {
      return GlobalType.read(stream);
    }
    throw new WasmParseExceptionImpl("Malformed import kind: 0x%02X", typeFlag);
  }

  enum Type {
    TYPE,
    TABLE,
    MEMORY,
    GLOBAL;

    static Type safeFromOrdinal(int ordinal) {
      if (ordinal < values().length) {
        return values()[ordinal];
      } else {
        return null;
      }
    }
  }
}
