package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmIllegalBytecodeException;

/**
 * Symbolic WASM types, for type validation.
 *
 * <p>While floating point number types are not supported during execution and evaluation, it is
 * desirable to maintain basic support in various data structures. For example it allows the parser
 * to parse all function definitions, even functions with floating point typed arguments.
 */
public enum ValType {
  /** 32-bit Integer. */
  I32(Int32.ZERO, "i32"),
  /** 64-bit Integer. */
  I64(Int64.ZERO, "i64"),
  /**
   * 32-bit floating point number.
   *
   * @see ValType floating point paragraf above.
   */
  F32(null, "f32"),
  /**
   * 64-bit floating point number.
   *
   * @see ValType floating point paragraf above.
   */
  F64(null, "f64"),
  /**
   * Represents an unknown/top type.
   *
   * @see com.partisiablockchain.language.wasm.validator.FunctionValidator
   */
  UNKNOWN(null, "unknown");

  private final Literal zero;

  /** The type name. */
  private final String typeName;

  ValType(Literal zero, String typeName) {
    this.zero = zero;
    this.typeName = typeName;
  }

  /**
   * Gets the zero value for a value type.
   *
   * @return zero value
   */
  public Literal getZero() {
    return zero;
  }

  /**
   * Get the name of the value type.
   *
   * @return The type name
   */
  public String getTypeName() {
    return typeName;
  }

  @Override
  public String toString() {
    return getTypeName();
  }

  /**
   * Create ValType from an integer value.
   *
   * @param value the integer value
   * @return created ValType
   */
  static ValType fromValue(final int value) {
    if (value == 0x7F) {
      return I32;
    } else if (value == 0x7E) {
      return I64;
    } else if (value == 0x7D) {
      return F32;
    } else if (value == 0x7C) {
      return F64;
    }
    throw new WasmIllegalBytecodeException("Invalid value type constant: 0x%02X", value);
  }

  /**
   * Read and create ValType a stream.
   *
   * @param stream stream to read from
   * @return created ValType
   */
  static ValType read(final WasmByteStream stream) {
    return fromValue(stream.readUint8());
  }
}
