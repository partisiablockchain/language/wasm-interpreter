package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.interpreter.Instruction;
import com.partisiablockchain.language.wasm.interpreter.InstructionParser;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import java.util.List;

/**
 * Implementation of <a
 * href="https://webassembly.github.io/spec/core/syntax/modules.html#element-segments">element
 * segment </a>.
 */
public final class ElementSegment {

  private final Uint31 tableIndex;
  private final List<Instruction> offset;
  private final List<Uint31> init;

  ElementSegment(Uint31 tableIndex, List<Instruction> offset, List<Uint31> init) {
    this.tableIndex = tableIndex;
    this.offset = offset;
    this.init = init;
  }

  /**
   * Gets the table index for this element segment.
   *
   * @return the table index
   */
  public Uint31 getTableIndex() {
    return tableIndex;
  }

  /**
   * Gets the instruction offset for this element segment.
   *
   * @return the instruction offset
   */
  public List<Instruction> getOffset() {
    return offset;
  }

  /**
   * Gets the init of this data segment.
   *
   * @return the init
   */
  public List<Uint31> getInit() {
    return init;
  }

  /**
   * Reads an element segment from a wasm byte stream.
   *
   * @param stream the stream
   * @return the created element segment
   */
  public static ElementSegment read(WasmByteStream stream) {
    return new ElementSegment(
        stream.readVarUint31(),
        new InstructionParser(stream).parseFunction(),
        stream.parseVectorStream(WasmByteStream::readVarUint31));
  }
}
