package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.sections.DataSection;
import com.partisiablockchain.language.wasm.common.sections.ElementSection;
import com.partisiablockchain.language.wasm.common.sections.ExportSection;
import com.partisiablockchain.language.wasm.common.sections.FunctionSection;
import com.partisiablockchain.language.wasm.common.sections.GlobalSection;
import com.partisiablockchain.language.wasm.common.sections.ImportSection;
import com.partisiablockchain.language.wasm.common.sections.MemorySection;
import com.partisiablockchain.language.wasm.common.sections.TableSection;
import com.partisiablockchain.language.wasm.common.sections.TypeSection;
import com.partisiablockchain.language.wasm.interpreter.ExecutableFunction;
import com.partisiablockchain.language.wasm.interpreter.FunctionCallable;
import com.partisiablockchain.language.wasm.interpreter.FunctionHosted;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of a <a href="https://www.w3.org/TR/wasm-core-1/#modules%E2%91%A0">module</a>.
 * Does not match specification perfectly, as we perform certain kinds of collation as early as
 * possible, in order to ease logic in later stages. For example, function type definitions are kept
 * together with their code section.
 *
 * <p>Additional fields:
 *
 * <ul>
 *   <li>{@link #functionsByIndex}: WASM executable functions by function index. The executable
 *       functions holds all information needed to execute the function. Can only contain {@link
 *       com.partisiablockchain.language.wasm.interpreter.FunctionCallable} and {@link
 *       com.partisiablockchain.language.wasm.interpreter.FunctionHosted}.
 *   <li>{@link #exportedFunctionsByName}: WASM executable functions conveniently mapped by function
 *       name. The name must be found from the exports-section of the WASM module. Each function may
 *       have multiple names. Can only contain {@link
 *       com.partisiablockchain.language.wasm.interpreter.FunctionCallable} and {@link
 *       com.partisiablockchain.language.wasm.interpreter.FunctionHosted}.
 * </ul>
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#modules%E2%91%A0">WASM Spec 2.5. Modules</a>
 */
public record WasmModule(
    FunctionSection functionSection,
    DataSection dataSection,
    ElementSection elementSection,
    ExportSection exportSection,
    GlobalSection globalSection,
    ImportSection<TypeIndex> functionImportSection,
    MemorySection memorySection,
    TableSection tableSection,
    TypeSection typeSection,
    Map<Uint31, List<String>> functionExportNamesByIndex,
    Map<Uint31, ExecutableFunction> functionsByIndex,
    Map<String, ExecutableFunction> exportedFunctionsByName) {

  /**
   * Constructor for a new immutable WASM module.
   *
   * @param functionSection combined code and type section for functions. Indexed by code index.
   * @param dataSection data section for new module
   * @param elementSection element section for new module
   * @param exportSection export section for new module
   * @param globalSection global section for new module
   * @param functionImportSection function import section for new module
   * @param memorySection memory section for new module
   * @param tableSection table section for new module
   * @param typeSection type section for new module
   * @return Combined WasmModule.
   */
  public static WasmModule withSections(
      final FunctionSection functionSection,
      final DataSection dataSection,
      final ElementSection elementSection,
      final ExportSection exportSection,
      final GlobalSection globalSection,
      final ImportSection<TypeIndex> functionImportSection,
      final MemorySection memorySection,
      final TableSection tableSection,
      final TypeSection typeSection) {
    // Precompute each function's export names.
    final var functionExportNamesByIndex = generateExportNamesByIndex(exportSection);

    // Register imports
    final var functionsByIndex =
        generateFunctionsByIndex(
            functionImportSection, functionSection, typeSection, functionExportNamesByIndex);

    // Initialize exported functions map
    final var exportedFunctionsByName =
        generateFunctionsByName(functionsByIndex, functionExportNamesByIndex);

    // Construct actual module
    return new WasmModule(
        functionSection,
        dataSection,
        elementSection,
        exportSection,
        globalSection,
        functionImportSection,
        memorySection,
        tableSection,
        typeSection,
        Map.copyOf(functionExportNamesByIndex),
        Map.copyOf(functionsByIndex),
        Map.copyOf(exportedFunctionsByName));
  }

  /**
   * Retrieves DataSection for module.
   *
   * <p>Legacy function.
   *
   * @return DataSection for module
   */
  public DataSection getDataSection() {
    return dataSection();
  }

  /**
   * Retrieves ElementSection for module.
   *
   * <p>Legacy function.
   *
   * @return ElementSection for module
   */
  public ElementSection getElementSection() {
    return elementSection();
  }

  /**
   * Retrieves ExportSection for module.
   *
   * <p>Legacy function.
   *
   * @return ExportSection for module
   */
  public ExportSection getExportSection() {
    return exportSection();
  }

  /**
   * Retrieves FunctionSection for module.
   *
   * <p>Legacy function.
   *
   * @return FunctionSection for module
   */
  public FunctionSection getFunctionSection() {
    return functionSection();
  }

  /**
   * Retrieves GlobalSection for module.
   *
   * <p>Legacy function.
   *
   * @return GlobalSection for module
   */
  public GlobalSection getGlobalSection() {
    return globalSection();
  }

  /**
   * Retrieves ImportSection for module.
   *
   * <p>Legacy function.
   *
   * @return ImportSection for module
   */
  public ImportSection<TypeIndex> getFunctionImportSection() {
    return functionImportSection();
  }

  /**
   * Retrieves MemorySection for module.
   *
   * <p>Legacy function.
   *
   * @return MemorySection for module
   */
  public MemorySection getMemorySection() {
    return memorySection();
  }

  /**
   * Retrieves TableSection for module.
   *
   * <p>Legacy function.
   *
   * @return TableSection for module
   */
  public TableSection getTableSection() {
    return tableSection();
  }

  /**
   * Retrieves TypeSection for module.
   *
   * <p>Legacy function.
   *
   * @return TypeSection for module
   */
  public TypeSection getTypeSection() {
    return typeSection();
  }

  private static Map<Uint31, List<String>> generateExportNamesByIndex(
      final ExportSection exportSection) {
    final Map<Uint31, List<String>> functionExportNamesByIndex = new HashMap<>();
    for (final Export export : exportSection) {
      // NOTE: Due to linking checks assume all names are unique, and functions exists.
      if (export.getKind().equals(AddressKind.FUNCTION)) {
        functionExportNamesByIndex
            .computeIfAbsent(export.getIndex(), k -> new ArrayList<>())
            .add(export.getName());
      }
    }
    // Make map and contained lists immutable
    functionExportNamesByIndex.replaceAll((k, v) -> List.copyOf(v));
    return Map.copyOf(functionExportNamesByIndex);
  }

  private static Map<Uint31, ExecutableFunction> generateFunctionsByIndex(
      final ImportSection<TypeIndex> functionImportSection,
      final FunctionSection functionSection,
      final TypeSection typeSection,
      final Map<Uint31, List<String>> functionExportNamesByIndex) {

    final var functionsByIndex = new HashMap<Uint31, ExecutableFunction>();
    int functionIdRawCounter = 0;

    for (final Import<TypeIndex> entry : functionImportSection) {
      final Uint31 functionId = new Uint31(functionIdRawCounter++);
      final TypeIndex typeIndex = entry.getImportDescriptor();
      final FunctionType type = typeSection.getEntryOrNull(typeIndex.getValue());
      // NOTE: Assume type != null due to validation

      final ExecutableFunction function =
          new FunctionHosted(functionId, entry.getImportedName(), type, entry);

      functionsByIndex.put(functionId, function);
    }

    // Register module-local functions with code
    for (final CollatedFunction func : functionSection) {
      final Uint31 functionId = func.functionId();
      final CodeSegment codeSegment = func.codeSegment();

      final List<String> export = functionExportNamesByIndex.get(functionId);

      final String name = export != null ? export.get(0) : func.debugName();
      final ExecutableFunction function =
          new FunctionCallable(functionId, name, func.functionType(), codeSegment);

      functionsByIndex.put(functionId, function);
    }

    return Map.copyOf(functionsByIndex);
  }

  /**
   * Produces a copy of the WasmModule's internal {@link #exportedFunctionsByName} map. This newly
   * created map uses the given {@code functionsByIndex} argument instead of the module's internal
   * {@link #functionsByIndex} map field.
   *
   * @param functionsByIndex Alternative functionsByIndex map to use.
   * @return Copy of the WasmModule's functionByName map field, with {@code ExecutableFunction}
   *     values taken from the given {@code functionsByIndex} map.
   */
  public Map<String, ExecutableFunction> generateFunctionsByName(
      final Map<Uint31, ExecutableFunction> functionsByIndex) {
    return generateFunctionsByName(functionsByIndex, functionExportNamesByIndex());
  }

  private static Map<String, ExecutableFunction> generateFunctionsByName(
      final Map<Uint31, ExecutableFunction> functionsByIndex,
      final Map<Uint31, List<String>> functionExportNamesByIndex) {
    // Initialize exported functions map
    final Map<String, ExecutableFunction> exportedFunctionsByName = new HashMap<>();
    for (final Map.Entry<Uint31, List<String>> indexWithExports :
        functionExportNamesByIndex.entrySet()) {
      final ExecutableFunction function = functionsByIndex.get(indexWithExports.getKey());
      for (final String exportName : indexWithExports.getValue()) {
        exportedFunctionsByName.put(exportName, function);
      }
    }
    return Map.copyOf(exportedFunctionsByName);
  }
}
