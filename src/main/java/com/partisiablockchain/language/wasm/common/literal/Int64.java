package com.partisiablockchain.language.wasm.common.literal;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.ValType;

/** Value type of 64-bit integer. */
@Immutable
public record Int64(long value) implements Literal {

  /** Constant zero. */
  public static final Int64 ZERO = new Int64(0L);

  /**
   * Construct the int from the raw bytes.
   *
   * @param buffer the buffer of raw bytes
   * @return The read integer. Never null.
   */
  public static Int64 fromRawParts(final byte[] buffer) {
    final long value =
        ((buffer[7] & 0xFFL) << 56)
            | ((buffer[6] & 0xFFL) << 48)
            | ((buffer[5] & 0xFFL) << 40)
            | ((buffer[4] & 0xFFL) << 32)
            | ((buffer[3] & 0xFFL) << 24)
            | ((buffer[2] & 0xFFL) << 16)
            | ((buffer[1] & 0xFFL) << 8)
            | (buffer[0] & 0xFFL);
    return new Int64(value);
  }

  @Override
  public byte[] toBytes() {
    byte[] result = new byte[8];
    result[0] = (byte) (value & 0xFFL);
    result[1] = (byte) ((value >> 8) & 0xFFL);
    result[2] = (byte) ((value >> 16) & 0xFFL);
    result[3] = (byte) ((value >> 24) & 0xFFL);
    result[4] = (byte) ((value >> 32) & 0xFFL);
    result[5] = (byte) ((value >> 40) & 0xFFL);
    result[6] = (byte) ((value >> 48) & 0xFFL);
    result[7] = (byte) ((value >> 56) & 0xFFL);
    return result;
  }

  /**
   * Get low 32 bits as signed integer.
   *
   * @return low 32 bits as signed integer
   */
  public int toInt32() {
    return (int) value;
  }

  /**
   * Compare greater than equal signed with {@link Int64}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean greaterThanSigned(Int64 that) {
    return value > that.value;
  }

  /**
   * Compare greater than equal signed with {@link Int64}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean greaterThanEqualSigned(Int64 that) {
    return value >= that.value;
  }

  /**
   * Compare greater than unsigned with {@link Int64}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean greaterThanUnsigned(Int64 that) {
    return Long.compareUnsigned(this.value, that.value) > 0;
  }

  /**
   * Compare greater than equal unsigned with {@link Int64}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean greaterThanEqualUnsigned(Int64 that) {
    return Long.compareUnsigned(this.value, that.value) >= 0;
  }

  /**
   * Compare less than signed with {@link Int64}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean lessThanSigned(Int64 that) {
    return value < that.value;
  }

  /**
   * Compare less than unsigned with {@link Int64}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean lessThanEqualSigned(Int64 that) {
    return value <= that.value;
  }

  /**
   * Compare less than equal signed with {@link Int64}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean lessThanUnsigned(Int64 that) {
    return Long.compareUnsigned(this.value, that.value) < 0;
  }

  /**
   * Compare less than equal unsigned with {@link Int64}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean lessThanEqualUnsigned(Int64 that) {
    return Long.compareUnsigned(this.value, that.value) <= 0;
  }

  /**
   * Multiply with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 multiply(Int64 that) {
    return new Int64(value * that.value);
  }

  /**
   * Add with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 add(Int64 that) {
    return new Int64(value + that.value);
  }

  /**
   * Subtract with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 subtract(Int64 that) {
    return new Int64(value - that.value);
  }

  /**
   * Divide signed with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 divideSigned(Int64 that) {
    trapIfZero(that);
    if (this.value == Long.MIN_VALUE && that.value == -1) {
      throw new TrapException("Integer overflow");
    }
    return new Int64(value / that.value);
  }

  /**
   * Divide unsigned with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 divideUnsigned(Int64 that) {
    trapIfZero(that);
    long result = Long.divideUnsigned(value, that.value);
    return new Int64(result);
  }

  /**
   * Compute remainder signed with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 remainderSigned(Int64 that) {
    trapIfZero(that);
    return new Int64(value % that.value);
  }

  /**
   * Compute remainder unsigned with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 remainderUnsigned(Int64 that) {
    trapIfZero(that);
    long remainder = Long.remainderUnsigned(value, that.value);
    return new Int64(remainder);
  }

  private void trapIfZero(Int64 that) {
    if (that.value == 0) {
      throw new TrapException("64-bit integer divide by zero");
    }
  }

  /**
   * Bitwise and with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 and(Int64 that) {
    return new Int64(value & that.value);
  }

  /**
   * Bitwise or with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 or(Int64 that) {
    return new Int64(value | that.value);
  }

  /**
   * Bitwise xor with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 xor(Int64 that) {
    return new Int64(value ^ that.value);
  }

  /**
   * Shift left by {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 shiftLeft(Int64 that) {
    return new Int64(value << that.value);
  }

  /**
   * Shift right signed by {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 shiftRightSigned(Int64 that) {
    return new Int64(value >> that.value);
  }

  /**
   * Shift right unsigned with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 shiftRightUnsigned(Int64 that) {
    return new Int64(value >>> that.value);
  }

  /**
   * Bitwise rotate left with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 bitwiseRotateLeft(Int64 that) {
    long result = Long.rotateLeft(value, that.toInt32());
    return new Int64(result);
  }

  /**
   * Bitwise rotate right with {@link Int64}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int64 bitwiseRotateRight(Int64 that) {
    long result = Long.rotateRight(value, that.toInt32());
    return new Int64(result);
  }

  /**
   * Count leading zeroes of literal.
   *
   * @return num leading zeroes
   */
  public Int64 countLeadingZeroes() {
    return new Int64(Long.numberOfLeadingZeros(value));
  }

  /**
   * Count trailing zeroes of literal.
   *
   * @return num trailing zeroes
   */
  public Int64 countTrailingZeroes() {
    return new Int64(Long.numberOfTrailingZeros(value));
  }

  /**
   * Population count of literal.
   *
   * @return population count
   */
  public Int64 populationCount() {
    return new Int64(Long.bitCount(value));
  }

  /**
   * Sign extend 32-bit integer to 64-bit integer.
   *
   * @param val 32-bit integer to extend
   * @return the 64-bit integer
   */
  public static Int64 extendSigned(Int32 val) {
    return new Int64(val.value());
  }

  /**
   * Zero-extend 32-bit integer to 64-bit integer.
   *
   * @param val 32-bit integer to extend
   * @return the 64-bit integer
   */
  public static Int64 extendUnsigned(Int32 val) {
    return new Int64(val.getUnsignedValue());
  }

  /**
   * Sign extend 8-bit integer to 64-bit integer.
   *
   * @param val 8-bit integer to extend
   * @return the 64-bit integer
   */
  public static Int64 extend8Signed(Int64 val) {
    return new Int64((byte) val.value());
  }

  /**
   * Sign extend 16-bit integer to 64-bit integer.
   *
   * @param val 16-bit integer to extend
   * @return the 64-bit integer
   */
  public static Int64 extend16Signed(Int64 val) {
    return new Int64((short) val.value());
  }

  /**
   * Sign extend 32-bit integer to 64-bit integer.
   *
   * @param val 32-bit integer to extend
   * @return the 64-bit integer
   */
  public static Int64 extend32Signed(Int64 val) {
    return new Int64((int) val.value());
  }

  /**
   * Compare with zero.
   *
   * @return true if and only if zero
   */
  public boolean equalsZero() {
    return value == 0;
  }

  //// Literal overwritten methods

  @Override
  public ValType getValType() {
    return ValType.I64;
  }

  @Override
  public Int32 expectInt32() {
    throw new TrapException("Expected i32 but got %s", getValType().getTypeName());
  }

  @Override
  public Int64 expectInt64() {
    return this;
  }

  //// General overwritten methods

  @Override
  public String toString() {
    return String.valueOf(value);
  }
}
