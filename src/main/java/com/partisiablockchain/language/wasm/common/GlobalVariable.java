package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Literal;

/**
 * Implementation of <a
 * href="https://webassembly.github.io/spec/core/syntax/modules.html#syntax-global">global variable
 * </a>.
 */
public final class GlobalVariable {

  private final GlobalType type;
  private Literal value;

  /**
   * Creates a global variable from a wasm byte stream.
   *
   * @param type the type
   * @param value the value
   */
  public GlobalVariable(GlobalType type, Literal value) {
    this.type = type;
    this.value = value;
  }

  /**
   * Whether global is mutable.
   *
   * @return true if and only if global is mutable
   */
  public boolean isMutable() {
    return type.isMutable();
  }

  /**
   * Literal value of the global.
   *
   * @return literal value
   */
  public Literal getValue() {
    return value;
  }

  /**
   * Sets the value for this global variable.
   *
   * @param value the value
   */
  public void setValue(Literal value) {
    if (!isMutable()) {
      throw new TrapException("Cannot set value on immutable global");
    } else if (value == null) {
      throw new TrapException("Cannot set value to null");
    } else {
      ValType expectedValType = type.getContentType();
      if (!expectedValType.equals(value.getValType())) {
        throw new TrapException("Invalid type: %s", value.getValType());
      }
      this.value = value;
    }
  }
}
