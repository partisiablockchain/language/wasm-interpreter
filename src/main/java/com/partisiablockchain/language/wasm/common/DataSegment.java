package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.interpreter.Instruction;
import com.partisiablockchain.language.wasm.interpreter.InstructionParser;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import java.util.List;

/**
 * Implementation of <a href=
 * "https://webassembly.github.io/spec/core/syntax/modules.html#data-segments">data segment </a>.
 */
public final class DataSegment {

  private final Uint31 memoryIndex;
  private final List<Instruction> offset;
  private final byte[] init;

  DataSegment(Uint31 memoryIndex, List<Instruction> offset, byte[] init) {
    this.memoryIndex = memoryIndex;
    this.offset = offset;
    this.init = init;
  }

  /**
   * Gets the memory index for this data segment.
   *
   * @return the memory index
   */
  public Uint31 getMemoryIndex() {
    return memoryIndex;
  }

  /**
   * Gets the instruction offset for this data segment.
   *
   * @return the instruction offset
   */
  public List<Instruction> getOffset() {
    return offset;
  }

  /**
   * Gets the init part of this data segment.
   *
   * @return the init part
   */
  public byte[] getInit() {
    return init.clone();
  }

  /**
   * Reads a data segment from a wasm byte stream.
   *
   * @param stream the stream
   * @return the created data segment
   */
  public static DataSegment read(WasmByteStream stream) {
    final Uint31 memoryIndex = stream.readVarUint31();
    final List<Instruction> offset = new InstructionParser(stream).parseFunction();
    final Uint31 payloadLength = stream.readVarUint31();
    return new DataSegment(memoryIndex, offset, stream.readBytes(payloadLength));
  }
}
