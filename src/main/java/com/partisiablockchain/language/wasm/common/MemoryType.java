package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;

/** THe type of memory. */
@Immutable
public final class MemoryType implements ImportEntryType {

  private final ResizableLimits resizableLimits;

  MemoryType(ResizableLimits resizableLimits) {
    this.resizableLimits = resizableLimits;
  }

  /**
   * Gets the resizable limits.
   *
   * @return the resizable limits
   */
  public ResizableLimits getLimits() {
    return resizableLimits;
  }

  /**
   * Reads the memory type from a wasm stream.
   *
   * @param stream the stream to read from
   * @return the memory type read
   */
  public static MemoryType read(WasmByteStream stream) {
    return new MemoryType(ResizableLimits.read(stream));
  }
}
