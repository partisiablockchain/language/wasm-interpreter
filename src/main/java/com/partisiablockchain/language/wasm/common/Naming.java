package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;

/** A naming in a wasm program. */
@Immutable
public final class Naming {

  private final Uint31 index;
  private final String name;

  Naming(Uint31 index, String name) {
    this.index = index;
    this.name = name;
  }

  /**
   * Gets the index.
   *
   * @return the index
   */
  public Uint31 getIndex() {
    return index;
  }

  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Reads the naming from a wasm stream.
   *
   * @param stream the stream to read from
   * @return the naming read
   */
  public static Naming read(WasmByteStream stream) {
    return new Naming(stream.readVarUint31(), stream.readStringBytes());
  }
}
