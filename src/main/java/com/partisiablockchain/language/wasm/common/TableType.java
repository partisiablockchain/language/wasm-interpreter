package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmParseExceptionImpl;

/** A table type in an import. */
@Immutable
public final class TableType implements ImportEntryType {

  private final AddressKind addressKind;
  private final ResizableLimits limits;

  /**
   * Construct a table type.
   *
   * @param addressKind the address type
   * @param limits the limits for the table
   */
  public TableType(AddressKind addressKind, ResizableLimits limits) {
    this.addressKind = addressKind;
    this.limits = limits;
  }

  /**
   * Gets the limits.
   *
   * @return the limits
   */
  public ResizableLimits getLimits() {
    return limits;
  }

  /**
   * Gets the address type.
   *
   * @return the address type
   */
  public AddressKind getAddressKind() {
    return addressKind;
  }

  /**
   * Parses the TableType from a byte stream.
   *
   * @param stream to parse from.
   * @return parsed TableType.
   */
  public static TableType read(WasmByteStream stream) {
    int typeFlag = stream.readUint8();
    if (typeFlag != 0x70) {
      throw new WasmParseExceptionImpl("Unrecognized table type: %X", typeFlag);
    }

    return new TableType(AddressKind.FUNCTION, ResizableLimits.read(stream));
  }
}
