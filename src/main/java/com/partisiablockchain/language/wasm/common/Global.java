package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.interpreter.Instruction;
import com.partisiablockchain.language.wasm.interpreter.InstructionParser;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import java.util.List;

/**
 * Implementation of <a
 * href="https://webassembly.github.io/spec/core/syntax/modules.html#syntax-global">global </a>.
 */
public final class Global {

  private final GlobalType type;
  private final List<Instruction> initializer;

  /**
   * Constructor for global definition for given type and initializer.
   *
   * @param type type of global
   * @param initializer code to initialize global
   */
  public Global(GlobalType type, List<Instruction> initializer) {
    this.type = type;
    this.initializer = initializer;
  }

  /**
   * Type of global.
   *
   * @return type of global
   */
  public GlobalType getType() {
    return type;
  }

  /**
   * Global initializer.
   *
   * @return initializer for global
   */
  public List<Instruction> getInitializer() {
    return initializer;
  }

  /**
   * Reads a global from a wasm byte stream.
   *
   * @param stream the stream
   * @return the created global
   */
  public static Global read(WasmByteStream stream) {
    return new Global(GlobalType.read(stream), new InstructionParser(stream).parseFunction());
  }
}
