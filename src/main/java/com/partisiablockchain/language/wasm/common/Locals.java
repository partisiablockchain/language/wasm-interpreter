package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;

/**
 * The local variables residing in a {@link CodeSegment} each with a {@link ValType}.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#contexts%E2%91%A0">WASM Spec</a>
 */
@Immutable
public final class Locals {

  private final Uint31 count;
  private final ValType type;

  Locals(Uint31 count, ValType type) {
    this.count = count;
    this.type = type;
  }

  /**
   * Gets the count of variables.
   *
   * @return the count of variables
   */
  public Uint31 getCount() {
    return count;
  }

  /**
   * Gets the type of these locals.
   *
   * @return the type
   */
  public ValType getType() {
    return type;
  }

  /**
   * Reads the locals from a wasm stream.
   *
   * @param stream the stream to read from
   * @return the locals read
   */
  public static Locals read(WasmByteStream stream) {
    return new Locals(stream.readVarUint31(), ValType.read(stream));
  }
}
