package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/** The code for a section. */
public enum SectionCode {
  /** CUSTOM. */
  CUSTOM(0),
  /** TYPE. */
  TYPE(1),
  /** IMPORT. */
  IMPORT(2),
  /** FUNCTION. */
  FUNCTION(3),
  /** TABLE. */
  TABLE(4),
  /** MEMORY. */
  MEMORY(5),
  /** GLOBAL. */
  GLOBAL(6),
  /** EXPORT. */
  EXPORT(7),
  /** START. */
  START(8),
  /** ELEMENT. */
  ELEMENT(9),
  /** CODE. */
  CODE(10),
  /** DATA. */
  DATA(11),
  /** EVENT. */
  EVENT(13),
  /** UNKNOWN. */
  UNKNOWN(-1),
  ;
  private static final SectionCode[] cache = new SectionCode[256];

  static {
    for (int i = 0; i < 256; i++) {
      cache[i] = UNKNOWN;
    }

    for (SectionCode code : values()) {
      cache[code.getValue() & 0xFF] = code;
    }
  }

  private final int value;

  SectionCode(int value) {
    this.value = value;
  }

  /**
   * Creates a section code from its integer value.
   *
   * @param id the id
   * @return the corresponding section code
   */
  public static SectionCode fromValue(int id) {
    return cache[id];
  }

  /**
   * Gets the value.
   *
   * @return The value
   */
  public int getValue() {
    return value;
  }
}
