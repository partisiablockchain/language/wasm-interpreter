package com.partisiablockchain.language.wasm.common.literal;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.common.Uint31;

/** A memory address in a wasm program. */
@Immutable
public final class MemoryAddress {

  private final Uint31 align;
  private final Uint31 offset;

  /**
   * Creates a new memory address.
   *
   * @param align align of the address
   * @param offset offset of the address
   */
  public MemoryAddress(Uint31 align, Uint31 offset) {
    this.align = align;
    this.offset = offset;
  }

  /**
   * Gets align of the address.
   *
   * @return align of the address
   */
  public Uint31 getAlign() {
    return align;
  }

  /**
   * Gets offset of the address.
   *
   * @return offset of the address
   */
  public Uint31 getOffset() {
    return offset;
  }

  @Override
  public String toString() {
    return "offset=" + offset + " flags=" + align;
  }
}
