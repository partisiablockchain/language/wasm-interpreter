package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;

/**
 * Implementation of <a
 * href="https://webassembly.github.io/spec/core/syntax/modules.html#exports">exports </a>.
 */
@Immutable
public final class Export {

  private final String name;
  private final AddressKind kind;
  private final Uint31 index;

  Export(final String name, final AddressKind kind, final Uint31 index) {
    this.name = name;
    this.kind = kind;
    this.index = index;
  }

  /**
   * Kind of export.
   *
   * @return kind of export
   */
  public AddressKind getKind() {
    return kind;
  }

  /**
   * Name of export.
   *
   * @return name of export
   */
  public String getName() {
    return name;
  }

  /**
   * Index of export.
   *
   * @return index of export
   */
  public Uint31 getIndex() {
    return index;
  }

  /**
   * Reads an export from a wasm byte stream.
   *
   * @param stream the stream
   * @return the created export
   */
  public static Export read(final WasmByteStream stream) {
    final String exportName = stream.readStringBytes();
    final AddressKind exportKind = AddressKind.fromValue(stream.readUint8());
    final Uint31 index = stream.readVarUint31();
    return new Export(exportName, exportKind, index);
  }
}
