package com.partisiablockchain.language.wasm.common.literal;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.ValType;

/** Value type of 32-bit integer. */
@Immutable
public record Int32(int value) implements Literal {

  /** Constant Int32 with zero value. */
  public static final Int32 ZERO = new Int32(0);

  private static final Int32 ONE = new Int32(1);

  /**
   * Downcast to unsigned 31-bit integer. Will return null if value cannot fit into that type (too
   * large/small, depending upon interpretation.)
   *
   * @return 31-bit unsigned integer or null
   */
  public Uint31 toUint31OrNull() {
    return Uint31.orNull(value);
  }

  /**
   * Construct Int32 from the unsigned 31-bit integers. (Fits perfectly)
   *
   * @param value unsigned 31-bit integer to upcast.
   * @return original integer as signed 32-bit integer.
   */
  public static Int32 fromUnsignedValue(final Uint31 value) {
    return new Int32(value.asInt());
  }

  /**
   * Unsigned value of integer as long.
   *
   * @return unsigned value of integer
   */
  public long getUnsignedValue() {
    return Integer.toUnsignedLong(value);
  }

  /**
   * Reads a value from a byte array.
   *
   * @param bytes the byte
   * @return the value read
   */
  public static Int32 fromRawParts(byte[] bytes) {
    int value =
        (bytes[3] & 0xFF) << 24
            | (bytes[2] & 0xFF) << 16
            | (bytes[1] & 0xFF) << 8
            | (bytes[0] & 0xFF);

    return new Int32(value);
  }

  @Override
  public byte[] toBytes() {
    byte[] result = new byte[4];
    result[0] = (byte) (value & 0xFF);
    result[1] = (byte) ((value >> 8) & 0xFF);
    result[2] = (byte) ((value >> 16) & 0xFF);
    result[3] = (byte) ((value >> 24) & 0xFF);
    return result;
  }

  /**
   * Creates a value from a boolean value.
   *
   * @param value the boolean
   * @return 0 if boolean is false, 1 otherwise
   */
  public static Int32 fromBoolean(boolean value) {
    if (value) {
      return ONE;
    } else {
      return ZERO;
    }
  }

  /**
   * Compare greater than signed with {@link Int32}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean greaterThanSigned(Int32 that) {
    return value > that.value;
  }

  /**
   * Compare greater than equal signed with {@link Int32}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean greaterThanEqualSigned(Int32 that) {
    return value >= that.value;
  }

  /**
   * Compare greater than unsigned with {@link Int32}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean greaterThanUnsigned(Int32 that) {
    return Integer.compareUnsigned(this.value, that.value) > 0;
  }

  /**
   * Compare greater than equal unsigned with {@link Int32}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean greaterThanEqualUnsigned(Int32 that) {
    return Integer.compareUnsigned(this.value, that.value) >= 0;
  }

  /**
   * Compare less than signed with {@link Int32}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean lessThanSigned(Int32 that) {
    return value < that.value;
  }

  /**
   * Compare less than unsigned with {@link Int32}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean lessThanUnsigned(Int32 that) {
    return Integer.compareUnsigned(this.value, that.value) < 0;
  }

  /**
   * Compare less than equal signed with {@link Int32}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean lessThanEqualSigned(Int32 that) {
    return value <= that.value;
  }

  /**
   * Compare less than equal unsigned with {@link Int32}.
   *
   * @param that literal to compare with
   * @return comparison result
   */
  public boolean lessThanEqualUnsigned(Int32 that) {
    return Integer.compareUnsigned(this.value, that.value) <= 0;
  }

  /**
   * Multiply with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 multiply(Int32 that) {
    return new Int32(value * that.value);
  }

  /**
   * Add with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 add(Int32 that) {
    return new Int32(value + that.value);
  }

  /**
   * Subtract with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 subtract(Int32 that) {
    return new Int32(value - that.value);
  }

  /**
   * Divide signed with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 divideSigned(Int32 that) {
    trapIfZero(that);
    if (this.value == Integer.MIN_VALUE && that.value == -1) {
      throw new TrapException("Integer overflow");
    }
    return new Int32(this.value / that.value);
  }

  /**
   * Divide unsigned with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 divideUnsigned(Int32 that) {
    trapIfZero(that);
    int result = Integer.divideUnsigned(value, that.value);
    return new Int32(result);
  }

  /**
   * Compute remainder signed with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 remainderSigned(Int32 that) {
    trapIfZero(that);
    return new Int32(value % that.value);
  }

  /**
   * Compute remainder unsigned with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 remainderUnsigned(Int32 that) {
    trapIfZero(that);
    int remainder = Integer.remainderUnsigned(value, that.value);
    return new Int32(remainder);
  }

  private void trapIfZero(Int32 that) {
    if (that.value == 0) {
      throw new TrapException("32-bit integer divide by zero");
    }
  }

  /**
   * Bitwise and with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 and(Int32 that) {
    return new Int32(value & that.value);
  }

  /**
   * Bitwise or with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 or(Int32 that) {
    return new Int32(value | that.value);
  }

  /**
   * Bitwise xor with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 xor(Int32 that) {
    return new Int32(value ^ that.value);
  }

  /**
   * Shift left by {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 shiftLeft(Int32 that) {
    return new Int32(value << that.value);
  }

  /**
   * Shift right signed by {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 shiftRightSigned(Int32 that) {
    return new Int32(value >> that.value);
  }

  /**
   * Shift right unsigned with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 shiftRightUnsigned(Int32 that) {
    return new Int32(value >>> that.value);
  }

  /**
   * Bitwise rotate left with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 bitwiseRotateLeft(Int32 that) {
    int result = Integer.rotateLeft(value, that.value);
    return new Int32(result);
  }

  /**
   * Bitwise rotate right with {@link Int32}.
   *
   * @param that literal to compute with
   * @return computation result
   */
  public Int32 bitwiseRotateRight(Int32 that) {
    int result = Integer.rotateRight(value, that.value);
    return new Int32(result);
  }

  /**
   * Count leading zeroes of literal.
   *
   * @return num leading zeroes
   */
  public Int32 countLeadingZeroes() {
    return new Int32(Integer.numberOfLeadingZeros(value));
  }

  /**
   * Count trailing zeroes of literal.
   *
   * @return num trailing zeroes
   */
  public Int32 countTrailingZeroes() {
    return new Int32(Integer.numberOfTrailingZeros(value));
  }

  /**
   * Population count of literal.
   *
   * @return population count
   */
  public Int32 populationCount() {
    return new Int32(Integer.bitCount(value));
  }

  /**
   * Compare with zero.
   *
   * @return true if and only if zero
   */
  public boolean equalsZero() {
    return value == 0;
  }

  /**
   * Sign extend 8-bit integer to 32-bit integer.
   *
   * @param val 8-bit integer to extend
   * @return the 32-bit integer
   */
  public static Int32 extend8Signed(Int32 val) {
    return new Int32((byte) val.value());
  }

  /**
   * Sign extend 16-bit integer to 32-bit integer.
   *
   * @param val 16-bit integer to extend
   * @return the 32-bit integer
   */
  public static Int32 extend16Signed(Int32 val) {
    return new Int32((short) val.value());
  }

  //// Literal overwritten methods

  @Override
  public ValType getValType() {
    return ValType.I32;
  }

  @Override
  public Int32 expectInt32() {
    return this;
  }

  @Override
  public Int64 expectInt64() {
    throw new TrapException("Expected i64 but got %s", getValType().getTypeName());
  }

  //// General overwritten methods

  @Override
  public String toString() {
    return String.valueOf(value);
  }
}
