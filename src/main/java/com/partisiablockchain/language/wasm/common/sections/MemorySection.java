package com.partisiablockchain.language.wasm.common.sections;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.MemoryType;
import java.util.List;

/**
 * Wasm memory section.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#modules%E2%91%A8">WASM Spec</a>
 */
public final class MemorySection extends AbstractSection<MemoryType> {

  /**
   * Constructor for section with the given entries.
   *
   * @param entries Entries for section
   */
  public MemorySection(List<MemoryType> entries) {
    super(entries);
  }
}
