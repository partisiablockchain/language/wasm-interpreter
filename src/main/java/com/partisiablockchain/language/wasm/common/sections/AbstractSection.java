package com.partisiablockchain.language.wasm.common.sections;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.Uint31;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Supertype of all sections.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#modules%E2%91%A8">WASM Spec</a>
 */
abstract class AbstractSection<T> implements Iterable<T> {

  private final List<T> entries;

  /**
   * Constructor for section with the given entries.
   *
   * @param entries Entries for section
   */
  protected AbstractSection(List<T> entries) {
    this.entries = entries;
  }

  /**
   * Determines number of entries in the section.
   *
   * @return number of entries in section
   */
  public Uint31 numEntries() {
    return new Uint31(entries.size());
  }

  /**
   * Retrieve specific entry.
   *
   * @param index index of entry
   * @return section's entry, null if entry doesn't exist.
   */
  public T getEntryOrNull(final Uint31 index) {
    if (index.asInt() >= entries.size()) {
      return null;
    }
    return entries.get(index.asInt());
  }

  @Override
  public final void forEach(final Consumer<? super T> action) {
    throw new UnsupportedOperationException("AbstractSection.forEach unsupported");
  }

  @Override
  public final Iterator<T> iterator() {
    return entries.iterator();
  }

  @Override
  public final Spliterator<T> spliterator() {
    throw new UnsupportedOperationException("AbstractSection.spliterator unsupported");
  }
}
