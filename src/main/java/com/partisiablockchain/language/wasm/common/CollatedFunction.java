package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Parsed function definition, collated with it's type definition, code segment, and possibly debug
 * name.
 */
public record CollatedFunction(
    Uint31 functionId, CodeSegment codeSegment, FunctionType functionType, String debugName) {

  /**
   * Function constructor.
   *
   * @param functionId Function's absolute id/index, including potential import offsets. Never null
   * @param codeSegment Function's code segment, possibly invalid. Never null
   * @param functionType Function's type. Never null
   * @param debugName Function's debug name, if any. Might be null, indicating that no debug name
   *     could be found.
   */
  public CollatedFunction {}
}
