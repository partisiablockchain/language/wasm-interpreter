package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.interpreter.WasmInstance;

/** An opcode in Wasm. */
public enum OpCode {
  /** UNREACHABLE. */
  UNREACHABLE(0x00),
  /** NOP. */
  NOP(0x01),
  /** BLOCK. */
  BLOCK(0x02),
  /** LOOP. */
  LOOP(0x03),
  /** IF. */
  IF(0x04),
  /** ELSE. */
  ELSE(0x05),
  /** END. */
  END(0x0b),
  /** BR. */
  BR(0x0c),
  /** BR_IF. */
  BR_IF(0x0d),
  /** BR_TABLE. */
  BR_TABLE(0x0e),
  /** RETURN. */
  RETURN(0x0f),
  /** CALL. */
  CALL(0x10),
  /** CALL_INDIRECT. */
  CALL_INDIRECT(0x11),
  /** DROP. */
  DROP(0x1a),
  /** SELECT. */
  SELECT(0x1b),
  /** LOCAL_GET. */
  LOCAL_GET(0x20),
  /** LOCAL_SET. */
  LOCAL_SET(0x21),
  /** LOCAL_TEE. */
  LOCAL_TEE(0x22),
  /** GLOBAL_GET. */
  GLOBAL_GET(0x23),
  /** GLOBAL_SET. */
  GLOBAL_SET(0x24),
  /** I_32_LOAD. */
  I_32_LOAD(0x28),
  /** I_64_LOAD. */
  I_64_LOAD(0x29),
  /** I_32_LOAD_8_S. */
  I_32_LOAD_8_S(0x2c),
  /** I_32_LOAD_8_U. */
  I_32_LOAD_8_U(0x2d),
  /** I_32_LOAD_16_S. */
  I_32_LOAD_16_S(0x2e),
  /** I_32_LOAD_16_U. */
  I_32_LOAD_16_U(0x2f),
  /** I_64_LOAD_8_S. */
  I_64_LOAD_8_S(0x30),
  /** I_64_LOAD_8_U. */
  I_64_LOAD_8_U(0x31),
  /** I_64_LOAD_16_S. */
  I_64_LOAD_16_S(0x32),
  /** I_64_LOAD_16_U. */
  I_64_LOAD_16_U(0x33),
  /** I_64_LOAD_32_S. */
  I_64_LOAD_32_S(0x34),
  /** I_64_LOAD_32_U. */
  I_64_LOAD_32_U(0x35),
  /** I_32_STORE. */
  I_32_STORE(0x36),
  /** I_64_STORE. */
  I_64_STORE(0x37),
  /** I_32_STORE_8. */
  I_32_STORE_8(0x3a),
  /** I_32_STORE_16. */
  I_32_STORE_16(0x3b),
  /** I_64_STORE_8. */
  I_64_STORE_8(0x3c),
  /** I_64_STORE_16. */
  I_64_STORE_16(0x3d),
  /** I_64_STORE_32. */
  I_64_STORE_32(0x3e),
  /** MEMORY_SIZE. */
  MEMORY_SIZE(0x3f),
  /** GROW_MEMORY. */
  GROW_MEMORY(0x40),
  /** I_32_CONST. */
  I_32_CONST(0x41),
  /** I_64_CONST. */
  I_64_CONST(0x42),
  /** I_32_EQZ. */
  I_32_EQZ(0x45),
  /** I_32_EQ. */
  I_32_EQ(0x46),
  /** I_32_NE. */
  I_32_NE(0x47),
  /** I_32_LT_S. */
  I_32_LT_S(0x48),
  /** I_32_LT_U. */
  I_32_LT_U(0x49),
  /** I_32_GT_S. */
  I_32_GT_S(0x4a),
  /** I_32_GT_U. */
  I_32_GT_U(0x4b),
  /** I_32_LE_S. */
  I_32_LE_S(0x4c),
  /** I_32_LE_U. */
  I_32_LE_U(0x4d),
  /** I_32_GE_S. */
  I_32_GE_S(0x4e),
  /** I_32_GE_U. */
  I_32_GE_U(0x4f),
  /** I_64_EQZ. */
  I_64_EQZ(0x50),
  /** I_64_EQ. */
  I_64_EQ(0x51),
  /** I_64_NE. */
  I_64_NE(0x52),
  /** I_64_LT_S. */
  I_64_LT_S(0x53),
  /** I_64_LT_U. */
  I_64_LT_U(0x54),
  /** I_64_GT_S. */
  I_64_GT_S(0x55),
  /** I_64_GT_U. */
  I_64_GT_U(0x56),
  /** I_64_LE_S. */
  I_64_LE_S(0x57),
  /** I_64_LE_U. */
  I_64_LE_U(0x58),
  /** I_64_GE_S. */
  I_64_GE_S(0x59),
  /** I_64_GE_U. */
  I_64_GE_U(0x5a),
  /** I_32_CLZ. */
  I_32_CLZ(0x67),
  /** I_32_CTZ. */
  I_32_CTZ(0x68),
  /** I_32_POPCNT. */
  I_32_POPCNT(0x69),
  /** I_32_ADD. */
  I_32_ADD(0x6a),
  /** I_32_SUB. */
  I_32_SUB(0x6b),
  /** I_32_MUL. */
  I_32_MUL(0x6c),
  /** I_32_DIV_S. */
  I_32_DIV_S(0x6d),
  /** I_32_DIV_U. */
  I_32_DIV_U(0x6e),
  /** I_32_REM_S. */
  I_32_REM_S(0x6f),
  /** I_32_REM_U. */
  I_32_REM_U(0x70),
  /** I_32_AND. */
  I_32_AND(0x71),
  /** I_32_OR. */
  I_32_OR(0x72),
  /** I_32_XOR. */
  I_32_XOR(0x73),
  /** I_32_SHL. */
  I_32_SHL(0x74),
  /** I_32_SHR_S. */
  I_32_SHR_S(0x75),
  /** I_32_SHR_U. */
  I_32_SHR_U(0x76),
  /** I_32_ROTL. */
  I_32_ROTL(0x77),
  /** I_32_ROTR. */
  I_32_ROTR(0x78),
  /** I_64_CLZ. */
  I_64_CLZ(0x79),
  /** I_64_CTZ. */
  I_64_CTZ(0x7a),
  /** I_64_POPCNT. */
  I_64_POPCNT(0x7b),
  /** I_64_ADD. */
  I_64_ADD(0x7c),
  /** I_64_SUB. */
  I_64_SUB(0x7d),
  /** I_64_MUL. */
  I_64_MUL(0x7e),
  /** I_64_DIV_S. */
  I_64_DIV_S(0x7f),
  /** I_64_DIV_U. */
  I_64_DIV_U(0x80),
  /** I_64_REM_S. */
  I_64_REM_S(0x81),
  /** I_64_REM_U. */
  I_64_REM_U(0x82),
  /** I_64_AND. */
  I_64_AND(0x83),
  /** I_64_OR. */
  I_64_OR(0x84),
  /** I_64_XOR. */
  I_64_XOR(0x85),
  /** I_64_SHL. */
  I_64_SHL(0x86),
  /** I_64_SHR_S. */
  I_64_SHR_S(0x87),
  /** I_64_SHR_U. */
  I_64_SHR_U(0x88),
  /** I_64_ROTL. */
  I_64_ROTL(0x89),
  /** I_64_ROTR. */
  I_64_ROTR(0x8a),
  /** I_32_WRAP_I_64. */
  I_32_WRAP_I_64(0xa7),
  /** I_64_EXTEND_I_32_S. */
  I_64_EXTEND_I_32_S(0xac),
  /** I_64_EXTEND_I_32_U. */
  I_64_EXTEND_I_32_U(0xad),
  /** I_32_EXTEND_8_S. */
  I_32_EXTEND_8_S(0xc0),
  /** I_32_EXTEND_16_S. */
  I_32_EXTEND_16_S(0xc1),
  /** I_64_EXTEND_8_S. */
  I_64_EXTEND_8_S(0xc2),
  /** I_64_EXTEND_16_S. */
  I_64_EXTEND_16_S(0xc3),
  /** I_64_EXTEND_32_S. */
  I_64_EXTEND_32_S(0xc4),
  /** UNKNOWN. */
  UNKNOWN(0x100),
  /**
   * UNSUPPORTED_FLOATING_POINT. Specifically for distinguishing unsupported floating point
   * instructions, as compared to actually unknown instructions.
   */
  UNSUPPORTED_FLOATING_POINT(0x101),
  ;

  private static final OpCode[] cache = new OpCode[0x102];
  private static final int[] FLOATING_POINT_INSTRUCTIONS =
      new int[] {
        0x2a, 0x2b, 0x38, 0x39, 0x43, 0x44, 0x5b, 0x5c, 0x5d, 0x5e, 0x5f, 0x60, 0x61, 0x62, 0x63,
        0x64, 0x65, 0x66, 0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x93, 0x94, 0x95, 0x96,
        0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5,
        0xa6, 0xa8, 0xa9, 0xaa, 0xab, 0xae, 0xaf, 0xb0, 0xb1, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7,
        0xb8, 0xb9, 0xba, 0xbb, 0xbc, 0xbd, 0xbe, 0xbf,
      };

  static {
    for (int i = 0; i < cache.length; i++) {
      cache[i] = UNKNOWN;
    }
    for (int opcode : FLOATING_POINT_INSTRUCTIONS) {
      cache[opcode] = UNSUPPORTED_FLOATING_POINT;
    }

    for (OpCode code : values()) {
      cache[code.value] = code;
    }
  }

  /** The WASM binary representation of the opcode. */
  private final int value;

  /** The cycles cost of executing the opcode. */
  private final Uint31 cyclesCost;

  OpCode(int value, Uint31 cyclesCost) {
    this.value = value;
    this.cyclesCost = cyclesCost;
  }

  OpCode(int value) {
    this(value, WasmInstance.GAS_PER_INSTRUCTION);
  }

  /**
   * Gets the cycles cost for executing this opcode.
   *
   * @return the cost
   */
  public Uint31 getCyclesCost() {
    return cyclesCost;
  }

  /**
   * Gets the value for this op code.
   *
   * @return the value
   */
  public int getValue() {
    return value;
  }

  /**
   * Finds the op code given its value.
   *
   * @param value the value
   * @return the op code
   */
  public static OpCode fromValue(int value) {
    return cache[value];
  }
}
