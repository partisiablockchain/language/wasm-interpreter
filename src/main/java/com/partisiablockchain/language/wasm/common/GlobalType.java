package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmIllegalBytecodeException;

/**
 * Implementation of <a
 * href="https://webassembly.github.io/spec/core/syntax/modules.html#syntax-global">global variable
 * types</a>.
 */
@Immutable
public final class GlobalType implements ImportEntryType {

  private final ValType contentType;
  private final boolean mutable;

  /**
   * Constructor for GlobalType.
   *
   * @param contentType value type of the global
   * @param mutable whether the global can be modified during execution.
   */
  public GlobalType(ValType contentType, boolean mutable) {
    this.contentType = contentType;
    this.mutable = mutable;
  }

  /**
   * Value type of the global.
   *
   * @return value type
   */
  public ValType getContentType() {
    return contentType;
  }

  /**
   * Whether the type is mutable.
   *
   * @return true if and only if type is mutable
   */
  public boolean isMutable() {
    return mutable;
  }

  /**
   * Reads a global variable type from a wasm byte stream.
   *
   * @param stream the stream
   * @return the created global variable type
   */
  public static GlobalType read(final WasmByteStream stream) {
    final ValType valType = ValType.read(stream);
    final int mutabilityHex = stream.readUint8();

    final boolean isMutable;
    if (mutabilityHex == 0x00) {
      isMutable = false;
    } else if (mutabilityHex == 0x01) {
      isMutable = true;
    } else {
      throw new WasmIllegalBytecodeException(
          "Invalid mutability indicator for GlobalType: 0x%02X (must be either 0x00 or 0x01)",
          mutabilityHex);
    }

    return new GlobalType(valType, isMutable);
  }
}
