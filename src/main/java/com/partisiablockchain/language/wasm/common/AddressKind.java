package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.parser.WasmParseExceptionImpl;

/**
 * Type of address in a {@link TableType}.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#addresses%E2%91%A0">WASM Spec</a>
 */
public enum AddressKind {
  /** FUNCTION. */
  FUNCTION,
  /** GLOBAL. */
  GLOBAL,
  /** MEMORY. */
  MEMORY,
  /** TABLE. */
  TABLE;

  static AddressKind fromValue(final int value) {
    return switch (value) {
      case 0x00 -> FUNCTION;
      case 0x01 -> TABLE;
      case 0x02 -> MEMORY;
      case 0x03 -> GLOBAL;
      default -> throw new WasmParseExceptionImpl("Unknown export kind id: 0x%02X", value);
    };
  }
}
