package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmParseExceptionImpl;
import java.util.List;

/**
 * Implementation of <a href="https://webassembly.github.io/spec/core/syntax/types.html">function
 * types</a>.
 */
public final class FunctionType {

  private final Uint31 index;
  private final List<ValType> params;
  private final List<ValType> returns;

  /**
   * Creates new {@code FunctionType} with the given parameter and return types.
   *
   * @param index index identifier of function type
   * @param params parameter types of the function
   * @param returns return types of the function
   */
  public FunctionType(Uint31 index, List<ValType> params, List<ValType> returns) {
    this.index = index;
    this.params = params;
    this.returns = returns;
  }

  /**
   * Parsers {@code FunctionType} from the given byte stream.
   *
   * @param stream the stream
   * @param index the index
   * @return the parsed function type
   */
  public static FunctionType parseFunctionType(WasmByteStream stream, Uint31 index) {
    int flag = stream.readUint8();
    if (flag != 0x60) {
      throw new WasmParseExceptionImpl("Invalid function type leading byte: %X", flag);
    }

    List<ValType> params = stream.parseVectorStream(ValType::read);
    List<ValType> results = stream.parseVectorStream(ValType::read);
    return new FunctionType(index, params, results);
  }

  /**
   * Gets the index.
   *
   * @return the index
   */
  public Uint31 getIndex() {
    return index;
  }

  /**
   * Gets the parameters types.
   *
   * @return parameters types.
   */
  public List<ValType> getParams() {
    return params;
  }

  /**
   * Gets the return types.
   *
   * @return the return types
   */
  public List<ValType> getReturns() {
    return returns;
  }

  /**
   * Returns true if the that function type is compatible with the current one (i.e. shares
   * signature).
   *
   * @param that the other function
   * @return true if compatible
   */
  public boolean callCompatibleWith(FunctionType that) {
    return sameTypes(getReturns(), that.getReturns()) && sameTypes(getParams(), that.getParams());
  }

  private boolean sameTypes(List<ValType> left, List<ValType> right) {
    return left.equals(right);
  }

  @Override
  public String toString() {
    return "FunctionType{" + "index=" + index + ", params=" + params + ", returns=" + returns + '}';
  }

  /**
   * Formats the function type similar to the format used in the WASM spec.
   *
   * @return string representing function type
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#function-types%E2%91%A0">WASM Spec 2.3.3.
   *     Function Types</a>
   */
  public String toTypeString() {
    return String.format("%s -> %s", params, returns);
  }
}
