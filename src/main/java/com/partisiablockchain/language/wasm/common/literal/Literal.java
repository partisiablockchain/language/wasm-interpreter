package com.partisiablockchain.language.wasm.common.literal;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.ValType;

/** A literal in a wasm program. */
@Immutable
public sealed interface Literal permits Int32, Int64 {

  /**
   * The value type of this literal.
   *
   * @return the value type.
   */
  ValType getValType();

  /**
   * Asserts that this literal is of the expected type, returning it as the expected type, or
   * otherwise throwing an exception.
   *
   * @return If the literal has the expected type. Never null.
   * @throws TrapException If the literal has the wrong type.
   */
  Int32 expectInt32();

  /**
   * Asserts that this literal is of the expected type, returning it as the expected type, or
   * otherwise throwing an exception.
   *
   * @return If the literal has the expected type. Never null.
   * @throws TrapException If the literal has the wrong type.
   */
  Int64 expectInt64();

  /**
   * Serialize literal to its memory format.
   *
   * @return serialized bytes
   */
  byte[] toBytes();
}
