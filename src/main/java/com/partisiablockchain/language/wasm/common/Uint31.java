package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;

/** Unsigned 32-bit integer wrapper. */
@Immutable
public final class Uint31 implements Comparable<Uint31> {

  /** Constant with value 0. */
  public static final Uint31 ZERO = new Uint31(0);

  /** Constant with value 1. */
  public static final Uint31 ONE = new Uint31(1);

  /** Constant with value 2. */
  public static final Uint31 TWO = new Uint31(2);

  /** Constant with value 4. */
  public static final Uint31 FOUR = new Uint31(4);

  /** Constant with minimal value (0). */
  public static final Uint31 MIN_VALUE = ZERO;

  /** Constant with maximal value (2^31-1 = 2147483647). */
  public static final Uint31 MAX_VALUE = new Uint31(2147483647);

  /** Invariant: value property is never negative. */
  private final int value;

  /**
   * Constructor taking the signed representation of the wrapped unsigned integer.
   *
   * @param value signed representation of the wanted integer.
   */
  public Uint31(final int value) {
    if (value < 0) {
      throw new IllegalArgumentException("Integer " + value + " too large/small for Uint31");
    }
    this.value = value;
  }

  /**
   * Constructor taking the signed representation of the wrapped unsigned integer, returns null if
   * value does not fit into a 31-bit unsigned integer.
   *
   * @param value signed representation of the wanted integer.
   * @return integer or null
   */
  public static Uint31 orNull(final int value) {
    if (0 <= value) {
      return new Uint31(value);
    }
    return null;
  }

  /**
   * Guarenteed to always be positive.
   *
   * @return positive 32-bit signed integer
   */
  public int asInt() {
    return this.value;
  }

  /**
   * Add value with other.
   *
   * @param other value to add with
   * @return computation result. Never null.
   */
  public Uint31 add(final Uint31 other) {
    return new Uint31(this.value + other.value);
  }

  /**
   * Subtract value by other.
   *
   * @param other value to subtract by
   * @return computation result. Never null.
   */
  public Uint31 minus(final Uint31 other) {
    return new Uint31(this.value - other.value);
  }

  /**
   * Multiply value with other.
   *
   * @param other value to multiply with
   * @return computation result. Never null.
   */
  public Uint31 mult(final Uint31 other) {
    return new Uint31(this.value * other.value);
  }

  /**
   * Divide value with other.
   *
   * @param other value to divide by
   * @return computation result. Never null.
   */
  public Uint31 div(final Uint31 other) {
    return new Uint31(Integer.divideUnsigned(this.value, other.value));
  }

  @Override
  public String toString() {
    return Integer.toUnsignedString(this.value, 10);
  }

  @Override
  public int compareTo(final Uint31 other) {
    return Integer.compareUnsigned(this.value, other.value);
  }

  @Override
  public boolean equals(final Object other) {
    return other instanceof Uint31 && this.compareTo((Uint31) other) == 0;
  }

  @Override
  public int hashCode() {
    return Integer.hashCode(this.value);
  }

  /**
   * Compare with other value.
   *
   * @param other value to compare with
   * @return computation result. Never null.
   */
  public boolean lessThan(final Uint31 other) {
    return this.compareTo(other) < 0;
  }

  /**
   * Compare with other value.
   *
   * @param other value to compare with
   * @return computation result. Never null.
   */
  public boolean lessThanOrEqual(final Uint31 other) {
    return this.compareTo(other) <= 0;
  }

  /**
   * Compare with other value.
   *
   * @param other value to compare with
   * @return computation result. Never null.
   */
  public boolean greaterThan(final Uint31 other) {
    return this.compareTo(other) > 0;
  }

  /**
   * Compare with other value.
   *
   * @param other value to compare with
   * @return computation result. Never null.
   */
  public Uint31 min(final Uint31 other) {
    return this.lessThan(other) ? this : other;
  }

  /**
   * Subtract 1 from value.
   *
   * @return computation result. Never null.
   */
  public Uint31 minusOne() {
    return new Uint31(this.value - 1);
  }

  /**
   * Add 1 to value.
   *
   * @return computation result. Never null.
   */
  public Uint31 addOne() {
    return new Uint31(this.value + 1);
  }
}
