package com.partisiablockchain.language.wasm.pbclib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmMemory;
import java.util.List;
import java.util.function.Function;

/**
 * Implements a quick and cheap memmove implementation, by invoking Java's built-in memmove
 * equivalent.
 *
 * <p>An instance of this class can be directly registered using {@link
 * WasmInstance#registerExternal}.
 */
public record HostedMemmove(Uint31 cyclesPerCall, Uint31 cyclesPerByte)
    implements Function<WasmInstance, List<Literal>> {

  @Override
  public List<Literal> apply(final WasmInstance instance) {
    // The raw arguments are kept around to give better error messages.
    final Int32 ptrDestRaw = (Int32) instance.getLocal(new Uint31(0));
    final Int32 ptrSrcRaw = (Int32) instance.getLocal(new Uint31(1));
    final Int32 lenRaw = (Int32) instance.getLocal(new Uint31(2));

    final Uint31 ptrDest = ptrDestRaw.toUint31OrNull();
    final Uint31 ptrSrc = ptrSrcRaw.toUint31OrNull();
    final Uint31 len = lenRaw.toUint31OrNull();

    // Bounds check
    final WasmMemory memory = instance.getMemory(Uint31.ZERO);
    if (ptrDest == null || len == null || !memory.isWithinBounds(ptrDest, len)) {
      throw memory.newBoundsChecksException(ptrDestRaw.value(), lenRaw.value(), "Writing");
    } else if (ptrSrc == null || !memory.isWithinBounds(ptrSrc, len)) {
      throw memory.newBoundsChecksException(ptrSrcRaw.value(), lenRaw.value(), "Copying");
    }

    // Actual effects
    instance.useCycles(len, cyclesPerByte());
    instance.useCycles(cyclesPerCall());
    memory.memmove(ptrDest, ptrSrc, len);
    return List.of(ptrDestRaw);
  }
}
