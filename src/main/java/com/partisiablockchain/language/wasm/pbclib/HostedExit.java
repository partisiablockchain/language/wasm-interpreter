package com.partisiablockchain.language.wasm.pbclib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.nio.charset.StandardCharsets.UTF_8;

import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmMemory;
import java.util.List;
import java.util.function.Function;
import org.slf4j.LoggerFactory;

/** Implements an exit method to print a given error msg from a rust panic. */
public record HostedExit() implements Function<WasmInstance, List<Literal>> {

  private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(HostedExit.class);

  /** The longest allowed message length. Messages longer than this will be truncated. */
  public static final Uint31 MAXIMUM_ALLOWED_MESSAGE_LENGTH = new Uint31(1024);

  private static final Uint31 MAXIMUM_ALLOWED_MESSAGE_LENGTH_AFTER_DOTS =
      MAXIMUM_ALLOWED_MESSAGE_LENGTH.minus(new Uint31(3));

  @Override
  public List<Literal> apply(final WasmInstance instance) {
    // The raw arguments for the function
    final Int32 ptrRaw = (Int32) instance.getLocal(new Uint31(0));
    final Int32 lenRaw = (Int32) instance.getLocal(new Uint31(1));

    final Uint31 ptr = ptrRaw.toUint31OrNull();
    final Uint31 len = lenRaw.toUint31OrNull();

    // Bounds check
    final WasmMemory memory = instance.getMemory(Uint31.ZERO);
    if (ptr == null || len == null || !memory.isWithinBounds(ptr, len)) {
      throw memory.newBoundsChecksException(
          ptrRaw.value(), lenRaw.value(), "Reading error message in exit call");
    }

    final boolean truncated = len.greaterThan(MAXIMUM_ALLOWED_MESSAGE_LENGTH);
    final Uint31 truncatedLen =
        truncated ? len.min(MAXIMUM_ALLOWED_MESSAGE_LENGTH_AFTER_DOTS) : len;

    // Read Error string
    final byte[] msgBytes = memory.read(ptr, truncatedLen);
    String msg = new String(msgBytes, UTF_8);
    if (truncated) {
      msg += "...";
    }

    LOGGER.error(msg);
    // Actual effects
    throw new WasmExitException(msg);
  }

  static final class WasmExitException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    WasmExitException(final String message) {
      super(String.format("Early exit: %s", message));
    }
  }
}
