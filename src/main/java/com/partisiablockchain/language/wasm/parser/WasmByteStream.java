package com.partisiablockchain.language.wasm.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.MemoryAddress;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Wraps a bytestream and provides parsing of WASM internal data structures.
 *
 * <p>Invariant: Parsing can only occur forwards, and never backwards.
 */
public final class WasmByteStream {

  private final byte[] data;

  /** Invariant: Position must only ever grow. */
  private int position;

  /**
   * Create byte stream operating over given byte stream.
   *
   * @param data byte stream to parse
   */
  public WasmByteStream(byte[] data) {
    this.data = data.clone();
  }

  /**
   * Retrives 32-bit integer under the cursor, without moving cursor.
   *
   * @return 32-bit integer under the cursor
   */
  public int peekInt32() {
    int b1 = data[position] & 0xFF;
    int b2 = data[position + 1] & 0xFF;
    int b3 = data[position + 2] & 0xFF;
    int b4 = data[position + 3] & 0xFF;

    return b1 | (b2 << 8) | (b3 << 16) | (b4 << 24);
  }

  /**
   * Reads unsigned 8-bit integer under the cursor, and moves cursor.
   *
   * @return 8-bit integer under the cursor
   */
  public int readUint8() {
    if (position >= data.length) {
      throw new WasmIllegalBytecodeException("Unexpected end of section or function");
    }
    return data[position++] & 0xFF;
  }

  /**
   * Reads string under cursor, and moves cursor.
   *
   * @return string bytes as byte array.
   */
  public String readStringBytes() {
    final byte[] bytes = readVarBytes();
    try {
      return utf8Decoder.decode(ByteBuffer.wrap(bytes)).toString();
    } catch (final CharacterCodingException e) {
      throw new WasmIllegalBytecodeException("Invalid UTF-8 encoding");
    }
  }

  /** Not static, to avoid accidental usage from multiple threads. */
  private final CharsetDecoder utf8Decoder =
      StandardCharsets.UTF_8.newDecoder().onMalformedInput(CodingErrorAction.REPORT);

  /**
   * Reads a variable amount of bytes under cursor, length stored along with data, and moves cursor.
   *
   * @return read bytes as byte array.
   */
  public byte[] readVarBytes() {
    Uint31 length = readVarUint31();
    return readBytes(length);
  }

  /**
   * Reads a specific number of bytes under cursor, and moves cursor.
   *
   * @param length number of bytes to read
   * @return Bytes read
   */
  public byte[] readBytes(Uint31 length) {
    byte[] slice = getSlice(getPosition(), length);
    skip(length);
    return slice;
  }

  /**
   * Checks whether the cursor is at the end, or not.
   *
   * @return true iff cursor is at the end.
   */
  public boolean isEof() {
    return position == data.length;
  }

  /**
   * Read a leb128 encoded 32 bit signed integer.
   *
   * @return the resulting int
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#integers%E2%91%A4">WASM Spec 5.2.2</a>
   */
  public Int32 readVarInt32() {
    return new Int32((int) Leb128.signed(32, this));
  }

  /**
   * Read a leb128 encoded 32 bit unsigned integer.
   *
   * @return the resulting unsigned integer
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#integers%E2%91%A4">WASM Spec 5.2.2</a>
   */
  public int readVarUint32() {
    return (int) Leb128.unsigned(32, this);
  }

  /**
   * Reads an index. While the specification notes these can be any unsigned 32-bit integer, we only
   * support 31-bit unsigned integers, due to Java limitations and security concerns. The value will
   * still be parsed as an 32-bit unsigned integer, but an exception will be thrown if the integer
   * doesn't fit into an 31-bit unsigned integer.
   *
   * @return the resulting unsigned integer wrapped in an Uint31
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#integers%E2%91%A4">WASM Spec 5.2.2</a>
   */
  public Uint31 readVarUint31() {
    int result = readVarUint32();
    if (result < 0) {
      throw new WasmInterpreterLimitationException(
          "WASM interpreter only supports 31-bits unsigned integers for indices and lengths, but"
              + " read integer %d",
          (long) result);
    }
    return new Uint31(result);
  }

  /**
   * Like {@link #readVarUint31}, but will return null rather than throwing an error.
   *
   * @return the resulting unsigned integer wrapped in an Uint31
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#integers%E2%91%A4">WASM Spec 5.2.2</a>
   */
  public Uint31 readVarUint31OrNull() {
    int result = readVarUint32();
    if (result < 0) {
      return null;
    }
    return new Uint31(result);
  }

  /**
   * Read a leb128 encoded 64 bit integer.
   *
   * @return the resulting value as a long
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#integers%E2%91%A4">WASM Spec 5.2.2</a>
   */
  public Int64 readInt64() {
    return new Int64(Leb128.signed(64, this));
  }

  private void checkBounds(Uint31 position) {
    if (position.greaterThan(size())) {
      String msg = String.format("Index %s out of bounds of [0..%d]", position, data.length);
      throw new ArrayIndexOutOfBoundsException(msg);
    }
  }

  /**
   * Skip a number of bytes.
   *
   * @param numBytes the number of bytes to skip
   */
  public void skip(Uint31 numBytes) {
    int newPosition = this.position + numBytes.asInt();
    if (newPosition < 0) {
      throw new WasmIllegalBytecodeException("Cannot skip beyond EOF");
    }
    this.position = newPosition;
  }

  /**
   * Get current position.
   *
   * @return current position
   */
  public Uint31 getPosition() {
    return new Uint31(position);
  }

  /**
   * Get the length of the data.
   *
   * @return the length of the raw data
   */
  public Uint31 size() {
    return new Uint31(data.length);
  }

  boolean hasBytes(final Uint31 numBytes) {
    return getPosition().add(numBytes).lessThanOrEqual(size());
  }

  /**
   * Read a memory address at the current position.
   *
   * @return the memory address
   */
  public MemoryAddress readMemory() {
    Uint31 flags = readVarUint31();
    Uint31 offset = readVarUint31();
    return new MemoryAddress(flags, offset);
  }

  /**
   * Get a copy of the data at the given index with the given lenght.
   *
   * @param index the starting index
   * @param length the number of bytes
   * @return a copy of the data
   */
  public byte[] getSlice(Uint31 index, Uint31 length) {
    Uint31 to = index.add(length);
    checkBounds(to);
    return Arrays.copyOfRange(data, index.asInt(), to.asInt());
  }

  /**
   * Reads vector length, and delegates to function to parse contents.
   *
   * @param <T> type of returned objects.
   * @param function delegated function.
   * @return contents as list of arbitrary objects.
   */
  public <T> List<T> parseVector(Supplier<T> function) {
    int count = readVarUint31().asInt();

    List<T> items = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      items.add(function.get());
    }
    return items;
  }

  /**
   * Reads vector length, and delegates to function to parse contents.
   *
   * @param <T> type of returned objects.
   * @param function delegated function.
   * @return contents as list of arbitrary objects.
   */
  public <T> List<T> parseVectorStream(Function<WasmByteStream, T> function) {
    int count = readVarUint31().asInt();
    List<T> items = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      items.add(function.apply(this));
    }
    return items;
  }

  /**
   * Reads vector length, and delegates to function to parse contents.
   *
   * @param <T> type of returned objects.
   * @param function delegated function.
   * @return contents as list of arbitrary objects.
   */
  public <T> List<T> parseVectorIndexed(Function<Uint31, T> function) {
    int count = readVarUint31().asInt();
    List<T> items = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      items.add(function.apply(new Uint31(i)));
    }
    return items;
  }
}
