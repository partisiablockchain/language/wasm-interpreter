package com.partisiablockchain.language.wasm.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.CodeSegment;
import com.partisiablockchain.language.wasm.common.CollatedFunction;
import com.partisiablockchain.language.wasm.common.DataSegment;
import com.partisiablockchain.language.wasm.common.ElementSegment;
import com.partisiablockchain.language.wasm.common.Export;
import com.partisiablockchain.language.wasm.common.FunctionType;
import com.partisiablockchain.language.wasm.common.Global;
import com.partisiablockchain.language.wasm.common.Import;
import com.partisiablockchain.language.wasm.common.ImportEntryType;
import com.partisiablockchain.language.wasm.common.MemoryType;
import com.partisiablockchain.language.wasm.common.Naming;
import com.partisiablockchain.language.wasm.common.ResizableLimits;
import com.partisiablockchain.language.wasm.common.SectionCode;
import com.partisiablockchain.language.wasm.common.TableType;
import com.partisiablockchain.language.wasm.common.TypeIndex;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.sections.DataSection;
import com.partisiablockchain.language.wasm.common.sections.ElementSection;
import com.partisiablockchain.language.wasm.common.sections.ExportSection;
import com.partisiablockchain.language.wasm.common.sections.FunctionSection;
import com.partisiablockchain.language.wasm.common.sections.GlobalSection;
import com.partisiablockchain.language.wasm.common.sections.ImportSection;
import com.partisiablockchain.language.wasm.common.sections.MemorySection;
import com.partisiablockchain.language.wasm.common.sections.TableSection;
import com.partisiablockchain.language.wasm.common.sections.TypeSection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/** Wraps byte array and provides WASM parsing functionality. */
@SuppressWarnings("DuplicatedCode")
public final class WasmParser {

  private static final byte[] WASM_MAGIC_BYTES = {0x00, 0x61, 0x73, 0x6d};
  private static final byte[] WASM_SUPPORTED_VERSION = {0x01, 0x00, 0x00, 0x00};

  private final WasmByteStream stream;

  /**
   * Parser from byte stream.
   *
   * @param stream byte stream to parse from
   */
  public WasmParser(WasmByteStream stream) {
    this.stream = stream;
  }

  /**
   * Parser from byte array.
   *
   * @param data byte array to parse from
   */
  public WasmParser(byte[] data) {
    this(new WasmByteStream(data));
  }

  /**
   * Parses a WASM module from the wrapped data.
   *
   * @return Parsed {%link WasmModule}.
   */
  public WasmModule parse() {
    parseMagic();
    parseVersion();

    int previousSectionId = -1;

    List<CodeSegment> codeSegments = List.of();
    DataSection dataSection = new DataSection(List.of());
    ElementSection elementSection = new ElementSection(List.of());
    ExportSection exportSection = new ExportSection(List.of());
    List<Uint31> codeIndiceToFunctionType = List.of();
    GlobalSection globalSection = new GlobalSection(List.of());
    ImportSection<ImportEntryType> importSection = new ImportSection<>(List.of());
    MemorySection memorySection = new MemorySection(List.of());
    TableSection tableSection = new TableSection(List.of());
    TypeSection typeSection = new TypeSection(List.of());
    List<Naming> functionNames = List.of();

    while (!stream.isEof()) {
      final int rawId = stream.readUint8();
      final SectionCode sectionId = SectionCode.fromValue(rawId);
      final Uint31 sectionLength = stream.readVarUint31OrNull();
      final Uint31 sectionStart = stream.getPosition();

      // Check that sections are correctly sized
      if (sectionLength == null) {
        throw new WasmInterpreterLimitationException("Section %s too long", sectionId);
      } else if (!stream.getPosition().lessThanOrEqual(Uint31.MAX_VALUE.minus(sectionLength))) {
        throw new WasmInterpreterLimitationException(
            "Section %s starting at %s too long: %s",
            sectionId, stream.getPosition(), sectionLength);
      }

      // Check that sections are not duplicated, and occur in correct order
      if (sectionId != SectionCode.CUSTOM) {
        if (previousSectionId > rawId) {
          throw new WasmParseExceptionImpl(
              "Sections are out of order. Just saw %s section, while previous section was %s",
              sectionId, SectionCode.fromValue(previousSectionId));
        } else if (previousSectionId == rawId) {
          throw new WasmParseExceptionImpl(
              "Sections are duplicated. %s section have occured twice in a row", sectionId);
        }
        previousSectionId = rawId;
      }

      // Parse individual sections
      if (sectionId == SectionCode.CUSTOM) {
        NameCustomSectionData customSectionData = readCustomSection(sectionLength);
        if (customSectionData != null) {
          functionNames = customSectionData.functionNames;
        }
      } else if (sectionId == SectionCode.TYPE) {
        List<FunctionType> functionTypes =
            stream.parseVectorIndexed(index -> FunctionType.parseFunctionType(stream, index));
        typeSection = new TypeSection(functionTypes);
      } else if (sectionId == SectionCode.IMPORT) {
        List<Import<ImportEntryType>> entries = stream.parseVector(() -> Import.read(stream));
        importSection = new ImportSection<>(entries);
      } else if (sectionId == SectionCode.FUNCTION) {
        codeIndiceToFunctionType = stream.parseVector(stream::readVarUint31);
      } else if (sectionId == SectionCode.TABLE) {
        List<TableType> tableTypes = stream.parseVectorStream(TableType::read);
        tableSection = new TableSection(tableTypes);
      } else if (sectionId == SectionCode.MEMORY) {
        List<MemoryType> memTypes = stream.parseVectorStream(MemoryType::read);
        memorySection = new MemorySection(memTypes);
      } else if (sectionId == SectionCode.GLOBAL) {
        List<Global> globals = stream.parseVectorStream(Global::read);
        globalSection = new GlobalSection(globals);
      } else if (sectionId == SectionCode.EXPORT) {
        List<Export> exports = stream.parseVectorStream(Export::read);
        exportSection = new ExportSection(exports);
      } else if (sectionId == SectionCode.START) {
        stream.readVarUint31();
      } else if (sectionId == SectionCode.ELEMENT) {
        List<ElementSegment> segments = stream.parseVectorStream(ElementSegment::read);
        elementSection = new ElementSection(segments);
      } else if (sectionId == SectionCode.CODE) {
        codeSegments = stream.parseVectorStream(CodeSegment::read);
      } else if (sectionId == SectionCode.DATA) {
        List<DataSegment> segments = stream.parseVectorStream(DataSegment::read);
        dataSection = new DataSection(segments);
      } else {
        throw new WasmParseExceptionImpl(
            "Unknown or malformed section id: 0x%02X at byte %s", rawId, sectionStart);
      }

      if (!stream.getPosition().equals(sectionStart.add(sectionLength))) {
        throw new WasmParseExceptionImpl(
            "Section size mismatch for %s. Expected %s, but parsed %s bytes",
            sectionId, sectionLength, stream.getPosition().minus(sectionStart));
      }
    }

    final ArrayList<Import<TypeIndex>> functionImports = new ArrayList<>();
    for (final Import<ImportEntryType> entry : importSection) {
      // Check that only functions can be imported
      if (entry.getImportDescriptor() instanceof TypeIndex typeIndexDescriptor) {
        functionImports.add(entry.<TypeIndex>copyWithDescriptor(typeIndexDescriptor));
      } else {
        throw new WasmInterpreterLimitationException(
            "Cannot import \"%s\" from module \"%s\": Only function imports are supported.",
            entry.getImportedName(), entry.getModuleName());
      }
    }
    final ImportSection<TypeIndex> importedFunctionSection =
        new ImportSection<TypeIndex>(List.copyOf(functionImports));

    for (DataSegment data : dataSection) {
      final MemoryType memoryType = memorySection.getEntryOrNull(data.getMemoryIndex());
      if (memoryType == null) {
        throw new WasmConsistencyException(
            "Data segment references unknown memory: %s", data.getMemoryIndex());
      }
    }

    // Check for a maximum of one memory
    if (tableSection.numEntries().greaterThan(Uint31.ONE)) {
      throw new WasmConsistencyException(
          "Multiple tables are not supported. Module had %s", tableSection.numEntries());
    }

    for (ElementSegment segment : elementSection) {
      TableType tableType = tableSection.getEntryOrNull(segment.getTableIndex());
      if (tableType == null) {
        throw new WasmConsistencyException(
            "Element segment references unknown table: %s", segment.getTableIndex());
      }
    }

    // Check for a maximum of one memory
    if (memorySection.numEntries().greaterThan(Uint31.ONE)) {
      throw new WasmConsistencyException(
          "Multiple memories are not supported. Module had %s", memorySection.numEntries());
    }

    // Check that memories are well-formed
    for (final MemoryType memory : memorySection) {
      final ResizableLimits limits = memory.getLimits();
      if (limits.getMinimum().greaterThan(limits.getMaximum())) {
        throw new WasmConsistencyException(
            "Invalid memory: Size minimum must not be greater than maximum. Minimum was %s, maximum"
                + " was %s",
            limits.getMinimum(), limits.getMaximum());
      }
    }

    // Collate code and functionSection.
    final FunctionSection realizedFunctionSection =
        realizeFunctionSection(
            codeSegments,
            codeIndiceToFunctionType,
            typeSection,
            functionNames,
            importedFunctionSection.numEntries());

    // Check that exports are uniquely named
    // Check that exported things are defined
    final HashSet<String> seenExportedNames = new HashSet<>();
    for (Export export : exportSection) {
      if (!seenExportedNames.add(export.getName())) {
        throw new WasmConsistencyException("Duplicate export name: %s", export.getName());
      }

      final Uint31 numOfType;
      switch (export.getKind()) {
        case FUNCTION:
          numOfType =
              realizedFunctionSection.numEntries().add(importedFunctionSection.numEntries());
          break;
        case GLOBAL:
          numOfType = globalSection.numEntries();
          break;
        case MEMORY:
          numOfType = memorySection.numEntries();
          break;
        case TABLE:
        default:
          numOfType = tableSection.numEntries();
          break;
      }

      if (!export.getIndex().lessThan(numOfType)) {
        throw new WasmConsistencyException(
            "Cannot export \"%s\": Unknown %s %s",
            export.getName(), export.getKind(), export.getIndex());
      }
    }

    // Check that tables references existing functions
    for (final ElementSegment element : elementSection) {
      for (final Uint31 referencedFunctionId : element.getInit()) {

        final Import<TypeIndex> importedFunction =
            importedFunctionSection.getEntryOrNull(referencedFunctionId);
        if (importedFunction != null) {
          continue; // All is well
        }

        final CollatedFunction referencedFunction =
            realizedFunctionSection.getEntryOrNull(
                referencedFunctionId.minus(importedFunctionSection.numEntries()));

        if (referencedFunction == null) {
          throw new WasmConsistencyException(
              "Element references unknown function %s for module with only %s functions",
              referencedFunctionId,
              importedFunctionSection.numEntries().add(realizedFunctionSection.numEntries()));
        }
      }
    }

    return WasmModule.withSections(
        realizedFunctionSection,
        dataSection,
        elementSection,
        exportSection,
        globalSection,
        importedFunctionSection,
        memorySection,
        tableSection,
        typeSection);
  }

  private static FunctionSection realizeFunctionSection(
      final List<CodeSegment> codeSegments,
      final List<Uint31> codeIndiceToFunctionType,
      final TypeSection typeSection,
      final List<Naming> functionNames,
      final Uint31 importSectionLength) {

    // Initial function names index
    final Map<Uint31, String> functionNamesByIndex = new HashMap<>();
    for (Naming naming : functionNames) {
      functionNamesByIndex.put(naming.getIndex(), naming.getName());
    }

    if (codeIndiceToFunctionType.size() != codeSegments.size()) {
      throw new WasmConsistencyException(
          "Function and code section have inconsistent lengths. Function section had %d, and code"
              + " had %d.",
          codeIndiceToFunctionType.size(), codeSegments.size());
    }

    // Collates information about individual functions. Essentially turning SOA
    // to AOS.
    final CollatedFunction[] collatedFunctions = new CollatedFunction[codeSegments.size()];
    for (int codeIndex = 0; codeIndex < codeSegments.size(); codeIndex++) {
      final Uint31 functionId = importSectionLength.add(new Uint31(codeIndex));
      final Uint31 functionTypeIdx = codeIndiceToFunctionType.get(codeIndex);
      final CodeSegment codeSegment = codeSegments.get(codeIndex);
      final FunctionType type = typeSection.getEntryOrNull(functionTypeIdx);
      if (type == null) {
        throw new WasmConsistencyException(
            "Function %s references unknown type %s for module with only %s types",
            functionId, functionTypeIdx, typeSection.numEntries());
      }
      final String debugName = functionNamesByIndex.get(functionId);

      collatedFunctions[codeIndex] = new CollatedFunction(functionId, codeSegment, type, debugName);
    }

    return new FunctionSection(List.<CollatedFunction>of(collatedFunctions));
  }

  /** Will return {@link NameCustomSectionData} for name sections. Will otherwise return nothing. */
  private NameCustomSectionData readCustomSection(Uint31 sectionLength) {
    Uint31 sectionEndExclusive = stream.getPosition().add(sectionLength);
    String sectionName = stream.readStringBytes();
    if (sectionName.equals("name")) {
      return readNameCustomSectionSubsections(sectionEndExclusive);
    } else if (sectionEndExclusive.greaterThan(stream.getPosition())) {
      stream.skip(sectionEndExclusive.minus(stream.getPosition()));
    }
    return null;
  }

  /**
   * Parses a custom "name" section.
   *
   * @see <a href="https://webassembly.github.io/spec/core/appendix/custom.html#name-section">WASM
   *     Spec</a>
   */
  private NameCustomSectionData readNameCustomSectionSubsections(Uint31 sectionDataEndExclusive) {
    List<Naming> functionNames = List.of();

    while (stream.getPosition().lessThan(sectionDataEndExclusive)) {
      int subsectionId = stream.readUint8();
      final Uint31 subsectionLength = stream.readVarUint31OrNull();
      if (subsectionLength == null) {
        throw new WasmInterpreterLimitationException(
            "Name subsection 0x%02X too long.", subsectionId);
      }

      final Uint31 subsectionStart = stream.getPosition();
      if (subsectionId == 0x01) {
        // Function names subsection
        functionNames = readNameMap();
      } else {
        // Module name subsection
        // Local names subsection
        // Possibly an unknown subsection
        stream.skip(subsectionLength); // Currently unused
      }

      if (!stream.getPosition().equals(subsectionStart.add(subsectionLength))) {
        throw new WasmParseExceptionImpl(
            "Name subsection 0x%X size mismatch. Expected %s, but parsed %s bytes",
            subsectionId, subsectionLength, stream.getPosition().minus(subsectionStart));
      }
    }

    return new NameCustomSectionData(functionNames);
  }

  private static final class NameCustomSectionData {
    final List<Naming> functionNames;

    NameCustomSectionData(List<Naming> functionNames) {
      this.functionNames = List.copyOf(functionNames);
    }
  }

  private List<Naming> readNameMap() {
    return stream.parseVectorStream(Naming::read);
  }

  private void parseVersion() {
    if (!stream.hasBytes(Uint31.FOUR)) {
      throw new WasmParseExceptionImpl("%s", "Unexpected end when parsing binary version");
    }
    byte[] version = stream.readBytes(Uint31.FOUR);
    if (!Arrays.equals(version, WASM_SUPPORTED_VERSION)) {
      throw new WasmParseExceptionImpl("Unknown binary version: %s", Arrays.toString(version));
    }
  }

  private void parseMagic() {
    if (!stream.hasBytes(Uint31.FOUR)) {
      throw new WasmParseExceptionImpl("%s", "Unexpected end when parsing magic header");
    }
    byte[] header = stream.readBytes(Uint31.FOUR);
    if (!hasWasmHeader(header)) {
      throw new WasmParseExceptionImpl(
          "Magic header not detected: %x %x %x %x", header[0], header[1], header[2], header[3]);
    }
  }

  static boolean hasWasmHeader(byte[] data) {
    return data.length >= 4 && Arrays.equals(data, 0, 4, WASM_MAGIC_BYTES, 0, 4);
  }
}
