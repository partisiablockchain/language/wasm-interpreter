package com.partisiablockchain.language.wasm.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * Decoder for LEB128 variable-length integer encoding WASM specification
 * https://webassembly.github.io/spec/core/binary/values.html
 *
 * <p>LEB128 was originally defined in the DWARF Debugging Information Format.
 *
 * <p>See section 7.6 in https://dwarfstd.org/doc/dwarf-2.0.0.pdf
 */
public final class Leb128 {

  /** Class has only static methods, so it cannot be instantiated. */
  private Leb128() {}

  /**
   * Decode an unsigned LEB128 N-bit number from a stream of bytes.
   *
   * <p>From https://webassembly.github.io/spec/core/binary/values.html
   *
   * <p><br>
   * U(N) ::=
   *
   * <ul>
   *   <li>n:byte &#x21D2; n (if n&lt;2<sup>7</sup> ∧ n&lt;2^<sup>N</sup> )
   *   <li>n:byte m:U(N−7) &#x21D2; 2<sup>7</sup>*m+(n−2<sup>7</sup>) (if n&ge;2<sup>7</sup> ∧
   *       N&gt;7)
   * </ul>
   *
   * @param bitsN The number of bits the resulting number should have
   * @param bytes The byte stream to fetch bytes from
   * @return The unsigned N-bit value as a long.
   * @throws Leb128Exception if the number cannot be parsed
   */
  public static long unsigned(int bitsN, WasmByteStream bytes) {
    if (bitsN > 63) {
      throw new Leb128Exception("LEB128 unsigned decoder only supports 63 bits.");
    }
    int n = bytes.readUint8();
    if (n < 0x80) {
      if (bitsN <= 6 && n >= (1L << bitsN)) {
        throw new Leb128Exception(
            "Integer too large: Too many bits in LEB128 encoded unsigned number.");
      }
      return n;
    } else {
      if (bitsN <= 7) {
        throw new Leb128Exception(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");
      }
      long m = unsigned(bitsN - 7, bytes);
      return (m << 7) + n - 0x80L;
    }
  }

  /**
   * Decode a signed LEB128 N-bit number from a stream of bytes.
   *
   * <p>From https://webassembly.github.io/spec/core/binary/values.html
   *
   * <p><br>
   * S(N) ::=
   *
   * <ul>
   *   <li>n:byte &#x21D2; n (if n&lt;2<sup>6</sup> ∧ n&lt;2<sup>N−1</sup> )
   *   <li>n:byte &#x21D2; n−2<sup>7</sup> (if 2<sup>6</sup>&le;n&lt;2<sup>7</sup> ∧
   *       n&ge;2<sup>7</sup>−2<sup>N-1</sup> )
   *   <li>n:byte m:S(N−7) &#x21D2; 2<sup>7</sup>*m+(n−2<sup>7</sup>) (if n&ge;2<sup>7</sup> ∧
   *       N&gt;7)
   * </ul>
   *
   * @param bitsN The number of bits the resulting number should have
   * @param bytes The byte stream to fetch bytes from
   * @return The signed N-bit value as a long.
   * @throws Leb128Exception if the number cannot be parsed
   */
  public static long signed(int bitsN, WasmByteStream bytes) {
    if (bitsN > 64) {
      throw new Leb128Exception("LEB128 signed decoder only supports 64 bits.");
    }
    int n = bytes.readUint8();
    if (n < 0x40) {
      if (bitsN <= 6 && n >= (1L << (bitsN - 1))) {
        throw new Leb128Exception(
            "Integer too large: Too many bits in LEB128 encoded signed number.");
      }
      return n;
    } else if (n < 0x80) {
      if (n < (0x80 - (1L << (bitsN - 1)))) {
        throw new Leb128Exception(
            "Integer too large: Too many bits in LEB128 encoded signed number.");
      }
      return n - 0x80L;
    } else {
      if (bitsN <= 7) {
        throw new Leb128Exception(
            "Integer representation too long: Too many bytes in LEB128 encoded signed number.");
      }
      long m = signed(bitsN - 7, bytes);
      return (m << 7) + n - 0x80L;
    }
  }
}
