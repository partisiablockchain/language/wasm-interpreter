package com.partisiablockchain.language.wasm.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.FormatMethod;

/**
 * For when a low-level byte parsing operations fail, due to invalid WASM bytecode.
 *
 * @see com.partisiablockchain.language.wasm.parser.WasmParser
 */
public final class WasmIllegalBytecodeException extends WasmParseException {

  private static final long serialVersionUID = 1L;

  /**
   * Constructor for exception with given message, using {@link String#format} to format.
   *
   * @param message base message
   * @param args arguments for formatting
   */
  @FormatMethod
  public WasmIllegalBytecodeException(final String message, final Object... args) {
    super(message, args);
  }
}
