package com.partisiablockchain.language.wasm.interpreter.operator.i32;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.ArrayStack;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.operator.StackOperatorExecutor;

/** Executor for 32-bit binary stack instructions. */
public abstract class BinaryStackOperatorExecutorI32 implements StackOperatorExecutor {

  @Override
  public final void execute(WasmInstance instance) {
    final ArrayStack<Literal> stack = instance.stack();

    Int32 c2 = stack.pop().expectInt32();
    Int32 c1 = stack.pop().expectInt32();

    Literal result = execute(instance, c1, c2);
    stack.push(result);
  }

  abstract Literal execute(WasmInstance instance, Int32 lhs, Int32 rhs);
}
