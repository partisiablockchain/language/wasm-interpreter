package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.AddressKind;
import com.partisiablockchain.language.wasm.common.CodeSegment;
import com.partisiablockchain.language.wasm.common.DataSegment;
import com.partisiablockchain.language.wasm.common.ElementSegment;
import com.partisiablockchain.language.wasm.common.Export;
import com.partisiablockchain.language.wasm.common.FunctionType;
import com.partisiablockchain.language.wasm.common.Global;
import com.partisiablockchain.language.wasm.common.GlobalVariable;
import com.partisiablockchain.language.wasm.common.MemoryType;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.ResizableLimits;
import com.partisiablockchain.language.wasm.common.TableType;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.ValType;
import com.partisiablockchain.language.wasm.common.WasmFrameCyclesProfile;
import com.partisiablockchain.language.wasm.common.WasmLimitations;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.operator.OperatorExec;
import com.partisiablockchain.language.wasm.interpreter.operator.OperatorExecutor;
import com.partisiablockchain.language.wasm.parser.WasmInterpreterLimitationException;
import com.partisiablockchain.language.wasm.validator.FunctionValidator;
import com.partisiablockchain.language.wasm.validator.WasmValidationException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

/**
 * An executable instance of a WASM program.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#semantic-phases%E2%91%A0">WASM Spec
 *     terminology</a>
 */
public final class WasmInstance {

  /** The WASM module of the WASM program. */
  private final WasmModule module;

  /** The global variables of the WASM program. */
  private final Map<Uint31, GlobalVariable> globals = new HashMap<>();

  /**
   * Stack containing a call frame for each nested function call entered during execution of the
   * program.
   */
  private final ArrayStack<WasmFrame> frameStack = new ArrayStack<WasmFrame>();

  /**
   * WASM memory instances. A memory instance is the runtime representation of a linear memory. It
   * holds a vector of bytes.
   *
   * <p>In WASM 1.0 at most <i>one</i> memory is allowed in a module.
   */
  private final Map<Uint31, WasmMemory> memories = new HashMap<>();

  /**
   * WASM table instances. A table instance is the runtime representation of a table that holds a
   * vector of function elements. A function element is effectively a function pointer, which can be
   * used for indirect calls.
   *
   * <p>In WASM 1.0 at most <i>one</i> table is allowed in a module.
   */
  private final Map<Uint31, Map<Uint31, Uint31>> tables = new HashMap<>();

  /**
   * WASM executable functions by function index. The executable functions holds all information
   * needed to execute the function.
   */
  private final Map<Uint31, ExecutableFunction> functionsByIndex;

  /**
   * WASM executable functions conveniently mapped by function name. The name must be found from the
   * exports-section of the WASM module. Each function may have multiple names.
   */
  private final Map<String, ExecutableFunction> exportedFunctionsByName;

  /** WASM executable globals mapped by export name. */
  private final Map<String, Uint31> exportedGlobalsByName = new HashMap<>();

  /** Java-functions registered as external functions that can be called from the WASM program. */
  private final Map<String, Function<WasmInstance, List<Literal>>> externalFunctions =
      new HashMap<>();

  /**
   * The maximal amount of cycles the instance is allowed to use.
   *
   * <p>Invariant: Always greater than or equal to {@link #cyclesUsed}.
   */
  private long cyclesMax;

  /**
   * The current amount of cycles the instance has used.
   *
   * <p>Invariant: Always less than or equal to {@link #cyclesMax}.
   */
  private long cyclesUsed;

  /** The amount of cycles used by executing a single WASM instruction. */
  public static final Uint31 GAS_PER_INSTRUCTION = Uint31.ONE;

  /**
   * Profiling Information: Tracks whether cycles profiling is currently enabled. Cycles profiling
   * can be enabled and disabled as desired.
   */
  private boolean cyclesProfilingEnabled = false;

  /**
   * Profiling Information: Tracks total cycles used when a specific frame name is on the stack,
   * including cycles used it the frame itself, and any called functions.
   */
  private final Map<String, Long> cyclesCostPerFunctionNameClosure = new HashMap<>();

  /**
   * Profiling Information: Tracks cycles used within a specific frame, excluding any cycles used in
   * a called frames.
   */
  private final Map<String, Long> cyclesCostPerFunctionNameSelf = new HashMap<>();

  /**
   * Profiling Information: Tracks the lowest current stack index of frame names. This is used to
   * prevent double counting of total/closure cycles usage.
   */
  private final Map<String, Integer> lowestStackIndexForFunctionName = new HashMap<>();

  /**
   * Configuration options for individual wasm instances.
   *
   * @see WasmLimitations
   */
  private final WasmLimitations wasmLimitations;

  private WasmInstance(final WasmModule module, final WasmLimitations wasmLimitations) {
    this.module = module;
    this.wasmLimitations = wasmLimitations;

    // Initialize validator
    final FunctionValidator validator = FunctionValidator.forModule(module);

    // Link and initialize module
    initGlobals(validator);
    initMemory();
    initData(validator);
    initTables(validator);
    final InitFunctionsResult initFunctionsResult = initFunctions(validator);
    this.functionsByIndex = initFunctionsResult.functionsByIndex();
    this.exportedFunctionsByName = initFunctionsResult.exportedFunctionsByName();
  }

  /**
   * Construct a WASM instance from the given module and limitations.
   *
   * @param module the module
   * @param wasmLimitations Limitations to run the module with.
   * @return new WasmInstance
   */
  public static WasmInstance forModule(
      final WasmModule module, final WasmLimitations wasmLimitations) {
    return new WasmInstance(module, wasmLimitations);
  }

  /**
   * Construct a WASM instance from the given module and default limitations.
   *
   * @param module the module
   * @return new WasmInstance
   */
  public static WasmInstance forModule(final WasmModule module) {
    return new WasmInstance(module, WasmLimitations.DEFAULT);
  }

  private void initTables(final FunctionValidator validator) {
    // Validate
    for (final ElementSegment segment : module.getElementSection()) {
      validator.validateConstantInitializers(segment.getOffset(), ValType.I32);
    }

    // Initialize tables
    for (int i = 0; i < module.getTableSection().numEntries().asInt(); i++) {
      tables.put(new Uint31(i), new HashMap<>());
    }

    for (final ElementSegment segment : module.getElementSection()) {
      final Uint31 tableIndex = segment.getTableIndex();
      final Map<Uint31, Uint31> table = tables.get(tableIndex);
      final TableType tableType = module.getTableSection().getEntryOrNull(tableIndex);
      final List<Uint31> elements = segment.getInit();

      // Keep offsetLiteral around to use in error message
      final Literal offsetLiteral = executeInitializer(segment.getOffset());

      // NOTE: Due to validation, offsetLiteral is guarenteed to be Int32.
      Uint31 offset = ((Int32) offsetLiteral).toUint31OrNull();

      // Check that all elements can be placed in table
      if (offset == null
          || offset
              .add(new Uint31(elements.size()))
              .greaterThan(tableType.getLimits().getMinimum())) {
        throw new WasmLinkingException(
            "Elements segment does not fit into table: Offset %s, length %s, table size %s",
            offsetLiteral, elements.size(), tableType.getLimits().getMinimum());
      }

      for (final Uint31 funcIndex : elements) {
        table.put(offset, funcIndex);
        offset = offset.addOne();
      }
    }
  }

  private void initData(final FunctionValidator validator) {
    // Validate
    for (final DataSegment segment : module.getDataSection()) {
      validator.validateConstantInitializers(segment.getOffset(), ValType.I32);
    }

    // Initialize
    for (final DataSegment segment : module.getDataSection()) {
      final WasmMemory memory = memories.get(segment.getMemoryIndex());
      final byte[] data = segment.getInit();

      // Keep addressLiteral around to use in error message
      final Literal addressLiteral = executeInitializer(segment.getOffset());

      // NOTE: Due to validation, addressLiteral is guarenteed to be Int32.
      final Uint31 address = ((Int32) addressLiteral).toUint31OrNull();

      // Check that all data can be placed in memory
      if (address == null || !memory.isWithinBounds(address, new Uint31(data.length))) {
        throw new WasmLinkingException(
            "Data segment does not fit into memory: Address %s, length %s, memory byte size %s",
            addressLiteral, data.length, memory.size());
      }

      memory.write(address, data);
    }
  }

  private void initMemory() {
    Uint31 memoryId = Uint31.ZERO;
    for (final MemoryType memoryType : module.getMemorySection()) {
      ResizableLimits limits = memoryType.getLimits();
      WasmMemory memory =
          new WasmMemory(
              limits.getMinimum(), limits.getMaximum().min(wasmLimitations.maxMemoryPages()));
      memories.put(memoryId, memory);
      memoryId = memoryId.addOne();
    }
  }

  private void initGlobals(final FunctionValidator validator) {
    // Validate
    for (final Global global : module.getGlobalSection()) {
      validator.validateConstantInitializers(
          global.getInitializer(), global.getType().getContentType());
    }

    // Initialize
    Uint31 globalId = Uint31.ZERO;
    for (final Global global : module.getGlobalSection()) {
      GlobalVariable variable =
          new GlobalVariable(global.getType(), executeInitializer(global.getInitializer()));
      globals.put(globalId, variable);
      globalId = globalId.addOne();
    }

    // Initiate lookup
    // NOTE: Due to linking checks assume all names are unique, and globals exists.
    for (final Export export : module.getExportSection()) {
      if (export.getKind().equals(AddressKind.GLOBAL)) {
        exportedGlobalsByName.put(export.getName(), export.getIndex());
      }
    }
  }

  private record InitFunctionsResult(
      Map<Uint31, ExecutableFunction> functionsByIndex,
      Map<String, ExecutableFunction> exportedFunctionsByName) {
    public InitFunctionsResult {
      Objects.requireNonNull(functionsByIndex);
      Objects.requireNonNull(exportedFunctionsByName);
    }
  }

  private InitFunctionsResult initFunctions(final FunctionValidator validator) {
    // Validate type section
    validator.validateTypeSection();

    // Validate all functions
    final var functionsByIndex = new HashMap<>(module.functionsByIndex());

    for (final Uint31 functionIndex : module.functionsByIndex().keySet()) {
      final ExecutableFunction function = functionsByIndex.get(functionIndex);
      ExecutableFunction newFunction = null;
      if (function instanceof FunctionHosted hostedFunction) {
        final String moduleName = hostedFunction.importDeclaration().getModuleName();
        if (!moduleName.equals("pbc")) {
          newFunction =
              new FunctionInvalid(
                  function.index(),
                  function.name(),
                  function.type(),
                  new WasmInterpreterLimitationException(
                      "Cannot import from module \"%s\": Arbitrarily imported modules are not"
                          + " supported.",
                      moduleName));
        }
      } else {
        final var callableFunction =
            (FunctionCallable) function; // Assumption by invariant on WasmModule
        final CodeSegment codeSegment = callableFunction.code();

        // Check for parsing problems
        if (codeSegment.getParseException() != null) {
          newFunction =
              new FunctionInvalid(
                  function.index(),
                  function.name(),
                  function.type(),
                  codeSegment.getParseException());

          // Check for validation problems
        } else {
          try {
            validator.validateFunction(codeSegment, function.type());
          } catch (final WasmValidationException validationException) {
            newFunction =
                new FunctionInvalid(
                    function.index(), function.name(), function.type(), validationException);
          }
        }
      }

      if (wasmLimitations.eagerValidation() && newFunction instanceof FunctionInvalid invalid) {
        throw invalid.exception();
      } else if (newFunction != null) {
        functionsByIndex.put(functionIndex, newFunction);
      }
    }

    // Initialize exported functions map
    return new InitFunctionsResult(
        Map.copyOf(functionsByIndex), module.generateFunctionsByName(functionsByIndex));
  }

  static Literal executeInitializer(final List<Instruction> initializer) {
    final Instruction op = initializer.get(0);
    if (op.getOpCode() == OpCode.I_32_CONST) {
      return ((InstructionI32) op).getArgument();
    } else if (op.getOpCode() == OpCode.I_64_CONST) {
      return ((InstructionI64) op).getArgument();
    }

    // Instruction is validated, but not supported due to weird interpreter limitations
    throw new WasmInterpreterLimitationException(
        "OpCode %s not supported in initializer", op.getOpCode());
  }

  /**
   * Retrieve stack of the currently executing frame.
   *
   * @return retrieved stack.
   */
  public ArrayStack<Literal> stack() {
    return frame().stack();
  }

  void setCyclesMax(final long cyclesMax) {
    this.cyclesMax = cyclesMax;
  }

  /**
   * Execute the current frame.
   *
   * @param maxCycles the maximum amount of cycles allowed
   * @throws TrapException if given maxCycles did not cover computation
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  private void run(long maxCycles) {
    this.cyclesUsed = 0;
    setCyclesMax(maxCycles);
    Instruction operator;
    while ((operator = frame().getNextInstruction()) != null) {
      frame().incrementProgramCounter();

      OperatorExecutor executor = OperatorExec.get(operator.getOpCode());
      useCycles(operator.getOpCode().getCyclesCost());
      try {
        executor.execute(this, operator);
      } catch (final TrapException throwable) {
        throwable.setWasmStackTrace(createStackTrace());
        throw throwable;
      } catch (final Exception exception) {
        // Wrap Exceptions in TrapException - but let Errors (i.e. OutOfMemoryError) pass through
        final TrapException trap = TrapException.withCause(exception, "%s", exception.getMessage());
        trap.setWasmStackTrace(createStackTrace());
        throw trap;
      }
    }
  }

  /**
   * Run a WASM function in this instance.
   *
   * @param functionName the function name
   * @param maxCycles the maximum amount of cycles to be used
   * @param arguments the arguments to the function
   * @return the result of the function
   * @throws TrapException if given maxCycles did not cover computation
   */
  public List<Literal> runFunction(String functionName, long maxCycles, List<Literal> arguments) {
    final ExecutableFunction function = exportedFunctionsByName.get(functionName);
    if (function == null) {
      throw new TrapException("Cannot find function: %s", functionName);
    }

    // Initialize call shim
    final Uint31 functionIndex = function.index();
    newFrame(
        Uint31.ZERO,
        "call_" + functionName,
        Uint31.ZERO,
        List.of(new InstructionU31(OpCode.CALL, functionIndex)));

    // Literals must be pushed to stack after frame have been initialized,
    // otherwise the frame will not have access to them.
    for (Literal argument : arguments) {
      stack().push(argument);
    }

    // Execute current function until completion
    run(maxCycles);

    // Determine results
    final ArrayStack<Literal> stack = stack();
    final List<Literal> results = new ArrayList<>();
    while (stack.isNonEmpty()) {
      results.add(stack.pop());
    }

    // Revert stack so instance can be reused.
    popFrame();

    return results;
  }

  /**
   * Run a host function in this instance. Assumes that a new frame have been created for the host
   * function context, and that arguments have been placed in appropriate locals registers.
   *
   * @param functionName the function name
   * @return the result of the function
   */
  public List<Literal> runHostFunction(String functionName) {
    Function<WasmInstance, List<Literal>> fn = externalFunctions.get(functionName);
    if (fn == null) {
      throw new TrapException("Cannot find external function: %s", functionName);
    }
    return fn.apply(this);
  }

  /**
   * Binds a given Java function to the "pbc" import namespace.
   *
   * @param functionName name that function should be imported by
   * @param fn Java function to run when calling
   */
  public void registerExternal(String functionName, Function<WasmInstance, List<Literal>> fn) {
    this.externalFunctions.put(functionName, fn);
  }

  /**
   * Gets local with index from the top stack frame.
   *
   * @param index index of local
   * @return literal value of local
   */
  public Literal getLocal(Uint31 index) {
    return frameStack.peek().getLocal(index);
  }

  /**
   * Updates local with index at the top stack frame.
   *
   * @param index index of local
   * @param value new value
   */
  public void setLocal(Uint31 index, Literal value) {
    frameStack.peek().setLocal(index, value);
  }

  /**
   * Finds function with given index.
   *
   * @param funcIndex index of local
   * @return definition of the wanted function
   */
  public ExecutableFunction lookupFunction(Uint31 funcIndex) {
    return functionsByIndex.get(funcIndex);
  }

  /**
   * Get all the exported functions in the instance.
   *
   * @return the exported functions
   */
  public List<ExecutableFunction> getExportedFunctions() {
    return new ArrayList<>(exportedFunctionsByName.values());
  }

  /**
   * Finds type definition with the given index.
   *
   * @param index index of type
   * @return wanted type or null
   */
  public FunctionType getTypeEntryOrNull(final Uint31 index) {
    return module.getTypeSection().getEntryOrNull(index);
  }

  /**
   * Push a new frame on the stack.
   *
   * @param localCount the number of locals
   * @param name the name of the frame
   * @param arity the arity of the frame
   * @param instructions the instructions for the frame
   * @return the pushed frame
   */
  public WasmFrame newFrame(
      Uint31 localCount, String name, Uint31 arity, List<Instruction> instructions) {
    final WasmFrame frame = new WasmFrame(localCount, name, this.getCyclesUsed());
    frame.pushOperandStack(new ArrayStack<Literal>(List.of()));
    frame.pushRootLabel(arity, instructions);

    if (frameStack.size() >= wasmLimitations.maxStackFrameDepth()) {
      throw new TrapException("call stack exhausted");
    }
    frameStack.push(frame);

    if (cyclesProfilingEnabled) {
      lowestStackIndexForFunctionName.computeIfAbsent(
          frame.getFrameName(), k -> frameStack.size() - 1);
    }

    return frame;
  }

  /**
   * Gets current stack frame.
   *
   * @return current stack frame
   */
  public WasmFrame frame() {
    return frameStack.peek();
  }

  /** Removes current stack frame, and replaces it with the previous. */
  public void popFrame() {
    final WasmFrame prevFrame = frameStack.pop();

    if (this.cyclesProfilingEnabled) {
      final long closureCyclesDifference =
          getCyclesUsed() - prevFrame.getCyclesAlreadyUsedWhenFrameCreated();
      final long specificCyclesDifference =
          closureCyclesDifference - prevFrame.getCalleeCyclesUse();

      if (lowestStackIndexForFunctionName.get(prevFrame.getFrameName()) == frameStack.size()) {
        cyclesCostPerFunctionNameClosure.compute(
            prevFrame.getFrameName(), (k, v) -> closureCyclesDifference + (v == null ? 0 : v));
        lowestStackIndexForFunctionName.remove(prevFrame.getFrameName());
      }
      cyclesCostPerFunctionNameSelf.compute(
          prevFrame.getFrameName(), (k, v) -> specificCyclesDifference + (v == null ? 0 : v));

      final WasmFrame parentFrame = frameStack.peek(0);
      if (parentFrame != null) {
        parentFrame.incCalleeCyclesUse(closureCyclesDifference);
      }
    }
  }

  /**
   * Enables cycles profiling. This is an expensive function, as it will touch the entire call stack
   * in order to update profiling information.
   */
  public void enableCyclesProfiling() {
    if (this.cyclesProfilingEnabled) {
      return;
    }
    this.cyclesProfilingEnabled = true;

    // Reset all frames on the stack
    // Also recompute lowestStackIndexForFunctionName
    for (int frameIdx = 0; frameIdx < frameStack.size(); frameIdx++) {
      final WasmFrame frame = frameStack.peek(frameIdx);

      // Reset cycles epoch for each frame
      frame.resetCyclesEpoch(this.getCyclesUsed());
    }

    // Recompute lowestStackIndexForFunctionName
    lowestStackIndexForFunctionName.clear();
    for (int frameAbsIdx = 0; frameAbsIdx < frameStack.size(); frameAbsIdx++) {
      final int relativeIdx = frameStack.size() - 1 - frameAbsIdx;
      final WasmFrame frame = frameStack.peek(relativeIdx);
      final int frameAbsIdx2 = frameAbsIdx;
      lowestStackIndexForFunctionName.computeIfAbsent(frame.getFrameName(), k -> frameAbsIdx2);
    }
  }

  /**
   * Disable cycles profiling. This is an expensive function, as it will touch the entire call stack
   * in order to update profiling statistics.
   */
  public void disableCyclesProfiling() {
    if (!this.cyclesProfilingEnabled) {
      return;
    }
    this.cyclesProfilingEnabled = false;

    // Mark cycles usage for functions on the stack.
    // Copy the tracked and append frames on the stack
    long cyclesOnChildFrameStart = getCyclesUsed();
    for (int frameIdx = 0; frameIdx < frameStack.size(); frameIdx++) {
      final WasmFrame frame = frameStack.peek(frameIdx);
      final long cyclesOnStartOfFrame = frame.getCyclesAlreadyUsedWhenFrameCreated();
      final long cyclesDifferenceClosure = getCyclesUsed() - cyclesOnStartOfFrame;
      final long cyclesDifferenceSelf =
          cyclesOnChildFrameStart - cyclesOnStartOfFrame - frame.getCalleeCyclesUse();
      cyclesCostPerFunctionNameClosure.compute(
          frame.getFrameName(), (k, v) -> cyclesDifferenceClosure + (v == null ? 0 : v));
      cyclesCostPerFunctionNameSelf.compute(
          frame.getFrameName(), (k, v) -> cyclesDifferenceSelf + (v == null ? 0 : v));
      cyclesOnChildFrameStart = cyclesOnStartOfFrame;
    }
  }

  /** Resets cycles profiling statistics. Will also automatically disable cycles profiling. */
  public void resetCyclesProfiling() {
    disableCyclesProfiling();
    cyclesCostPerFunctionNameSelf.clear();
    cyclesCostPerFunctionNameClosure.clear();
  }

  /**
   * Produces a string report of the cycles usage. Sort order is undefined.
   *
   * <p>Note on output: {#link WasmFrameCyclesProfile#cyclesUsedSpecific} is how much cycles was
   * used by the frame itself, while it was on the top of the stack; {#link
   * WasmFrameCyclesProfile#cyclesUsedClosure} is how much was used by the frame and any called
   * frame, e.g. while the frame was anywhere on the stack.
   *
   * <p>This is potentially extra expensive to call when profiling is enabled, as profiling will be
   * toggle it off and on again, in order to get accurate information for current stack frames.
   *
   * @return report containing an entry for each frame stack that existed during the period(s) when
   *     cycles profiling was enabled.
   */
  public List<WasmFrameCyclesProfile> getCyclesProfilingReport() {

    if (cyclesProfilingEnabled) {
      disableCyclesProfiling();
      enableCyclesProfiling();
    }

    final List<WasmFrameCyclesProfile> report = new ArrayList<>();
    for (final String fnName : cyclesCostPerFunctionNameClosure.keySet()) {
      report.add(
          new WasmFrameCyclesProfile(
              fnName,
              cyclesCostPerFunctionNameSelf.getOrDefault(fnName, 0L),
              cyclesCostPerFunctionNameClosure.getOrDefault(fnName, 0L)));
    }

    return report;
  }

  /**
   * Gets global with given index.
   *
   * @param index index of wanted global
   * @return definition of wanted global
   */
  public GlobalVariable getGlobal(Uint31 index) {
    return Objects.requireNonNull(globals.get(index));
  }

  /**
   * Gets global with given name. Mainly for testing suite.
   *
   * @param globalName name of wanted global
   * @return value of wanted global. Null if global was undefined.
   */
  public Literal getGlobalValue(final String globalName) {
    final Uint31 globalIndex = exportedGlobalsByName.getOrDefault(globalName, null);
    if (globalIndex == null) {
      return null;
    }
    return globals.get(globalIndex).getValue();
  }

  /**
   * Create a stack trace for the instance.
   *
   * @return the stack trace
   */
  public List<String> createStackTrace() {
    List<String> functionNames = new ArrayList<>();
    while (frameStack.isNonEmpty()) {
      WasmFrame frame = frameStack.pop();
      String frameName = frame.getFrameName();
      Uint31 pc = frame.calculateProgramCounterForStackTrace();
      functionNames.add("  at " + frameName + "():" + pc);
    }
    return functionNames;
  }

  /**
   * Gets memory with index.
   *
   * @param index index of wanted memory
   * @return wanted memory
   */
  public WasmMemory getMemory(Uint31 index) {
    return memories.get(index);
  }

  /**
   * Gets table with index.
   *
   * @param index index of wanted table
   * @return wanted table
   */
  public Map<Uint31, Uint31> getTable(Uint31 index) {
    return tables.get(index);
  }

  /**
   * Gets table with index.
   *
   * @param index index of wanted table
   * @return wanted table or null
   */
  public TableType getTableTypeOrNull(final Uint31 index) {
    return module.getTableSection().getEntryOrNull(index);
  }

  /**
   * Gets the amount of cycles used so far.
   *
   * @return amount of cycles
   */
  public long getCyclesUsed() {
    return cyclesUsed;
  }

  /**
   * Use an amount of cycles. If we run out of cycles then throw {@link OutOfCyclesException}
   *
   * @param additionalCyclesUsed amount of cycles to use.
   * @throws OutOfCyclesException if the amount of cycles used is larger than {@link #cyclesMax}
   */
  public void useCycles(final Uint31 additionalCyclesUsed) {
    useCycles(additionalCyclesUsed, Uint31.ONE);
  }

  /**
   * Use some cycles. If we run out of cycles then throw {@link OutOfCyclesException}. This
   * multiparam variant will safely multiply the arguments, without risk of overflow.
   *
   * @param value units to consume.
   * @param multiplier how much cycles each unit costs to use.
   * @throws OutOfCyclesException if the amount of cycles used is larger than {@link #cyclesMax}
   */
  public void useCycles(final Uint31 value, final Uint31 multiplier) {
    // Note that additionalCyclesConsumed: 0 <= additionalCyclesConsumed <= (2^31-1)^2
    // Standard overflow check. Assumes invariant of: cyclesUsed <= cyclesMax
    long additionalCyclesConsumed = ((long) value.asInt()) * ((long) multiplier.asInt());
    if (cyclesMax - cyclesUsed < additionalCyclesConsumed) {
      throw new OutOfCyclesException(cyclesUsed, additionalCyclesConsumed, cyclesMax);
    }
    cyclesUsed += additionalCyclesConsumed;
  }
}
