package com.partisiablockchain.language.wasm.interpreter.operator.i32;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import java.util.function.BiPredicate;

/** Executor for 32-bit comparison instructions. */
public final class BinaryComparisonOperatorExecutorI32 extends BinaryStackOperatorExecutorI32 {

  private final BiPredicate<Int32, Int32> calcFunction;

  /**
   * Constructor for instruction with a given comparison function.
   *
   * @param calcFunction comparison that should be performed by instruction
   */
  public BinaryComparisonOperatorExecutorI32(BiPredicate<Int32, Int32> calcFunction) {
    this.calcFunction = calcFunction;
  }

  @Override
  Literal execute(WasmInstance instance, Int32 lhs, Int32 rhs) {
    boolean test = calcFunction.test(lhs, rhs);
    return Int32.fromBoolean(test);
  }
}
