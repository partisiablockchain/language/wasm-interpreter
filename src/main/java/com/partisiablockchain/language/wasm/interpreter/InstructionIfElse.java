package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.BlockResultType;
import com.partisiablockchain.language.wasm.common.OpCode;
import java.util.List;

/**
 * An If-Else instruction.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#numeric-instructions%E2%91%A0">WASM Spec</a>
 */
public final class InstructionIfElse extends Instruction {

  /** Type of the result value that execution of the block produces. */
  private final BlockResultType blockResultType;

  /** The instructions that are run if condition evaluates to true. */
  private final List<Instruction> trueCase;

  /** The instructions that are run if condition evaluates to false. */
  private final List<Instruction> falseCase;

  /**
   * Creates an if-else instruction.
   *
   * @param blockResultType The value (if any) to remain on the stack after evaluating block
   * @param trueCase Instructions to run if condition evaluates to true
   * @param falseCase Instructions to run if condition evaluates to false
   */
  public InstructionIfElse(
      final BlockResultType blockResultType,
      final List<Instruction> trueCase,
      final List<Instruction> falseCase) {
    super(OpCode.IF);
    this.blockResultType = blockResultType;
    this.trueCase = trueCase;
    this.falseCase = falseCase;
  }

  /**
   * If-clause of instruction, the instructions that are run if condition evaluates to true.
   *
   * @return instructions of if-clause
   */
  public List<Instruction> getIfClause() {
    return trueCase;
  }

  /**
   * Else-clause of instruction, the instructions that are run if condition evaluates to false.
   *
   * @return instructions of else-clause
   */
  public List<Instruction> getElseClause() {
    return falseCase;
  }

  /**
   * Block result type of the instruction.
   *
   * @return block result type of instruction
   */
  public BlockResultType getBlockResultType() {
    return blockResultType;
  }

  @Override
  public String toString() {
    return String.format(
        "%s %s (if = %d, else = %d)",
        getOpCode(), blockResultType, trueCase.size(), getElseClause().size());
  }
}
