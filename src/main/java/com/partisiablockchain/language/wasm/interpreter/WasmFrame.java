package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.BlockResultType;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

/** A WASM call frame representing a call to a specific function at run-time. */
public final class WasmFrame {

  /** Function parameters and the functions declared local variables. */
  private final Literal[] locals;

  /**
   * Stack containing the instructions for each nested block entered during execution of the
   * function.
   */
  private final Deque<List<Instruction>> instructionStack = new ArrayDeque<>();

  /**
   * Stack containing a label for each nested block entered during execution of the function. The
   * label keeps track of the state of the block being executed.
   */
  private final ArrayStack<Label> labelStack = new ArrayStack<Label>(List.of());

  /**
   * Stack containing values pushed/pulled during execution for each nested block entered during
   * execution of the function.
   */
  private final Deque<ArrayStack<Literal>> operandStacks = new ArrayDeque<>();

  /**
   * The index of the next instruction to execute in the current instruction block. The current
   * instruction block is the blockat the top of the {@link #instructionStack}.
   */
  private Uint31 programCounter;

  /**
   * A name for the current frame on the form <code>#<i>nn</i> <i>name</i>()</code> where
   * <i>name</i> is the function name (or "null" if it is unnamed) and <i>nn</i> is the depth of the
   * frame on the frame stack.
   *
   * <p>This syntax matches the syntax used in GDB when printing stack frames.
   */
  private String frameName;

  /**
   * Profiling information; tracks how many cycles were already consumed when entering the stack
   * frame.
   */
  private long cyclesAlreadyUsedWhenCreated;

  /**
   * Profiling information; tracks how much cycles is being consumed by the child frames, and can
   * thus be used to determine how much have been used by the frame itself.
   */
  private long calleeCyclesUse = 0;

  /**
   * Constructor for a wasm frame.
   *
   * @param locals the number of locals for this frame.
   * @param frameName name of the stack frame.
   * @param cyclesAlreadyUsedWhenCreated cycles profiling data used for tracking "when" (in cycles
   *     usage) the frame was created.
   */
  public WasmFrame(
      final Uint31 locals, final String frameName, final long cyclesAlreadyUsedWhenCreated) {
    this.locals = new Literal[locals.asInt()];
    this.frameName = frameName;
    this.cyclesAlreadyUsedWhenCreated = cyclesAlreadyUsedWhenCreated;
  }

  /**
   * Retrieves a local value in the stack frame.
   *
   * @param index index of wanted local
   * @return value of wanted local
   */
  public Literal getLocal(Uint31 index) {
    return locals[index.asInt()];
  }

  long getCyclesAlreadyUsedWhenFrameCreated() {
    return this.cyclesAlreadyUsedWhenCreated;
  }

  /**
   * Sets cyclesAlreadyUsedWhenCreated. Note that calleeCyclesUse is reset, as it is useless at that
   * point.
   */
  void resetCyclesEpoch(final long cyclesAlreadyUsedWhenCreated) {
    this.cyclesAlreadyUsedWhenCreated = cyclesAlreadyUsedWhenCreated;
    this.calleeCyclesUse = 0;
  }

  void incCalleeCyclesUse(final long calleeCyclesUse) {
    this.calleeCyclesUse += calleeCyclesUse;
  }

  long getCalleeCyclesUse() {
    return this.calleeCyclesUse;
  }

  /**
   * Updates value of local in the stack frame.
   *
   * @param index index of the local
   * @param value new value of local
   */
  public void setLocal(Uint31 index, Literal value) {
    locals[index.asInt()] = value;
  }

  private List<Instruction> getCurrentInstructions() {
    return instructionStack.peek();
  }

  /**
   * Push a label onto the label stack.
   *
   * @param arity the arity of the label
   * @param type the block type
   * @param loop whether or not it is a loop
   * @param instructions the instructions for the label
   */
  public void pushLabel(
      Uint31 arity, BlockResultType type, boolean loop, List<Instruction> instructions) {
    labelStack.push(new Label(arity, programCounter, type, loop));
    instructionStack.push(instructions);
    operandStacks.push(new ArrayStack<Literal>(List.of()));
    resetPc();
  }

  /**
   * Push the root label for the frame.
   *
   * @param arity the arity of the frame
   * @param instructions the instructions of the stackframe
   */
  public void pushRootLabel(Uint31 arity, List<Instruction> instructions) {
    labelStack.push(new Label(arity, programCounter, BlockResultType.EMPTY, false));
    instructionStack.push(instructions);
    resetPc();
  }

  /** Resets program counter to zero. */
  public void resetPc() {
    setPc(Uint31.ZERO);
  }

  /** Sets program counter to end of current recursive instruction. */
  public void goToEnd() {
    setPc(new Uint31(instructionStack.peek().size() - 1));
  }

  /**
   * Updates program counter to the given value.
   *
   * @param programCounter new program counter value
   */
  public void setPc(Uint31 programCounter) {
    this.programCounter = programCounter;
  }

  /**
   * Pop the top label from the stack and return the label's operand stack.
   *
   * @return the operand stack for the label from the top of the stack
   */
  public ArrayStack<Literal> popLabel() {
    Label label = labelStack.pop();
    programCounter = label.getPreviousPc();
    instructionStack.pop();
    return operandStacks.pop();
  }

  /**
   * Get the next instruction to execute.
   *
   * @return the next instruction or null when EOF
   */
  public Instruction getNextInstruction() {
    if (getCurrentInstructions().size() == programCounter.asInt()) {
      return null;
    }
    return getCurrentInstructions().get(programCounter.asInt());
  }

  /** Increments program counter to point to the next instruction. */
  public void incrementProgramCounter() {
    programCounter = programCounter.addOne();
  }

  /**
   * Check that frame has label.
   *
   * @return true if and only if the frame currently possess a label
   */
  public boolean hasLabel() {
    return labelStack.size() > 1;
  }

  /**
   * The current operand stack.
   *
   * @return the current operand stack
   */
  public ArrayStack<Literal> stack() {
    return operandStacks.peek();
  }

  /**
   * Adds a new operand stack.
   *
   * @param stack the new operand stack
   */
  public void pushOperandStack(ArrayStack<Literal> stack) {
    operandStacks.push(stack);
  }

  String getFrameName() {
    return frameName;
  }

  /**
   * Calculate the program counter for a stacktrace.
   *
   * @return the calculated program counter
   */
  public Uint31 calculateProgramCounterForStackTrace() {
    Uint31 counter = programCounter;
    while (hasLabel()) {
      Label frame = labelStack.pop();
      counter = counter.add(frame.getPreviousPc());
    }
    return counter;
  }

  /**
   * Get label for the given relative index.
   *
   * @param labelIndex wanted relative index
   * @return wanted label
   */
  public Label getLabel(Uint31 labelIndex) {
    return labelStack.peek(labelIndex.asInt());
  }

  /**
   * Gets arity of the current stack.
   *
   * @return arity of current stack
   */
  public Uint31 getReturnArity() {
    return labelStack.peek(labelStack.size() - 1).getEndArity();
  }
}
