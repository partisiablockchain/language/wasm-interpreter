package com.partisiablockchain.language.wasm.interpreter.operator.i32;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.operator.IntegerLoad;

/** Executor for 32-bit load instructions. */
public final class LoadI32 extends IntegerLoad {

  /**
   * Constructor for instruction loading a given number of bits with a given sign.
   *
   * @param bits number of bits to load
   * @param signed whether bits should be sign-extended or zero-extended
   */
  public LoadI32(int bits, boolean signed) {
    super(bits, signed);
  }

  @Override
  protected Literal convertRaw(byte[] rawBytes, int bits, boolean signed) {
    return new Int32(fromBytes(rawBytes, bits, signed));
  }

  /**
   * Parses integer from bytes.
   *
   * @param rawBytes bytes to parse from.
   * @param bits Number of bits to parse.
   * @param signed Whether the parsed integer should be signed or not.
   * @return parsed integer
   */
  public static int fromBytes(byte[] rawBytes, int bits, boolean signed) {
    if (bits == 8) {
      if (signed) {
        return rawBytes[0];
      } else {
        return rawBytes[0] & 0xFF;
      }
    } else if (bits == 16) {
      int intValue = ((rawBytes[1] & 0xFF) << 8) | (rawBytes[0] & 0xFF);
      short value = (short) intValue;
      if (signed) {
        return value;
      } else {
        return value & 0xFFFF;
      }
    } else if (bits == 32) {
      return Int32.fromRawParts(rawBytes).value();
    } else {
      throw new TrapException("Unknown bit size: %d", bits);
    }
  }
}
