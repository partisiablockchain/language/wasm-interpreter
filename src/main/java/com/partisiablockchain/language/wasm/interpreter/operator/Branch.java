package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import com.partisiablockchain.language.wasm.interpreter.Label;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import java.util.List;

/** Branch operator. */
public final class Branch implements OperatorExecutor<InstructionU31> {

  @Override
  public void execute(WasmInstance instance, InstructionU31 instruction) {
    Uint31 labelIndex = instruction.getArgument();
    Label label = instance.frame().getLabel(labelIndex);

    final List<Literal> valn = instance.stack().popN(label.getJumpArity().asInt());
    final int count = labelIndex.asInt();
    for (int k = 0; k < count; k++) {
      instance.frame().popLabel();
    }
    if (label.isLoop()) {
      instance.frame().resetPc();
    } else {
      if (instance.frame().hasLabel()) {
        instance.frame().popLabel();
      } else {
        instance.frame().goToEnd();
      }
    }

    instance.stack().pushReversed(valn);
  }
}
