package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.BlockResultType;
import com.partisiablockchain.language.wasm.common.Uint31;

/** Representing a WASM label. */
public final class Label {

  private final Uint31 arity;
  private final Uint31 previousPc;
  private final BlockResultType blockResultType;
  private final boolean loop;

  /**
   * Construct a new label.
   *
   * @param arity the arity of the label
   * @param previousPc the program counter at which to continue
   * @param blockResultType the block type
   * @param loop whether or not it is a loop
   */
  public Label(Uint31 arity, Uint31 previousPc, BlockResultType blockResultType, boolean loop) {
    this.arity = arity;
    this.previousPc = previousPc;
    this.blockResultType = blockResultType;
    this.loop = loop;
  }

  /**
   * Program counter at which to continue.
   *
   * @return the program counter
   */
  public Uint31 getPreviousPc() {
    return previousPc;
  }

  /**
   * Which {@link BlockResultType} this Label uses.
   *
   * @return the block type
   */
  public BlockResultType getBlockResultType() {
    return blockResultType;
  }

  /**
   * Get the arity for the label when exiting a block.
   *
   * @return the arity for the label
   */
  public Uint31 getEndArity() {
    if (arity != null) {
      return arity;
    }
    return getBlockResultType().getArity();
  }

  /**
   * Get the arity of the label when jumping to the label.
   *
   * <p>The only case where this is different from the end arity is for {@link
   * com.partisiablockchain.language.wasm.common.OpCode#LOOP} instructions, where the jump arity is
   * always zero.
   *
   * @return the arity for the label
   */
  public Uint31 getJumpArity() {
    if (isLoop()) {
      return Uint31.ZERO;
    }
    return getEndArity();
  }

  /**
   * Whether the Label is a loop or not.
   *
   * @return true if a loop, false if not
   */
  public boolean isLoop() {
    return loop;
  }
}
