package com.partisiablockchain.language.wasm.interpreter.operator.i64;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import java.util.function.BiFunction;

/** Executor for 64-bit binary instructions. */
public final class BinaryCalcOperatorExecutorI64 extends BinaryStackOperatorExecutorI64 {

  private final BiFunction<Int64, Int64, Int64> calcFunction;

  /**
   * Constructor for instruction with a given computation function.
   *
   * @param calcFunction computation that should be performed by instruction
   */
  public BinaryCalcOperatorExecutorI64(BiFunction<Int64, Int64, Int64> calcFunction) {
    this.calcFunction = calcFunction;
  }

  @Override
  Literal execute(WasmInstance instance, Int64 lhs, Int64 rhs) {
    return calcFunction.apply(lhs, rhs);
  }
}
