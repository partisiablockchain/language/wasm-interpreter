package com.partisiablockchain.language.wasm.interpreter.operator.i32;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import java.util.function.Function;

/** General executor for instructions casting from 32-bit to 64-bits. */
public final class UpcastI32ToI64 extends UnaryStackOperatorExecutorI32 {

  private final Function<Int32, Int64> function;

  /**
   * Constructor for instruction with a given computation function.
   *
   * @param function computation that should be performed by instruction
   */
  public UpcastI32ToI64(Function<Int32, Int64> function) {
    this.function = function;
  }

  @Override
  public Literal execute(WasmInstance instance, Int32 value) {
    return function.apply(value);
  }
}
