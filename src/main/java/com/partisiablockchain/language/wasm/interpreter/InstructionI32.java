package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.literal.Int32;

/**
 * Supertype of instructions on 32-bit integers.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#numeric-instructions%E2%91%A0">WASM Spec</a>
 */
public final class InstructionI32 extends InstructionUnaryArg<Int32> {

  /**
   * Constructor for instruction with the given opcode and constant argument.
   *
   * @param opCode instruction opcode
   * @param argument constant argument
   */
  public InstructionI32(OpCode opCode, Int32 argument) {
    super(opCode, argument);
  }
}
