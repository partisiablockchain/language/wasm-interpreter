package com.partisiablockchain.language.wasm.interpreter.operator.i64;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.interpreter.operator.IntegerStore;

/** Executor for 64-bit store instructions. */
public final class StoreI64 extends IntegerStore<Int64> {

  /**
   * Constructor for instruction storing a given number of bits.
   *
   * @param bits number of bits instruction should store
   */
  public StoreI64(int bits) {
    super(bits);
  }
}
