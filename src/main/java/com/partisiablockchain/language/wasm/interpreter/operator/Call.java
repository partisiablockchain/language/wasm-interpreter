package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.FunctionType;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.ValType;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.ArrayStack;
import com.partisiablockchain.language.wasm.interpreter.ExecutableFunction;
import com.partisiablockchain.language.wasm.interpreter.FunctionCallable;
import com.partisiablockchain.language.wasm.interpreter.FunctionHosted;
import com.partisiablockchain.language.wasm.interpreter.FunctionInvalid;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import com.partisiablockchain.language.wasm.interpreter.WasmFrame;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import java.util.List;

/** Call operator. */
public final class Call implements OperatorExecutor<InstructionU31> {

  @Override
  public void execute(WasmInstance instance, InstructionU31 instruction) {
    Uint31 funcIndex = instruction.getArgument();
    ExecutableFunction func = instance.lookupFunction(funcIndex);

    final FunctionType typeEntry = func.type();
    final ArrayStack<Literal> stack = instance.stack();
    final List<ValType> params = typeEntry.getParams();

    if (func instanceof FunctionCallable funcLocal) {
      List<Literal> locals = funcLocal.localLiterals();

      WasmFrame frame =
          instance.newFrame(
              new Uint31(locals.size()).add(new Uint31(params.size())),
              funcLocal.name(),
              new Uint31(typeEntry.getReturns().size()),
              funcLocal.getInstructions());

      // Place locals in registers
      Uint31 localIndex = new Uint31(params.size());
      for (int i = 0; i < locals.size(); i++) {
        Literal literal = locals.get(i);
        frame.setLocal(localIndex, literal);
        localIndex = localIndex.addOne();
      }

      // Place parameters in registers
      for (int i = 0; i < params.size(); i++) {
        int index = params.size() - i - 1;
        Literal literal = stack.pop();
        validateTypes(params, index, literal);
        frame.setLocal(new Uint31(index), literal);
      }
    } else if (func instanceof FunctionHosted funcImported) {
      final List<Literal> parameters = stack.popN(params.size());
      WasmFrame frame =
          instance.newFrame(
              new Uint31(params.size()),
              "hosted:" + funcImported.name(),
              new Uint31(typeEntry.getReturns().size()),
              List.of());

      // Place parameters in registers
      for (int i = 0; i < parameters.size(); i++) {
        final int index = parameters.size() - i - 1;
        final Literal literal = parameters.get(i);
        validateTypes(params, index, literal);
        frame.setLocal(new Uint31(index), literal);
      }

      // The Escape from WASM (to Javaland)
      // NOTE: Assumes funcImported.externalFunction().equals("pbc") due to
      // instance validation.
      List<Literal> resultingValues =
          instance.runHostFunction(funcImported.importDeclaration().getImportedName());
      instance.stack().pushReversed(resultingValues);

      Return.executeReturn(instance);
      instance.useCycles(OpCode.RETURN.getCyclesCost());
    } else { // Guarenteed to be invalid
      FunctionInvalid funcInvalid = (FunctionInvalid) func;
      throw TrapException.withCause(
          funcInvalid.exception(), "Calling invalid function %s", funcInvalid.name());
    }
  }

  static void validateTypes(List<ValType> params, int i, Literal literal) {
    ValType param = params.get(i);
    if (param == ValType.I32) {
      literal.expectInt32();
    } else if (param == ValType.I64) {
      literal.expectInt64();
    } else if (param == ValType.F32 || param == ValType.F64) {
      throw new TrapException("Floating point type not supported: %s", param);
    } else {
      throw new TrapException("Invalid type: %s", literal);
    }
  }
}
