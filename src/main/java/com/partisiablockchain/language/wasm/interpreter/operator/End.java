package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.ArrayStack;
import com.partisiablockchain.language.wasm.interpreter.InstructionNoArgs;
import com.partisiablockchain.language.wasm.interpreter.Label;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import java.util.List;

/** End operator. */
public final class End implements OperatorExecutor<InstructionNoArgs> {

  @Override
  public void execute(WasmInstance instance, InstructionNoArgs instruction) {
    final Label label = instance.frame().getLabel(Uint31.ZERO);
    final boolean exitFunction = !instance.frame().hasLabel();
    final ArrayStack<Literal> oldStack = instance.frame().popLabel();
    final List<Literal> retainedValues = oldStack.popN(label.getEndArity().asInt());
    if (exitFunction) {
      instance.popFrame();
    }
    instance.stack().pushReversed(retainedValues);
  }
}
