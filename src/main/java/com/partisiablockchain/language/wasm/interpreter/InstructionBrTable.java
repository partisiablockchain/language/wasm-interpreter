package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.Uint31;

/**
 * Branch table instruction.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#control-instructions%E2%91%A0">WASM Spec</a>
 */
public final class InstructionBrTable extends Instruction {

  private final Uint31[] valueBranches;
  private final Uint31 defaultBranch;

  /**
   * Constructs a br table instruction.
   *
   * @param opCode the op code
   * @param valueBranches branches with specific values
   * @param defaultBranch default branch
   */
  public InstructionBrTable(OpCode opCode, Uint31[] valueBranches, Uint31 defaultBranch) {
    super(opCode);
    this.valueBranches = valueBranches.clone();
    this.defaultBranch = defaultBranch;
  }

  /**
   * Gets value branches; branches with explicit indexes.
   *
   * @return array of value branch indexes
   */
  public Uint31[] getValueBranches() {
    return this.valueBranches.clone();
  }

  /**
   * Gets default branch.
   *
   * @return default branch index
   */
  public Uint31 getDefaultBranch() {
    return this.defaultBranch;
  }
}
