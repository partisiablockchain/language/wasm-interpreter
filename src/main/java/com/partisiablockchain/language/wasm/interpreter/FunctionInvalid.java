package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.FunctionType;
import com.partisiablockchain.language.wasm.common.Uint31;

/**
 * An invalid WASM function. Produced when instruction parsing or type validation fails.
 *
 * <p>Fields:
 *
 * <ul>
 *   <li>{@code index}: index of function in code section
 *   <li>{@code name}: name of function
 *   <li>{@code type}: function type of function
 *   <li>{@code exception}: reason that function is invalid
 * </ul>
 */
public record FunctionInvalid(
    Uint31 index, String name, FunctionType type, RuntimeException exception)
    implements ExecutableFunction {}
