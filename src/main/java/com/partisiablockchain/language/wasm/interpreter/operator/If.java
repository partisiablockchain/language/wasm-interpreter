package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.interpreter.InstructionIfElse;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;

/** If operator. */
public final class If implements OperatorExecutor<InstructionIfElse> {

  @Override
  public void execute(WasmInstance instance, InstructionIfElse instruction) {
    final Int32 pop = instance.stack().pop().expectInt32();
    if (pop.equalsZero()) {
      instance
          .frame()
          .pushLabel(null, instruction.getBlockResultType(), false, instruction.getElseClause());
    } else {
      instance
          .frame()
          .pushLabel(null, instruction.getBlockResultType(), false, instruction.getIfClause());
    }
  }
}
