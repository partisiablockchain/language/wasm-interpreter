package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import java.util.Arrays;

/** Web Assembly memory. */
public final class WasmMemory {

  /** Size of each page in bytes. */
  public static final Uint31 PAGE_SIZE = new Uint31(64 * 1024);

  /** Maximum number of pages this memory can allocate. */
  private final Uint31 maxPages;

  /** Buffer holding the memory data. */
  private byte[] buffer;

  /** The number of pages allocated in this memory. */
  private Uint31 pages;

  /**
   * Constructs a memory with the given amount of pages.
   *
   * @param pages number of pages to initialize with.
   * @param maxPages maximum number of allowed pages.
   */
  public WasmMemory(Uint31 pages, Uint31 maxPages) {
    this.maxPages = maxPages;
    this.pages = Uint31.ZERO;
    this.buffer = new byte[0];
    this.grow(pages);
  }

  /**
   * Grows the memory unit to the desired amount of pages. Might fail, if the new size is larger
   * than the maximum allowed number of pages.
   *
   * @param additional number of pages to grow.
   * @return Whether or not the memory unit grew.
   */
  public boolean grow(Uint31 additional) {
    Uint31 newSize = pages.add(additional);
    if (maxPages.lessThan(newSize)) {
      return false;
    } else {
      byte[] newMemory = new byte[newSize.mult(PAGE_SIZE).asInt()];
      System.arraycopy(buffer, 0, newMemory, 0, buffer.length);

      this.buffer = newMemory;
      this.pages = newSize;

      return true;
    }
  }

  /**
   * Reads contiguous view of memory.
   *
   * @param address Base address of view.
   * @param bytes Length of view.
   * @return view of memory.
   */
  public byte[] read(final Uint31 address, final Uint31 bytes) {
    checkBounds(address, bytes);

    return Arrays.copyOfRange(buffer, address.asInt(), address.add(bytes).asInt());
  }

  /**
   * Write several bytes at given address.
   *
   * @param address address to update
   * @param bytes new values to write
   */
  public void write(Uint31 address, byte[] bytes) {
    checkBounds(address, new Uint31(bytes.length));
    System.arraycopy(bytes, 0, buffer, address.asInt(), bytes.length);
  }

  /**
   * Moves from one address to another within the same memory. Addresses are allowed to overlap.
   *
   * @param destination address to write to
   * @param source address to write from
   * @param length number of bytes to copy
   */
  public void memmove(final Uint31 destination, final Uint31 source, final Uint31 length) {
    checkBounds(destination, length);
    checkBounds(source, length);
    System.arraycopy(buffer, source.asInt(), buffer, destination.asInt(), length.asInt());
  }

  /**
   * Gets size of memory.
   *
   * @return size of memory
   */
  public Uint31 size() {
    return new Uint31(buffer.length);
  }

  /**
   * Get the number of pages of the memory.
   *
   * @return The number opf pages.
   */
  public Uint31 getNumPages() {
    return pages;
  }

  /**
   * Max number of pages of this memory.
   *
   * @return number of pages
   */
  Uint31 getMaxPages() {
    return maxPages;
  }

  private void checkBounds(final Uint31 address, final Uint31 numBytes) {
    if (!isWithinBounds(address, numBytes)) {
      throw newBoundsChecksException(address.asInt(), numBytes.asInt(), "Reading");
    }
  }

  /**
   * Creates new TrapException based on a bounds check.
   *
   * @param address base addess for bounds check
   * @param numBytes length of bounds check
   * @param action what was being done at the bounds check. For example "Reading".
   * @return a new exception with a formatted message
   */
  public TrapException newBoundsChecksException(
      final int address, final int numBytes, final String action) {
    return new TrapException(
        "Out of bounds memory access. " + "%s %d bytes at address %d, buffer len: %d",
        action, numBytes, address, buffer.length);
  }

  /**
   * Performs bounds checking for any given slice. Prefer this to doing any custom checking, as it
   * correctly deals with overflow.
   *
   * @param address base address of slice
   * @param numBytes number of bytes in slice
   * @return true if and only if the slice is fully contained within the memory instance.
   */
  public boolean isWithinBounds(final Uint31 address, final Uint31 numBytes) {
    // The last clause is a little weird, because the naïve version would
    // produce overflow. Notice that the naïve version below can be rewritten:
    //     address + numBytes <= size
    // =>  numBytes <= size - address

    final Uint31 size = this.size();
    return address.lessThanOrEqual(size)
        && numBytes.lessThanOrEqual(size)
        && numBytes.lessThanOrEqual(size.minus(address));
  }
}
