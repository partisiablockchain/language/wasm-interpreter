package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.BinaryCalcOperatorExecutorI32;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.BinaryComparisonOperatorExecutorI32;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.ConstI32;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.LoadI32;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.SelfComparisonOperatorExecutorI32;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.StoreI32;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.UnaryCalcOpI32;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.UpcastI32ToI64;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.WrapI32;
import com.partisiablockchain.language.wasm.interpreter.operator.i64.BinaryCalcOperatorExecutorI64;
import com.partisiablockchain.language.wasm.interpreter.operator.i64.BinaryComparisonOperatorExecutorI64;
import com.partisiablockchain.language.wasm.interpreter.operator.i64.ConstI64;
import com.partisiablockchain.language.wasm.interpreter.operator.i64.LoadI64;
import com.partisiablockchain.language.wasm.interpreter.operator.i64.SelfComparisonOperatorExecutorI64;
import com.partisiablockchain.language.wasm.interpreter.operator.i64.StoreI64;
import com.partisiablockchain.language.wasm.interpreter.operator.i64.UnaryCalcOpI64;
import java.util.function.BiPredicate;

/** Map of all operators to their respective handlers. */
public final class OperatorExec {

  private OperatorExec() {}

  private static final int NUM_OPCODES = 0x100;
  private static final OperatorExecutor<?>[] operators = new OperatorExecutor<?>[NUM_OPCODES];
  private static final Unreachable UNREACHABLE_EXECUTOR = new Unreachable();

  static {
    for (int i = 0; i < NUM_OPCODES; i++) {
      operators[i] = UNREACHABLE_EXECUTOR;
    }

    operators[OpCode.UNREACHABLE.getValue()] = UNREACHABLE_EXECUTOR;
    operators[OpCode.NOP.getValue()] = new NoOpOperatorExecutor();
    operators[OpCode.BLOCK.getValue()] = new Block();
    operators[OpCode.LOOP.getValue()] = new Loop();
    operators[OpCode.IF.getValue()] = new If();
    operators[OpCode.ELSE.getValue()] = new Else();
    operators[OpCode.END.getValue()] = new End();
    operators[OpCode.BR.getValue()] = new Branch();
    operators[OpCode.BR_IF.getValue()] = new BranchIf();
    operators[OpCode.BR_TABLE.getValue()] = new BranchTable();
    operators[OpCode.RETURN.getValue()] = new Return();
    operators[OpCode.CALL.getValue()] = new Call();
    operators[OpCode.CALL_INDIRECT.getValue()] = new CallIndirect();
    operators[OpCode.DROP.getValue()] = new Drop();
    operators[OpCode.SELECT.getValue()] = new Select();
    operators[OpCode.LOCAL_GET.getValue()] = new LocalGet();
    operators[OpCode.LOCAL_SET.getValue()] = new LocalSet();
    operators[OpCode.LOCAL_TEE.getValue()] = new LocalTee();
    operators[OpCode.GLOBAL_GET.getValue()] = new GlobalGet();
    operators[OpCode.GLOBAL_SET.getValue()] = new GlobalSet();

    operators[OpCode.MEMORY_SIZE.getValue()] = new MemorySize();
    operators[OpCode.GROW_MEMORY.getValue()] = new MemoryGrow();

    /* i64 */
    operators[OpCode.I_64_CONST.getValue()] = new ConstI64();
    operators[OpCode.I_64_LOAD.getValue()] = new LoadI64(64, false);
    operators[OpCode.I_64_LOAD_8_S.getValue()] = new LoadI64(8, true);
    operators[OpCode.I_64_LOAD_8_U.getValue()] = new LoadI64(8, false);
    operators[OpCode.I_64_LOAD_16_S.getValue()] = new LoadI64(16, true);
    operators[OpCode.I_64_LOAD_16_U.getValue()] = new LoadI64(16, false);
    operators[OpCode.I_64_LOAD_32_S.getValue()] = new LoadI64(32, true);
    operators[OpCode.I_64_LOAD_32_U.getValue()] = new LoadI64(32, false);

    operators[OpCode.I_64_STORE.getValue()] = new StoreI64(64);
    operators[OpCode.I_64_STORE_8.getValue()] = new StoreI64(8);
    operators[OpCode.I_64_STORE_16.getValue()] = new StoreI64(16);
    operators[OpCode.I_64_STORE_32.getValue()] = new StoreI64(32);

    operators[OpCode.I_64_EQZ.getValue()] =
        new SelfComparisonOperatorExecutorI64(Int64::equalsZero);
    operators[OpCode.I_64_EQ.getValue()] = new BinaryComparisonOperatorExecutorI64(Int64::equals);
    operators[OpCode.I_64_NE.getValue()] =
        new BinaryComparisonOperatorExecutorI64(not(Int64::equals));

    operators[OpCode.I_64_LT_S.getValue()] =
        new BinaryComparisonOperatorExecutorI64(Int64::lessThanSigned);
    operators[OpCode.I_64_LT_U.getValue()] =
        new BinaryComparisonOperatorExecutorI64(Int64::lessThanUnsigned);

    operators[OpCode.I_64_GT_S.getValue()] =
        new BinaryComparisonOperatorExecutorI64(Int64::greaterThanSigned);
    operators[OpCode.I_64_GT_U.getValue()] =
        new BinaryComparisonOperatorExecutorI64(Int64::greaterThanUnsigned);

    operators[OpCode.I_64_LE_S.getValue()] =
        new BinaryComparisonOperatorExecutorI64(Int64::lessThanEqualSigned);
    operators[OpCode.I_64_LE_U.getValue()] =
        new BinaryComparisonOperatorExecutorI64(Int64::lessThanEqualUnsigned);

    operators[OpCode.I_64_GE_S.getValue()] =
        new BinaryComparisonOperatorExecutorI64(Int64::greaterThanEqualSigned);
    operators[OpCode.I_64_GE_U.getValue()] =
        new BinaryComparisonOperatorExecutorI64(Int64::greaterThanEqualUnsigned);

    operators[OpCode.I_64_CLZ.getValue()] = new UnaryCalcOpI64(Int64::countLeadingZeroes);
    operators[OpCode.I_64_CTZ.getValue()] = new UnaryCalcOpI64(Int64::countTrailingZeroes);
    operators[OpCode.I_64_POPCNT.getValue()] = new UnaryCalcOpI64(Int64::populationCount);

    operators[OpCode.I_64_ADD.getValue()] = new BinaryCalcOperatorExecutorI64(Int64::add);
    operators[OpCode.I_64_SUB.getValue()] = new BinaryCalcOperatorExecutorI64(Int64::subtract);
    operators[OpCode.I_64_MUL.getValue()] = new BinaryCalcOperatorExecutorI64(Int64::multiply);
    operators[OpCode.I_64_DIV_S.getValue()] =
        new BinaryCalcOperatorExecutorI64(Int64::divideSigned);
    operators[OpCode.I_64_DIV_U.getValue()] =
        new BinaryCalcOperatorExecutorI64(Int64::divideUnsigned);
    operators[OpCode.I_64_REM_S.getValue()] =
        new BinaryCalcOperatorExecutorI64(Int64::remainderSigned);
    operators[OpCode.I_64_REM_U.getValue()] =
        new BinaryCalcOperatorExecutorI64(Int64::remainderUnsigned);

    operators[OpCode.I_64_AND.getValue()] = new BinaryCalcOperatorExecutorI64(Int64::and);
    operators[OpCode.I_64_OR.getValue()] = new BinaryCalcOperatorExecutorI64(Int64::or);
    operators[OpCode.I_64_XOR.getValue()] = new BinaryCalcOperatorExecutorI64(Int64::xor);
    operators[OpCode.I_64_SHL.getValue()] = new BinaryCalcOperatorExecutorI64(Int64::shiftLeft);
    operators[OpCode.I_64_SHR_S.getValue()] =
        new BinaryCalcOperatorExecutorI64(Int64::shiftRightSigned);
    operators[OpCode.I_64_SHR_U.getValue()] =
        new BinaryCalcOperatorExecutorI64(Int64::shiftRightUnsigned);
    operators[OpCode.I_64_ROTL.getValue()] =
        new BinaryCalcOperatorExecutorI64(Int64::bitwiseRotateLeft);
    operators[OpCode.I_64_ROTR.getValue()] =
        new BinaryCalcOperatorExecutorI64(Int64::bitwiseRotateRight);

    operators[OpCode.I_64_EXTEND_I_32_S.getValue()] = new UpcastI32ToI64(Int64::extendSigned);
    operators[OpCode.I_64_EXTEND_I_32_U.getValue()] = new UpcastI32ToI64(Int64::extendUnsigned);

    /* i32 */
    operators[OpCode.I_32_CONST.getValue()] = new ConstI32();
    operators[OpCode.I_32_LOAD.getValue()] = new LoadI32(32, false);
    operators[OpCode.I_32_LOAD_8_S.getValue()] = new LoadI32(8, true);
    operators[OpCode.I_32_LOAD_8_U.getValue()] = new LoadI32(8, false);
    operators[OpCode.I_32_LOAD_16_S.getValue()] = new LoadI32(16, true);
    operators[OpCode.I_32_LOAD_16_U.getValue()] = new LoadI32(16, false);

    operators[OpCode.I_32_STORE.getValue()] = new StoreI32(32);
    operators[OpCode.I_32_STORE_8.getValue()] = new StoreI32(8);
    operators[OpCode.I_32_STORE_16.getValue()] = new StoreI32(16);

    operators[OpCode.I_32_EQZ.getValue()] =
        new SelfComparisonOperatorExecutorI32(Int32::equalsZero);
    operators[OpCode.I_32_EQ.getValue()] = new BinaryComparisonOperatorExecutorI32(Int32::equals);
    operators[OpCode.I_32_NE.getValue()] =
        new BinaryComparisonOperatorExecutorI32(not(Int32::equals));

    operators[OpCode.I_32_LT_S.getValue()] =
        new BinaryComparisonOperatorExecutorI32(Int32::lessThanSigned);
    operators[OpCode.I_32_LT_U.getValue()] =
        new BinaryComparisonOperatorExecutorI32(Int32::lessThanUnsigned);

    operators[OpCode.I_32_GT_S.getValue()] =
        new BinaryComparisonOperatorExecutorI32(Int32::greaterThanSigned);
    operators[OpCode.I_32_GT_U.getValue()] =
        new BinaryComparisonOperatorExecutorI32(Int32::greaterThanUnsigned);

    operators[OpCode.I_32_LE_S.getValue()] =
        new BinaryComparisonOperatorExecutorI32(Int32::lessThanEqualSigned);
    operators[OpCode.I_32_LE_U.getValue()] =
        new BinaryComparisonOperatorExecutorI32(Int32::lessThanEqualUnsigned);

    operators[OpCode.I_32_GE_S.getValue()] =
        new BinaryComparisonOperatorExecutorI32(Int32::greaterThanEqualSigned);
    operators[OpCode.I_32_GE_U.getValue()] =
        new BinaryComparisonOperatorExecutorI32(Int32::greaterThanEqualUnsigned);

    operators[OpCode.I_32_CLZ.getValue()] = new UnaryCalcOpI32(Int32::countLeadingZeroes);
    operators[OpCode.I_32_CTZ.getValue()] = new UnaryCalcOpI32(Int32::countTrailingZeroes);
    operators[OpCode.I_32_POPCNT.getValue()] = new UnaryCalcOpI32(Int32::populationCount);

    operators[OpCode.I_32_ADD.getValue()] = new BinaryCalcOperatorExecutorI32(Int32::add);
    operators[OpCode.I_32_SUB.getValue()] = new BinaryCalcOperatorExecutorI32(Int32::subtract);
    operators[OpCode.I_32_MUL.getValue()] = new BinaryCalcOperatorExecutorI32(Int32::multiply);
    operators[OpCode.I_32_DIV_S.getValue()] =
        new BinaryCalcOperatorExecutorI32(Int32::divideSigned);
    operators[OpCode.I_32_DIV_U.getValue()] =
        new BinaryCalcOperatorExecutorI32(Int32::divideUnsigned);
    operators[OpCode.I_32_REM_S.getValue()] =
        new BinaryCalcOperatorExecutorI32(Int32::remainderSigned);
    operators[OpCode.I_32_REM_U.getValue()] =
        new BinaryCalcOperatorExecutorI32(Int32::remainderUnsigned);

    operators[OpCode.I_32_AND.getValue()] = new BinaryCalcOperatorExecutorI32(Int32::and);
    operators[OpCode.I_32_OR.getValue()] = new BinaryCalcOperatorExecutorI32(Int32::or);
    operators[OpCode.I_32_XOR.getValue()] = new BinaryCalcOperatorExecutorI32(Int32::xor);
    operators[OpCode.I_32_SHL.getValue()] = new BinaryCalcOperatorExecutorI32(Int32::shiftLeft);
    operators[OpCode.I_32_SHR_S.getValue()] =
        new BinaryCalcOperatorExecutorI32(Int32::shiftRightSigned);
    operators[OpCode.I_32_SHR_U.getValue()] =
        new BinaryCalcOperatorExecutorI32(Int32::shiftRightUnsigned);
    operators[OpCode.I_32_ROTL.getValue()] =
        new BinaryCalcOperatorExecutorI32(Int32::bitwiseRotateLeft);
    operators[OpCode.I_32_ROTR.getValue()] =
        new BinaryCalcOperatorExecutorI32(Int32::bitwiseRotateRight);

    operators[OpCode.I_32_WRAP_I_64.getValue()] = new WrapI32();

    operators[OpCode.I_32_EXTEND_8_S.getValue()] = new UnaryCalcOpI32(Int32::extend8Signed);
    operators[OpCode.I_32_EXTEND_16_S.getValue()] = new UnaryCalcOpI32(Int32::extend16Signed);
    operators[OpCode.I_64_EXTEND_8_S.getValue()] = new UnaryCalcOpI64(Int64::extend8Signed);
    operators[OpCode.I_64_EXTEND_16_S.getValue()] = new UnaryCalcOpI64(Int64::extend16Signed);
    operators[OpCode.I_64_EXTEND_32_S.getValue()] = new UnaryCalcOpI64(Int64::extend32Signed);
  }

  /**
   * Get an operator executor for the given opcode.
   *
   * @param code the opcode
   * @return the resulting executor. Never null, even for unknown opcodes.
   */
  public static OperatorExecutor<?> get(OpCode code) {
    int codeValue = code.getValue();
    if (codeValue < operators.length) {
      return operators[codeValue];
    }
    return UNREACHABLE_EXECUTOR;
  }

  private static <T> BiPredicate<T, T> not(BiPredicate<T, T> function) {
    return (lhs, rhs) -> !function.test(lhs, rhs);
  }
}
