package com.partisiablockchain.language.wasm.interpreter.operator.i64;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import java.util.function.Predicate;

/** Executor for 64-bit self-comparison instructions. */
public final class SelfComparisonOperatorExecutorI64 extends UnaryStackOperatorExecutorI64 {

  private final Predicate<Int64> calcFunction;

  /**
   * Constructor for instruction with a given comparison function.
   *
   * @param calcFunction comparison that should be performed by instruction
   */
  public SelfComparisonOperatorExecutorI64(Predicate<Int64> calcFunction) {
    this.calcFunction = calcFunction;
  }

  @Override
  public Literal execute(WasmInstance instance, Int64 lhs) {
    boolean test = calcFunction.test(lhs);
    return Int32.fromBoolean(test);
  }
}
