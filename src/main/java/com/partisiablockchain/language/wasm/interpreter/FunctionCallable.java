package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.CodeSegment;
import com.partisiablockchain.language.wasm.common.FunctionType;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.ValType;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import java.util.List;

/** An module WASM function. Produced for correctly parsed and well-typed WASM functions. */
public record FunctionCallable(
    Uint31 index, String name, FunctionType type, CodeSegment code, List<Literal> localLiterals)
    implements ExecutableFunction {

  /**
   * Constructor from module code.
   *
   * @param index index of function in code section
   * @param code code segment definition of function
   * @param name name of function
   * @param type type of function
   */
  public FunctionCallable(
      final Uint31 index, final String name, final FunctionType type, final CodeSegment code) {
    this(index, name, type, code, createLocalLiterals(code));
  }

  /**
   * Get the bytecode instructions for this function.
   *
   * @return the list of functions
   */
  public List<Instruction> getInstructions() {
    return code.getCode();
  }

  private static List<Literal> createLocalLiterals(final CodeSegment code) {
    return code.getLocalTypes().stream().map(ValType::getZero).toList();
  }
}
