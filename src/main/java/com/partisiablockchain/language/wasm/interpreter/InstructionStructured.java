package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.BlockResultType;
import com.partisiablockchain.language.wasm.common.OpCode;
import java.util.List;

/**
 * A Structured control flow instruction {@link OpCode#LOOP} or {@link OpCode#BLOCK}.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#control-instructions%E2%91%A0">WASM Spec</a>
 */
public final class InstructionStructured extends Instruction {

  /** Type of the result value that execution of the block produces. */
  private final BlockResultType blockResultType;

  /** The instructions of the block. */
  private final List<Instruction> instructions;

  /**
   * Creates a nesting structured instruction.
   *
   * @param opCode Opcode for the instruction
   * @param blockResultType The value (if any) to remain on the stack after evaluating block
   * @param instructions Inner instructions of block
   */
  public InstructionStructured(
      final OpCode opCode,
      final BlockResultType blockResultType,
      final List<Instruction> instructions) {
    super(opCode);
    this.blockResultType = blockResultType;
    this.instructions = instructions;
  }

  /**
   * Recursive instructions.
   *
   * @return instructions within instruction
   */
  public List<Instruction> getInstructions() {
    return instructions;
  }

  /**
   * Block type of instruction.
   *
   * @return block type of instruction
   */
  public BlockResultType getBlockResultType() {
    return blockResultType;
  }

  @Override
  public String toString() {
    return String.format("%s %s (len = %d)", getOpCode(), blockResultType, instructions.size());
  }
}
