package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.BlockResultType;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.MemoryAddress;
import com.partisiablockchain.language.wasm.parser.Leb128;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmParseExceptionImpl;
import java.util.ArrayList;
import java.util.List;

/** Wraps WasmByteStream and provides WASM instruction parsing functionality. */
public final class InstructionParser {

  private static final InstructionNoArgs OPERATOR_END = new InstructionNoArgs(OpCode.END);

  private final WasmByteStream stream;

  /**
   * Constructor with a WasmByteStream.
   *
   * @param stream byte stream to parse from.
   */
  public InstructionParser(final WasmByteStream stream) {
    this.stream = stream;
  }

  private Instruction parseBlock(OpCode code, BlockResultType blockResultType) {
    final ArrayList<Instruction> instructions = new ArrayList<>();
    while (true) {
      Instruction next = parseNextRecursive();
      instructions.add(next);

      if (next.getOpCode() == OpCode.END) {
        break;
      }
    }
    return new InstructionStructured(code, blockResultType, instructions);
  }

  private Instruction parseIfElse(BlockResultType blockResultType) {
    final ArrayList<Instruction> instructionsIfClause = new ArrayList<>();
    final ArrayList<Instruction> instructionsElseClause = new ArrayList<>();
    List<Instruction> instructions = instructionsIfClause;
    while (true) {
      Instruction next = parseNextRecursive();
      OpCode code = next.getOpCode();
      if (code == OpCode.ELSE) {
        instructions.add(OPERATOR_END);
        instructions = instructionsElseClause;
      } else {
        instructions.add(next);
      }

      if (next.getOpCode() == OpCode.END) {
        break;
      }
    }

    if (instructionsElseClause.isEmpty()) {
      instructionsElseClause.add(OPERATOR_END);
    }
    return new InstructionIfElse(blockResultType, instructionsIfClause, instructionsElseClause);
  }

  /**
   * Parses and returns the next single function.
   *
   * @return parsed instructions in function.
   */
  public List<Instruction> parseFunction() {
    List<Instruction> list = new ArrayList<>();
    while (!stream.isEof()) {
      Instruction operator = parseNextRecursive();
      list.add(operator);
      if (operator.getOpCode() == OpCode.END) {
        break;
      }
    }
    return list;
  }

  /**
   * Parses and returns the next single full instructions, treating blocks as single instructions.
   *
   * @return parsed instruction.
   */
  public Instruction parseNextRecursive() {
    byte rawOpCode = (byte) stream.readUint8();

    OpCode code = OpCode.fromValue(rawOpCode & 0xFF);
    switch (code) {
      case BLOCK:
      case LOOP:
        return parseBlock(code, BlockResultType.fromValue(stream.readUint8()));
      case IF:
        return parseIfElse(BlockResultType.fromValue(stream.readUint8()));
      case BR_TABLE:
        int tableCount = stream.readVarUint31().asInt();
        Uint31[] brTable = new Uint31[tableCount];
        for (int i = 0; i < tableCount; i++) {
          brTable[i] = stream.readVarUint31();
        }
        Uint31 brDefault = stream.readVarUint31();
        return new InstructionBrTable(code, brTable, brDefault);
      case BR:
      case BR_IF:
      case CALL:
      case CALL_INDIRECT:
      case LOCAL_GET:
      case LOCAL_SET:
      case LOCAL_TEE:
      case GLOBAL_GET:
      case GLOBAL_SET:
        InstructionU31 op = new InstructionU31(code, stream.readVarUint31());
        if (code == OpCode.CALL_INDIRECT) {
          long tableIndex = Leb128.unsigned(32, stream);
          if (tableIndex != 0) {
            throw new WasmParseExceptionImpl(
                "Unknown table index for CALL_INDIRECT. Expected '0', but was '%s'", tableIndex);
          }
        }
        return op;
      case MEMORY_SIZE:
      case GROW_MEMORY:
        int memoryIdx = stream.readUint8();
        if (memoryIdx != 0x00) {
          throw new WasmParseExceptionImpl(
              "Explicit zero flag expected instead of 0x%02X for %s instruction", memoryIdx, code);
        }
        return new InstructionU31(code, new Uint31(memoryIdx));
      case I_32_LOAD:
      case I_64_LOAD:
      case I_32_LOAD_8_S:
      case I_32_LOAD_8_U:
      case I_32_LOAD_16_S:
      case I_32_LOAD_16_U:
      case I_64_LOAD_8_S:
      case I_64_LOAD_8_U:
      case I_64_LOAD_16_S:
      case I_64_LOAD_16_U:
      case I_64_LOAD_32_S:
      case I_64_LOAD_32_U:
      case I_32_STORE:
      case I_64_STORE:
      case I_32_STORE_8:
      case I_32_STORE_16:
      case I_64_STORE_8:
      case I_64_STORE_16:
      case I_64_STORE_32:
        MemoryAddress memoryAddress = stream.readMemory();
        return new InstructionMemory(code, memoryAddress);
      case I_32_CONST:
        return new InstructionI32(code, stream.readVarInt32());
      case I_64_CONST:
        return new InstructionI64(code, stream.readInt64());
      case UNREACHABLE:
      case NOP:
      case ELSE:
      case END:
      case RETURN:
      case DROP:
      case SELECT:
      case I_32_EQZ:
      case I_32_EQ:
      case I_32_NE:
      case I_32_LT_S:
      case I_32_LT_U:
      case I_32_GT_S:
      case I_32_GT_U:
      case I_32_LE_S:
      case I_32_LE_U:
      case I_32_GE_S:
      case I_32_GE_U:
      case I_64_EQZ:
      case I_64_EQ:
      case I_64_NE:
      case I_64_LT_S:
      case I_64_LT_U:
      case I_64_GT_S:
      case I_64_GT_U:
      case I_64_LE_S:
      case I_64_LE_U:
      case I_64_GE_S:
      case I_64_GE_U:
      case I_32_CLZ:
      case I_32_CTZ:
      case I_32_POPCNT:
      case I_32_ADD:
      case I_32_SUB:
      case I_32_MUL:
      case I_32_DIV_S:
      case I_32_DIV_U:
      case I_32_REM_S:
      case I_32_REM_U:
      case I_32_AND:
      case I_32_OR:
      case I_32_XOR:
      case I_32_SHL:
      case I_32_SHR_S:
      case I_32_SHR_U:
      case I_32_ROTL:
      case I_32_ROTR:
      case I_64_CLZ:
      case I_64_CTZ:
      case I_64_POPCNT:
      case I_64_ADD:
      case I_64_SUB:
      case I_64_MUL:
      case I_64_DIV_S:
      case I_64_DIV_U:
      case I_64_REM_S:
      case I_64_REM_U:
      case I_64_AND:
      case I_64_OR:
      case I_64_XOR:
      case I_64_SHL:
      case I_64_SHR_S:
      case I_64_SHR_U:
      case I_64_ROTL:
      case I_64_ROTR:
      case I_32_WRAP_I_64:
      case I_64_EXTEND_I_32_S:
      case I_64_EXTEND_I_32_U:
      case I_32_EXTEND_8_S:
      case I_32_EXTEND_16_S:
      case I_64_EXTEND_8_S:
      case I_64_EXTEND_16_S:
      case I_64_EXTEND_32_S:
        return new InstructionNoArgs(code);
      case UNSUPPORTED_FLOATING_POINT:
        throw new WasmParseExceptionImpl("Floating point opcode not supported: 0x%02X", rawOpCode);
      case UNKNOWN:
      default:
        throw new WasmParseExceptionImpl("Unknown or illegal opcode: 0x%02X", rawOpCode);
    }
  }
}
