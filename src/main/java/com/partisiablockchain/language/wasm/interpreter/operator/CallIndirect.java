package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.FunctionType;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.TableType;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.ArrayStack;
import com.partisiablockchain.language.wasm.interpreter.ExecutableFunction;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;

/** Call indirect operator. */
public final class CallIndirect implements OperatorExecutor<InstructionU31> {

  @Override
  public void execute(WasmInstance instance, InstructionU31 instruction) {

    final Uint31 tableIndex = Uint31.ZERO; // NOTE: Implicit in WASM 1.0

    // Figure out table entry to call through
    // NOTE: We keep tableIndexRaw around in order to produce nice error messages.
    final ArrayStack<Literal> stack = instance.stack();
    final Int32 elementIndexRaw = stack.pop().expectInt32();
    final Uint31 elementIndex = elementIndexRaw.toUint31OrNull();

    final TableType tableType = instance.getTableTypeOrNull(tableIndex);
    // NOTE: Assume tableType != null due to validation
    if (elementIndex == null || !elementIndex.lessThan(tableType.getLimits().getMinimum())) {
      throw new TrapException(
          "Undefined element %s for table %s with type %s",
          elementIndexRaw.getUnsignedValue(), tableIndex, tableType);
    }

    // Determine function to call
    final Uint31 funcIndex = instance.getTable(tableIndex).getOrDefault(elementIndex, null);
    if (funcIndex == null) {
      throw new TrapException(
          "Cannot indirectly call uninitialized index %s in table %s", elementIndex, tableIndex);
    }

    final ExecutableFunction function = instance.lookupFunction(funcIndex);
    final Uint31 expectedTypeIndex = instruction.getArgument();

    // Check that the type of the callee is what the instruction expects to call into.
    if (!function.type().getIndex().equals(expectedTypeIndex)) {
      final FunctionType expectedType = instance.getTypeEntryOrNull(expectedTypeIndex);
      // NOTE: Assume expectedType != null due to validation
      final FunctionType actualType = function.type();

      if (!expectedType.callCompatibleWith(actualType)) {
        throw new TrapException(
            "Indirect call type mismatch. Instruction wanted %s, stack argument referenced %s",
            expectedType.toTypeString(), function.type().toTypeString());
      }
    }

    final Call call = new Call();
    final InstructionU31 callOp = new InstructionU31(OpCode.CALL, funcIndex);
    call.execute(instance, callOp);
  }
}
