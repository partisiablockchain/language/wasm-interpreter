package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.common.literal.MemoryAddress;
import com.partisiablockchain.language.wasm.interpreter.ArrayStack;
import com.partisiablockchain.language.wasm.interpreter.InstructionMemory;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmMemory;

/** Abstract class for handling integer loads. */
public abstract class IntegerLoad implements OperatorExecutor<InstructionMemory> {

  private final int bits;
  private final boolean signed;

  /**
   * Instruction to load with a specific number of bits.
   *
   * @param bits Number of bits to load
   * @param signed whether to sign-extend or zero-extend
   */
  protected IntegerLoad(int bits, boolean signed) {
    this.bits = bits;
    this.signed = signed;
  }

  @Override
  public final void execute(WasmInstance instance, InstructionMemory instruction) {
    WasmMemory memory = instance.getMemory(Uint31.ZERO);

    final ArrayStack<Literal> stack = instance.stack();

    Uint31 basePointer = stack.pop().expectInt32().toUint31OrNull();
    if (basePointer == null) {
      throw new TrapException("Out of bounds memory access.");
    }

    MemoryAddress argument = instruction.getArgument();
    Uint31 offset = argument.getOffset();
    Uint31 address = basePointer.add(offset);

    Uint31 numBytes = new Uint31(bits / Byte.SIZE);
    byte[] rawBytes = memory.read(address, numBytes);

    Literal value = convertRaw(rawBytes, bits, signed);
    stack.push(value);
  }

  /**
   * Produce a literal from a bunch of bytes.
   *
   * @param rawBytes raw bytes to parse
   * @param bits number of bits to parse
   * @param signed whether output should be signed or not
   * @return the parsed literal
   */
  protected abstract Literal convertRaw(byte[] rawBytes, int bits, boolean signed);
}
