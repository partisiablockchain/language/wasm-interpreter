package com.partisiablockchain.language.wasm.interpreter.operator.i64;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.operator.IntegerLoad;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.LoadI32;

/** Executor for 64-bit load instructions. */
public final class LoadI64 extends IntegerLoad {

  /**
   * Constructor for instruction loading a given number of bits with a given sign.
   *
   * @param bits number of bits to load
   * @param signed whether bits should be sign-extended or zero-extended
   */
  public LoadI64(int bits, boolean signed) {
    super(bits, signed);
  }

  @Override
  protected Literal convertRaw(byte[] rawBytes, int bits, boolean signed) {
    if (bits == 64) {
      return Int64.fromRawParts(rawBytes);
    } else {
      int value = LoadI32.fromBytes(rawBytes, bits, signed);
      if (signed) {
        return new Int64(value);
      } else {
        return new Int64(Integer.toUnsignedLong(value));
      }
    }
  }
}
