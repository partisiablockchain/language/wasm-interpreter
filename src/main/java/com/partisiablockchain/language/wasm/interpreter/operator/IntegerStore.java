package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.common.literal.MemoryAddress;
import com.partisiablockchain.language.wasm.interpreter.InstructionMemory;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmMemory;
import java.util.Arrays;

/** Abstract class for handling integer stores. */
public abstract class IntegerStore<T extends Literal>
    implements OperatorExecutor<InstructionMemory> {

  private final int bits;

  /**
   * Constructor for a instruction storing a given number of bits.
   *
   * @param bits number of bits that the instruction can store
   */
  protected IntegerStore(int bits) {
    this.bits = bits;
  }

  @Override
  public final void execute(WasmInstance instance, InstructionMemory instruction) {
    MemoryAddress argument = instruction.getArgument();
    execute(instance, argument);
  }

  @SuppressWarnings("unchecked")
  private void execute(WasmInstance instance, MemoryAddress argument) {
    WasmMemory memory = instance.getMemory(Uint31.ZERO);

    T value = (T) instance.stack().pop();

    Uint31 basePointer = instance.stack().pop().expectInt32().toUint31OrNull();
    if (basePointer == null) {
      throw new TrapException("Out of bounds memory access.");
    }

    Uint31 offset = argument.getOffset();
    Uint31 address = basePointer.add(offset);

    byte[] bytes = convertLiteral(value, bits);
    memory.write(address, bytes);
  }

  private byte[] convertLiteral(T literal, int bits) {
    byte[] bytes = literal.toBytes();
    int byteCount = bits / Byte.SIZE;
    return Arrays.copyOfRange(bytes, 0, byteCount);
  }
}
