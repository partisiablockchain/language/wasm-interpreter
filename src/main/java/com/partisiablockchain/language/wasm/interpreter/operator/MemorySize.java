package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmMemory;

/** Memory.size operator. */
public final class MemorySize implements OperatorExecutor<InstructionU31> {

  @Override
  public void execute(WasmInstance instance, InstructionU31 instruction) {
    if (!instruction.getArgument().equals(Uint31.ZERO)) {
      // According to spec
      throw new TrapException("Memory size argument must be 0");
    }

    WasmMemory memory = instance.getMemory(Uint31.ZERO);
    Uint31 numPages = memory.getNumPages();
    instance.stack().push(Int32.fromUnsignedValue(numPages));
  }
}
