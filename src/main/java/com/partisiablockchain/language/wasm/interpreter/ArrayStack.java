package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.TrapException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/** Stack implementation with random access peek support. */
public final class ArrayStack<T> implements Cloneable {

  private final ArrayList<T> stack;

  /** Constructs an empty stack. */
  public ArrayStack() {
    this(List.of());
  }

  /**
   * Constructs a stack containing the elements of the specified collection, in the order they are
   * returned by the collection's iterator. First element will lie at the bottom of the stack, while
   * the last element will lie at the top.
   *
   * @param collection the collection whose elements are to be placed into this stack
   */
  public ArrayStack(final Collection<T> collection) {
    this.stack = new ArrayList<>(collection);
  }

  /**
   * Push an item on to the stack.
   *
   * @param item the item to push
   */
  public void push(T item) {
    stack.add(Objects.requireNonNull(item));
  }

  /**
   * Peek the top of the stack.
   *
   * @return the item at the top, null if it's empty
   */
  public T peek() {
    if (stack.isEmpty()) {
      return null;
    }
    return stack.get(lastIndex());
  }

  /**
   * Peek the nth element on the stack.
   *
   * @param index the item to peek
   * @return the item at the given index, null if index is out of bounds
   */
  public T peek(int index) {
    int size = size();
    if (index < 0 || index >= size) {
      return null;
    }
    return stack.get(size - 1 - index);
  }

  /**
   * The stack size.
   *
   * @return the size of the stack
   */
  public int size() {
    return stack.size();
  }

  /**
   * Pop an item from the stack. Throws an exception if the stack is empty.
   *
   * @return the item at the top of the stack
   */
  public T pop() {
    if (stack.isEmpty()) {
      throw new TrapException("Empty stack");
    }

    int index = lastIndex();
    return stack.remove(index);
  }

  /**
   * Pops an arbitrary number of items from the stack.
   *
   * @param numItems number of items to pop.
   * @return List of literals, top items first, bottom items last.
   */
  public List<T> popN(final int numItems) {
    if (stack.size() < numItems) {
      throw new TrapException("Empty stack");
    }
    final ArrayList<T> list = new ArrayList<>(numItems);
    for (int i = 0; i < numItems; i++) {
      list.add(pop());
    }
    return list;
  }

  /**
   * Push a list of items onto the stack, in reverse order. First items will thus be placed on the
   * top, and last items on the bottom.
   *
   * <p>This is the correct reverse operation of {@link #popN}, and {@code
   * stack.pushReversed(stack.popN(numItems))} is thus a no-op.
   *
   * @param newItems new items to push to stack. First item will be placed on the top, and last item
   *     on the bottom.
   */
  public void pushReversed(final List<T> newItems) {
    for (int newItemsIndex = newItems.size() - 1; newItemsIndex >= 0; newItemsIndex--) {
      push(newItems.get(newItemsIndex));
    }
  }

  /**
   * Checks if the stack has items.
   *
   * @return true if it has items, false otherwise
   */
  public boolean isNonEmpty() {
    return size() > 0;
  }

  private int lastIndex() {
    return size() - 1;
  }

  @Override
  public String toString() {
    String elements = stack.stream().map(String::valueOf).collect(Collectors.joining(", "));
    return "[" + elements + "]";
  }

  /**
   * Returns a shallow copy of this {@code ArrayList} instance. (The elements themselves are not
   * copied.)
   */
  @Override
  public ArrayStack<T> clone() {
    @SuppressWarnings("unchecked")
    final ArrayList<T> elements = (ArrayList<T>) stack.clone();
    return new ArrayStack<T>(elements);
  }
}
