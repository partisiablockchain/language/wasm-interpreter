package com.partisiablockchain.language.wasm.validator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.util.Map.entry;

import com.partisiablockchain.language.wasm.common.AddressKind;
import com.partisiablockchain.language.wasm.common.BlockResultType;
import com.partisiablockchain.language.wasm.common.CodeSegment;
import com.partisiablockchain.language.wasm.common.CollatedFunction;
import com.partisiablockchain.language.wasm.common.FunctionType;
import com.partisiablockchain.language.wasm.common.Global;
import com.partisiablockchain.language.wasm.common.GlobalType;
import com.partisiablockchain.language.wasm.common.Import;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.TableType;
import com.partisiablockchain.language.wasm.common.TypeIndex;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.ValType;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.sections.GlobalSection;
import com.partisiablockchain.language.wasm.common.sections.TableSection;
import com.partisiablockchain.language.wasm.common.sections.TypeSection;
import com.partisiablockchain.language.wasm.interpreter.ArrayStack;
import com.partisiablockchain.language.wasm.interpreter.Instruction;
import com.partisiablockchain.language.wasm.interpreter.InstructionBrTable;
import com.partisiablockchain.language.wasm.interpreter.InstructionIfElse;
import com.partisiablockchain.language.wasm.interpreter.InstructionMemory;
import com.partisiablockchain.language.wasm.interpreter.InstructionStructured;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Validation system for instructions and function bodies.
 *
 * @see <a href="https://www.w3.org/TR/wasm-core-1/#instructions%E2%91%A2">The WASM Spec's section
 *     on instruction validation.</a>
 */
public final class FunctionValidator {

  private final Uint31 numMemories;
  private final GlobalSection globalSection;
  private final Map<Uint31, FunctionType> typeOfFunctionByIndex;
  private final TypeSection typeSection;
  private final TableSection tableSection;

  /**
   * Actual constructor of validator. Avoid calling directly; prefer using forModule constructor,
   * direct instatiation is reserved for tests.
   */
  FunctionValidator(
      final Uint31 numMemories,
      final GlobalSection globalSection,
      final Map<Uint31, FunctionType> typeOfFunctionByIndex,
      final TypeSection typeSection,
      final TableSection tableSection) {
    this.numMemories = numMemories;
    this.globalSection = globalSection;
    this.typeOfFunctionByIndex = typeOfFunctionByIndex;
    this.typeSection = typeSection;
    this.tableSection = tableSection;
  }

  /**
   * Constructor for validator for the given global context. The same validator can be used for all
   * functions/instructions in the same module.
   *
   * @param module module that validator will be run on
   * @return new validator
   */
  public static FunctionValidator forModule(final WasmModule module) {
    // Make map from both import and function section
    final Map<Uint31, FunctionType> typeOfFunctionByIndex = new HashMap<>();

    int functionIdRawCounter = 0;
    for (final Import<TypeIndex> entry : module.getFunctionImportSection()) {
      final TypeIndex typeIndex = entry.getImportDescriptor();
      final Uint31 functionId = new Uint31(functionIdRawCounter++);
      final FunctionType functionType =
          module.getTypeSection().getEntryOrNull(typeIndex.getValue());
      if (functionType == null) {
        throw new WasmValidationException(
            "Function import %s references unknown type %s for module with only %s types",
            functionId, typeIndex.getValue(), module.getTypeSection().numEntries());
      }
      typeOfFunctionByIndex.put(functionId, functionType);
    }

    for (final CollatedFunction func : module.getFunctionSection()) {
      typeOfFunctionByIndex.put(func.functionId(), func.functionType());
    }

    return new FunctionValidator(
        module.getMemorySection().numEntries(),
        module.getGlobalSection(),
        typeOfFunctionByIndex,
        module.getTypeSection(),
        module.getTableSection());
  }

  /** Validates given type section. Throws errors if validation fails. */
  public void validateTypeSection() {
    for (final FunctionType functionType : typeSection) {
      final List<ValType> returnTypes = functionType.getReturns();

      // Validate correct result arity
      if (returnTypes.size() > 1) {
        throw new WasmValidationException(
            "Invalid result arity %s for type %s: Multiple return types like %s are not supported."
                + " ",
            returnTypes.size(), functionType.getIndex(), returnTypes);
      }
    }
  }

  /**
   * Validates function. Throws errors if validation fails.
   *
   * @param codeSegment code of function to validate
   * @param functionType type of function to validate
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#functions%E2%91%A2">WASM Spec 3.4.1 Validation
   *     of Function</a>
   */
  public void validateFunction(final CodeSegment codeSegment, final FunctionType functionType) {
    validateFunction(codeSegment.getLocalTypes(), codeSegment.getCode(), functionType);
  }

  void validateFunction(
      final List<ValType> localTypes,
      final List<Instruction> instructions,
      final FunctionType functionType) {
    // Construct locals list
    final ArrayList<ValType> locals = new ArrayList<ValType>();
    locals.addAll(functionType.getParams());
    locals.addAll(localTypes);

    final List<ValType> returnTypes = functionType.getReturns();

    // Construct context
    final TypeContext initialContext =
        TypeContext.EMPTY
            .withReturnTypes(returnTypes)
            .withLabelPushed(BlockResultType.of(returnTypes))
            .withLocals(locals);

    // Perform validation on instruction
    final TypeContext finalContext = validateExpression(instructions, initialContext);

    // Check that values on stack are correctly typed.
    finalContext.withPopped(returnTypes).mustBeEmpty();

    // Validate that function doesn't mention floats
    if (Stream.of(functionType.getParams(), returnTypes)
        .flatMap(Collection::stream)
        .anyMatch(x -> x.equals(ValType.F32) || x.equals(ValType.F64))) {
      throw new WasmValidationException(
          "Function had type %s, but floating point parameters and return types are not"
              + " supported.",
          functionType.toTypeString());
    } else if (localTypes.stream().anyMatch(x -> x.equals(ValType.F32) || x.equals(ValType.F64))) {
      throw new WasmValidationException(
          "Function had locals %s, but floating point locals are not supported.", localTypes);
    }
  }

  /**
   * Validates constant initializer expression. Throws errors if validation fails.
   *
   * @param expression instructions of expression to check
   * @param expectedType type that expression is expected to emit
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#constant-expressions%E2%91%A0">WASM Spec
   *     3.3.7.2. Constant Expression</a>
   */
  public void validateConstantInitializers(
      final List<Instruction> expression, final ValType expectedType) {
    // Note that constant initializers specifically does not possess return
    // types.
    final TypeContext initialContext = TypeContext.EMPTY;

    // Perform validation on instruction
    final TypeContext finalContext = validateExpression(expression, initialContext);

    // Check that values on stack are correctly typed.
    finalContext.withPopped(expectedType).mustBeEmpty();

    // Check that expression is constant
    if (!isConstantExpression(expression)) {
      throw new WasmValidationException("Malformed initializer: Constant expression required");
    }
  }

  /**
   * Checks for constant-ness of expression. Guarenteed to not throw errors if expression can be
   * validated.
   *
   * @param expression instructions of expression to check
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#constant-expressions%E2%91%A0">WASM Spec
   *     3.3.7.2. Constant Expression</a>
   */
  private boolean isConstantExpression(final List<Instruction> expression) {
    return expression.stream()
        .filter(it -> it.getOpCode() != OpCode.END)
        .allMatch(this::isConstantInstruction);
  }

  private boolean isConstantInstruction(final Instruction insn) {
    switch (insn.getOpCode()) {
      case I_32_CONST:
      case I_64_CONST:
        return true;
      case GLOBAL_GET:
        final boolean globalIsMutable =
            globalSection
                .getEntryOrNull(((InstructionU31) insn).getArgument())
                .getType()
                .isMutable();
        return !globalIsMutable;
      default:
        return false;
    }
  }

  /**
   * Validates expressions.
   *
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#expressions%E2%91%A4">WASM Spec 4.4.8
   *     Expressions</a>
   */
  TypeContext validateExpression(final List<Instruction> insns, final TypeContext initialContext) {
    if (insns.get(insns.size() - 1).getOpCode() != OpCode.END) {
      throw new WasmValidationException("Expression did not end with an END instruction.");
    }

    TypeContext context = initialContext;
    for (int insnIndex = 0; insnIndex < insns.size() - 1; insnIndex++) {
      Instruction insn = insns.get(insnIndex);
      context = validateInstruction(insn, context);
    }

    return context;
  }

  /**
   * Small-step validation of individual instructions.
   *
   * @see <a href="https://www.w3.org/TR/wasm-core-1/#instructions%E2%91%A4">WASM Spec 4.4
   *     Instructions</a>
   */
  private TypeContext validateInstruction(final Instruction insn, final TypeContext context) {
    switch (insn.getOpCode()) {
      case BLOCK:
        // Inner context with no external stack, but with the block's result
        // type on the label stack.
        final InstructionStructured insnBlock = (InstructionStructured) insn;
        final TypeContext blockContextInitial =
            context.withEmptyValueStack().withLabelPushed(insnBlock.getBlockResultType());

        // Validate block inner expression, and stack behaviour
        final TypeContext blockContextFinal =
            validateExpression(insnBlock.getInstructions(), blockContextInitial);
        blockContextFinal.withPopped(insnBlock.getBlockResultType()).mustBeEmpty();

        // Result is kept in the outer context.
        return context.withPushed(insnBlock.getBlockResultType());

      case LOOP:
        // Inner context with no external stack
        final InstructionStructured insnLoop = (InstructionStructured) insn;
        final TypeContext loopContextInitial =
            context.withEmptyValueStack().withLabelPushed(BlockResultType.EMPTY);

        // Validate block inner expression, and stack behaviour
        final TypeContext loopContextFinal =
            validateExpression(insnLoop.getInstructions(), loopContextInitial);
        loopContextFinal.withPopped(insnLoop.getBlockResultType()).mustBeEmpty();

        // Result is kept in the outer context.
        return context.withPushed(insnLoop.getBlockResultType());
      case IF:
        final InstructionIfElse insnIf = (InstructionIfElse) insn;
        final BlockResultType expectedReturns = insnIf.getBlockResultType();

        // Inner context with no external stack
        final TypeContext ifContextAfterConditionPopped = context.withPopped(ValType.I32);
        final TypeContext ifContextInnerInitial =
            ifContextAfterConditionPopped.withEmptyValueStack().withLabelPushed(expectedReturns);

        // Validate branches and behaviour
        final TypeContext ifContextFinalIfCase =
            validateExpression(insnIf.getIfClause(), ifContextInnerInitial);
        final TypeContext ifContextFinalElseCase =
            validateExpression(insnIf.getElseClause(), ifContextInnerInitial);

        ifContextFinalIfCase.withPopped(expectedReturns).mustBeEmpty();
        ifContextFinalElseCase.withPopped(expectedReturns).mustBeEmpty();

        // Result is kept in outer context
        return ifContextAfterConditionPopped.withPushed(expectedReturns);
      case BR_TABLE:
        // Validate that branches each have identical result types.
        final InstructionBrTable insnBrTable = (InstructionBrTable) insn;
        final BlockResultType defaultBranchBlockType =
            context.peekLabelStack(insnBrTable.getDefaultBranch());

        for (final Uint31 branchLabel : insnBrTable.getValueBranches()) {
          final BlockResultType branchBlockType = context.peekLabelStack(branchLabel);
          if (!branchBlockType.equals(defaultBranchBlockType)) {
            throw new WasmValidationException(
                "Type mismatch: Result types for branches did not agree. Branch to label %s had"
                    + " type %s, whereas default branch had type %s",
                branchLabel, branchBlockType, defaultBranchBlockType);
          }
        }

        // Update context
        return context
            .withPopped(ValType.I32)
            .withPopped(defaultBranchBlockType)
            .withNonDeterministicValueStack();
      case BR:
        // Pretty simple: needs result type on the stack
        final InstructionU31 insnBr = (InstructionU31) insn;
        final BlockResultType resultType = context.peekLabelStack(insnBr.getArgument());

        return context.withPopped(resultType).withNonDeterministicValueStack();
      case BR_IF:
        final InstructionU31 insnBrIf = (InstructionU31) insn;
        final BlockResultType ifBrLabelExpectsType = context.peekLabelStack(insnBrIf.getArgument());
        return context
            .withPopped(ValType.I32)
            .withPopped(ifBrLabelExpectsType)
            .withPushed(ifBrLabelExpectsType);
      case CALL:
        final FunctionType calledFunctionType =
            getTypeOfFunction(((InstructionU31) insn).getArgument());

        // * Parameters; must lie on stack in order: first lowest, last highest.
        // * Return values; must lie on stack in order: first lowest, last highest.
        return context
            .withPopped(calledFunctionType.getParams())
            .withPushed(calledFunctionType.getReturns());
      case CALL_INDIRECT:
        // Check that table of correct type exists
        final Uint31 tableIndex = Uint31.ZERO;
        final TableType tableType = getTypeOfTable(tableIndex);
        if (tableType.getAddressKind() != AddressKind.FUNCTION) {
          throw new WasmValidationException(
              "Opcode %s cannot call through table %s of type %s",
              insn.getOpCode(), tableIndex, tableType.getAddressKind());
        }

        // Check that type exists
        final Uint31 functionTypeIndex = ((InstructionU31) insn).getArgument();
        final FunctionType functionType = getType(functionTypeIndex);

        // * One i32 for indexing
        // * Parameters; must lie on stack in order: first lowest, last highest.
        // * Return values; must lie on stack in order: first lowest, last highest.
        return context
            .withPopped(ValType.I32)
            .withPopped(functionType.getParams())
            .withPushed(functionType.getReturns());
      case LOCAL_GET:
        final InstructionU31 insnLocalGet = (InstructionU31) insn;
        final ValType localTypeForGet = context.getTypeOfLocal(insnLocalGet.getArgument());
        return context.withPushed(localTypeForGet);
      case LOCAL_SET:
        final InstructionU31 insnLocalSet = (InstructionU31) insn;
        final ValType localTypeForSet = context.getTypeOfLocal(insnLocalSet.getArgument());
        return context.withPopped(localTypeForSet);
      case LOCAL_TEE:
        final InstructionU31 insnLocalTee = (InstructionU31) insn;
        final ValType localTypeForTee = context.getTypeOfLocal(insnLocalTee.getArgument());
        return context.withPopped(localTypeForTee).withPushed(localTypeForTee);
      case GLOBAL_GET:
        final Uint31 globalIndexGet = ((InstructionU31) insn).getArgument();
        final GlobalType globalTypeGet = getTypeOfGlobal(globalIndexGet);
        return context.withPushed(globalTypeGet.getContentType());
      case GLOBAL_SET:
        final Uint31 globalIndexSet = ((InstructionU31) insn).getArgument();
        final GlobalType globalTypeSet = getTypeOfGlobal(globalIndexSet);

        if (!globalTypeSet.isMutable()) {
          throw new WasmValidationException(
              "OpCode %s cannot set immutable global %s", insn.getOpCode(), globalIndexSet);
        }

        return context.withPopped(globalTypeSet.getContentType());
      case MEMORY_SIZE:
        assertHasMemory(((InstructionU31) insn).getArgument());
        return context.withPushed(ValType.I32);
      case GROW_MEMORY:
        assertHasMemory(((InstructionU31) insn).getArgument());
        return context.withPopped(ValType.I32).withPushed(ValType.I32);
      case I_32_LOAD:
      case I_32_LOAD_8_S:
      case I_32_LOAD_8_U:
      case I_32_LOAD_16_S:
      case I_32_LOAD_16_U:
        assertHasMemory(Uint31.ZERO);
        assertValidAlignment(
            ((InstructionMemory) insn).getArgument().getAlign(),
            resultNaturalSizeInBytesLog2(insn.getOpCode()));
        return context.withPopped(ValType.I32).withPushed(ValType.I32);
      case I_64_LOAD:
      case I_64_LOAD_8_S:
      case I_64_LOAD_8_U:
      case I_64_LOAD_16_S:
      case I_64_LOAD_16_U:
      case I_64_LOAD_32_S:
      case I_64_LOAD_32_U:
        assertHasMemory(Uint31.ZERO);
        assertValidAlignment(
            ((InstructionMemory) insn).getArgument().getAlign(),
            resultNaturalSizeInBytesLog2(insn.getOpCode()));
        return context.withPopped(ValType.I32).withPushed(ValType.I64);
      case I_32_STORE:
      case I_32_STORE_8:
      case I_32_STORE_16:
        assertHasMemory(Uint31.ZERO);
        assertValidAlignment(
            ((InstructionMemory) insn).getArgument().getAlign(),
            resultNaturalSizeInBytesLog2(insn.getOpCode()));
        return context.withPopped(ValType.I32, ValType.I32);
      case I_64_STORE:
      case I_64_STORE_8:
      case I_64_STORE_16:
      case I_64_STORE_32:
        assertHasMemory(Uint31.ZERO);
        assertValidAlignment(
            ((InstructionMemory) insn).getArgument().getAlign(),
            resultNaturalSizeInBytesLog2(insn.getOpCode()));
        return context.withPopped(ValType.I32, ValType.I64);
      case I_32_CONST:
        return context.withPushed(ValType.I32);
      case I_64_CONST:
        return context.withPushed(ValType.I64);
      case UNREACHABLE:
        return context.withNonDeterministicValueStack();
      case NOP:
        return context; // Do nothing, like any good nop
      case ELSE:
      case END:
        throw new WasmValidationException(
            "%s instruction only allowed at end of expression", insn.getOpCode());
      case RETURN:
        final List<ValType> expectedReturnTypes = context.getReturnTypes();
        if (expectedReturnTypes == null) {
          throw new WasmValidationException(
              "Cannot return from this context; no return types given.");
        }

        return context.withPopped(expectedReturnTypes).withNonDeterministicValueStack();
      case DROP:
        return context.withPopped(context.peek());
      case SELECT:
        final TypeContext postPopContext = context.withPopped(ValType.I32);
        final ValType typeOnTop = postPopContext.peek();
        return postPopContext.withPopped(typeOnTop, typeOnTop).withPushed(typeOnTop);
      case I_32_EQZ:
        return context.withPopped(ValType.I32).withPushed(ValType.I32);
      case I_32_EQ:
      case I_32_NE:
      case I_32_LT_S:
      case I_32_LT_U:
      case I_32_GT_S:
      case I_32_GT_U:
      case I_32_LE_S:
      case I_32_LE_U:
      case I_32_GE_S:
      case I_32_GE_U:
        return context.withPopped(ValType.I32, ValType.I32).withPushed(ValType.I32);
      case I_64_EQZ:
        return context.withPopped(ValType.I64).withPushed(ValType.I32);
      case I_64_EQ:
      case I_64_NE:
      case I_64_LT_S:
      case I_64_LT_U:
      case I_64_GT_S:
      case I_64_GT_U:
      case I_64_LE_S:
      case I_64_LE_U:
      case I_64_GE_S:
      case I_64_GE_U:
        return context.withPopped(ValType.I64, ValType.I64).withPushed(ValType.I32);
      case I_32_CLZ:
      case I_32_CTZ:
      case I_32_POPCNT:
        return context.withPopped(ValType.I32).withPushed(ValType.I32);
      case I_32_ADD:
      case I_32_SUB:
      case I_32_MUL:
      case I_32_DIV_S:
      case I_32_DIV_U:
      case I_32_REM_S:
      case I_32_REM_U:
      case I_32_AND:
      case I_32_OR:
      case I_32_XOR:
      case I_32_SHL:
      case I_32_SHR_S:
      case I_32_SHR_U:
      case I_32_ROTL:
      case I_32_ROTR:
        return context.withPopped(ValType.I32, ValType.I32).withPushed(ValType.I32);
      case I_64_CLZ:
      case I_64_CTZ:
      case I_64_POPCNT:
        return context.withPopped(ValType.I64).withPushed(ValType.I64);
      case I_64_ADD:
      case I_64_SUB:
      case I_64_MUL:
      case I_64_DIV_S:
      case I_64_DIV_U:
      case I_64_REM_S:
      case I_64_REM_U:
      case I_64_AND:
      case I_64_OR:
      case I_64_XOR:
      case I_64_SHL:
      case I_64_SHR_S:
      case I_64_SHR_U:
      case I_64_ROTL:
      case I_64_ROTR:
        return context.withPopped(ValType.I64, ValType.I64).withPushed(ValType.I64);
      case I_32_WRAP_I_64:
        return context.withPopped(ValType.I64).withPushed(ValType.I32);
      case I_64_EXTEND_I_32_S:
        return context.withPopped(ValType.I32).withPushed(ValType.I64);
      case I_64_EXTEND_I_32_U:
        return context.withPopped(ValType.I32).withPushed(ValType.I64);
      case I_32_EXTEND_8_S:
      case I_32_EXTEND_16_S:
        return context.withPopped(ValType.I32).withPushed(ValType.I32);
      case I_64_EXTEND_8_S:
      case I_64_EXTEND_16_S:
      case I_64_EXTEND_32_S:
        return context.withPopped(ValType.I64).withPushed(ValType.I64);
      case UNSUPPORTED_FLOATING_POINT:
        throw new WasmValidationException("Floating point opcode not supported");
      case UNKNOWN:
      default:
        throw new WasmValidationException("Unknown or illegal opcode");
    }
  }

  private GlobalType getTypeOfGlobal(final Uint31 globalIndex) {
    final Global global = globalSection.getEntryOrNull(globalIndex);
    if (global == null) {
      throw new WasmValidationException(
          "Unknown global %s; context had %s globals", globalIndex, globalSection.numEntries());
    }
    return global.getType();
  }

  private FunctionType getTypeOfFunction(final Uint31 functionIndex) {
    final FunctionType functionType = typeOfFunctionByIndex.get(functionIndex);
    if (functionType == null) {
      throw new WasmValidationException(
          "Unknown function %s; context had %s functions",
          functionIndex, typeOfFunctionByIndex.size());
    }
    return functionType;
  }

  private TableType getTypeOfTable(final Uint31 tableIndex) {
    final TableType tableType = tableSection.getEntryOrNull(tableIndex);
    if (tableType == null) {
      throw new WasmValidationException(
          "Unknown table %s; context had %s tables", tableIndex, tableSection.numEntries());
    }
    return tableType;
  }

  private FunctionType getType(final Uint31 typeIndex) {
    final FunctionType type = typeSection.getEntryOrNull(typeIndex);
    if (type == null) {
      throw new WasmValidationException(
          "Unknown type %s; context had %s types", typeIndex, typeSection.numEntries());
    }
    return type;
  }

  private void assertHasMemory(final Uint31 memoryIndex) {
    if (!memoryIndex.lessThan(numMemories)) {
      throw new WasmValidationException(
          "Unknown memory %s; context had %s memories", memoryIndex, numMemories);
    }
  }

  static void assertValidAlignment(final Uint31 alignment, final Uint31 typeSizeBytesLog2) {
    if (alignment.asInt() > typeSizeBytesLog2.asInt()) {
      throw new WasmValidationException(
          "Malformed alignment: Alignment must not be larger than natural. Type is %s bits wide,"
              + " but alignment specifies %s bits",
          8 << typeSizeBytesLog2.asInt(), 8 << alignment.asInt());
    }
  }

  private static Uint31 resultNaturalSizeInBytesLog2(final OpCode opcode) {
    return OPCODE_RESULT_NATURAL_SIZE_IN_BYTES_LOG_2.get(opcode);
  }

  private static final Map<OpCode, Uint31> OPCODE_RESULT_NATURAL_SIZE_IN_BYTES_LOG_2 =
      Map.ofEntries(
          entry(OpCode.I_32_LOAD, new Uint31(2)),
          entry(OpCode.I_32_LOAD_8_S, new Uint31(0)),
          entry(OpCode.I_32_LOAD_8_U, new Uint31(0)),
          entry(OpCode.I_32_LOAD_16_S, new Uint31(1)),
          entry(OpCode.I_32_LOAD_16_U, new Uint31(1)),
          entry(OpCode.I_64_LOAD, new Uint31(3)),
          entry(OpCode.I_64_LOAD_8_S, new Uint31(0)),
          entry(OpCode.I_64_LOAD_8_U, new Uint31(0)),
          entry(OpCode.I_64_LOAD_16_S, new Uint31(1)),
          entry(OpCode.I_64_LOAD_16_U, new Uint31(1)),
          entry(OpCode.I_64_LOAD_32_S, new Uint31(2)),
          entry(OpCode.I_64_LOAD_32_U, new Uint31(2)),
          entry(OpCode.I_32_STORE, new Uint31(2)),
          entry(OpCode.I_32_STORE_8, new Uint31(0)),
          entry(OpCode.I_32_STORE_16, new Uint31(1)),
          entry(OpCode.I_64_STORE, new Uint31(3)),
          entry(OpCode.I_64_STORE_8, new Uint31(0)),
          entry(OpCode.I_64_STORE_16, new Uint31(1)),
          entry(OpCode.I_64_STORE_32, new Uint31(2)));

  /**
   * Represents the typing context of the current function and scope.
   *
   * <p>Is an immutable value class.
   */
  static final class TypeContext {

    /**
     * Tracks types of the "values" on the stack. Never null.
     *
     * <p>While {@link typedStackNonDeterminism} is {@code false} this field presents the entire
     * stack, from top to bottom.
     *
     * <p>When {@link typedStackNonDeterminism} is {@code true}, this field only tracks the top of
     * the stack; Popping while this field is empty will produce an unknown type that can satisfy
     * any type constraint. Thus whatever is below the top of the stack is undeterministic and can
     * satisfy whatever type constraint.
     */
    private final ArrayDeque<ValType> typedStack;

    /**
     * True if and only if the stack is non-deterministic/stack polymorphic. This is required due to
     * a weird quirk in the non-deterministic big-step type semantics that WASM uses:
     *
     * <p>Essentially, a few instructions can operate on polymorphic stacks, for example {@code BR},
     * allowing the {@code TypeContext} to satisfy any stack. Concretely, instructions following the
     * stack polymorphic instruction are unreachable during execution, but may have important
     * type-effects.
     */
    private final boolean typedStackNonDeterminism;

    /**
     * Tracks the locals in the scope. These are set for the whole function, and should not be
     * changed during expression validation.
     */
    private final ValType[] localsTypes;

    /**
     * Tracks the result types of nested blocks. Each block is represented as by it's declared
     * result type. Jumping to that label then expects those types on the top of the value stack.
     */
    private final ArrayStack<BlockResultType> labelStack;

    /**
     * Contains the return types of the current function. The return types are used by RETURN
     * instructions to validate that stack layout is correct on RETURN. Order is first type should
     * be lowest on the stack, and last type should be on the top of the stack.
     *
     * <p>Allowed to be null, this may occur if and only if the context disallows RETURN
     * instructions. A RETURN instruction in such a context is not valid, and a validation error
     * will be occur. Global initializers and data/element segment offset expressions are examples
     * of contexts where return is disallowed.
     *
     * <p>The context return type list can also be empty, which differs from a null result type list
     * by representing a function that does not return anything, similar to Java's void functions.
     *
     * @see <a href="https://www.w3.org/TR/wasm-core-1/#-hrefsyntax-instr-controlmathsfreturn">WASM
     *     Spec 3.3.5.9. Return Instruction Validation</a>
     */
    private final List<ValType> returnTypes;

    private TypeContext(
        final ArrayDeque<ValType> typedStack,
        final ValType[] localsTypes,
        final ArrayStack<BlockResultType> labelStack,
        final List<ValType> returnTypes,
        final boolean typedStackNonDeterminism) {
      this.typedStack = typedStack;
      this.localsTypes = localsTypes;
      this.labelStack = labelStack;
      this.returnTypes = returnTypes;
      this.typedStackNonDeterminism = typedStackNonDeterminism;
    }

    public static final TypeContext EMPTY =
        new TypeContext(
            new ArrayDeque<>(),
            new ValType[0],
            new ArrayStack<BlockResultType>(List.of()),
            null,
            false);

    public TypeContext withPushed(final ValType type) {
      return withPushed(List.of(type));
    }

    public TypeContext withPushed(final List<ValType> types) {
      final ArrayDeque<ValType> newStack = this.typedStack.clone();
      for (ValType type : types) {
        newStack.push(type);
      }
      return new TypeContext(
          newStack,
          this.localsTypes,
          this.labelStack,
          this.returnTypes,
          this.typedStackNonDeterminism);
    }

    public TypeContext withPushed(final BlockResultType blockResultType) {
      return this.withPushed(blockResultType.asTypeList());
    }

    /**
     * Checks that value stack is empty.
     *
     * @throws WasmValidationException if stack is not empty.
     */
    public void mustBeEmpty() {
      if (typedStack.size() > 0) {
        throw new WasmValidationException(
            "Type mismatch: Stack contained more values than expected: %s", typedStack);
      }
    }

    /** Pops a result type from the stack and asserts that it is as expected. */
    public TypeContext withPopped(final BlockResultType blockResultType) {
      return withPopped(blockResultType.asTypeList());
    }

    /**
     * Pops types from the stack and asserts that they are as expected.
     *
     * <p>Note: This uses the same syntax used in the semantics. If the semantics write `[t t i32]`,
     * we write in code `.withPopped(List.of(t, t, ValType.I32))`.
     */
    public TypeContext withPopped(final List<ValType> expectedTypes) {
      return withPopped(expectedTypes.toArray(ValType[]::new));
    }

    /**
     * Pops types from the stack and asserts that they are as expected.
     *
     * <p>Note: This uses the same syntax used in the semantics. If the semantics write `[t t i32]`,
     * we write in code `.withPopped(t, t, ValType.I32)`.
     */
    public TypeContext withPopped(final ValType... expectedTypes) {
      if (!typedStackNonDeterminism && typedStack.size() < expectedTypes.length) {
        throw new WasmValidationException(
            "Type mismatch: Value stack too shallow. Expected at least %s elements, but had %s",
            expectedTypes.length, typedStack.size());
      }
      final ArrayDeque<ValType> newStack = this.typedStack.clone();
      for (int i = expectedTypes.length - 1; i >= 0; i--) {
        final ValType expectedType = expectedTypes[i];
        if (typedStackNonDeterminism && newStack.isEmpty()) {
          break;
        }
        final ValType gottenType = newStack.pop();
        if (!compatible(gottenType, expectedType)) {
          throw new WasmValidationException(
              "Type mismatch: Wrong stack type. Expected %s, but got %s.",
              expectedType, gottenType);
        }
      }
      return new TypeContext(
          newStack,
          this.localsTypes,
          this.labelStack,
          this.returnTypes,
          this.typedStackNonDeterminism);
    }

    private ValType peek() {
      final ValType typeOnTop = typedStack.peek();
      if (typeOnTop != null) {
        return typeOnTop;
      } else if (typedStackNonDeterminism) {
        return ValType.UNKNOWN;
      }
      throw new WasmValidationException(
          "Type mismatch: Value stack too shallow. Expected at least %s elements, but had %s",
          1, 0);
    }

    public ValType getTypeOfLocal(Uint31 localIndex) {
      if (new Uint31(localsTypes.length).lessThanOrEqual(localIndex)) {
        throw new WasmValidationException(
            "Unknown local %s; context had %s locals", localIndex, localsTypes.length);
      }
      return localsTypes[localIndex.asInt()];
    }

    public TypeContext withLocals(final List<ValType> locals) {
      return new TypeContext(
          this.typedStack,
          locals.toArray(new ValType[0]),
          this.labelStack,
          this.returnTypes,
          this.typedStackNonDeterminism);
    }

    public TypeContext withEmptyValueStack() {
      return new TypeContext(
          new ArrayDeque<>(), this.localsTypes, this.labelStack, this.returnTypes, false);
    }

    public BlockResultType peekLabelStack(Uint31 relativeStackIndex) {
      final BlockResultType labelType = labelStack.peek(relativeStackIndex.asInt());

      if (labelType == null) {
        throw new WasmValidationException(
            "Unknown label: Stack too shallow. Expected at least %s labels, but had only %s",
            relativeStackIndex.addOne(), labelStack.size());
      }

      return labelType;
    }

    public TypeContext withLabelPushed(final BlockResultType type) {
      final ArrayStack<BlockResultType> newLabelStack = this.labelStack.clone();
      newLabelStack.push(type);
      return new TypeContext(
          this.typedStack,
          this.localsTypes,
          newLabelStack,
          this.returnTypes,
          this.typedStackNonDeterminism);
    }

    public TypeContext withNonDeterministicValueStack() {
      return new TypeContext(
          new ArrayDeque<>(), this.localsTypes, this.labelStack, this.returnTypes, true);
    }

    /**
     * Gets the return types of the current context.
     *
     * @see #returnTypes
     * @return null if and only if no return types;
     */
    public List<ValType> getReturnTypes() {
      return returnTypes;
    }

    /**
     * Sets the return types of the current context. Allowed to be null.
     *
     * @param type return types of context
     * @return new context with return types updated
     * @see #returnTypes
     */
    public TypeContext withReturnTypes(final List<ValType> type) {
      return new TypeContext(
          this.typedStack, this.localsTypes, this.labelStack, type, this.typedStackNonDeterminism);
    }
  }

  static boolean compatible(final ValType a, final ValType b) {
    return a == ValType.UNKNOWN || b == ValType.UNKNOWN || a.equals(b);
  }
}
