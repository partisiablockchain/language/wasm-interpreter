(module
  (type (;0;) (func))
  (type (;1;) (func (result i32)))
  (type (;2;) (func (result i64)))
  (type (;3;) (func (result f32)))
  (type (;4;) (func (result f64)))
  (type (;5;) (func (param i32) (result i32)))
  (type (;6;) (func (param i64) (result i64)))
  (type (;7;) (func (param f32) (result f32)))
  (type (;8;) (func (param f64) (result f64)))
  (type (;9;) (func (param f32 i32) (result i32)))
  (type (;10;) (func (param i32 i64) (result i64)))
  (type (;11;) (func (param f64 f32) (result f32)))
  (type (;12;) (func (param i64 f64) (result f64)))
  (type (;13;) (func (param i32) (result i32)))
  (type (;14;) (func (param i64) (result i64)))
  (type (;15;) (func (param f32) (result f32)))
  (type (;16;) (func (param f64) (result f64)))
  (type (;17;) (func (param i64)))
  (type (;18;) (func (param i64 f64 i32 i64)))
  (type (;19;) (func (param i64) (result i32)))
  (type (;20;) (func (param i64 f64 i32 i64) (result i32)))
  (type (;21;) (func (param i32) (result i64)))
  (func (;0;) (type 1) (result i32)
    i32.const 306)
  (func (;1;) (type 2) (result i64)
    i64.const 356)
  (func (;2;) (type 3) (result f32)
    f32.const 0x1.e64p+11 (;=3890;))
  (func (;3;) (type 4) (result f64)
    f64.const 0x1.ec8p+11 (;=3940;))
  (func (;4;) (type 5) (param i32) (result i32)
    local.get 0)
  (func (;5;) (type 6) (param i64) (result i64)
    local.get 0)
  (func (;6;) (type 7) (param f32) (result f32)
    local.get 0)
  (func (;7;) (type 8) (param f64) (result f64)
    local.get 0)
  (func (;8;) (type 10) (param i32 i64) (result i64)
    local.get 1)
  (func (;9;) (type 12) (param i64 f64) (result f64)
    local.get 1)
  (func (;10;) (type 9) (param f32 i32) (result i32)
    local.get 1)
  (func (;11;) (type 11) (param f64 f32) (result f32)
    local.get 1)
  (func (;12;) (type 13) (param i32) (result i32)
    local.get 0)
  (func (;13;) (type 14) (param i64) (result i64)
    local.get 0)
  (func (;14;) (type 15) (param f32) (result f32)
    local.get 0)
  (func (;15;) (type 16) (param f64) (result f64)
    local.get 0)
  (func (;16;) (type 0)
    i32.const 0
    call_indirect (type 0)
    i64.const 0
    i32.const 0
    call_indirect (type 17)
    i64.const 0
    f64.const 0x0p+0 (;=0;)
    i32.const 0
    i64.const 0
    i32.const 0
    call_indirect (type 18)
    i32.const 0
    call_indirect (type 0)
    i32.const 0
    call_indirect (type 1)
    i32.eqz
    drop
    i32.const 0
    call_indirect (type 1)
    i32.eqz
    drop
    i64.const 0
    i32.const 0
    call_indirect (type 19)
    i32.eqz
    drop
    i64.const 0
    f64.const 0x0p+0 (;=0;)
    i32.const 0
    i64.const 0
    i32.const 0
    call_indirect (type 20)
    i32.eqz
    drop
    i64.const 0
    i32.const 0
    call_indirect (type 6)
    i64.eqz
    drop)
  (func (;17;) (type 1) (result i32)
    i32.const 0
    call_indirect (type 1))
  (func (;18;) (type 2) (result i64)
    i32.const 1
    call_indirect (type 2))
  (func (;19;) (type 3) (result f32)
    i32.const 2
    call_indirect (type 3))
  (func (;20;) (type 4) (result f64)
    i32.const 3
    call_indirect (type 4))
  (func (;21;) (type 2) (result i64)
    i64.const 100
    i32.const 5
    call_indirect (type 6))
  (func (;22;) (type 1) (result i32)
    i32.const 32
    i32.const 4
    call_indirect (type 5))
  (func (;23;) (type 2) (result i64)
    i64.const 64
    i32.const 5
    call_indirect (type 6))
  (func (;24;) (type 3) (result f32)
    f32.const 0x1.51eb86p+0 (;=1.32;)
    i32.const 6
    call_indirect (type 7))
  (func (;25;) (type 4) (result f64)
    f64.const 0x1.a3d70a3d70a3dp+0 (;=1.64;)
    i32.const 7
    call_indirect (type 8))
  (func (;26;) (type 1) (result i32)
    f32.const 0x1.00ccccp+5 (;=32.1;)
    i32.const 32
    i32.const 8
    call_indirect (type 9))
  (func (;27;) (type 2) (result i64)
    i32.const 32
    i64.const 64
    i32.const 9
    call_indirect (type 10))
  (func (;28;) (type 3) (result f32)
    f64.const 0x1p+6 (;=64;)
    f32.const 0x1p+5 (;=32;)
    i32.const 10
    call_indirect (type 11))
  (func (;29;) (type 4) (result f64)
    i64.const 64
    f64.const 0x1.0066666666666p+6 (;=64.1;)
    i32.const 11
    call_indirect (type 12))
  (func (;30;) (type 10) (param i32 i64) (result i64)
    local.get 1
    local.get 0
    call_indirect (type 6))
  (func (;31;) (type 21) (param i32) (result i64)
    i64.const 9
    local.get 0
    call_indirect (type 14))
  (func (;32;) (type 6) (param i64) (result i64)
    local.get 0
    i64.eqz
    if (result i64)  ;; label = @1
      i64.const 1
    else
      local.get 0
      local.get 0
      i64.const 1
      i64.sub
      i32.const 12
      call_indirect (type 6)
      i64.mul
    end)
  (func (;33;) (type 6) (param i64) (result i64)
    local.get 0
    i64.const 1
    i64.le_u
    if (result i64)  ;; label = @1
      i64.const 1
    else
      local.get 0
      i64.const 2
      i64.sub
      i32.const 13
      call_indirect (type 6)
      local.get 0
      i64.const 1
      i64.sub
      i32.const 13
      call_indirect (type 6)
      i64.add
    end)
  (func (;34;) (type 5) (param i32) (result i32)
    local.get 0
    i32.eqz
    if (result i32)  ;; label = @1
      i32.const 44
    else
      local.get 0
      i32.const 1
      i32.sub
      i32.const 15
      call_indirect (type 5)
    end)
  (func (;35;) (type 5) (param i32) (result i32)
    local.get 0
    i32.eqz
    if (result i32)  ;; label = @1
      i32.const 99
    else
      local.get 0
      i32.const 1
      i32.sub
      i32.const 14
      call_indirect (type 5)
    end)
  (func (;36;) (type 0)
    i32.const 16
    call_indirect (type 0))
  (func (;37;) (type 0)
    i32.const 18
    call_indirect (type 0))
  (func (;38;) (type 0)
    i32.const 17
    call_indirect (type 0))
  (table (;0;) 23 23 funcref)
  (export "type-i32" (func 17))
  (export "type-i64" (func 18))
  (export "type-f32" (func 19))
  (export "type-f64" (func 20))
  (export "type-index" (func 21))
  (export "type-first-i32" (func 22))
  (export "type-first-i64" (func 23))
  (export "type-first-f32" (func 24))
  (export "type-first-f64" (func 25))
  (export "type-second-i32" (func 26))
  (export "type-second-i64" (func 27))
  (export "type-second-f32" (func 28))
  (export "type-second-f64" (func 29))
  (export "dispatch" (func 30))
  (export "dispatch-structural" (func 31))
  (export "fac" (func 32))
  (export "fib" (func 33))
  (export "even" (func 34))
  (export "odd" (func 35))
  (export "runaway" (func 36))
  (export "mutual-runaway" (func 37))
  (elem (;0;) (i32.const 0) func 0 1 2 3 4 5 6 7 10 8 11 9 32 33 34 35 36 37 38 12 13 14 15))
