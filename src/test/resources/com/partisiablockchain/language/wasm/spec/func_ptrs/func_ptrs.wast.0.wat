(module
  (type (;0;) (func))
  (type (;1;) (func))
  (type (;2;) (func))
  (type (;3;) (func (result i32)))
  (type (;4;) (func (result i32)))
  (type (;5;) (func (param i32) (result i32)))
  (type (;6;) (func (param i32)))
  (import "spectest" "print_i32" (func (;0;) (type 6)))
  (func (;1;) (type 0))
  (func (;2;) (type 1))
  (func (;3;) (type 4) (result i32)
    i32.const 13)
  (func (;4;) (type 5) (param i32) (result i32)
    local.get 0
    i32.const 1
    i32.add)
  (func (;5;) (type 5) (param i32) (result i32)
    local.get 0
    i32.const 2
    i32.sub)
  (func (;6;) (type 6) (param i32)
    local.get 0
    call 0)
  (export "one" (func 3))
  (export "two" (func 4))
  (export "three" (func 5))
  (export "four" (func 6)))
