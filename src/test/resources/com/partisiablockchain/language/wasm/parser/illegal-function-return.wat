;; A WASM function returning two values. This is explicitly forbidden in WASM 1.0
(module
  (type (;0;) (func (param i32 i32) (result i32 i32)))
  (func $sum (export "sum") (type 0) (param $p0 i32) (param $p1 i32) (result i32 i32)
    get_local $p1
    get_local $p0
    i32.add
    i32.const 0
  )
)
