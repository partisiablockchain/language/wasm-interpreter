(module
  (type (;0;) (func (param i32 i32)))
  (type (;1;) (func (param i32 i32 i32) (result i32)))
  (type (;2;) (func (param i32 i32) (result i32)))
  (type (;3;) (func (param i64 i32) (result i32)))
  (type (;4;) (func (param i32 i32 i32 i32)))
  (type (;5;) (func (param i32 i32 i32)))
  (type (;6;) (func (result i32)))
  (type (;7;) (func (param i32 i32 i32 i32) (result i32)))
  (type (;8;) (func (param i32) (result i64)))
  (type (;9;) (func (param i32)))
  (type (;10;) (func (param i32) (result i32)))
  (type (;11;) (func (param i32 i32 i32 i32 i32) (result i32)))
  (type (;12;) (func))
  (import "pbc" "read_context_into_address" (func $_ZN12vector_magic25read_context_into_address17hf283157fc61a25ceE (type 3)))
  (func $_ZN5alloc7raw_vec11finish_grow17h4f36a7032fdca413E.llvm.12613437329368369019 (type 4) (param i32 i32 i32 i32)
    (local i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 2
                      i32.eqz
                      br_if 0 (;@9;)
                      i32.const 1
                      local.set 4
                      local.get 1
                      i32.const 0
                      i32.lt_s
                      br_if 1 (;@8;)
                      local.get 3
                      i32.load
                      local.tee 5
                      i32.eqz
                      br_if 2 (;@7;)
                      local.get 3
                      i32.load offset=4
                      local.tee 3
                      br_if 4 (;@5;)
                      local.get 1
                      i32.eqz
                      br_if 3 (;@6;)
                      br 5 (;@4;)
                    end
                    local.get 0
                    local.get 1
                    i32.store offset=4
                    i32.const 1
                    local.set 4
                  end
                  i32.const 0
                  local.set 1
                  br 6 (;@1;)
                end
                local.get 1
                br_if 2 (;@4;)
              end
              local.get 2
              local.set 3
              br 2 (;@3;)
            end
            local.get 5
            local.get 3
            local.get 2
            local.get 1
            call $__rust_realloc
            local.tee 3
            br_if 1 (;@3;)
            br 2 (;@2;)
          end
          local.get 1
          local.get 2
          call $__rust_alloc
          local.tee 3
          i32.eqz
          br_if 1 (;@2;)
        end
        local.get 0
        local.get 3
        i32.store offset=4
        i32.const 0
        local.set 4
        br 1 (;@1;)
      end
      local.get 0
      local.get 1
      i32.store offset=4
      local.get 2
      local.set 1
    end
    local.get 0
    local.get 4
    i32.store
    local.get 0
    i32.const 8
    i32.add
    local.get 1
    i32.store)
  (func $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h1f24052c23caed3dE (type 5) (param i32 i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 3
    global.set 0
    block  ;; label = @1
      local.get 1
      local.get 2
      i32.add
      local.tee 2
      local.get 1
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 1
      i32.const 1
      i32.shl
      local.tee 4
      local.get 2
      local.get 4
      local.get 2
      i32.gt_u
      select
      local.tee 2
      i32.const 8
      local.get 2
      i32.const 8
      i32.gt_u
      select
      local.set 2
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          i32.const 16
          i32.add
          i32.const 8
          i32.add
          i32.const 1
          i32.store
          local.get 3
          local.get 1
          i32.store offset=20
          local.get 3
          local.get 0
          i32.load
          i32.store offset=16
          br 1 (;@2;)
        end
        local.get 3
        i32.const 0
        i32.store offset=16
      end
      local.get 3
      local.get 2
      i32.const 1
      local.get 3
      i32.const 16
      i32.add
      call $_ZN5alloc7raw_vec11finish_grow17h4f36a7032fdca413E.llvm.12613437329368369019
      block  ;; label = @2
        local.get 3
        i32.load
        i32.const 1
        i32.ne
        br_if 0 (;@2;)
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        i32.load offset=4
        local.get 0
        call $_ZN5alloc5alloc18handle_alloc_error17h6e739d356a100151E
        unreachable
      end
      local.get 0
      local.get 3
      i64.load offset=4 align=4
      i64.store align=4
      local.get 3
      i32.const 32
      i32.add
      global.set 0
      return
    end
    call $_ZN5alloc7raw_vec17capacity_overflow17h30bd6b88dca05d1aE
    unreachable)
  (func $foo (type 6) (result i32)
    (local i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 65552
    i32.sub
    local.tee 0
    global.set 0
    local.get 0
    i32.const 0
    i32.const 65536
    call $memset
    local.tee 0
    i64.extend_i32_u
    i32.const 65536
    call $_ZN12vector_magic25read_context_into_address17hf283157fc61a25ceE
    local.set 1
    block  ;; label = @1
      i32.const 65536
      i32.const 1
      call $__rust_alloc
      local.tee 2
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 65536
      i32.store offset=65540
      local.get 0
      local.get 2
      i32.store offset=65536
      local.get 2
      local.get 0
      i32.const 65536
      call $memcpy
      drop
      local.get 0
      i32.const 65536
      i32.store offset=65544
      block  ;; label = @2
        local.get 1
        i32.const 65537
        i32.lt_u
        br_if 0 (;@2;)
        local.get 0
        i32.const 65536
        i32.add
        i32.const 65536
        local.get 1
        i32.const -65536
        i32.add
        local.tee 3
        call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17h1f24052c23caed3dE
        local.get 0
        i32.load offset=65536
        local.tee 4
        local.get 0
        i32.load offset=65544
        local.tee 2
        i32.add
        local.set 5
        block  ;; label = @3
          local.get 3
          i32.const 2
          i32.lt_u
          br_if 0 (;@3;)
          local.get 5
          i32.const 0
          local.get 1
          i32.const -65537
          i32.add
          local.tee 1
          call $memset
          drop
          local.get 4
          local.get 2
          local.get 1
          i32.add
          local.tee 2
          i32.add
          local.set 5
        end
        local.get 5
        i32.const 0
        i32.store8
        local.get 2
        i32.const 1
        i32.add
        local.set 1
      end
      local.get 0
      local.get 1
      i32.store offset=65544
      block  ;; label = @2
        local.get 0
        i32.load offset=65540
        local.tee 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.load offset=65536
        local.get 1
        i32.const 1
        call $__rust_dealloc
      end
      local.get 0
      i32.const 65552
      i32.add
      global.set 0
      i32.const 42
      return
    end
    i32.const 65536
    i32.const 1
    call $_ZN5alloc5alloc18handle_alloc_error17h6e739d356a100151E
    unreachable)
  (func $__rust_alloc (type 2) (param i32 i32) (result i32)
    (local i32)
    local.get 0
    local.get 1
    call $__rdl_alloc
    local.set 2
    local.get 2
    return)
  (func $__rust_dealloc (type 5) (param i32 i32 i32)
    local.get 0
    local.get 1
    local.get 2
    call $__rdl_dealloc
    return)
  (func $__rust_realloc (type 7) (param i32 i32 i32 i32) (result i32)
    (local i32)
    local.get 0
    local.get 1
    local.get 2
    local.get 3
    call $__rdl_realloc
    local.set 4
    local.get 4
    return)
  (func $__rust_alloc_error_handler (type 0) (param i32 i32)
    local.get 0
    local.get 1
    call $__rg_oom
    return)
  (func $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17h4414432cd29569beE (type 8) (param i32) (result i64)
    i64.const 9147559743429524724)
  (func $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17ha49e06e3af8b40b6E (type 8) (param i32) (result i64)
    i64.const 3309754977978907185)
  (func $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17hc15596ccdf28816aE (type 5) (param i32 i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 3
    global.set 0
    block  ;; label = @1
      local.get 1
      local.get 2
      i32.add
      local.tee 2
      local.get 1
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 1
      i32.const 1
      i32.shl
      local.tee 4
      local.get 2
      local.get 4
      local.get 2
      i32.gt_u
      select
      local.tee 2
      i32.const 8
      local.get 2
      i32.const 8
      i32.gt_u
      select
      local.set 2
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.eqz
          br_if 0 (;@3;)
          local.get 3
          i32.const 16
          i32.add
          i32.const 8
          i32.add
          i32.const 1
          i32.store
          local.get 3
          local.get 1
          i32.store offset=20
          local.get 3
          local.get 0
          i32.load
          i32.store offset=16
          br 1 (;@2;)
        end
        local.get 3
        i32.const 0
        i32.store offset=16
      end
      local.get 3
      local.get 2
      i32.const 1
      local.get 3
      i32.const 16
      i32.add
      call $_ZN5alloc7raw_vec11finish_grow17h086b302f057d87a7E
      block  ;; label = @2
        local.get 3
        i32.load
        i32.const 1
        i32.ne
        br_if 0 (;@2;)
        local.get 3
        i32.const 8
        i32.add
        i32.load
        local.tee 0
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        i32.load offset=4
        local.get 0
        call $_ZN5alloc5alloc18handle_alloc_error17h6e739d356a100151E
        unreachable
      end
      local.get 0
      local.get 3
      i64.load offset=4 align=4
      i64.store align=4
      local.get 3
      i32.const 32
      i32.add
      global.set 0
      return
    end
    call $_ZN5alloc7raw_vec17capacity_overflow17h30bd6b88dca05d1aE
    unreachable)
  (func $_ZN4core3ptr100drop_in_place$LT$$RF$mut$u20$std..io..Write..write_fmt..Adapter$LT$alloc..vec..Vec$LT$u8$GT$$GT$$GT$17h988a5cf7bcbb5d02E (type 9) (param i32))
  (func $_ZN4core3ptr226drop_in_place$LT$std..error..$LT$impl$u20$core..convert..From$LT$alloc..string..String$GT$$u20$for$u20$alloc..boxed..Box$LT$dyn$u20$std..error..Error$u2b$core..marker..Sync$u2b$core..marker..Send$GT$$GT$..from..StringError$GT$17h77c6f010d389f350E (type 9) (param i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 4
      i32.add
      i32.load
      local.tee 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      local.get 0
      i32.const 1
      call $__rust_dealloc
    end)
  (func $_ZN4core3ptr70drop_in_place$LT$std..panicking..begin_panic_handler..PanicPayload$GT$17h1cdd59289064cfc3E (type 9) (param i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=4
      local.tee 1
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      i32.const 8
      i32.add
      i32.load
      local.tee 0
      i32.eqz
      br_if 0 (;@1;)
      local.get 1
      local.get 0
      i32.const 1
      call $__rust_dealloc
    end)
  (func $_ZN4core6option15Option$LT$T$GT$6unwrap17h06fd127e5ae45399E (type 10) (param i32) (result i32)
    block  ;; label = @1
      local.get 0
      br_if 0 (;@1;)
      i32.const 1048600
      i32.const 43
      i32.const 1048696
      call $_ZN4core9panicking5panic17h855ff10bbf7bb4ddE
      unreachable
    end
    local.get 0)
  (func $_ZN4core6option15Option$LT$T$GT$6unwrap17hc525709d044cea0bE (type 2) (param i32 i32) (result i32)
    block  ;; label = @1
      local.get 0
      br_if 0 (;@1;)
      i32.const 1048600
      i32.const 43
      local.get 1
      call $_ZN4core9panicking5panic17h855ff10bbf7bb4ddE
      unreachable
    end
    local.get 0)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$10write_char17h9aaf994318b5ee48E (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 0
    i32.load
    local.set 0
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 127
        i32.gt_u
        br_if 0 (;@2;)
        block  ;; label = @3
          local.get 0
          i32.load offset=8
          local.tee 3
          local.get 0
          i32.const 4
          i32.add
          i32.load
          i32.ne
          br_if 0 (;@3;)
          local.get 0
          local.get 3
          i32.const 1
          call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17hc15596ccdf28816aE
          local.get 0
          i32.load offset=8
          local.set 3
        end
        local.get 0
        local.get 3
        i32.const 1
        i32.add
        i32.store offset=8
        local.get 0
        i32.load
        local.get 3
        i32.add
        local.get 1
        i32.store8
        br 1 (;@1;)
      end
      local.get 2
      i32.const 0
      i32.store offset=12
      block  ;; label = @2
        block  ;; label = @3
          local.get 1
          i32.const 2048
          i32.lt_u
          br_if 0 (;@3;)
          block  ;; label = @4
            local.get 1
            i32.const 65536
            i32.lt_u
            br_if 0 (;@4;)
            local.get 2
            local.get 1
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=15
            local.get 2
            local.get 1
            i32.const 18
            i32.shr_u
            i32.const 240
            i32.or
            i32.store8 offset=12
            local.get 2
            local.get 1
            i32.const 6
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=14
            local.get 2
            local.get 1
            i32.const 12
            i32.shr_u
            i32.const 63
            i32.and
            i32.const 128
            i32.or
            i32.store8 offset=13
            i32.const 4
            local.set 1
            br 2 (;@2;)
          end
          local.get 2
          local.get 1
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=14
          local.get 2
          local.get 1
          i32.const 12
          i32.shr_u
          i32.const 224
          i32.or
          i32.store8 offset=12
          local.get 2
          local.get 1
          i32.const 6
          i32.shr_u
          i32.const 63
          i32.and
          i32.const 128
          i32.or
          i32.store8 offset=13
          i32.const 3
          local.set 1
          br 1 (;@2;)
        end
        local.get 2
        local.get 1
        i32.const 63
        i32.and
        i32.const 128
        i32.or
        i32.store8 offset=13
        local.get 2
        local.get 1
        i32.const 6
        i32.shr_u
        i32.const 192
        i32.or
        i32.store8 offset=12
        i32.const 2
        local.set 1
      end
      block  ;; label = @2
        local.get 0
        i32.const 4
        i32.add
        i32.load
        local.get 0
        i32.const 8
        i32.add
        local.tee 4
        i32.load
        local.tee 3
        i32.sub
        local.get 1
        i32.ge_u
        br_if 0 (;@2;)
        local.get 0
        local.get 3
        local.get 1
        call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17hc15596ccdf28816aE
        local.get 4
        i32.load
        local.set 3
      end
      local.get 0
      i32.load
      local.get 3
      i32.add
      local.get 2
      i32.const 12
      i32.add
      local.get 1
      call $memcpy
      drop
      local.get 4
      local.get 3
      local.get 1
      i32.add
      i32.store
    end
    local.get 2
    i32.const 16
    i32.add
    global.set 0
    i32.const 0)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_fmt17h6a2be3faf981302cE (type 2) (param i32 i32) (result i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 0
    i32.load
    i32.store offset=4
    local.get 2
    i32.const 8
    i32.add
    i32.const 16
    i32.add
    local.get 1
    i32.const 16
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    i32.const 8
    i32.add
    i32.const 8
    i32.add
    local.get 1
    i32.const 8
    i32.add
    i64.load align=4
    i64.store
    local.get 2
    local.get 1
    i64.load align=4
    i64.store offset=8
    local.get 2
    i32.const 4
    i32.add
    i32.const 1048576
    local.get 2
    i32.const 8
    i32.add
    call $_ZN4core3fmt5write17h0923b640446a3c30E
    local.set 1
    local.get 2
    i32.const 32
    i32.add
    global.set 0
    local.get 1)
  (func $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_str17h98a5960856085077E (type 1) (param i32 i32 i32) (result i32)
    (local i32 i32)
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 3
      i32.const 4
      i32.add
      i32.load
      local.get 3
      i32.const 8
      i32.add
      local.tee 4
      i32.load
      local.tee 0
      i32.sub
      local.get 2
      i32.ge_u
      br_if 0 (;@1;)
      local.get 3
      local.get 0
      local.get 2
      call $_ZN5alloc7raw_vec19RawVec$LT$T$C$A$GT$7reserve21do_reserve_and_handle17hc15596ccdf28816aE
      local.get 4
      i32.load
      local.set 0
    end
    local.get 3
    i32.load
    local.get 0
    i32.add
    local.get 1
    local.get 2
    call $memcpy
    drop
    local.get 4
    local.get 0
    local.get 2
    i32.add
    i32.store
    i32.const 0)
  (func $_ZN5alloc7raw_vec11finish_grow17h086b302f057d87a7E (type 4) (param i32 i32 i32 i32)
    (local i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    local.get 2
                    i32.eqz
                    br_if 0 (;@8;)
                    i32.const 0
                    local.set 4
                    i32.const 1
                    local.set 5
                    local.get 1
                    i32.const 0
                    i32.lt_s
                    br_if 7 (;@1;)
                    local.get 3
                    i32.load
                    local.tee 4
                    i32.eqz
                    br_if 2 (;@6;)
                    local.get 3
                    i32.load offset=4
                    local.tee 3
                    br_if 1 (;@7;)
                    local.get 1
                    br_if 3 (;@5;)
                    br 5 (;@3;)
                  end
                  local.get 0
                  local.get 1
                  i32.store offset=4
                  i32.const 1
                  local.set 5
                  i32.const 0
                  local.set 4
                  br 6 (;@1;)
                end
                local.get 4
                local.get 3
                local.get 2
                local.get 1
                call $__rust_realloc
                local.set 3
                br 2 (;@4;)
              end
              local.get 1
              i32.eqz
              br_if 2 (;@3;)
            end
            local.get 1
            local.get 2
            call $__rust_alloc
            local.set 3
          end
          local.get 1
          local.set 4
          br 1 (;@2;)
        end
        i32.const 0
        local.set 4
        local.get 2
        local.set 3
      end
      block  ;; label = @2
        local.get 3
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        local.get 3
        i32.store offset=4
        i32.const 0
        local.set 5
        br 1 (;@1;)
      end
      local.get 0
      local.get 1
      i32.store offset=4
      local.get 2
      local.set 4
    end
    local.get 0
    local.get 5
    i32.store
    local.get 0
    i32.const 8
    i32.add
    local.get 4
    i32.store)
  (func $_ZN8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h70fb551a667bd6b2E (type 2) (param i32 i32) (result i32)
    (local i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.const 9
        i32.lt_u
        br_if 0 (;@2;)
        block  ;; label = @3
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          local.get 1
          i32.le_u
          br_if 0 (;@3;)
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          local.set 1
        end
        i32.const 0
        local.set 2
        i32.const 0
        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
        local.set 3
        local.get 3
        local.get 3
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        i32.sub
        i32.const 20
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        i32.sub
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        i32.sub
        i32.const -65544
        i32.add
        i32.const -9
        i32.and
        i32.const -3
        i32.add
        local.tee 3
        i32.const 0
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        i32.const 2
        i32.shl
        i32.sub
        local.tee 4
        local.get 4
        local.get 3
        i32.gt_u
        select
        local.get 1
        i32.sub
        local.get 0
        i32.le_u
        br_if 1 (;@1;)
        local.get 1
        i32.const 16
        local.get 0
        i32.const 4
        i32.add
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        i32.const -5
        i32.add
        local.get 0
        i32.gt_u
        select
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        local.tee 4
        i32.add
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        i32.add
        i32.const -4
        i32.add
        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h558ac717aff7de04E
        local.tee 3
        i32.eqz
        br_if 1 (;@1;)
        local.get 3
        call $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h325ccd9f977bf5d6E
        local.set 0
        block  ;; label = @3
          block  ;; label = @4
            local.get 1
            i32.const -1
            i32.add
            local.tee 2
            local.get 3
            i32.and
            br_if 0 (;@4;)
            local.get 0
            local.set 1
            br 1 (;@3;)
          end
          local.get 2
          local.get 3
          i32.add
          i32.const 0
          local.get 1
          i32.sub
          i32.and
          call $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h325ccd9f977bf5d6E
          local.set 2
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          local.set 3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
          local.get 2
          local.get 2
          local.get 1
          i32.add
          local.get 2
          local.get 0
          i32.sub
          local.get 3
          i32.gt_u
          select
          local.tee 1
          local.get 0
          i32.sub
          local.tee 2
          i32.sub
          local.set 3
          block  ;; label = @4
            local.get 0
            call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17heb33c93ad8506845E
            br_if 0 (;@4;)
            local.get 1
            local.get 3
            call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
            local.get 0
            local.get 2
            call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
            local.get 0
            local.get 2
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17h68843c482a778867E
            br 1 (;@3;)
          end
          local.get 0
          i32.load
          local.set 0
          local.get 1
          local.get 3
          i32.store offset=4
          local.get 1
          local.get 0
          local.get 2
          i32.add
          i32.store
        end
        block  ;; label = @3
          local.get 1
          call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17heb33c93ad8506845E
          br_if 0 (;@3;)
          local.get 1
          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
          local.tee 0
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          local.get 4
          i32.add
          i32.le_u
          br_if 0 (;@3;)
          local.get 1
          local.get 4
          call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
          local.set 2
          local.get 1
          local.get 4
          call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
          local.get 2
          local.get 0
          local.get 4
          i32.sub
          local.tee 0
          call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
          local.get 2
          local.get 0
          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17h68843c482a778867E
        end
        local.get 1
        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
        local.set 0
        local.get 1
        call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17heb33c93ad8506845E
        drop
        local.get 0
        return
      end
      local.get 0
      call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h558ac717aff7de04E
      local.set 2
    end
    local.get 2)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h558ac717aff7de04E (type 10) (param i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i64)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 1
    global.set 0
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          i32.const 245
          i32.lt_u
          br_if 0 (;@3;)
          i32.const 0
          local.set 2
          i32.const 0
          call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
          local.set 3
          local.get 3
          local.get 3
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          i32.sub
          i32.const 20
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          i32.sub
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          i32.sub
          i32.const -65544
          i32.add
          i32.const -9
          i32.and
          i32.const -3
          i32.add
          local.tee 3
          i32.const 0
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          i32.const 2
          i32.shl
          i32.sub
          local.tee 4
          local.get 4
          local.get 3
          i32.gt_u
          select
          local.get 0
          i32.le_u
          br_if 2 (;@1;)
          local.get 0
          i32.const 4
          i32.add
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          local.set 3
          i32.const 0
          i32.load offset=1048888
          i32.eqz
          br_if 1 (;@2;)
          i32.const 0
          local.set 5
          block  ;; label = @4
            local.get 3
            i32.const 8
            i32.shr_u
            local.tee 0
            i32.eqz
            br_if 0 (;@4;)
            i32.const 31
            local.set 5
            local.get 3
            i32.const 16777215
            i32.gt_u
            br_if 0 (;@4;)
            local.get 3
            i32.const 6
            local.get 0
            i32.clz
            local.tee 0
            i32.sub
            i32.const 31
            i32.and
            i32.shr_u
            i32.const 1
            i32.and
            local.get 0
            i32.const 1
            i32.shl
            i32.sub
            i32.const 62
            i32.add
            local.set 5
          end
          i32.const 0
          local.get 3
          i32.sub
          local.set 2
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                local.get 5
                i32.const 2
                i32.shl
                i32.const 1049156
                i32.add
                i32.load
                local.tee 0
                i32.eqz
                br_if 0 (;@6;)
                local.get 3
                local.get 5
                call $_ZN8dlmalloc8dlmalloc24leftshift_for_tree_index17h3fccfa1fd0332fcaE
                i32.const 31
                i32.and
                i32.shl
                local.set 6
                i32.const 0
                local.set 7
                i32.const 0
                local.set 4
                loop  ;; label = @7
                  block  ;; label = @8
                    local.get 0
                    call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
                    call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
                    local.tee 8
                    local.get 3
                    i32.lt_u
                    br_if 0 (;@8;)
                    local.get 8
                    local.get 3
                    i32.sub
                    local.tee 8
                    local.get 2
                    i32.ge_u
                    br_if 0 (;@8;)
                    local.get 8
                    local.set 2
                    local.get 0
                    local.set 4
                    local.get 8
                    br_if 0 (;@8;)
                    i32.const 0
                    local.set 2
                    local.get 0
                    local.set 4
                    br 3 (;@5;)
                  end
                  local.get 0
                  i32.const 20
                  i32.add
                  i32.load
                  local.tee 8
                  local.get 7
                  local.get 8
                  local.get 0
                  local.get 6
                  i32.const 29
                  i32.shr_u
                  i32.const 4
                  i32.and
                  i32.add
                  i32.const 16
                  i32.add
                  i32.load
                  local.tee 0
                  i32.ne
                  select
                  local.get 7
                  local.get 8
                  select
                  local.set 7
                  local.get 6
                  i32.const 1
                  i32.shl
                  local.set 6
                  local.get 0
                  br_if 0 (;@7;)
                end
                block  ;; label = @7
                  local.get 7
                  i32.eqz
                  br_if 0 (;@7;)
                  local.get 7
                  local.set 0
                  br 2 (;@5;)
                end
                local.get 4
                br_if 2 (;@4;)
              end
              i32.const 0
              local.set 4
              i32.const 1
              local.get 5
              i32.const 31
              i32.and
              i32.shl
              call $_ZN8dlmalloc8dlmalloc9left_bits17h80354a56302cce1fE
              i32.const 0
              i32.load offset=1048888
              i32.and
              local.tee 0
              i32.eqz
              br_if 3 (;@2;)
              local.get 0
              call $_ZN8dlmalloc8dlmalloc9least_bit17he2fbd395f0b4f5d7E
              i32.ctz
              i32.const 2
              i32.shl
              i32.const 1049156
              i32.add
              i32.load
              local.tee 0
              i32.eqz
              br_if 3 (;@2;)
            end
            loop  ;; label = @5
              local.get 0
              local.get 4
              local.get 0
              call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
              call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
              local.tee 7
              local.get 3
              i32.ge_u
              local.get 7
              local.get 3
              i32.sub
              local.tee 7
              local.get 2
              i32.lt_u
              i32.and
              local.tee 6
              select
              local.set 4
              local.get 7
              local.get 2
              local.get 6
              select
              local.set 2
              local.get 0
              call $_ZN8dlmalloc8dlmalloc9TreeChunk14leftmost_child17h2daf7c374aefdac8E
              local.tee 0
              br_if 0 (;@5;)
            end
            local.get 4
            i32.eqz
            br_if 2 (;@2;)
          end
          block  ;; label = @4
            i32.const 0
            i32.load offset=1049284
            local.tee 0
            local.get 3
            i32.lt_u
            br_if 0 (;@4;)
            local.get 2
            local.get 0
            local.get 3
            i32.sub
            i32.ge_u
            br_if 2 (;@2;)
          end
          local.get 4
          call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
          local.tee 0
          local.get 3
          call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
          local.set 7
          local.get 4
          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.const 16
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
              i32.lt_u
              br_if 0 (;@5;)
              local.get 0
              local.get 3
              call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hd978cf052c211783E
              local.get 7
              local.get 2
              call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
              block  ;; label = @6
                local.get 2
                i32.const 256
                i32.lt_u
                br_if 0 (;@6;)
                local.get 7
                local.get 2
                call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17he1b0e8180890506aE
                br 2 (;@4;)
              end
              local.get 2
              i32.const 3
              i32.shr_u
              local.tee 4
              i32.const 3
              i32.shl
              i32.const 1048892
              i32.add
              local.set 2
              block  ;; label = @6
                block  ;; label = @7
                  i32.const 0
                  i32.load offset=1048884
                  local.tee 6
                  i32.const 1
                  local.get 4
                  i32.shl
                  local.tee 4
                  i32.and
                  i32.eqz
                  br_if 0 (;@7;)
                  local.get 2
                  i32.load offset=8
                  local.set 4
                  br 1 (;@6;)
                end
                i32.const 0
                local.get 6
                local.get 4
                i32.or
                i32.store offset=1048884
                local.get 2
                local.set 4
              end
              local.get 2
              local.get 7
              i32.store offset=8
              local.get 4
              local.get 7
              i32.store offset=12
              local.get 7
              local.get 2
              i32.store offset=12
              local.get 7
              local.get 4
              i32.store offset=8
              br 1 (;@4;)
            end
            local.get 0
            local.get 2
            local.get 3
            i32.add
            call $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17haac305c878b66d79E
          end
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
          local.tee 2
          i32.eqz
          br_if 1 (;@2;)
          br 2 (;@1;)
        end
        i32.const 16
        local.get 0
        i32.const 4
        i32.add
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        i32.const -5
        i32.add
        local.get 0
        i32.gt_u
        select
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        local.set 3
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      i32.const 0
                      i32.load offset=1048884
                      local.tee 7
                      local.get 3
                      i32.const 3
                      i32.shr_u
                      local.tee 2
                      i32.const 31
                      i32.and
                      local.tee 4
                      i32.shr_u
                      local.tee 0
                      i32.const 3
                      i32.and
                      br_if 0 (;@9;)
                      local.get 3
                      i32.const 0
                      i32.load offset=1049284
                      i32.le_u
                      br_if 7 (;@2;)
                      local.get 0
                      br_if 1 (;@8;)
                      i32.const 0
                      i32.load offset=1048888
                      local.tee 0
                      i32.eqz
                      br_if 7 (;@2;)
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc9least_bit17he2fbd395f0b4f5d7E
                      i32.ctz
                      i32.const 2
                      i32.shl
                      i32.const 1049156
                      i32.add
                      i32.load
                      local.tee 4
                      call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
                      call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
                      local.get 3
                      i32.sub
                      local.set 2
                      block  ;; label = @10
                        local.get 4
                        call $_ZN8dlmalloc8dlmalloc9TreeChunk14leftmost_child17h2daf7c374aefdac8E
                        local.tee 0
                        i32.eqz
                        br_if 0 (;@10;)
                        loop  ;; label = @11
                          local.get 0
                          call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
                          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
                          local.get 3
                          i32.sub
                          local.tee 7
                          local.get 2
                          local.get 7
                          local.get 2
                          i32.lt_u
                          local.tee 7
                          select
                          local.set 2
                          local.get 0
                          local.get 4
                          local.get 7
                          select
                          local.set 4
                          local.get 0
                          call $_ZN8dlmalloc8dlmalloc9TreeChunk14leftmost_child17h2daf7c374aefdac8E
                          local.tee 0
                          br_if 0 (;@11;)
                        end
                      end
                      local.get 4
                      call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
                      local.tee 0
                      local.get 3
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                      local.set 7
                      local.get 4
                      call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E
                      local.get 2
                      i32.const 16
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                      i32.lt_u
                      br_if 5 (;@4;)
                      local.get 7
                      call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
                      local.set 7
                      local.get 0
                      local.get 3
                      call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hd978cf052c211783E
                      local.get 7
                      local.get 2
                      call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
                      i32.const 0
                      i32.load offset=1049284
                      local.tee 4
                      i32.eqz
                      br_if 4 (;@5;)
                      local.get 4
                      i32.const 3
                      i32.shr_u
                      local.tee 8
                      i32.const 3
                      i32.shl
                      i32.const 1048892
                      i32.add
                      local.set 6
                      i32.const 0
                      i32.load offset=1049292
                      local.set 4
                      i32.const 0
                      i32.load offset=1048884
                      local.tee 5
                      i32.const 1
                      local.get 8
                      i32.const 31
                      i32.and
                      i32.shl
                      local.tee 8
                      i32.and
                      i32.eqz
                      br_if 2 (;@7;)
                      local.get 6
                      i32.load offset=8
                      local.set 8
                      br 3 (;@6;)
                    end
                    block  ;; label = @9
                      block  ;; label = @10
                        local.get 0
                        i32.const -1
                        i32.xor
                        i32.const 1
                        i32.and
                        local.get 2
                        i32.add
                        local.tee 3
                        i32.const 3
                        i32.shl
                        local.tee 4
                        i32.const 1048900
                        i32.add
                        i32.load
                        local.tee 0
                        i32.const 8
                        i32.add
                        i32.load
                        local.tee 2
                        local.get 4
                        i32.const 1048892
                        i32.add
                        local.tee 4
                        i32.eq
                        br_if 0 (;@10;)
                        local.get 2
                        local.get 4
                        i32.store offset=12
                        local.get 4
                        local.get 2
                        i32.store offset=8
                        br 1 (;@9;)
                      end
                      i32.const 0
                      local.get 7
                      i32.const -2
                      local.get 3
                      i32.rotl
                      i32.and
                      i32.store offset=1048884
                    end
                    local.get 0
                    local.get 3
                    i32.const 3
                    i32.shl
                    call $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17haac305c878b66d79E
                    local.get 0
                    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                    local.set 2
                    br 7 (;@1;)
                  end
                  block  ;; label = @8
                    block  ;; label = @9
                      i32.const 1
                      local.get 4
                      i32.shl
                      call $_ZN8dlmalloc8dlmalloc9left_bits17h80354a56302cce1fE
                      local.get 0
                      local.get 4
                      i32.shl
                      i32.and
                      call $_ZN8dlmalloc8dlmalloc9least_bit17he2fbd395f0b4f5d7E
                      i32.ctz
                      local.tee 2
                      i32.const 3
                      i32.shl
                      local.tee 7
                      i32.const 1048900
                      i32.add
                      i32.load
                      local.tee 0
                      i32.const 8
                      i32.add
                      i32.load
                      local.tee 4
                      local.get 7
                      i32.const 1048892
                      i32.add
                      local.tee 7
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 4
                      local.get 7
                      i32.store offset=12
                      local.get 7
                      local.get 4
                      i32.store offset=8
                      br 1 (;@8;)
                    end
                    i32.const 0
                    i32.const 0
                    i32.load offset=1048884
                    i32.const -2
                    local.get 2
                    i32.rotl
                    i32.and
                    i32.store offset=1048884
                  end
                  local.get 0
                  local.get 3
                  call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hd978cf052c211783E
                  local.get 0
                  local.get 3
                  call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                  local.tee 4
                  local.get 2
                  i32.const 3
                  i32.shl
                  local.get 3
                  i32.sub
                  local.tee 7
                  call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
                  block  ;; label = @8
                    i32.const 0
                    i32.load offset=1049284
                    local.tee 3
                    i32.eqz
                    br_if 0 (;@8;)
                    local.get 3
                    i32.const 3
                    i32.shr_u
                    local.tee 6
                    i32.const 3
                    i32.shl
                    i32.const 1048892
                    i32.add
                    local.set 2
                    i32.const 0
                    i32.load offset=1049292
                    local.set 3
                    block  ;; label = @9
                      block  ;; label = @10
                        i32.const 0
                        i32.load offset=1048884
                        local.tee 8
                        i32.const 1
                        local.get 6
                        i32.const 31
                        i32.and
                        i32.shl
                        local.tee 6
                        i32.and
                        i32.eqz
                        br_if 0 (;@10;)
                        local.get 2
                        i32.load offset=8
                        local.set 6
                        br 1 (;@9;)
                      end
                      i32.const 0
                      local.get 8
                      local.get 6
                      i32.or
                      i32.store offset=1048884
                      local.get 2
                      local.set 6
                    end
                    local.get 2
                    local.get 3
                    i32.store offset=8
                    local.get 6
                    local.get 3
                    i32.store offset=12
                    local.get 3
                    local.get 2
                    i32.store offset=12
                    local.get 3
                    local.get 6
                    i32.store offset=8
                  end
                  i32.const 0
                  local.get 4
                  i32.store offset=1049292
                  i32.const 0
                  local.get 7
                  i32.store offset=1049284
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                  local.set 2
                  br 6 (;@1;)
                end
                i32.const 0
                local.get 5
                local.get 8
                i32.or
                i32.store offset=1048884
                local.get 6
                local.set 8
              end
              local.get 6
              local.get 4
              i32.store offset=8
              local.get 8
              local.get 4
              i32.store offset=12
              local.get 4
              local.get 6
              i32.store offset=12
              local.get 4
              local.get 8
              i32.store offset=8
            end
            i32.const 0
            local.get 7
            i32.store offset=1049292
            i32.const 0
            local.get 2
            i32.store offset=1049284
            br 1 (;@3;)
          end
          local.get 0
          local.get 2
          local.get 3
          i32.add
          call $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17haac305c878b66d79E
        end
        local.get 0
        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
        local.tee 2
        br_if 1 (;@1;)
      end
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        i32.const 0
                        i32.load offset=1049284
                        local.tee 2
                        local.get 3
                        i32.ge_u
                        br_if 0 (;@10;)
                        i32.const 0
                        i32.load offset=1049288
                        local.tee 0
                        local.get 3
                        i32.gt_u
                        br_if 4 (;@6;)
                        i32.const 0
                        local.set 2
                        local.get 1
                        i32.const 1048884
                        local.get 3
                        i32.const 0
                        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                        local.tee 0
                        i32.sub
                        local.get 0
                        i32.const 8
                        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                        i32.add
                        i32.const 20
                        i32.const 8
                        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                        i32.add
                        i32.const 16
                        i32.const 8
                        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                        i32.add
                        i32.const 8
                        i32.add
                        i32.const 65536
                        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                        call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$5alloc17h8da5b6fc9598f5eeE
                        local.get 1
                        i32.load
                        local.tee 6
                        i32.eqz
                        br_if 9 (;@1;)
                        local.get 1
                        i32.load offset=8
                        local.set 9
                        i32.const 0
                        i32.const 0
                        i32.load offset=1049300
                        local.get 1
                        i32.load offset=4
                        local.tee 8
                        i32.add
                        local.tee 0
                        i32.store offset=1049300
                        i32.const 0
                        i32.const 0
                        i32.load offset=1049304
                        local.tee 2
                        local.get 0
                        local.get 2
                        local.get 0
                        i32.gt_u
                        select
                        i32.store offset=1049304
                        i32.const 0
                        i32.load offset=1049296
                        i32.eqz
                        br_if 1 (;@9;)
                        i32.const 1049308
                        local.set 0
                        loop  ;; label = @11
                          local.get 6
                          local.get 0
                          call $_ZN8dlmalloc8dlmalloc7Segment3top17hd821f753a6e92d41E
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 0
                          i32.load offset=8
                          local.tee 0
                          br_if 0 (;@11;)
                          br 4 (;@7;)
                        end
                      end
                      i32.const 0
                      i32.load offset=1049292
                      local.set 0
                      block  ;; label = @10
                        local.get 2
                        local.get 3
                        i32.sub
                        local.tee 2
                        i32.const 16
                        i32.const 8
                        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                        i32.ge_u
                        br_if 0 (;@10;)
                        i32.const 0
                        i32.const 0
                        i32.store offset=1049292
                        i32.const 0
                        i32.load offset=1049284
                        local.set 3
                        i32.const 0
                        i32.const 0
                        i32.store offset=1049284
                        local.get 0
                        local.get 3
                        call $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17haac305c878b66d79E
                        local.get 0
                        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                        local.set 2
                        br 9 (;@1;)
                      end
                      local.get 0
                      local.get 3
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                      local.set 4
                      i32.const 0
                      local.get 2
                      i32.store offset=1049284
                      i32.const 0
                      local.get 4
                      i32.store offset=1049292
                      local.get 4
                      local.get 2
                      call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
                      local.get 0
                      local.get 3
                      call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hd978cf052c211783E
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                      local.set 2
                      br 8 (;@1;)
                    end
                    block  ;; label = @9
                      block  ;; label = @10
                        i32.const 0
                        i32.load offset=1049328
                        local.tee 0
                        i32.eqz
                        br_if 0 (;@10;)
                        local.get 6
                        local.get 0
                        i32.ge_u
                        br_if 1 (;@9;)
                      end
                      i32.const 0
                      local.get 6
                      i32.store offset=1049328
                    end
                    i32.const 0
                    i32.const 4095
                    i32.store offset=1049332
                    i32.const 0
                    local.get 9
                    i32.store offset=1049320
                    i32.const 0
                    local.get 8
                    i32.store offset=1049312
                    i32.const 0
                    local.get 6
                    i32.store offset=1049308
                    i32.const 0
                    i32.const 1048892
                    i32.store offset=1048904
                    i32.const 0
                    i32.const 1048900
                    i32.store offset=1048912
                    i32.const 0
                    i32.const 1048892
                    i32.store offset=1048900
                    i32.const 0
                    i32.const 1048908
                    i32.store offset=1048920
                    i32.const 0
                    i32.const 1048900
                    i32.store offset=1048908
                    i32.const 0
                    i32.const 1048916
                    i32.store offset=1048928
                    i32.const 0
                    i32.const 1048908
                    i32.store offset=1048916
                    i32.const 0
                    i32.const 1048924
                    i32.store offset=1048936
                    i32.const 0
                    i32.const 1048916
                    i32.store offset=1048924
                    i32.const 0
                    i32.const 1048932
                    i32.store offset=1048944
                    i32.const 0
                    i32.const 1048924
                    i32.store offset=1048932
                    i32.const 0
                    i32.const 1048940
                    i32.store offset=1048952
                    i32.const 0
                    i32.const 1048932
                    i32.store offset=1048940
                    i32.const 0
                    i32.const 1048948
                    i32.store offset=1048960
                    i32.const 0
                    i32.const 1048940
                    i32.store offset=1048948
                    i32.const 0
                    i32.const 1048956
                    i32.store offset=1048968
                    i32.const 0
                    i32.const 1048948
                    i32.store offset=1048956
                    i32.const 0
                    i32.const 1048956
                    i32.store offset=1048964
                    i32.const 0
                    i32.const 1048964
                    i32.store offset=1048976
                    i32.const 0
                    i32.const 1048964
                    i32.store offset=1048972
                    i32.const 0
                    i32.const 1048972
                    i32.store offset=1048984
                    i32.const 0
                    i32.const 1048972
                    i32.store offset=1048980
                    i32.const 0
                    i32.const 1048980
                    i32.store offset=1048992
                    i32.const 0
                    i32.const 1048980
                    i32.store offset=1048988
                    i32.const 0
                    i32.const 1048988
                    i32.store offset=1049000
                    i32.const 0
                    i32.const 1048988
                    i32.store offset=1048996
                    i32.const 0
                    i32.const 1048996
                    i32.store offset=1049008
                    i32.const 0
                    i32.const 1048996
                    i32.store offset=1049004
                    i32.const 0
                    i32.const 1049004
                    i32.store offset=1049016
                    i32.const 0
                    i32.const 1049004
                    i32.store offset=1049012
                    i32.const 0
                    i32.const 1049012
                    i32.store offset=1049024
                    i32.const 0
                    i32.const 1049012
                    i32.store offset=1049020
                    i32.const 0
                    i32.const 1049020
                    i32.store offset=1049032
                    i32.const 0
                    i32.const 1049028
                    i32.store offset=1049040
                    i32.const 0
                    i32.const 1049020
                    i32.store offset=1049028
                    i32.const 0
                    i32.const 1049036
                    i32.store offset=1049048
                    i32.const 0
                    i32.const 1049028
                    i32.store offset=1049036
                    i32.const 0
                    i32.const 1049044
                    i32.store offset=1049056
                    i32.const 0
                    i32.const 1049036
                    i32.store offset=1049044
                    i32.const 0
                    i32.const 1049052
                    i32.store offset=1049064
                    i32.const 0
                    i32.const 1049044
                    i32.store offset=1049052
                    i32.const 0
                    i32.const 1049060
                    i32.store offset=1049072
                    i32.const 0
                    i32.const 1049052
                    i32.store offset=1049060
                    i32.const 0
                    i32.const 1049068
                    i32.store offset=1049080
                    i32.const 0
                    i32.const 1049060
                    i32.store offset=1049068
                    i32.const 0
                    i32.const 1049076
                    i32.store offset=1049088
                    i32.const 0
                    i32.const 1049068
                    i32.store offset=1049076
                    i32.const 0
                    i32.const 1049084
                    i32.store offset=1049096
                    i32.const 0
                    i32.const 1049076
                    i32.store offset=1049084
                    i32.const 0
                    i32.const 1049092
                    i32.store offset=1049104
                    i32.const 0
                    i32.const 1049084
                    i32.store offset=1049092
                    i32.const 0
                    i32.const 1049100
                    i32.store offset=1049112
                    i32.const 0
                    i32.const 1049092
                    i32.store offset=1049100
                    i32.const 0
                    i32.const 1049108
                    i32.store offset=1049120
                    i32.const 0
                    i32.const 1049100
                    i32.store offset=1049108
                    i32.const 0
                    i32.const 1049116
                    i32.store offset=1049128
                    i32.const 0
                    i32.const 1049108
                    i32.store offset=1049116
                    i32.const 0
                    i32.const 1049124
                    i32.store offset=1049136
                    i32.const 0
                    i32.const 1049116
                    i32.store offset=1049124
                    i32.const 0
                    i32.const 1049132
                    i32.store offset=1049144
                    i32.const 0
                    i32.const 1049124
                    i32.store offset=1049132
                    i32.const 0
                    i32.const 1049140
                    i32.store offset=1049152
                    i32.const 0
                    i32.const 1049132
                    i32.store offset=1049140
                    i32.const 0
                    i32.const 1049140
                    i32.store offset=1049148
                    i32.const 0
                    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                    local.tee 2
                    i32.const 8
                    call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                    local.set 4
                    i32.const 20
                    i32.const 8
                    call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                    local.set 7
                    i32.const 16
                    i32.const 8
                    call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                    local.set 5
                    local.get 6
                    local.get 6
                    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                    local.tee 0
                    i32.const 8
                    call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                    local.get 0
                    i32.sub
                    local.tee 9
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                    local.set 0
                    i32.const 0
                    local.get 8
                    local.get 2
                    i32.add
                    local.get 4
                    i32.sub
                    local.get 7
                    i32.sub
                    local.get 5
                    i32.sub
                    local.get 9
                    i32.sub
                    local.tee 2
                    i32.store offset=1049288
                    i32.const 0
                    local.get 0
                    i32.store offset=1049296
                    local.get 0
                    local.get 2
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    i32.const 0
                    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                    local.tee 4
                    i32.const 8
                    call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                    local.set 7
                    i32.const 20
                    i32.const 8
                    call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                    local.set 6
                    i32.const 16
                    i32.const 8
                    call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                    local.set 8
                    local.get 0
                    local.get 2
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                    local.get 8
                    local.get 6
                    local.get 7
                    local.get 4
                    i32.sub
                    i32.add
                    i32.add
                    i32.store offset=4
                    i32.const 0
                    i32.const 2097152
                    i32.store offset=1049324
                    br 6 (;@2;)
                  end
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc7Segment9is_extern17hc9f5fc18760cb8eeE
                  br_if 0 (;@7;)
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc7Segment9sys_flags17hcb57db9d8d4f2284E
                  local.get 9
                  i32.ne
                  br_if 0 (;@7;)
                  local.get 0
                  i32.const 0
                  i32.load offset=1049296
                  call $_ZN8dlmalloc8dlmalloc7Segment5holds17h657a3112dfa72cfeE
                  br_if 2 (;@5;)
                end
                i32.const 0
                i32.const 0
                i32.load offset=1049328
                local.tee 0
                local.get 6
                local.get 6
                local.get 0
                i32.gt_u
                select
                i32.store offset=1049328
                local.get 6
                local.get 8
                i32.add
                local.set 2
                i32.const 1049308
                local.set 0
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      loop  ;; label = @10
                        local.get 0
                        i32.load
                        local.get 2
                        i32.eq
                        br_if 1 (;@9;)
                        local.get 0
                        i32.load offset=8
                        local.tee 0
                        br_if 0 (;@10;)
                        br 2 (;@8;)
                      end
                    end
                    local.get 0
                    call $_ZN8dlmalloc8dlmalloc7Segment9is_extern17hc9f5fc18760cb8eeE
                    br_if 0 (;@8;)
                    local.get 0
                    call $_ZN8dlmalloc8dlmalloc7Segment9sys_flags17hcb57db9d8d4f2284E
                    local.get 9
                    i32.eq
                    br_if 1 (;@7;)
                  end
                  i32.const 0
                  i32.load offset=1049296
                  local.set 4
                  i32.const 1049308
                  local.set 0
                  block  ;; label = @8
                    loop  ;; label = @9
                      block  ;; label = @10
                        local.get 0
                        i32.load
                        local.get 4
                        i32.gt_u
                        br_if 0 (;@10;)
                        local.get 0
                        call $_ZN8dlmalloc8dlmalloc7Segment3top17hd821f753a6e92d41E
                        local.get 4
                        i32.gt_u
                        br_if 2 (;@8;)
                      end
                      local.get 0
                      i32.load offset=8
                      local.tee 0
                      br_if 0 (;@9;)
                    end
                    i32.const 0
                    local.set 0
                  end
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc7Segment3top17hd821f753a6e92d41E
                  local.tee 7
                  i32.const 20
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  local.tee 10
                  i32.sub
                  i32.const -23
                  i32.add
                  local.set 0
                  local.get 4
                  local.get 0
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                  local.tee 2
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  local.get 2
                  i32.sub
                  i32.add
                  local.tee 0
                  local.get 0
                  local.get 4
                  i32.const 16
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  i32.add
                  i32.lt_u
                  select
                  local.tee 5
                  call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                  local.set 2
                  local.get 5
                  local.get 10
                  call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                  local.set 0
                  i32.const 0
                  call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                  local.tee 11
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  local.set 12
                  i32.const 20
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  local.set 13
                  i32.const 16
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  local.set 14
                  local.get 6
                  local.get 6
                  call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                  local.tee 15
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  local.get 15
                  i32.sub
                  local.tee 16
                  call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                  local.set 15
                  i32.const 0
                  local.get 8
                  local.get 11
                  i32.add
                  local.get 12
                  i32.sub
                  local.get 13
                  i32.sub
                  local.get 14
                  i32.sub
                  local.get 16
                  i32.sub
                  local.tee 11
                  i32.store offset=1049288
                  i32.const 0
                  local.get 15
                  i32.store offset=1049296
                  local.get 15
                  local.get 11
                  i32.const 1
                  i32.or
                  i32.store offset=4
                  i32.const 0
                  call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                  local.tee 12
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  local.set 13
                  i32.const 20
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  local.set 14
                  i32.const 16
                  i32.const 8
                  call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                  local.set 16
                  local.get 15
                  local.get 11
                  call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                  local.get 16
                  local.get 14
                  local.get 13
                  local.get 12
                  i32.sub
                  i32.add
                  i32.add
                  i32.store offset=4
                  i32.const 0
                  i32.const 2097152
                  i32.store offset=1049324
                  local.get 5
                  local.get 10
                  call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hd978cf052c211783E
                  i32.const 0
                  i64.load offset=1049308 align=4
                  local.set 17
                  local.get 2
                  i32.const 8
                  i32.add
                  i32.const 0
                  i64.load offset=1049316 align=4
                  i64.store align=4
                  local.get 2
                  local.get 17
                  i64.store align=4
                  i32.const 0
                  local.get 9
                  i32.store offset=1049320
                  i32.const 0
                  local.get 8
                  i32.store offset=1049312
                  i32.const 0
                  local.get 6
                  i32.store offset=1049308
                  i32.const 0
                  local.get 2
                  i32.store offset=1049316
                  loop  ;; label = @8
                    local.get 0
                    i32.const 4
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                    local.set 2
                    local.get 0
                    call $_ZN8dlmalloc8dlmalloc5Chunk14fencepost_head17h4398ad35e48866cdE
                    i32.store offset=4
                    local.get 2
                    local.set 0
                    local.get 7
                    local.get 2
                    i32.const 4
                    i32.add
                    i32.gt_u
                    br_if 0 (;@8;)
                  end
                  local.get 5
                  local.get 4
                  i32.eq
                  br_if 5 (;@2;)
                  local.get 5
                  local.get 4
                  i32.sub
                  local.set 0
                  local.get 4
                  local.get 0
                  local.get 4
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                  call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17hf0551f771db2db55E
                  block  ;; label = @8
                    local.get 0
                    i32.const 256
                    i32.lt_u
                    br_if 0 (;@8;)
                    local.get 4
                    local.get 0
                    call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17he1b0e8180890506aE
                    br 6 (;@2;)
                  end
                  local.get 0
                  i32.const 3
                  i32.shr_u
                  local.tee 2
                  i32.const 3
                  i32.shl
                  i32.const 1048892
                  i32.add
                  local.set 0
                  block  ;; label = @8
                    block  ;; label = @9
                      i32.const 0
                      i32.load offset=1048884
                      local.tee 7
                      i32.const 1
                      local.get 2
                      i32.shl
                      local.tee 2
                      i32.and
                      i32.eqz
                      br_if 0 (;@9;)
                      local.get 0
                      i32.load offset=8
                      local.set 2
                      br 1 (;@8;)
                    end
                    i32.const 0
                    local.get 7
                    local.get 2
                    i32.or
                    i32.store offset=1048884
                    local.get 0
                    local.set 2
                  end
                  local.get 0
                  local.get 4
                  i32.store offset=8
                  local.get 2
                  local.get 4
                  i32.store offset=12
                  local.get 4
                  local.get 0
                  i32.store offset=12
                  local.get 4
                  local.get 2
                  i32.store offset=8
                  br 5 (;@2;)
                end
                local.get 0
                i32.load
                local.set 7
                local.get 0
                local.get 6
                i32.store
                local.get 0
                local.get 0
                i32.load offset=4
                local.get 8
                i32.add
                i32.store offset=4
                local.get 6
                call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                local.tee 0
                i32.const 8
                call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                local.set 2
                local.get 7
                call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                local.tee 8
                i32.const 8
                call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                local.set 5
                local.get 6
                local.get 2
                local.get 0
                i32.sub
                i32.add
                local.tee 2
                local.get 3
                call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                local.set 4
                local.get 2
                local.get 3
                call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hd978cf052c211783E
                local.get 7
                local.get 5
                local.get 8
                i32.sub
                i32.add
                local.tee 0
                local.get 2
                i32.sub
                local.get 3
                i32.sub
                local.set 3
                i32.const 0
                i32.load offset=1049296
                local.get 0
                i32.eq
                br_if 2 (;@4;)
                i32.const 0
                i32.load offset=1049292
                local.get 0
                i32.eq
                br_if 3 (;@3;)
                block  ;; label = @7
                  local.get 0
                  call $_ZN8dlmalloc8dlmalloc5Chunk5inuse17h997eeb1ac8180a07E
                  br_if 0 (;@7;)
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
                      local.tee 7
                      i32.const 256
                      i32.lt_u
                      br_if 0 (;@9;)
                      local.get 0
                      call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E
                      br 1 (;@8;)
                    end
                    block  ;; label = @9
                      local.get 0
                      i32.const 12
                      i32.add
                      i32.load
                      local.tee 6
                      local.get 0
                      i32.const 8
                      i32.add
                      i32.load
                      local.tee 8
                      i32.eq
                      br_if 0 (;@9;)
                      local.get 8
                      local.get 6
                      i32.store offset=12
                      local.get 6
                      local.get 8
                      i32.store offset=8
                      br 1 (;@8;)
                    end
                    i32.const 0
                    i32.const 0
                    i32.load offset=1048884
                    i32.const -2
                    local.get 7
                    i32.const 3
                    i32.shr_u
                    i32.rotl
                    i32.and
                    i32.store offset=1048884
                  end
                  local.get 7
                  local.get 3
                  i32.add
                  local.set 3
                  local.get 0
                  local.get 7
                  call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                  local.set 0
                end
                local.get 4
                local.get 3
                local.get 0
                call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17hf0551f771db2db55E
                block  ;; label = @7
                  local.get 3
                  i32.const 256
                  i32.lt_u
                  br_if 0 (;@7;)
                  local.get 4
                  local.get 3
                  call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17he1b0e8180890506aE
                  local.get 2
                  call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                  local.set 2
                  br 6 (;@1;)
                end
                local.get 3
                i32.const 3
                i32.shr_u
                local.tee 3
                i32.const 3
                i32.shl
                i32.const 1048892
                i32.add
                local.set 0
                block  ;; label = @7
                  block  ;; label = @8
                    i32.const 0
                    i32.load offset=1048884
                    local.tee 7
                    i32.const 1
                    local.get 3
                    i32.shl
                    local.tee 3
                    i32.and
                    i32.eqz
                    br_if 0 (;@8;)
                    local.get 0
                    i32.load offset=8
                    local.set 3
                    br 1 (;@7;)
                  end
                  i32.const 0
                  local.get 7
                  local.get 3
                  i32.or
                  i32.store offset=1048884
                  local.get 0
                  local.set 3
                end
                local.get 0
                local.get 4
                i32.store offset=8
                local.get 3
                local.get 4
                i32.store offset=12
                local.get 4
                local.get 0
                i32.store offset=12
                local.get 4
                local.get 3
                i32.store offset=8
                local.get 2
                call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
                local.set 2
                br 5 (;@1;)
              end
              i32.const 0
              local.get 0
              local.get 3
              i32.sub
              local.tee 2
              i32.store offset=1049288
              i32.const 0
              i32.const 0
              i32.load offset=1049296
              local.tee 0
              local.get 3
              call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
              local.tee 4
              i32.store offset=1049296
              local.get 4
              local.get 2
              i32.const 1
              i32.or
              i32.store offset=4
              local.get 0
              local.get 3
              call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hd978cf052c211783E
              local.get 0
              call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
              local.set 2
              br 4 (;@1;)
            end
            local.get 0
            local.get 0
            i32.load offset=4
            local.get 8
            i32.add
            i32.store offset=4
            i32.const 0
            i32.load offset=1049288
            local.set 2
            i32.const 0
            i32.load offset=1049296
            local.set 0
            local.get 0
            local.get 0
            call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
            local.tee 4
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.get 4
            i32.sub
            local.tee 4
            call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
            local.set 0
            i32.const 0
            local.get 2
            local.get 8
            i32.add
            local.get 4
            i32.sub
            local.tee 2
            i32.store offset=1049288
            i32.const 0
            local.get 0
            i32.store offset=1049296
            local.get 0
            local.get 2
            i32.const 1
            i32.or
            i32.store offset=4
            i32.const 0
            call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
            local.tee 4
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 7
            i32.const 20
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 6
            i32.const 16
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 8
            local.get 0
            local.get 2
            call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
            local.get 8
            local.get 6
            local.get 7
            local.get 4
            i32.sub
            i32.add
            i32.add
            i32.store offset=4
            i32.const 0
            i32.const 2097152
            i32.store offset=1049324
            br 2 (;@2;)
          end
          i32.const 0
          local.get 4
          i32.store offset=1049296
          i32.const 0
          i32.const 0
          i32.load offset=1049288
          local.get 3
          i32.add
          local.tee 0
          i32.store offset=1049288
          local.get 4
          local.get 0
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 2
          call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
          local.set 2
          br 2 (;@1;)
        end
        i32.const 0
        local.get 4
        i32.store offset=1049292
        i32.const 0
        i32.const 0
        i32.load offset=1049284
        local.get 3
        i32.add
        local.tee 0
        i32.store offset=1049284
        local.get 4
        local.get 0
        call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
        local.get 2
        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
        local.set 2
        br 1 (;@1;)
      end
      i32.const 0
      local.set 2
      i32.const 0
      i32.load offset=1049288
      local.tee 0
      local.get 3
      i32.le_u
      br_if 0 (;@1;)
      i32.const 0
      local.get 0
      local.get 3
      i32.sub
      local.tee 2
      i32.store offset=1049288
      i32.const 0
      i32.const 0
      i32.load offset=1049296
      local.tee 0
      local.get 3
      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
      local.tee 4
      i32.store offset=1049296
      local.get 4
      local.get 2
      i32.const 1
      i32.or
      i32.store offset=4
      local.get 0
      local.get 3
      call $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hd978cf052c211783E
      local.get 0
      call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
      local.set 2
    end
    local.get 1
    i32.const 16
    i32.add
    global.set 0
    local.get 2)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17h68843c482a778867E (type 0) (param i32 i32)
    (local i32 i32 i32 i32)
    local.get 0
    local.get 1
    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
    local.set 2
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk6pinuse17h9c470725d1a420e8E
          br_if 0 (;@3;)
          local.get 0
          i32.load
          local.set 3
          block  ;; label = @4
            block  ;; label = @5
              local.get 0
              call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17heb33c93ad8506845E
              br_if 0 (;@5;)
              local.get 3
              local.get 1
              i32.add
              local.set 1
              local.get 0
              local.get 3
              call $_ZN8dlmalloc8dlmalloc5Chunk12minus_offset17hbd5bbeda7048d425E
              local.tee 0
              i32.const 0
              i32.load offset=1049292
              i32.ne
              br_if 1 (;@4;)
              local.get 2
              i32.load offset=4
              i32.const 3
              i32.and
              i32.const 3
              i32.ne
              br_if 2 (;@3;)
              i32.const 0
              local.get 1
              i32.store offset=1049284
              local.get 0
              local.get 1
              local.get 2
              call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17hf0551f771db2db55E
              return
            end
            i32.const 1048884
            local.get 0
            local.get 3
            i32.sub
            local.get 3
            local.get 1
            i32.add
            i32.const 16
            i32.add
            local.tee 0
            call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$4free17h862b88854ea6869cE
            i32.eqz
            br_if 2 (;@2;)
            i32.const 0
            i32.const 0
            i32.load offset=1049300
            local.get 0
            i32.sub
            i32.store offset=1049300
            return
          end
          block  ;; label = @4
            local.get 3
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 0
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 0
            i32.const 12
            i32.add
            i32.load
            local.tee 4
            local.get 0
            i32.const 8
            i32.add
            i32.load
            local.tee 5
            i32.eq
            br_if 0 (;@4;)
            local.get 5
            local.get 4
            i32.store offset=12
            local.get 4
            local.get 5
            i32.store offset=8
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1048884
          i32.const -2
          local.get 3
          i32.const 3
          i32.shr_u
          i32.rotl
          i32.and
          i32.store offset=1048884
        end
        block  ;; label = @3
          local.get 2
          call $_ZN8dlmalloc8dlmalloc5Chunk6cinuse17hbba15e47aaf97771E
          i32.eqz
          br_if 0 (;@3;)
          local.get 0
          local.get 1
          local.get 2
          call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17hf0551f771db2db55E
          br 2 (;@1;)
        end
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 0
            i32.load offset=1049296
            i32.eq
            br_if 0 (;@4;)
            local.get 2
            i32.const 0
            i32.load offset=1049292
            i32.ne
            br_if 1 (;@3;)
            i32.const 0
            local.get 0
            i32.store offset=1049292
            i32.const 0
            i32.const 0
            i32.load offset=1049284
            local.get 1
            i32.add
            local.tee 1
            i32.store offset=1049284
            local.get 0
            local.get 1
            call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
            return
          end
          i32.const 0
          local.get 0
          i32.store offset=1049296
          i32.const 0
          i32.const 0
          i32.load offset=1049288
          local.get 1
          i32.add
          local.tee 1
          i32.store offset=1049288
          local.get 0
          local.get 1
          i32.const 1
          i32.or
          i32.store offset=4
          local.get 0
          i32.const 0
          i32.load offset=1049292
          i32.ne
          br_if 1 (;@2;)
          i32.const 0
          i32.const 0
          i32.store offset=1049284
          i32.const 0
          i32.const 0
          i32.store offset=1049292
          return
        end
        local.get 2
        call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
        local.tee 3
        local.get 1
        i32.add
        local.set 1
        block  ;; label = @3
          block  ;; label = @4
            local.get 3
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 2
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 2
            i32.const 12
            i32.add
            i32.load
            local.tee 4
            local.get 2
            i32.const 8
            i32.add
            i32.load
            local.tee 2
            i32.eq
            br_if 0 (;@4;)
            local.get 2
            local.get 4
            i32.store offset=12
            local.get 4
            local.get 2
            i32.store offset=8
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1048884
          i32.const -2
          local.get 3
          i32.const 3
          i32.shr_u
          i32.rotl
          i32.and
          i32.store offset=1048884
        end
        local.get 0
        local.get 1
        call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
        local.get 0
        i32.const 0
        i32.load offset=1049292
        i32.ne
        br_if 1 (;@1;)
        i32.const 0
        local.get 1
        i32.store offset=1049284
      end
      return
    end
    block  ;; label = @1
      local.get 1
      i32.const 256
      i32.lt_u
      br_if 0 (;@1;)
      local.get 0
      local.get 1
      call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17he1b0e8180890506aE
      return
    end
    local.get 1
    i32.const 3
    i32.shr_u
    local.tee 2
    i32.const 3
    i32.shl
    i32.const 1048892
    i32.add
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        i32.const 0
        i32.load offset=1048884
        local.tee 3
        i32.const 1
        local.get 2
        i32.shl
        local.tee 2
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load offset=8
        local.set 2
        br 1 (;@1;)
      end
      i32.const 0
      local.get 3
      local.get 2
      i32.or
      i32.store offset=1048884
      local.get 1
      local.set 2
    end
    local.get 1
    local.get 0
    i32.store offset=8
    local.get 2
    local.get 0
    i32.store offset=12
    local.get 0
    local.get 1
    i32.store offset=12
    local.get 0
    local.get 2
    i32.store offset=8)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E (type 9) (param i32)
    (local i32 i32 i32 i32 i32)
    local.get 0
    i32.load offset=24
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc9TreeChunk4next17ha4e1b089f5bbada0E
          local.get 0
          i32.ne
          br_if 0 (;@3;)
          local.get 0
          i32.const 20
          i32.const 16
          local.get 0
          i32.const 20
          i32.add
          local.tee 2
          i32.load
          local.tee 3
          select
          i32.add
          i32.load
          local.tee 4
          br_if 1 (;@2;)
          i32.const 0
          local.set 3
          br 2 (;@1;)
        end
        local.get 0
        call $_ZN8dlmalloc8dlmalloc9TreeChunk4prev17h1ff2aa549c95c1feE
        local.tee 4
        local.get 0
        call $_ZN8dlmalloc8dlmalloc9TreeChunk4next17ha4e1b089f5bbada0E
        local.tee 3
        call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
        i32.store offset=12
        local.get 3
        local.get 4
        call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
        i32.store offset=8
        br 1 (;@1;)
      end
      local.get 2
      local.get 0
      i32.const 16
      i32.add
      local.get 3
      select
      local.set 2
      loop  ;; label = @2
        local.get 2
        local.set 5
        block  ;; label = @3
          local.get 4
          local.tee 3
          i32.const 20
          i32.add
          local.tee 2
          i32.load
          local.tee 4
          br_if 0 (;@3;)
          local.get 3
          i32.const 16
          i32.add
          local.set 2
          local.get 3
          i32.load offset=16
          local.set 4
        end
        local.get 4
        br_if 0 (;@2;)
      end
      local.get 5
      i32.const 0
      i32.store
    end
    block  ;; label = @1
      local.get 1
      i32.eqz
      br_if 0 (;@1;)
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          i32.load offset=28
          i32.const 2
          i32.shl
          i32.const 1049156
          i32.add
          local.tee 4
          i32.load
          local.get 0
          i32.eq
          br_if 0 (;@3;)
          local.get 1
          i32.const 16
          i32.const 20
          local.get 1
          i32.load offset=16
          local.get 0
          i32.eq
          select
          i32.add
          local.get 3
          i32.store
          local.get 3
          br_if 1 (;@2;)
          br 2 (;@1;)
        end
        local.get 4
        local.get 3
        i32.store
        local.get 3
        br_if 0 (;@2;)
        i32.const 0
        i32.const 0
        i32.load offset=1048888
        i32.const -2
        local.get 0
        i32.load offset=28
        i32.rotl
        i32.and
        i32.store offset=1048888
        return
      end
      local.get 3
      local.get 1
      i32.store offset=24
      block  ;; label = @2
        local.get 0
        i32.load offset=16
        local.tee 4
        i32.eqz
        br_if 0 (;@2;)
        local.get 3
        local.get 4
        i32.store offset=16
        local.get 4
        local.get 3
        i32.store offset=24
      end
      local.get 0
      i32.const 20
      i32.add
      i32.load
      local.tee 4
      i32.eqz
      br_if 0 (;@1;)
      local.get 3
      i32.const 20
      i32.add
      local.get 4
      i32.store
      local.get 4
      local.get 3
      i32.store offset=24
      return
    end)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17he1b0e8180890506aE (type 0) (param i32 i32)
    (local i32 i32 i32 i32 i32)
    i32.const 0
    local.set 2
    block  ;; label = @1
      local.get 1
      i32.const 8
      i32.shr_u
      local.tee 3
      i32.eqz
      br_if 0 (;@1;)
      i32.const 31
      local.set 2
      local.get 1
      i32.const 16777215
      i32.gt_u
      br_if 0 (;@1;)
      local.get 1
      i32.const 6
      local.get 3
      i32.clz
      local.tee 2
      i32.sub
      i32.const 31
      i32.and
      i32.shr_u
      i32.const 1
      i32.and
      local.get 2
      i32.const 1
      i32.shl
      i32.sub
      i32.const 62
      i32.add
      local.set 2
    end
    local.get 0
    i64.const 0
    i64.store offset=16 align=4
    local.get 0
    local.get 2
    i32.store offset=28
    local.get 2
    i32.const 2
    i32.shl
    i32.const 1049156
    i32.add
    local.set 3
    local.get 0
    call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            block  ;; label = @5
              i32.const 0
              i32.load offset=1048888
              local.tee 5
              i32.const 1
              local.get 2
              i32.const 31
              i32.and
              i32.shl
              local.tee 6
              i32.and
              i32.eqz
              br_if 0 (;@5;)
              local.get 3
              i32.load
              local.set 5
              local.get 2
              call $_ZN8dlmalloc8dlmalloc24leftshift_for_tree_index17h3fccfa1fd0332fcaE
              local.set 2
              local.get 5
              call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
              call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
              local.get 1
              i32.ne
              br_if 1 (;@4;)
              local.get 5
              local.set 2
              br 2 (;@3;)
            end
            i32.const 0
            local.get 5
            local.get 6
            i32.or
            i32.store offset=1048888
            local.get 3
            local.get 0
            i32.store
            local.get 0
            local.get 3
            i32.store offset=24
            br 3 (;@1;)
          end
          local.get 1
          local.get 2
          i32.const 31
          i32.and
          i32.shl
          local.set 3
          loop  ;; label = @4
            local.get 5
            local.get 3
            i32.const 29
            i32.shr_u
            i32.const 4
            i32.and
            i32.add
            i32.const 16
            i32.add
            local.tee 6
            i32.load
            local.tee 2
            i32.eqz
            br_if 2 (;@2;)
            local.get 3
            i32.const 1
            i32.shl
            local.set 3
            local.get 2
            local.set 5
            local.get 2
            call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
            call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
            local.get 1
            i32.ne
            br_if 0 (;@4;)
          end
        end
        local.get 2
        call $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE
        local.tee 2
        i32.load offset=8
        local.tee 3
        local.get 4
        i32.store offset=12
        local.get 2
        local.get 4
        i32.store offset=8
        local.get 4
        local.get 2
        i32.store offset=12
        local.get 4
        local.get 3
        i32.store offset=8
        local.get 0
        i32.const 0
        i32.store offset=24
        return
      end
      local.get 6
      local.get 0
      i32.store
      local.get 0
      local.get 5
      i32.store offset=24
    end
    local.get 4
    local.get 4
    i32.store offset=8
    local.get 4
    local.get 4
    i32.store offset=12)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$23release_unused_segments17h9c54ff4f0de5916eE (type 6) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      i32.const 0
      i32.load offset=1049316
      local.tee 0
      br_if 0 (;@1;)
      i32.const 0
      i32.const 4095
      i32.store offset=1049332
      i32.const 0
      return
    end
    i32.const 1049308
    local.set 1
    i32.const 0
    local.set 2
    i32.const 0
    local.set 3
    loop  ;; label = @1
      local.get 0
      local.tee 4
      i32.load offset=8
      local.set 0
      local.get 4
      i32.load offset=4
      local.set 5
      local.get 4
      i32.load
      local.set 6
      block  ;; label = @2
        block  ;; label = @3
          i32.const 1048884
          local.get 4
          i32.const 12
          i32.add
          i32.load
          i32.const 1
          i32.shr_u
          call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$16can_release_part17h6036300ce92b71c2E
          br_if 0 (;@3;)
          local.get 4
          local.set 1
          br 1 (;@2;)
        end
        block  ;; label = @3
          local.get 4
          call $_ZN8dlmalloc8dlmalloc7Segment9is_extern17hc9f5fc18760cb8eeE
          i32.eqz
          br_if 0 (;@3;)
          local.get 4
          local.set 1
          br 1 (;@2;)
        end
        local.get 6
        local.get 6
        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
        local.tee 7
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        local.get 7
        i32.sub
        i32.add
        local.tee 7
        call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
        local.set 8
        i32.const 0
        call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
        local.tee 9
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        local.set 10
        i32.const 20
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        local.set 11
        i32.const 16
        i32.const 8
        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
        local.set 12
        block  ;; label = @3
          local.get 7
          call $_ZN8dlmalloc8dlmalloc5Chunk5inuse17h997eeb1ac8180a07E
          i32.eqz
          br_if 0 (;@3;)
          local.get 4
          local.set 1
          br 1 (;@2;)
        end
        block  ;; label = @3
          local.get 7
          local.get 8
          i32.add
          local.get 6
          local.get 5
          local.get 9
          i32.add
          local.get 10
          i32.sub
          local.get 11
          i32.sub
          local.get 12
          i32.sub
          i32.add
          i32.ge_u
          br_if 0 (;@3;)
          local.get 4
          local.set 1
          br 1 (;@2;)
        end
        block  ;; label = @3
          block  ;; label = @4
            i32.const 0
            i32.load offset=1049292
            local.get 7
            i32.eq
            br_if 0 (;@4;)
            local.get 7
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.store offset=1049284
          i32.const 0
          i32.const 0
          i32.store offset=1049292
        end
        block  ;; label = @3
          i32.const 1048884
          local.get 6
          local.get 5
          call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$4free17h862b88854ea6869cE
          br_if 0 (;@3;)
          local.get 7
          local.get 8
          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17he1b0e8180890506aE
          local.get 4
          local.set 1
          br 1 (;@2;)
        end
        i32.const 0
        i32.const 0
        i32.load offset=1049300
        local.get 5
        i32.sub
        i32.store offset=1049300
        local.get 1
        local.get 0
        i32.store offset=8
        local.get 5
        local.get 2
        i32.add
        local.set 2
      end
      local.get 3
      i32.const 1
      i32.add
      local.set 3
      local.get 0
      br_if 0 (;@1;)
    end
    i32.const 0
    local.get 3
    i32.const 4095
    local.get 3
    i32.const 4095
    i32.gt_u
    select
    i32.store offset=1049332
    local.get 2)
  (func $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$4free17h2e0354f65f071533E (type 9) (param i32)
    (local i32 i32 i32 i32 i32 i32)
    local.get 0
    call $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h325ccd9f977bf5d6E
    local.set 0
    local.get 0
    local.get 0
    call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
    local.tee 1
    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
    local.set 2
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk6pinuse17h9c470725d1a420e8E
          br_if 0 (;@3;)
          local.get 0
          i32.load
          local.set 3
          block  ;; label = @4
            block  ;; label = @5
              local.get 0
              call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17heb33c93ad8506845E
              br_if 0 (;@5;)
              local.get 3
              local.get 1
              i32.add
              local.set 1
              local.get 0
              local.get 3
              call $_ZN8dlmalloc8dlmalloc5Chunk12minus_offset17hbd5bbeda7048d425E
              local.tee 0
              i32.const 0
              i32.load offset=1049292
              i32.ne
              br_if 1 (;@4;)
              local.get 2
              i32.load offset=4
              i32.const 3
              i32.and
              i32.const 3
              i32.ne
              br_if 2 (;@3;)
              i32.const 0
              local.get 1
              i32.store offset=1049284
              local.get 0
              local.get 1
              local.get 2
              call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17hf0551f771db2db55E
              return
            end
            i32.const 1048884
            local.get 0
            local.get 3
            i32.sub
            local.get 3
            local.get 1
            i32.add
            i32.const 16
            i32.add
            local.tee 0
            call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$4free17h862b88854ea6869cE
            i32.eqz
            br_if 2 (;@2;)
            i32.const 0
            i32.const 0
            i32.load offset=1049300
            local.get 0
            i32.sub
            i32.store offset=1049300
            return
          end
          block  ;; label = @4
            local.get 3
            i32.const 256
            i32.lt_u
            br_if 0 (;@4;)
            local.get 0
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E
            br 1 (;@3;)
          end
          block  ;; label = @4
            local.get 0
            i32.const 12
            i32.add
            i32.load
            local.tee 4
            local.get 0
            i32.const 8
            i32.add
            i32.load
            local.tee 5
            i32.eq
            br_if 0 (;@4;)
            local.get 5
            local.get 4
            i32.store offset=12
            local.get 4
            local.get 5
            i32.store offset=8
            br 1 (;@3;)
          end
          i32.const 0
          i32.const 0
          i32.load offset=1048884
          i32.const -2
          local.get 3
          i32.const 3
          i32.shr_u
          i32.rotl
          i32.and
          i32.store offset=1048884
        end
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            call $_ZN8dlmalloc8dlmalloc5Chunk6cinuse17hbba15e47aaf97771E
            i32.eqz
            br_if 0 (;@4;)
            local.get 0
            local.get 1
            local.get 2
            call $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17hf0551f771db2db55E
            br 1 (;@3;)
          end
          block  ;; label = @4
            block  ;; label = @5
              local.get 2
              i32.const 0
              i32.load offset=1049296
              i32.eq
              br_if 0 (;@5;)
              local.get 2
              i32.const 0
              i32.load offset=1049292
              i32.ne
              br_if 1 (;@4;)
              i32.const 0
              local.get 0
              i32.store offset=1049292
              i32.const 0
              i32.const 0
              i32.load offset=1049284
              local.get 1
              i32.add
              local.tee 1
              i32.store offset=1049284
              local.get 0
              local.get 1
              call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
              return
            end
            i32.const 0
            local.get 0
            i32.store offset=1049296
            i32.const 0
            i32.const 0
            i32.load offset=1049288
            local.get 1
            i32.add
            local.tee 1
            i32.store offset=1049288
            local.get 0
            local.get 1
            i32.const 1
            i32.or
            i32.store offset=4
            block  ;; label = @5
              local.get 0
              i32.const 0
              i32.load offset=1049292
              i32.ne
              br_if 0 (;@5;)
              i32.const 0
              i32.const 0
              i32.store offset=1049284
              i32.const 0
              i32.const 0
              i32.store offset=1049292
            end
            i32.const 0
            i32.load offset=1049324
            local.get 1
            i32.ge_u
            br_if 2 (;@2;)
            i32.const 0
            call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
            local.tee 0
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 1
            i32.const 20
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 2
            i32.const 16
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 3
            i32.const 16
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 4
            i32.const 0
            i32.load offset=1049296
            i32.eqz
            br_if 2 (;@2;)
            local.get 0
            local.get 1
            i32.sub
            local.get 2
            i32.sub
            local.get 3
            i32.sub
            i32.const -65544
            i32.add
            i32.const -9
            i32.and
            i32.const -3
            i32.add
            local.tee 0
            i32.const 0
            local.get 4
            i32.const 2
            i32.shl
            i32.sub
            local.tee 1
            local.get 1
            local.get 0
            i32.gt_u
            select
            i32.eqz
            br_if 2 (;@2;)
            i32.const 0
            call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
            local.tee 0
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 1
            i32.const 20
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 3
            i32.const 16
            i32.const 8
            call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
            local.set 4
            i32.const 0
            local.set 2
            block  ;; label = @5
              i32.const 0
              i32.load offset=1049288
              local.tee 5
              local.get 4
              local.get 3
              local.get 1
              local.get 0
              i32.sub
              i32.add
              i32.add
              local.tee 0
              i32.le_u
              br_if 0 (;@5;)
              local.get 5
              local.get 0
              i32.const -1
              i32.xor
              i32.add
              i32.const -65536
              i32.and
              local.set 3
              i32.const 0
              i32.load offset=1049296
              local.set 1
              i32.const 1049308
              local.set 0
              block  ;; label = @6
                loop  ;; label = @7
                  block  ;; label = @8
                    local.get 0
                    i32.load
                    local.get 1
                    i32.gt_u
                    br_if 0 (;@8;)
                    local.get 0
                    call $_ZN8dlmalloc8dlmalloc7Segment3top17hd821f753a6e92d41E
                    local.get 1
                    i32.gt_u
                    br_if 2 (;@6;)
                  end
                  local.get 0
                  i32.load offset=8
                  local.tee 0
                  br_if 0 (;@7;)
                end
                i32.const 0
                local.set 0
              end
              i32.const 0
              local.set 2
              local.get 0
              call $_ZN8dlmalloc8dlmalloc7Segment9is_extern17hc9f5fc18760cb8eeE
              br_if 0 (;@5;)
              i32.const 1048884
              local.get 0
              i32.const 12
              i32.add
              i32.load
              i32.const 1
              i32.shr_u
              call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$16can_release_part17h6036300ce92b71c2E
              i32.eqz
              br_if 0 (;@5;)
              local.get 0
              i32.load offset=4
              local.get 3
              i32.lt_u
              br_if 0 (;@5;)
              i32.const 1049308
              local.set 1
              loop  ;; label = @6
                local.get 0
                local.get 1
                call $_ZN8dlmalloc8dlmalloc7Segment5holds17h657a3112dfa72cfeE
                br_if 1 (;@5;)
                local.get 1
                i32.load offset=8
                local.tee 1
                br_if 0 (;@6;)
              end
              i32.const 1048884
              local.get 0
              i32.load
              local.get 0
              i32.load offset=4
              local.tee 1
              local.get 1
              local.get 3
              i32.sub
              call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$9free_part17hf800c5052cf9e7acE
              local.set 1
              local.get 3
              i32.eqz
              br_if 0 (;@5;)
              local.get 1
              i32.eqz
              br_if 0 (;@5;)
              local.get 0
              local.get 0
              i32.load offset=4
              local.get 3
              i32.sub
              i32.store offset=4
              i32.const 0
              i32.const 0
              i32.load offset=1049300
              local.get 3
              i32.sub
              i32.store offset=1049300
              i32.const 0
              i32.load offset=1049288
              local.set 1
              i32.const 0
              i32.load offset=1049296
              local.set 0
              local.get 0
              local.get 0
              call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
              local.tee 2
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
              local.get 2
              i32.sub
              local.tee 2
              call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
              local.set 0
              i32.const 0
              local.get 1
              local.get 3
              i32.sub
              local.get 2
              i32.sub
              local.tee 1
              i32.store offset=1049288
              i32.const 0
              local.get 0
              i32.store offset=1049296
              local.get 0
              local.get 1
              i32.const 1
              i32.or
              i32.store offset=4
              i32.const 0
              call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
              local.tee 2
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
              local.set 4
              i32.const 20
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
              local.set 5
              i32.const 16
              i32.const 8
              call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
              local.set 6
              local.get 0
              local.get 1
              call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
              local.get 6
              local.get 5
              local.get 4
              local.get 2
              i32.sub
              i32.add
              i32.add
              i32.store offset=4
              i32.const 0
              i32.const 2097152
              i32.store offset=1049324
              local.get 3
              local.set 2
            end
            local.get 2
            i32.const 0
            call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$23release_unused_segments17h9c54ff4f0de5916eE
            i32.sub
            i32.ne
            br_if 2 (;@2;)
            i32.const 0
            i32.load offset=1049288
            i32.const 0
            i32.load offset=1049324
            i32.le_u
            br_if 2 (;@2;)
            i32.const 0
            i32.const -1
            i32.store offset=1049324
            return
          end
          local.get 2
          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
          local.tee 3
          local.get 1
          i32.add
          local.set 1
          block  ;; label = @4
            block  ;; label = @5
              local.get 3
              i32.const 256
              i32.lt_u
              br_if 0 (;@5;)
              local.get 2
              call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E
              br 1 (;@4;)
            end
            block  ;; label = @5
              local.get 2
              i32.const 12
              i32.add
              i32.load
              local.tee 4
              local.get 2
              i32.const 8
              i32.add
              i32.load
              local.tee 2
              i32.eq
              br_if 0 (;@5;)
              local.get 2
              local.get 4
              i32.store offset=12
              local.get 4
              local.get 2
              i32.store offset=8
              br 1 (;@4;)
            end
            i32.const 0
            i32.const 0
            i32.load offset=1048884
            i32.const -2
            local.get 3
            i32.const 3
            i32.shr_u
            i32.rotl
            i32.and
            i32.store offset=1048884
          end
          local.get 0
          local.get 1
          call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
          local.get 0
          i32.const 0
          i32.load offset=1049292
          i32.ne
          br_if 0 (;@3;)
          i32.const 0
          local.get 1
          i32.store offset=1049284
          br 1 (;@2;)
        end
        local.get 1
        i32.const 256
        i32.lt_u
        br_if 1 (;@1;)
        local.get 0
        local.get 1
        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18insert_large_chunk17he1b0e8180890506aE
        i32.const 0
        i32.const 0
        i32.load offset=1049332
        i32.const -1
        i32.add
        local.tee 0
        i32.store offset=1049332
        local.get 0
        br_if 0 (;@2;)
        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$23release_unused_segments17h9c54ff4f0de5916eE
        drop
        return
      end
      return
    end
    local.get 1
    i32.const 3
    i32.shr_u
    local.tee 2
    i32.const 3
    i32.shl
    i32.const 1048892
    i32.add
    local.set 1
    block  ;; label = @1
      block  ;; label = @2
        i32.const 0
        i32.load offset=1048884
        local.tee 3
        i32.const 1
        local.get 2
        i32.shl
        local.tee 2
        i32.and
        i32.eqz
        br_if 0 (;@2;)
        local.get 1
        i32.load offset=8
        local.set 2
        br 1 (;@1;)
      end
      i32.const 0
      local.get 3
      local.get 2
      i32.or
      i32.store offset=1048884
      local.get 1
      local.set 2
    end
    local.get 1
    local.get 0
    i32.store offset=8
    local.get 2
    local.get 0
    i32.store offset=12
    local.get 0
    local.get 1
    i32.store offset=12
    local.get 0
    local.get 2
    i32.store offset=8)
  (func $_ZN3std10sys_common9backtrace26__rust_end_short_backtrace17hbe5e6af58f564f9dE (type 9) (param i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    local.get 0
    i32.load offset=8
    call $_ZN3std9panicking19begin_panic_handler28_$u7b$$u7b$closure$u7d$$u7d$17h930198390374894cE
    unreachable)
  (func $_ZN3std9panicking19begin_panic_handler28_$u7b$$u7b$closure$u7d$$u7d$17h930198390374894cE (type 5) (param i32 i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 3
    global.set 0
    local.get 0
    i32.const 20
    i32.add
    i32.load
    local.set 4
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 0
            i32.const 4
            i32.add
            i32.load
            br_table 0 (;@4;) 1 (;@3;) 3 (;@1;)
          end
          local.get 4
          br_if 2 (;@1;)
          i32.const 1048600
          local.set 0
          i32.const 0
          local.set 4
          br 1 (;@2;)
        end
        local.get 4
        br_if 1 (;@1;)
        local.get 0
        i32.load
        local.tee 0
        i32.load offset=4
        local.set 4
        local.get 0
        i32.load
        local.set 0
      end
      local.get 3
      local.get 4
      i32.store offset=4
      local.get 3
      local.get 0
      i32.store
      local.get 3
      i32.const 1048732
      local.get 1
      call $_ZN4core5panic10panic_info9PanicInfo7message17h1e9de86ac7eb9f53E
      local.get 2
      call $_ZN3std9panicking20rust_panic_with_hook17h91eba802db76d52aE
      unreachable
    end
    local.get 3
    i32.const 0
    i32.store offset=4
    local.get 3
    local.get 0
    i32.store
    local.get 3
    i32.const 1048712
    local.get 1
    call $_ZN4core5panic10panic_info9PanicInfo7message17h1e9de86ac7eb9f53E
    local.get 2
    call $_ZN3std9panicking20rust_panic_with_hook17h91eba802db76d52aE
    unreachable)
  (func $_ZN3std5alloc24default_alloc_error_hook17ha9d99781a59146d7E (type 0) (param i32 i32))
  (func $rust_oom (type 0) (param i32 i32)
    (local i32)
    local.get 0
    local.get 1
    i32.const 0
    i32.load offset=1048864
    local.tee 2
    i32.const 1
    local.get 2
    select
    call_indirect (type 0)
    unreachable
    unreachable)
  (func $__rdl_alloc (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    call $_ZN8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h70fb551a667bd6b2E)
  (func $__rdl_dealloc (type 5) (param i32 i32 i32)
    local.get 0
    call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$4free17h2e0354f65f071533E)
  (func $__rdl_realloc (type 7) (param i32 i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32)
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.const 9
            i32.lt_u
            br_if 0 (;@4;)
            local.get 3
            local.get 2
            call $_ZN8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h70fb551a667bd6b2E
            local.tee 2
            br_if 1 (;@3;)
            i32.const 0
            return
          end
          i32.const 0
          local.set 2
          i32.const 0
          call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E
          local.set 1
          local.get 1
          local.get 1
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          i32.sub
          i32.const 20
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          i32.sub
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          i32.sub
          i32.const -65544
          i32.add
          i32.const -9
          i32.and
          i32.const -3
          i32.add
          local.tee 1
          i32.const 0
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          i32.const 2
          i32.shl
          i32.sub
          local.tee 4
          local.get 4
          local.get 1
          i32.gt_u
          select
          local.get 3
          i32.le_u
          br_if 1 (;@2;)
          i32.const 16
          local.get 3
          i32.const 4
          i32.add
          i32.const 16
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          i32.const -5
          i32.add
          local.get 3
          i32.gt_u
          select
          i32.const 8
          call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
          local.set 4
          local.get 0
          call $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h325ccd9f977bf5d6E
          local.set 1
          local.get 1
          local.get 1
          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
          local.tee 5
          call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
          local.set 6
          block  ;; label = @4
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  block  ;; label = @8
                    block  ;; label = @9
                      block  ;; label = @10
                        block  ;; label = @11
                          local.get 1
                          call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17heb33c93ad8506845E
                          br_if 0 (;@11;)
                          local.get 5
                          local.get 4
                          i32.ge_u
                          br_if 1 (;@10;)
                          local.get 6
                          i32.const 0
                          i32.load offset=1049296
                          i32.eq
                          br_if 2 (;@9;)
                          local.get 6
                          i32.const 0
                          i32.load offset=1049292
                          i32.eq
                          br_if 3 (;@8;)
                          local.get 6
                          call $_ZN8dlmalloc8dlmalloc5Chunk6cinuse17hbba15e47aaf97771E
                          br_if 7 (;@4;)
                          local.get 6
                          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
                          local.tee 7
                          local.get 5
                          i32.add
                          local.tee 5
                          local.get 4
                          i32.lt_u
                          br_if 7 (;@4;)
                          local.get 5
                          local.get 4
                          i32.sub
                          local.set 8
                          local.get 7
                          i32.const 256
                          i32.lt_u
                          br_if 4 (;@7;)
                          local.get 6
                          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$18unlink_large_chunk17hb7b819a2b83e62f2E
                          br 5 (;@6;)
                        end
                        local.get 1
                        call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
                        local.set 5
                        local.get 4
                        i32.const 256
                        i32.lt_u
                        br_if 6 (;@4;)
                        block  ;; label = @11
                          local.get 5
                          local.get 4
                          i32.const 4
                          i32.add
                          i32.lt_u
                          br_if 0 (;@11;)
                          local.get 5
                          local.get 4
                          i32.sub
                          i32.const 131073
                          i32.lt_u
                          br_if 6 (;@5;)
                        end
                        i32.const 1048884
                        local.get 1
                        local.get 1
                        i32.load
                        local.tee 6
                        i32.sub
                        local.get 5
                        local.get 6
                        i32.add
                        i32.const 16
                        i32.add
                        local.tee 7
                        local.get 4
                        i32.const 31
                        i32.add
                        i32.const 1048884
                        call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$9page_size17h89ed2c4e643b1943E
                        call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                        local.tee 5
                        i32.const 1
                        call $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$5remap17hb51e4f563159d79eE
                        local.tee 4
                        i32.eqz
                        br_if 6 (;@4;)
                        local.get 4
                        local.get 6
                        i32.add
                        local.tee 1
                        local.get 5
                        local.get 6
                        i32.sub
                        local.tee 3
                        i32.const -16
                        i32.add
                        local.tee 2
                        i32.store offset=4
                        call $_ZN8dlmalloc8dlmalloc5Chunk14fencepost_head17h4398ad35e48866cdE
                        local.set 0
                        local.get 1
                        local.get 2
                        call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                        local.get 0
                        i32.store offset=4
                        local.get 1
                        local.get 3
                        i32.const -12
                        i32.add
                        call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                        i32.const 0
                        i32.store offset=4
                        i32.const 0
                        i32.const 0
                        i32.load offset=1049300
                        local.get 5
                        local.get 7
                        i32.sub
                        i32.add
                        local.tee 3
                        i32.store offset=1049300
                        i32.const 0
                        i32.const 0
                        i32.load offset=1049328
                        local.tee 2
                        local.get 4
                        local.get 4
                        local.get 2
                        i32.gt_u
                        select
                        i32.store offset=1049328
                        i32.const 0
                        i32.const 0
                        i32.load offset=1049304
                        local.tee 2
                        local.get 3
                        local.get 2
                        local.get 3
                        i32.gt_u
                        select
                        i32.store offset=1049304
                        br 9 (;@1;)
                      end
                      local.get 5
                      local.get 4
                      i32.sub
                      local.tee 5
                      i32.const 16
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                      i32.lt_u
                      br_if 4 (;@5;)
                      local.get 1
                      local.get 4
                      call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                      local.set 6
                      local.get 1
                      local.get 4
                      call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
                      local.get 6
                      local.get 5
                      call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
                      local.get 6
                      local.get 5
                      call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17h68843c482a778867E
                      br 4 (;@5;)
                    end
                    i32.const 0
                    i32.load offset=1049288
                    local.get 5
                    i32.add
                    local.tee 5
                    local.get 4
                    i32.le_u
                    br_if 4 (;@4;)
                    local.get 1
                    local.get 4
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                    local.set 6
                    local.get 1
                    local.get 4
                    call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
                    local.get 6
                    local.get 5
                    local.get 4
                    i32.sub
                    local.tee 4
                    i32.const 1
                    i32.or
                    i32.store offset=4
                    i32.const 0
                    local.get 4
                    i32.store offset=1049288
                    i32.const 0
                    local.get 6
                    i32.store offset=1049296
                    br 3 (;@5;)
                  end
                  i32.const 0
                  i32.load offset=1049284
                  local.get 5
                  i32.add
                  local.tee 5
                  local.get 4
                  i32.lt_u
                  br_if 3 (;@4;)
                  block  ;; label = @8
                    block  ;; label = @9
                      local.get 5
                      local.get 4
                      i32.sub
                      local.tee 6
                      i32.const 16
                      i32.const 8
                      call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                      i32.ge_u
                      br_if 0 (;@9;)
                      local.get 1
                      local.get 5
                      call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
                      i32.const 0
                      local.set 6
                      i32.const 0
                      local.set 5
                      br 1 (;@8;)
                    end
                    local.get 1
                    local.get 4
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                    local.tee 5
                    local.get 6
                    call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                    local.set 7
                    local.get 1
                    local.get 4
                    call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
                    local.get 5
                    local.get 6
                    call $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E
                    local.get 7
                    call $_ZN8dlmalloc8dlmalloc5Chunk12clear_pinuse17h874db9915d25264aE
                  end
                  i32.const 0
                  local.get 5
                  i32.store offset=1049292
                  i32.const 0
                  local.get 6
                  i32.store offset=1049284
                  br 2 (;@5;)
                end
                block  ;; label = @7
                  local.get 6
                  i32.const 12
                  i32.add
                  i32.load
                  local.tee 9
                  local.get 6
                  i32.const 8
                  i32.add
                  i32.load
                  local.tee 6
                  i32.eq
                  br_if 0 (;@7;)
                  local.get 6
                  local.get 9
                  i32.store offset=12
                  local.get 9
                  local.get 6
                  i32.store offset=8
                  br 1 (;@6;)
                end
                i32.const 0
                i32.const 0
                i32.load offset=1048884
                i32.const -2
                local.get 7
                i32.const 3
                i32.shr_u
                i32.rotl
                i32.and
                i32.store offset=1048884
              end
              block  ;; label = @6
                local.get 8
                i32.const 16
                i32.const 8
                call $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E
                i32.lt_u
                br_if 0 (;@6;)
                local.get 1
                local.get 4
                call $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE
                local.set 5
                local.get 1
                local.get 4
                call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
                local.get 5
                local.get 8
                call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
                local.get 5
                local.get 8
                call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$13dispose_chunk17h68843c482a778867E
                br 1 (;@5;)
              end
              local.get 1
              local.get 5
              call $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E
            end
            local.get 1
            br_if 3 (;@1;)
          end
          local.get 3
          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$6malloc17h558ac717aff7de04E
          local.tee 4
          i32.eqz
          br_if 1 (;@2;)
          local.get 4
          local.get 0
          local.get 3
          local.get 1
          call $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE
          i32.const -8
          i32.const -4
          local.get 1
          call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17heb33c93ad8506845E
          select
          i32.add
          local.tee 2
          local.get 2
          local.get 3
          i32.gt_u
          select
          call $memcpy
          local.set 3
          local.get 0
          call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$4free17h2e0354f65f071533E
          local.get 3
          return
        end
        local.get 2
        local.get 0
        local.get 3
        local.get 1
        local.get 1
        local.get 3
        i32.gt_u
        select
        call $memcpy
        drop
        local.get 0
        call $_ZN8dlmalloc8dlmalloc17Dlmalloc$LT$A$GT$4free17h2e0354f65f071533E
      end
      local.get 2
      return
    end
    local.get 1
    call $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17heb33c93ad8506845E
    drop
    local.get 1
    call $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E)
  (func $rust_begin_unwind (type 9) (param i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 1
    global.set 0
    local.get 0
    call $_ZN4core5panic10panic_info9PanicInfo8location17hb53158190fff4feaE
    i32.const 1048680
    call $_ZN4core6option15Option$LT$T$GT$6unwrap17hc525709d044cea0bE
    local.set 2
    local.get 0
    call $_ZN4core5panic10panic_info9PanicInfo7message17h1e9de86ac7eb9f53E
    call $_ZN4core6option15Option$LT$T$GT$6unwrap17h06fd127e5ae45399E
    local.set 3
    local.get 1
    local.get 2
    i32.store offset=8
    local.get 1
    local.get 0
    i32.store offset=4
    local.get 1
    local.get 3
    i32.store
    local.get 1
    call $_ZN3std10sys_common9backtrace26__rust_end_short_backtrace17hbe5e6af58f564f9dE
    unreachable)
  (func $_ZN3std9panicking20rust_panic_with_hook17h91eba802db76d52aE (type 4) (param i32 i32 i32 i32)
    (local i32 i32 i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 4
    global.set 0
    i32.const 1
    local.set 5
    i32.const 0
    i32.const 0
    i32.load offset=1048880
    local.tee 6
    i32.const 1
    i32.add
    i32.store offset=1048880
    block  ;; label = @1
      block  ;; label = @2
        i32.const 0
        i32.load offset=1049336
        i32.const 1
        i32.ne
        br_if 0 (;@2;)
        i32.const 0
        i32.load offset=1049340
        i32.const 1
        i32.add
        local.set 5
        br 1 (;@1;)
      end
      i32.const 0
      i32.const 1
      i32.store offset=1049336
    end
    i32.const 0
    local.get 5
    i32.store offset=1049340
    block  ;; label = @1
      block  ;; label = @2
        local.get 6
        i32.const 0
        i32.lt_s
        br_if 0 (;@2;)
        local.get 5
        i32.const 2
        i32.gt_u
        br_if 0 (;@2;)
        local.get 4
        local.get 3
        i32.store offset=28
        local.get 4
        local.get 2
        i32.store offset=24
        i32.const 0
        i32.load offset=1048868
        local.tee 6
        i32.const -1
        i32.le_s
        br_if 0 (;@2;)
        i32.const 0
        local.get 6
        i32.const 1
        i32.add
        local.tee 6
        i32.store offset=1048868
        block  ;; label = @3
          i32.const 0
          i32.load offset=1048876
          local.tee 2
          i32.eqz
          br_if 0 (;@3;)
          i32.const 0
          i32.load offset=1048872
          local.set 6
          local.get 4
          i32.const 8
          i32.add
          local.get 0
          local.get 1
          i32.load offset=16
          call_indirect (type 0)
          local.get 4
          local.get 4
          i64.load offset=8
          i64.store offset=16
          local.get 6
          local.get 4
          i32.const 16
          i32.add
          local.get 2
          i32.load offset=20
          call_indirect (type 0)
          i32.const 0
          i32.load offset=1048868
          local.set 6
        end
        i32.const 0
        local.get 6
        i32.const -1
        i32.add
        i32.store offset=1048868
        local.get 5
        i32.const 1
        i32.le_u
        br_if 1 (;@1;)
      end
      unreachable
      unreachable
    end
    local.get 0
    local.get 1
    call $rust_panic
    unreachable)
  (func $_ZN90_$LT$std..panicking..begin_panic_handler..PanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17h8c74629b35ff2b51E (type 0) (param i32 i32)
    (local i32 i32 i32 i32 i64)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    local.get 1
    i32.const 4
    i32.add
    local.set 3
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.load offset=4
        i32.eqz
        br_if 0 (;@2;)
        i32.const 0
        i32.load offset=1048644
        local.set 4
        br 1 (;@1;)
      end
      local.get 1
      i32.load
      local.set 5
      local.get 2
      i64.const 0
      i64.store offset=12 align=4
      local.get 2
      i32.const 0
      i32.load offset=1048644
      local.tee 4
      i32.store offset=8
      local.get 2
      local.get 2
      i32.const 8
      i32.add
      i32.store offset=20
      local.get 2
      i32.const 24
      i32.add
      i32.const 16
      i32.add
      local.get 5
      i32.const 16
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      i32.const 24
      i32.add
      i32.const 8
      i32.add
      local.get 5
      i32.const 8
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      local.get 5
      i64.load align=4
      i64.store offset=24
      local.get 2
      i32.const 20
      i32.add
      i32.const 1048576
      local.get 2
      i32.const 24
      i32.add
      call $_ZN4core3fmt5write17h0923b640446a3c30E
      drop
      local.get 3
      i32.const 8
      i32.add
      local.get 2
      i32.const 8
      i32.add
      i32.const 8
      i32.add
      i32.load
      i32.store
      local.get 3
      local.get 2
      i64.load offset=8
      i64.store align=4
    end
    local.get 2
    i32.const 24
    i32.add
    i32.const 8
    i32.add
    local.tee 5
    local.get 3
    i32.const 8
    i32.add
    i32.load
    i32.store
    local.get 1
    i32.const 12
    i32.add
    i32.const 0
    i32.store
    local.get 3
    i64.load align=4
    local.set 6
    local.get 1
    i32.const 8
    i32.add
    i32.const 0
    i32.store
    local.get 1
    local.get 4
    i32.store offset=4
    local.get 2
    local.get 6
    i64.store offset=24
    block  ;; label = @1
      i32.const 12
      i32.const 4
      call $__rust_alloc
      local.tee 1
      br_if 0 (;@1;)
      i32.const 12
      i32.const 4
      call $_ZN5alloc5alloc18handle_alloc_error17h6e739d356a100151E
      unreachable
    end
    local.get 1
    local.get 2
    i64.load offset=24
    i64.store align=4
    local.get 1
    i32.const 8
    i32.add
    local.get 5
    i32.load
    i32.store
    local.get 0
    i32.const 1048752
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store
    local.get 2
    i32.const 48
    i32.add
    global.set 0)
  (func $_ZN90_$LT$std..panicking..begin_panic_handler..PanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$3get17h79b0afd46bbc90abE (type 0) (param i32 i32)
    (local i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 2
    global.set 0
    local.get 1
    i32.const 4
    i32.add
    local.set 3
    block  ;; label = @1
      local.get 1
      i32.load offset=4
      br_if 0 (;@1;)
      local.get 1
      i32.load
      local.set 1
      local.get 2
      i64.const 0
      i64.store offset=12 align=4
      local.get 2
      i32.const 0
      i32.load offset=1048644
      i32.store offset=8
      local.get 2
      local.get 2
      i32.const 8
      i32.add
      i32.store offset=20
      local.get 2
      i32.const 24
      i32.add
      i32.const 16
      i32.add
      local.get 1
      i32.const 16
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      i32.const 24
      i32.add
      i32.const 8
      i32.add
      local.get 1
      i32.const 8
      i32.add
      i64.load align=4
      i64.store
      local.get 2
      local.get 1
      i64.load align=4
      i64.store offset=24
      local.get 2
      i32.const 20
      i32.add
      i32.const 1048576
      local.get 2
      i32.const 24
      i32.add
      call $_ZN4core3fmt5write17h0923b640446a3c30E
      drop
      local.get 3
      i32.const 8
      i32.add
      local.get 2
      i32.const 8
      i32.add
      i32.const 8
      i32.add
      i32.load
      i32.store
      local.get 3
      local.get 2
      i64.load offset=8
      i64.store align=4
    end
    local.get 0
    i32.const 1048752
    i32.store offset=4
    local.get 0
    local.get 3
    i32.store
    local.get 2
    i32.const 48
    i32.add
    global.set 0)
  (func $_ZN93_$LT$std..panicking..begin_panic_handler..StrPanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17hc0b16d96ecfa69f3E (type 0) (param i32 i32)
    (local i32 i32)
    local.get 1
    i32.load offset=4
    local.set 2
    local.get 1
    i32.load
    local.set 3
    block  ;; label = @1
      i32.const 8
      i32.const 4
      call $__rust_alloc
      local.tee 1
      br_if 0 (;@1;)
      i32.const 8
      i32.const 4
      call $_ZN5alloc5alloc18handle_alloc_error17h6e739d356a100151E
      unreachable
    end
    local.get 1
    local.get 2
    i32.store offset=4
    local.get 1
    local.get 3
    i32.store
    local.get 0
    i32.const 1048768
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store)
  (func $_ZN93_$LT$std..panicking..begin_panic_handler..StrPanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$3get17h22a824a2e9bdb720E (type 0) (param i32 i32)
    local.get 0
    i32.const 1048768
    i32.store offset=4
    local.get 0
    local.get 1
    i32.store)
  (func $rust_panic (type 0) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 1
    i32.store offset=12
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 2
    i32.const 8
    i32.add
    call $__rust_start_panic
    drop
    unreachable
    unreachable)
  (func $__rust_start_panic (type 10) (param i32) (result i32)
    unreachable
    unreachable)
  (func $_ZN8dlmalloc8dlmalloc8align_up17he7b28a5456cff036E (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.add
    i32.const -1
    i32.add
    i32.const 0
    local.get 1
    i32.sub
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc9left_bits17h80354a56302cce1fE (type 10) (param i32) (result i32)
    local.get 0
    i32.const 1
    i32.shl
    local.tee 0
    i32.const 0
    local.get 0
    i32.sub
    i32.or)
  (func $_ZN8dlmalloc8dlmalloc9least_bit17he2fbd395f0b4f5d7E (type 10) (param i32) (result i32)
    i32.const 0
    local.get 0
    i32.sub
    local.get 0
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc24leftshift_for_tree_index17h3fccfa1fd0332fcaE (type 10) (param i32) (result i32)
    i32.const 0
    i32.const 25
    local.get 0
    i32.const 1
    i32.shr_u
    i32.sub
    local.get 0
    i32.const 31
    i32.eq
    select)
  (func $_ZN8dlmalloc8dlmalloc5Chunk14fencepost_head17h4398ad35e48866cdE (type 6) (result i32)
    i32.const 7)
  (func $_ZN8dlmalloc8dlmalloc5Chunk4size17hc43a8859fdb6521cE (type 10) (param i32) (result i32)
    local.get 0
    i32.load offset=4
    i32.const -8
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc5Chunk6cinuse17hbba15e47aaf97771E (type 10) (param i32) (result i32)
    local.get 0
    i32.load8_u offset=4
    i32.const 2
    i32.and
    i32.const 1
    i32.shr_u)
  (func $_ZN8dlmalloc8dlmalloc5Chunk6pinuse17h9c470725d1a420e8E (type 10) (param i32) (result i32)
    local.get 0
    i32.load offset=4
    i32.const 1
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc5Chunk12clear_pinuse17h874db9915d25264aE (type 9) (param i32)
    local.get 0
    local.get 0
    i32.load offset=4
    i32.const -2
    i32.and
    i32.store offset=4)
  (func $_ZN8dlmalloc8dlmalloc5Chunk5inuse17h997eeb1ac8180a07E (type 10) (param i32) (result i32)
    local.get 0
    i32.load offset=4
    i32.const 3
    i32.and
    i32.const 1
    i32.ne)
  (func $_ZN8dlmalloc8dlmalloc5Chunk7mmapped17heb33c93ad8506845E (type 10) (param i32) (result i32)
    local.get 0
    i32.load8_u offset=4
    i32.const 3
    i32.and
    i32.eqz)
  (func $_ZN8dlmalloc8dlmalloc5Chunk9set_inuse17h1f2d98cf3b655af9E (type 0) (param i32 i32)
    local.get 0
    local.get 0
    i32.load offset=4
    i32.const 1
    i32.and
    local.get 1
    i32.or
    i32.const 2
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.tee 0
    local.get 0
    i32.load offset=4
    i32.const 1
    i32.or
    i32.store offset=4)
  (func $_ZN8dlmalloc8dlmalloc5Chunk20set_inuse_and_pinuse17haac305c878b66d79E (type 0) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 3
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.tee 0
    local.get 0
    i32.load offset=4
    i32.const 1
    i32.or
    i32.store offset=4)
  (func $_ZN8dlmalloc8dlmalloc5Chunk34set_size_and_pinuse_of_inuse_chunk17hd978cf052c211783E (type 0) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 3
    i32.or
    i32.store offset=4)
  (func $_ZN8dlmalloc8dlmalloc5Chunk33set_size_and_pinuse_of_free_chunk17h88f68ccdcc961a25E (type 0) (param i32 i32)
    local.get 0
    local.get 1
    i32.const 1
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.get 1
    i32.store)
  (func $_ZN8dlmalloc8dlmalloc5Chunk20set_free_with_pinuse17hf0551f771db2db55E (type 5) (param i32 i32 i32)
    local.get 2
    local.get 2
    i32.load offset=4
    i32.const -2
    i32.and
    i32.store offset=4
    local.get 0
    local.get 1
    i32.const 1
    i32.or
    i32.store offset=4
    local.get 0
    local.get 1
    i32.add
    local.get 1
    i32.store)
  (func $_ZN8dlmalloc8dlmalloc5Chunk11plus_offset17h0404d8b05278f70eE (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.add)
  (func $_ZN8dlmalloc8dlmalloc5Chunk12minus_offset17hbd5bbeda7048d425E (type 2) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.sub)
  (func $_ZN8dlmalloc8dlmalloc5Chunk6to_mem17h03b11f6a04db6f57E (type 10) (param i32) (result i32)
    local.get 0
    i32.const 8
    i32.add)
  (func $_ZN8dlmalloc8dlmalloc5Chunk8from_mem17h325ccd9f977bf5d6E (type 10) (param i32) (result i32)
    local.get 0
    i32.const -8
    i32.add)
  (func $_ZN8dlmalloc8dlmalloc9TreeChunk14leftmost_child17h2daf7c374aefdac8E (type 10) (param i32) (result i32)
    (local i32)
    block  ;; label = @1
      local.get 0
      i32.load offset=16
      local.tee 1
      br_if 0 (;@1;)
      local.get 0
      i32.const 20
      i32.add
      i32.load
      local.set 1
    end
    local.get 1)
  (func $_ZN8dlmalloc8dlmalloc9TreeChunk5chunk17h6fde8adb1989012cE (type 10) (param i32) (result i32)
    local.get 0)
  (func $_ZN8dlmalloc8dlmalloc9TreeChunk4next17ha4e1b089f5bbada0E (type 10) (param i32) (result i32)
    local.get 0
    i32.load offset=12)
  (func $_ZN8dlmalloc8dlmalloc9TreeChunk4prev17h1ff2aa549c95c1feE (type 10) (param i32) (result i32)
    local.get 0
    i32.load offset=8)
  (func $_ZN8dlmalloc8dlmalloc7Segment9is_extern17hc9f5fc18760cb8eeE (type 10) (param i32) (result i32)
    local.get 0
    i32.load offset=12
    i32.const 1
    i32.and)
  (func $_ZN8dlmalloc8dlmalloc7Segment9sys_flags17hcb57db9d8d4f2284E (type 10) (param i32) (result i32)
    local.get 0
    i32.load offset=12
    i32.const 1
    i32.shr_u)
  (func $_ZN8dlmalloc8dlmalloc7Segment5holds17h657a3112dfa72cfeE (type 2) (param i32 i32) (result i32)
    (local i32 i32)
    i32.const 0
    local.set 2
    block  ;; label = @1
      local.get 0
      i32.load
      local.tee 3
      local.get 1
      i32.gt_u
      br_if 0 (;@1;)
      local.get 3
      local.get 0
      i32.load offset=4
      i32.add
      local.get 1
      i32.gt_u
      local.set 2
    end
    local.get 2)
  (func $_ZN8dlmalloc8dlmalloc7Segment3top17hd821f753a6e92d41E (type 10) (param i32) (result i32)
    local.get 0
    i32.load
    local.get 0
    i32.load offset=4
    i32.add)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$5alloc17h8da5b6fc9598f5eeE (type 5) (param i32 i32 i32)
    (local i32)
    local.get 2
    i32.const 16
    i32.shr_u
    memory.grow
    local.set 3
    local.get 0
    i32.const 0
    i32.store offset=8
    local.get 0
    i32.const 0
    local.get 2
    i32.const -65536
    i32.and
    local.get 3
    i32.const -1
    i32.eq
    local.tee 2
    select
    i32.store offset=4
    local.get 0
    i32.const 0
    local.get 3
    i32.const 16
    i32.shl
    local.get 2
    select
    i32.store)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$5remap17hb51e4f563159d79eE (type 11) (param i32 i32 i32 i32 i32) (result i32)
    i32.const 0)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$9free_part17hf800c5052cf9e7acE (type 7) (param i32 i32 i32 i32) (result i32)
    i32.const 0)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$4free17h862b88854ea6869cE (type 1) (param i32 i32 i32) (result i32)
    i32.const 0)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$16can_release_part17h6036300ce92b71c2E (type 2) (param i32 i32) (result i32)
    i32.const 0)
  (func $_ZN61_$LT$dlmalloc..sys..System$u20$as$u20$dlmalloc..Allocator$GT$9page_size17h89ed2c4e643b1943E (type 10) (param i32) (result i32)
    i32.const 65536)
  (func $_ZN5alloc5alloc18handle_alloc_error17h6e739d356a100151E (type 0) (param i32 i32)
    local.get 0
    local.get 1
    call $__rust_alloc_error_handler
    unreachable)
  (func $__rg_oom (type 0) (param i32 i32)
    local.get 0
    local.get 1
    call $rust_oom
    unreachable)
  (func $_ZN5alloc7raw_vec17capacity_overflow17h30bd6b88dca05d1aE (type 12)
    i32.const 1048812
    i32.const 17
    i32.const 1048832
    call $_ZN4core9panicking5panic17h855ff10bbf7bb4ddE
    unreachable)
  (func $_ZN4core3ops8function6FnOnce9call_once17h9b0d4f1f1a0375c1E (type 2) (param i32 i32) (result i32)
    local.get 0
    i32.load
    drop
    loop (result i32)  ;; label = @1
      br 0 (;@1;)
    end)
  (func $_ZN4core3ptr102drop_in_place$LT$$RF$core..iter..adapters..copied..Copied$LT$core..slice..iter..Iter$LT$u8$GT$$GT$$GT$17he2cc0633e1828163E (type 9) (param i32))
  (func $_ZN4core9panicking5panic17h855ff10bbf7bb4ddE (type 5) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    i32.const 20
    i32.add
    i32.const 0
    i32.store
    local.get 3
    i32.const 1048848
    i32.store offset=16
    local.get 3
    i64.const 1
    i64.store offset=4 align=4
    local.get 3
    local.get 1
    i32.store offset=28
    local.get 3
    local.get 0
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 24
    i32.add
    i32.store
    local.get 3
    local.get 2
    call $_ZN4core9panicking9panic_fmt17hec165b0930ecb32eE
    unreachable)
  (func $_ZN4core9panicking9panic_fmt17hec165b0930ecb32eE (type 0) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 1
    i32.store offset=12
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 2
    i32.const 1048848
    i32.store offset=4
    local.get 2
    i32.const 1048848
    i32.store
    local.get 2
    call $rust_begin_unwind
    unreachable)
  (func $_ZN4core3fmt5write17h0923b640446a3c30E (type 1) (param i32 i32 i32) (result i32)
    (local i32 i32 i32 i32 i32 i32 i32 i32 i32 i32)
    global.get 0
    i32.const 48
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    i32.const 36
    i32.add
    local.get 1
    i32.store
    local.get 3
    i32.const 3
    i32.store8 offset=40
    local.get 3
    i64.const 137438953472
    i64.store offset=8
    local.get 3
    local.get 0
    i32.store offset=32
    i32.const 0
    local.set 1
    local.get 3
    i32.const 0
    i32.store offset=24
    local.get 3
    i32.const 0
    i32.store offset=16
    block  ;; label = @1
      block  ;; label = @2
        block  ;; label = @3
          block  ;; label = @4
            local.get 2
            i32.load offset=8
            local.tee 0
            br_if 0 (;@4;)
            local.get 2
            i32.load
            local.set 4
            local.get 2
            i32.load offset=4
            local.tee 5
            local.get 2
            i32.const 20
            i32.add
            i32.load
            local.tee 0
            local.get 0
            local.get 5
            i32.gt_u
            select
            local.tee 6
            i32.eqz
            br_if 1 (;@3;)
            local.get 2
            i32.load offset=16
            local.set 7
            i32.const 0
            local.set 0
            local.get 6
            local.set 1
            loop  ;; label = @5
              block  ;; label = @6
                local.get 4
                local.get 0
                i32.add
                local.tee 8
                i32.const 4
                i32.add
                i32.load
                local.tee 9
                i32.eqz
                br_if 0 (;@6;)
                local.get 3
                i32.load offset=32
                local.get 8
                i32.load
                local.get 9
                local.get 3
                i32.load offset=36
                i32.load offset=12
                call_indirect (type 1)
                br_if 4 (;@2;)
              end
              local.get 7
              local.get 0
              i32.add
              local.tee 8
              i32.load
              local.get 3
              i32.const 8
              i32.add
              local.get 8
              i32.const 4
              i32.add
              i32.load
              call_indirect (type 2)
              br_if 3 (;@2;)
              local.get 0
              i32.const 8
              i32.add
              local.set 0
              local.get 1
              i32.const -1
              i32.add
              local.tee 1
              br_if 0 (;@5;)
            end
            local.get 6
            local.set 1
            br 1 (;@3;)
          end
          local.get 2
          i32.load
          local.set 4
          local.get 2
          i32.load offset=4
          local.tee 5
          local.get 2
          i32.const 12
          i32.add
          i32.load
          local.tee 8
          local.get 8
          local.get 5
          i32.gt_u
          select
          local.tee 10
          i32.eqz
          br_if 0 (;@3;)
          local.get 10
          local.set 11
          local.get 4
          local.set 1
          loop  ;; label = @4
            block  ;; label = @5
              local.get 1
              i32.const 4
              i32.add
              i32.load
              local.tee 8
              i32.eqz
              br_if 0 (;@5;)
              local.get 3
              i32.load offset=32
              local.get 1
              i32.load
              local.get 8
              local.get 3
              i32.load offset=36
              i32.load offset=12
              call_indirect (type 1)
              br_if 3 (;@2;)
            end
            local.get 3
            local.get 0
            i32.const 28
            i32.add
            i32.load8_u
            i32.store8 offset=40
            local.get 3
            local.get 0
            i32.const 4
            i32.add
            i64.load align=4
            i64.const 32
            i64.rotl
            i64.store offset=8
            local.get 0
            i32.const 24
            i32.add
            i32.load
            local.set 8
            local.get 2
            i32.load offset=16
            local.set 7
            i32.const 0
            local.set 6
            i32.const 0
            local.set 9
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 0
                  i32.const 20
                  i32.add
                  i32.load
                  br_table 1 (;@6;) 0 (;@7;) 2 (;@5;) 1 (;@6;)
                end
                local.get 8
                i32.const 3
                i32.shl
                local.set 12
                i32.const 0
                local.set 9
                local.get 7
                local.get 12
                i32.add
                local.tee 12
                i32.load offset=4
                i32.const 14
                i32.ne
                br_if 1 (;@5;)
                local.get 12
                i32.load
                i32.load
                local.set 8
              end
              i32.const 1
              local.set 9
            end
            local.get 3
            local.get 8
            i32.store offset=20
            local.get 3
            local.get 9
            i32.store offset=16
            local.get 0
            i32.const 16
            i32.add
            i32.load
            local.set 8
            block  ;; label = @5
              block  ;; label = @6
                block  ;; label = @7
                  local.get 0
                  i32.const 12
                  i32.add
                  i32.load
                  br_table 1 (;@6;) 0 (;@7;) 2 (;@5;) 1 (;@6;)
                end
                local.get 8
                i32.const 3
                i32.shl
                local.set 9
                local.get 7
                local.get 9
                i32.add
                local.tee 9
                i32.load offset=4
                i32.const 14
                i32.ne
                br_if 1 (;@5;)
                local.get 9
                i32.load
                i32.load
                local.set 8
              end
              i32.const 1
              local.set 6
            end
            local.get 3
            local.get 8
            i32.store offset=28
            local.get 3
            local.get 6
            i32.store offset=24
            local.get 7
            local.get 0
            i32.load
            i32.const 3
            i32.shl
            i32.add
            local.tee 8
            i32.load
            local.get 3
            i32.const 8
            i32.add
            local.get 8
            i32.load offset=4
            call_indirect (type 2)
            br_if 2 (;@2;)
            local.get 0
            i32.const 32
            i32.add
            local.set 0
            local.get 1
            i32.const 8
            i32.add
            local.set 1
            local.get 11
            i32.const -1
            i32.add
            local.tee 11
            br_if 0 (;@4;)
          end
          local.get 10
          local.set 1
        end
        block  ;; label = @3
          local.get 1
          local.get 5
          i32.ge_u
          br_if 0 (;@3;)
          local.get 3
          i32.load offset=32
          local.get 4
          local.get 1
          i32.const 3
          i32.shl
          i32.add
          local.tee 0
          i32.load
          local.get 0
          i32.load offset=4
          local.get 3
          i32.load offset=36
          i32.load offset=12
          call_indirect (type 1)
          br_if 1 (;@2;)
        end
        i32.const 0
        local.set 0
        br 1 (;@1;)
      end
      i32.const 1
      local.set 0
    end
    local.get 3
    i32.const 48
    i32.add
    global.set 0
    local.get 0)
  (func $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17hcb495b7c373d7078E (type 8) (param i32) (result i64)
    i64.const -1028311936265645192)
  (func $_ZN4core5panic10panic_info9PanicInfo7message17h1e9de86ac7eb9f53E (type 10) (param i32) (result i32)
    local.get 0
    i32.load offset=8)
  (func $_ZN4core5panic10panic_info9PanicInfo8location17hb53158190fff4feaE (type 10) (param i32) (result i32)
    local.get 0
    i32.load offset=12)
  (func $memcpy (type 1) (param i32 i32 i32) (result i32)
    (local i32)
    block  ;; label = @1
      local.get 2
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.set 3
      loop  ;; label = @2
        local.get 3
        local.get 1
        i32.load8_u
        i32.store8
        local.get 1
        i32.const 1
        i32.add
        local.set 1
        local.get 3
        i32.const 1
        i32.add
        local.set 3
        local.get 2
        i32.const -1
        i32.add
        local.tee 2
        br_if 0 (;@2;)
      end
    end
    local.get 0)
  (func $memset (type 1) (param i32 i32 i32) (result i32)
    (local i32)
    block  ;; label = @1
      local.get 2
      i32.eqz
      br_if 0 (;@1;)
      local.get 0
      local.set 3
      loop  ;; label = @2
        local.get 3
        local.get 1
        i32.store8
        local.get 3
        i32.const 1
        i32.add
        local.set 3
        local.get 2
        i32.const -1
        i32.add
        local.tee 2
        br_if 0 (;@2;)
      end
    end
    local.get 0)
  (table (;0;) 17 17 funcref)
  (memory (;0;) 17)
  (global (;0;) (mut i32) (i32.const 1048576))
  (global (;1;) i32 (i32.const 1049344))
  (global (;2;) i32 (i32.const 1049344))
  (export "memory" (memory 0))
  (export "foo" (func $foo))
  (export "__data_end" (global 1))
  (export "__heap_base" (global 2))
  (elem (;0;) (i32.const 1) func $_ZN3std5alloc24default_alloc_error_hook17ha9d99781a59146d7E $_ZN4core3ptr100drop_in_place$LT$$RF$mut$u20$std..io..Write..write_fmt..Adapter$LT$alloc..vec..Vec$LT$u8$GT$$GT$$GT$17h988a5cf7bcbb5d02E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_str17h98a5960856085077E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$10write_char17h9aaf994318b5ee48E $_ZN50_$LT$$RF$mut$u20$W$u20$as$u20$core..fmt..Write$GT$9write_fmt17h6a2be3faf981302cE $_ZN4core3ptr70drop_in_place$LT$std..panicking..begin_panic_handler..PanicPayload$GT$17h1cdd59289064cfc3E $_ZN90_$LT$std..panicking..begin_panic_handler..PanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17h8c74629b35ff2b51E $_ZN90_$LT$std..panicking..begin_panic_handler..PanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$3get17h79b0afd46bbc90abE $_ZN93_$LT$std..panicking..begin_panic_handler..StrPanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$8take_box17hc0b16d96ecfa69f3E $_ZN93_$LT$std..panicking..begin_panic_handler..StrPanicPayload$u20$as$u20$core..panic..BoxMeUp$GT$3get17h22a824a2e9bdb720E $_ZN4core3ptr226drop_in_place$LT$std..error..$LT$impl$u20$core..convert..From$LT$alloc..string..String$GT$$u20$for$u20$alloc..boxed..Box$LT$dyn$u20$std..error..Error$u2b$core..marker..Sync$u2b$core..marker..Send$GT$$GT$..from..StringError$GT$17h77c6f010d389f350E $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17ha49e06e3af8b40b6E $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17h4414432cd29569beE $_ZN4core3ops8function6FnOnce9call_once17h9b0d4f1f1a0375c1E $_ZN4core3ptr102drop_in_place$LT$$RF$core..iter..adapters..copied..Copied$LT$core..slice..iter..Iter$LT$u8$GT$$GT$$GT$17he2cc0633e1828163E $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17hcb495b7c373d7078E)
  (data (;0;) (i32.const 1048576) "\02\00\00\00\04\00\00\00\04\00\00\00\03\00\00\00\04\00\00\00\05\00\00\00called `Option::unwrap()` on a `None` value\00\01\00\00\00\00\00\00\00library/std/src/panicking.rsL\00\10\00\1c\00\00\00\03\02\00\00\1f\00\00\00L\00\10\00\1c\00\00\00\04\02\00\00\1e\00\00\00\06\00\00\00\10\00\00\00\04\00\00\00\07\00\00\00\08\00\00\00\02\00\00\00\08\00\00\00\04\00\00\00\09\00\00\00\0a\00\00\00\0b\00\00\00\0c\00\00\00\04\00\00\00\0c\00\00\00\02\00\00\00\08\00\00\00\04\00\00\00\0d\00\00\00library/alloc/src/raw_vec.rscapacity overflow\00\00\00\d0\00\10\00\1c\00\00\00/\02\00\00\05\00\00\00\0f\00\00\00\00\00\00\00\01\00\00\00\10\00\00\00"))
