;; Stack has invalid I64 when if instruction is executed. (if requires i32)
(module
  (type (;0;) (func (param i64) (result i64)))
  (func $even (type 0) (param i64) (result i64)
    local.get 0
    i64.const 2
    i64.rem_s
    if
      i64.const 0
      return
    end
    local.get 0
    return)
  (export "even" (func $even)))
