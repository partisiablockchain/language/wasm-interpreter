;; Recursive routine that exactly hits max stack depth (300) without exhausting it
(module
  (type (;0;) (func (param i32) ))
  (func $recursive (type 0) (param i32)
    local.get 0
    i32.const 1
    i32.sub
    local.tee 0
    i32.eqz
    if
      return
    end
    local.get 0
    call $recursive
  )
  (export "recursive" (func $recursive))
)
