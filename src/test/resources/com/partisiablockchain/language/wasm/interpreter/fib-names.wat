(module
  (type $t0 (func (param i32) (result i32)))
  (func $fib (type $t0) (param $p0 i32) (result i32)
    local.get $p0
    i32.const 2
    i32.lt_s
    if $I0
      local.get $p0
      return
    end
    local.get $p0
    i32.const 2
    i32.sub
    call $fib
    local.get $p0
    i32.const 1
    i32.sub
    call $fib
    i32.add
    return)
  (export "fib" (func $fib)))
