(module
  (type (;0;) (func (param i32)))
  (type (;1;) (func (param i32 i32) (result i32)))
  (type (;2;) (func (param i32 i32 i32) (result i32)))
  (type (;3;) (func (param i64 i64) (result i64)))
  (type (;4;) (func (param i64 i64 i64) (result i64)))
  (type (;5;) (func (param i32 i32 i32)))
  (type (;6;) (func (param i32 i32)))
  (type (;7;) (func (param i32) (result i64)))
  (func $rust_begin_unwind (type 0) (param i32)
    loop  ;; label = @1
      br 0 (;@1;)
    end)
  (func $i32_add (type 1) (param i32 i32) (result i32)
    local.get 1
    local.get 0
    i32.add)
  (func $i32_and (type 1) (param i32 i32) (result i32)
    local.get 1
    local.get 0
    i32.and)
  (func $i32_mul (type 1) (param i32 i32) (result i32)
    local.get 1
    local.get 0
    i32.mul)
  (func $i32_div (type 1) (param i32 i32) (result i32)
    block  ;; label = @1
      block  ;; label = @2
        local.get 1
        i32.eqz
        br_if 0 (;@2;)
        local.get 0
        i32.const -2147483648
        i32.ne
        br_if 1 (;@1;)
        local.get 1
        i32.const -1
        i32.ne
        br_if 1 (;@1;)
        i32.const 1048640
        i32.const 31
        i32.const 1048588
        call $_ZN4core9panicking5panic17h855ff10bbf7bb4ddE
        unreachable
      end
      i32.const 1048608
      i32.const 25
      i32.const 1048588
      call $_ZN4core9panicking5panic17h855ff10bbf7bb4ddE
      unreachable
    end
    local.get 0
    local.get 1
    i32.div_s)
  (func $i32_sub (type 1) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.sub)
  (func $i32_shl (type 1) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.const 31
    i32.and
    i32.shl)
  (func $i32_xor (type 1) (param i32 i32) (result i32)
    local.get 1
    local.get 0
    i32.xor)
  (func $i32_shr (type 1) (param i32 i32) (result i32)
    local.get 0
    local.get 1
    i32.const 31
    i32.and
    i32.shr_s)
  (func $i32_or (type 1) (param i32 i32) (result i32)
    local.get 1
    local.get 0
    i32.or)
  (func $i32_add_mul (type 2) (param i32 i32 i32) (result i32)
    local.get 1
    local.get 2
    call $i32_add
    local.get 0
    i32.mul)
  (func $i64_add (type 3) (param i64 i64) (result i64)
    local.get 1
    local.get 0
    i64.add)
  (func $i64_and (type 3) (param i64 i64) (result i64)
    local.get 1
    local.get 0
    i64.and)
  (func $i64_mul (type 3) (param i64 i64) (result i64)
    local.get 1
    local.get 0
    i64.mul)
  (func $i64_sub (type 3) (param i64 i64) (result i64)
    local.get 0
    local.get 1
    i64.sub)
  (func $i64_shl (type 3) (param i64 i64) (result i64)
    local.get 0
    local.get 1
    i64.const 63
    i64.and
    i64.shl)
  (func $i64_xor (type 3) (param i64 i64) (result i64)
    local.get 1
    local.get 0
    i64.xor)
  (func $i64_shr (type 3) (param i64 i64) (result i64)
    local.get 0
    local.get 1
    i64.const 63
    i64.and
    i64.shr_s)
  (func $i64_or (type 3) (param i64 i64) (result i64)
    local.get 1
    local.get 0
    i64.or)
  (func $i64_add_mul (type 4) (param i64 i64 i64) (result i64)
    local.get 1
    local.get 2
    call $i64_add
    local.get 0
    i64.mul)
  (func $call_stack (type 2) (param i32 i32 i32) (result i32)
    (local i32 i32)
    local.get 0
    local.get 1
    local.get 2
    call $i32_add_mul
    local.set 3
    local.get 1
    local.get 2
    local.get 0
    call $i32_add_mul
    local.set 4
    local.get 3
    local.get 2
    local.get 0
    local.get 1
    call $i32_add_mul
    local.get 4
    call $i32_add
    i32.mul)
  (func $_ZN4core3ptr102drop_in_place$LT$$RF$core..iter..adapters..copied..Copied$LT$core..slice..iter..Iter$LT$u8$GT$$GT$$GT$17he2cc0633e1828163E (type 0) (param i32))
  (func $_ZN4core9panicking5panic17h855ff10bbf7bb4ddE (type 5) (param i32 i32 i32)
    (local i32)
    global.get 0
    i32.const 32
    i32.sub
    local.tee 3
    global.set 0
    local.get 3
    i32.const 20
    i32.add
    i32.const 0
    i32.store
    local.get 3
    i32.const 1048672
    i32.store offset=16
    local.get 3
    i64.const 1
    i64.store offset=4 align=4
    local.get 3
    local.get 1
    i32.store offset=28
    local.get 3
    local.get 0
    i32.store offset=24
    local.get 3
    local.get 3
    i32.const 24
    i32.add
    i32.store
    local.get 3
    local.get 2
    call $_ZN4core9panicking9panic_fmt17hec165b0930ecb32eE
    unreachable)
  (func $_ZN4core9panicking9panic_fmt17hec165b0930ecb32eE (type 6) (param i32 i32)
    (local i32)
    global.get 0
    i32.const 16
    i32.sub
    local.tee 2
    global.set 0
    local.get 2
    local.get 1
    i32.store offset=12
    local.get 2
    local.get 0
    i32.store offset=8
    local.get 2
    i32.const 1048672
    i32.store offset=4
    local.get 2
    i32.const 1048672
    i32.store
    local.get 2
    call $rust_begin_unwind
    unreachable)
  (func $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17hcb495b7c373d7078E (type 7) (param i32) (result i64)
    i64.const -1028311936265645192)
  (table (;0;) 3 3 funcref)
  (memory (;0;) 17)
  (global (;0;) (mut i32) (i32.const 1048576))
  (global (;1;) i32 (i32.const 1048688))
  (global (;2;) i32 (i32.const 1048688))
  (export "memory" (memory 0))
  (export "i32_add" (func $i32_add))
  (export "i32_and" (func $i32_and))
  (export "i32_mul" (func $i32_mul))
  (export "i32_div" (func $i32_div))
  (export "i32_sub" (func $i32_sub))
  (export "i32_shl" (func $i32_shl))
  (export "i32_xor" (func $i32_xor))
  (export "i32_shr" (func $i32_shr))
  (export "i32_or" (func $i32_or))
  (export "i32_add_mul" (func $i32_add_mul))
  (export "i64_add" (func $i64_add))
  (export "i64_and" (func $i64_and))
  (export "i64_mul" (func $i64_mul))
  (export "i64_sub" (func $i64_sub))
  (export "i64_shl" (func $i64_shl))
  (export "i64_xor" (func $i64_xor))
  (export "i64_shr" (func $i64_shr))
  (export "i64_or" (func $i64_or))
  (export "i64_add_mul" (func $i64_add_mul))
  (export "call_stack" (func $call_stack))
  (export "__data_end" (global 1))
  (export "__heap_base" (global 2))
  (elem (;0;) (i32.const 1) func $_ZN4core3ptr102drop_in_place$LT$$RF$core..iter..adapters..copied..Copied$LT$core..slice..iter..Iter$LT$u8$GT$$GT$$GT$17he2cc0633e1828163E $_ZN36_$LT$T$u20$as$u20$core..any..Any$GT$7type_id17hcb495b7c373d7078E)
  (data (;0;) (i32.const 1048576) "src/main.rs\00\00\00\10\00\0b\00\00\00 \00\00\00\05\00\00\00\00\00\00\00attempt to divide by zero\00\00\00\00\00\00\00attempt to divide with overflow\00\01\00\00\00\00\00\00\00\01\00\00\00\02\00\00\00"))
