(module
  (type (;0;) (func (param i32) (result i32)))
  (type (;1;) (func (result i32)))
  (func (;0;) (type 0) (param i32) (result i32)
    local.get 0
    memory.grow
    return)
  (func (;1;) (type 1) (result i32)
    memory.size
    return)
  (memory 1 20)
  (export "memgrow" (func 0))
  (export "memsize" (func 1))
  )
