//! Fast and cheap memory operations through PBC hosted functions
//!
//! Exposes the PBC hosted functions for memmove and memcpy, both significantly cheaper on cycles than
//! the versions normally included by the linker.

#[link(wasm_import_module = "pbc")]
extern "C" {
    #[link_name = "memmove"]
    fn pbc_memmove(dest: *mut u8, src: *const u8, len: usize) -> *mut u8;
}

/// The linker will prefer this definition to linking it's own version.
#[inline(always)]
#[no_mangle]
pub fn memcpy(dest: *mut u8, src: *const u8, len: usize) -> *mut u8 {
    unsafe { pbc_memmove(dest, src, len) }
}

/// The linker will prefer this definition to linking it's own version.
#[inline(always)]
#[no_mangle]
pub fn memmove(dest: *mut u8, src: *const u8, len: usize) -> *mut u8 {
    unsafe { pbc_memmove(dest, src, len) }
}
