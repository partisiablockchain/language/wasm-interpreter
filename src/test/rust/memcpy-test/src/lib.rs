//! Library for testing that the PBC hosted "memmove" function is invoked.

mod pbclib;

#[no_mangle]
#[inline(never)]
fn initialize_vector(len: usize) -> Vec<u32> {
    let mut v = Vec::new();
    for i in 0..len {
        v.push(i as u32);
    }
    v
}

#[no_mangle]
#[inline(never)]
fn copy_from_slice_to_slice(source_buf: &[u32], target_buf: &mut [u32]) {
    target_buf.copy_from_slice(&source_buf[500..600]);
}

fn perform_some_test(len: usize) -> u32 {
    let v = initialize_vector(len);
    let mut buf: [u32; 100] = [0; 100];
    copy_from_slice_to_slice(&v, &mut buf);
    return buf.iter().sum();
}

/// Main test routing function
#[no_mangle]
pub extern "C" fn main(len: usize) -> u32 {
    perform_some_test(len)
}
