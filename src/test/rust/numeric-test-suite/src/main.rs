#![no_std]
#![no_main]

use core::panic::PanicInfo;

#[panic_handler]
fn panic(_panic: &PanicInfo<'_>) -> ! {
    loop {}
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_add(a: i32, b: i32) -> i32 {
    a + b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_and(a: i32, b: i32) -> i32 {
    a & b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_mul(a: i32, b: i32) -> i32 {
    a * b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_div(a: i32, b: i32) -> i32 {
    a / b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_sub(a: i32, b: i32) -> i32 {
    a - b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_shl(a: i32, b: i32) -> i32 {
    a << b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_xor(a: i32, b: i32) -> i32 {
    a ^ b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_shr(a: i32, b: i32) -> i32 {
    a >> b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_or(a: i32, b: i32) -> i32 {
    a | b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i32_add_mul(a: i32, b: i32, c: i32) -> i32 {
    a * i32_add(b, c)
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_add(a: i64, b: i64) -> i64 {
    a + b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_and(a: i64, b: i64) -> i64 {
    a & b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_mul(a: i64, b: i64) -> i64 {
    a * b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_sub(a: i64, b: i64) -> i64 {
    a - b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_shl(a: i64, b: i64) -> i64 {
    a << b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_xor(a: i64, b: i64) -> i64 {
    a ^ b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_shr(a: i64, b: i64) -> i64 {
    a >> b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_or(a: i64, b: i64) -> i64 {
    a | b
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_add_mul(a: i64, b: i64, c: i64) -> i64 {
    a * i64_add(b, c)
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn i64_pop_count(a: i64) -> i64 {
    a.popcnt()
}


#[no_mangle]
pub extern "C" fn call_stack(a: i32, b: i32, c: i32) -> i32 {
    let x = i32_add_mul(a, b, c);
    let y = i32_add_mul(b, c, a);
    let z = i32_add_mul(c, a, b);
    x * i32_add(z, y)
}
