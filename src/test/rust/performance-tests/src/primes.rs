//! Prime number generation benchmarks

/// Classic Sieve of Eratosthenes algorithm.
///
/// Returns the sieve itself; with each prime number in their slots; the slots of all other numbers
/// have been set to zero.
pub fn by_sieve(largest_number_checked: usize) -> Vec<usize> {
    // Initialize sieve
    let mut arr: Vec<usize> = Vec::with_capacity(largest_number_checked);
    for n in 0..largest_number_checked {
        arr.push(n);
    }

    // Perform sieve
    for n in 2..largest_number_checked / 2 {
        let mut m = n * 2;
        while m < largest_number_checked {
            arr[m] = 0;
            m += n;
        }
    }

    arr
}

/// Classic Naïve prime finding algorithm.
///
/// Uses a vec to store the found numbers.
///
/// Returns the list of primes.
pub fn by_list(largest_number_checked: usize) -> Vec<usize> {
    let mut seen_primes: Vec<usize> = Vec::new();
    for n in 2..largest_number_checked {
        if seen_primes.iter().all(|p| n % p != 0) {
            seen_primes.push(n);
        }
    }
    seen_primes
}
