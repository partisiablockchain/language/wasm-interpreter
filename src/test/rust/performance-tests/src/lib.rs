//! Provides a variety of useful benchmarks.

mod contract;
mod map;
mod primes;
mod rand;
mod sort;
mod trampoline;

use crate::map::*;
use std::collections::BTreeMap;
use std::collections::HashMap;

// Overwrite builtins with PBC imports.
extern crate pbc_lib as _;

const CONTRACT_RUNS: usize = 1;

/// Main routing function for benchmarks
///
/// A description of each benchmark can be found in the relevant modules/functions.
///
/// Arguments:
///
/// - `program`: The benchmark ID.
/// - `param`: Parameter for the benchmark. Oftentimes acts as an size parameter,
/// - `param2`: Parameter 2 for the benchmark. Oftentimes acts as an num repeated runs parameter,
///
/// Returns zero if given an unknown benchmark ID.
#[no_mangle]
#[allow(clippy::inconsistent_digit_grouping)]
pub extern "C" fn run_program(program: u32, param: usize, param2: usize) -> usize {
    match program {
        // Misc small benchmarks
        11_00 => primes::by_sieve(param).iter().filter(|&&p| p >= 2).sum(),
        11_01 => primes::by_list(param).iter().sum(),
        11_02 => trampoline::is_odd(param) as usize,
        11_03 => trampoline::plus_zero(param),
        11_04 => sort::generate_and_sort(param),

        // Map benchmarks
        20_0_1 => map::generate_and_lookup_first(NopMap::new(), param),
        20_0_2 => map::generate_and_insert_and_search(NopMap::new(), param, default(param2, 100)),

        20_1_1 => map::generate_and_lookup_first(BTreeMap::new(), param),
        20_1_2 => map::generate_and_insert_and_search(BTreeMap::new(), param, default(param2, 100)),

        20_2_1 => map::generate_and_lookup_first(VecMap::with_capacity(param), param),
        20_2_2 => map::generate_and_insert_and_search(
            VecMap::with_capacity(100),
            param,
            default(param2, 100),
        ),

        20_3_1 => map::generate_and_lookup_first(SortedVecMap::with_capacity(param), param),
        20_3_2 => map::generate_and_insert_and_search(
            SortedVecMap::with_capacity(100),
            param,
            default(param2, 100),
        ),

        20_4_1 => map::generate_and_lookup_first(HashMap::with_capacity(param * 2), param),
        20_4_2 => map::generate_and_insert_and_search(
            HashMap::with_capacity(100 * 2),
            param,
            default(param2, 100),
        ),

        // State serialization/deserialization benchmarks
        30_0_0 => contract::test_deserialize::<contract::StateWithBTreeMap>(
            param,
            default(param2, CONTRACT_RUNS),
        ),
        30_0_1 => contract::test_serialize::<contract::StateWithBTreeMap>(
            param,
            default(param2, CONTRACT_RUNS),
        ),

        30_1_0 => contract::test_deserialize::<contract::StateVecTup>(
            param,
            default(param2, CONTRACT_RUNS),
        ),
        30_1_1 => {
            contract::test_serialize::<contract::StateVecTup>(param, default(param2, CONTRACT_RUNS))
        }

        30_2_0 => contract::test_deserialize::<contract::EmptyState>(
            param,
            default(param2, CONTRACT_RUNS),
        ),
        30_2_1 => {
            contract::test_serialize::<contract::EmptyState>(param, default(param2, CONTRACT_RUNS))
        }

        30_3_0 => contract::test_deserialize::<contract::StateWithVecMap>(
            param,
            default(param2, CONTRACT_RUNS),
        ),
        30_3_1 => contract::test_serialize::<contract::StateWithVecMap>(
            param,
            default(param2, CONTRACT_RUNS),
        ),

        30_4_0 => contract::test_deserialize::<contract::StateWithListMap>(
            param,
            default(param2, CONTRACT_RUNS),
        ),
        30_4_1 => contract::test_serialize::<contract::StateWithListMap>(
            param,
            default(param2, CONTRACT_RUNS),
        ),

        30_5_0 => contract::test_deserialize::<contract::StateVecU8>(
            param,
            default(param2, CONTRACT_RUNS),
        ),
        30_5_1 => {
            contract::test_serialize::<contract::StateVecU8>(param, default(param2, CONTRACT_RUNS))
        }

        30_6_0 => contract::test_deserialize::<contract::StateVecU32>(
            param,
            default(param2, CONTRACT_RUNS),
        ),
        30_6_1 => {
            contract::test_serialize::<contract::StateVecU32>(param, default(param2, CONTRACT_RUNS))
        }

        // Unknown benchmark
        _ => 0,
    }
}

fn default(param: usize, default: usize) -> usize {
    if param == usize::MAX {
        default
    } else {
        param
    }
}
