use pbc_traits::ReadWriteState;
use read_write_state_derive::ReadWriteState;
use std::collections::BTreeMap;
use std::fmt::Debug;
use std::io::{Read, Write};

use crate::map::ListMap;
use crate::map::VecMap;
use crate::rand::get_vector_with_poor_random;

/// Generator trait for programatically producing state for a state.
pub trait StateGenerator {
    fn generate(state_size: usize) -> Self;
}

/// General benchmark for deserializing contract state.
#[inline(never)]
pub fn test_deserialize<State: ReadWriteState + StateGenerator + PartialEq + Debug>(
    state_size: usize,
    num_times: usize,
) -> usize {
    // Setup
    let mut buffer = Vec::with_capacity(state_size);
    let state = State::generate(state_size);
    let mut state2 = State::generate(state_size);
    state
        .state_write_to(&mut buffer)
        .expect("Could not write initial buffer");

    // Benchmark focus
    for _idx in 0..(num_times as u32) {
        state2 = State::state_read_from(&mut buffer.as_slice());

        // Used for debugging benchmark
        #[cfg(not(target_arch = "wasm32"))]
        assert_eq!(state, state2);
    }

    // Optimization disabler
    if state == state2 {
        2222
    } else {
        3333
    }
}

/// General benchmark for serializing contract state.
#[inline(never)]
pub fn test_serialize<State: ReadWriteState + StateGenerator + PartialEq>(
    state_size: usize,
    num_times: usize,
) -> usize {
    // Setup
    let mut buffer1 = Vec::with_capacity(state_size);
    let state = State::generate(state_size);
    state
        .state_write_to(&mut buffer1)
        .expect("Could not write initial buffer1");
    let mut buffer2 = Vec::with_capacity(buffer1.len());

    // Benchmark focus
    for idx in 0..(num_times as u32) {
        state
            .state_write_to(&mut buffer2)
            .expect("Could not write to buffer2");
        if idx < (num_times as u32) - 1 {
            buffer2.clear();
        }
    }
    // Optimization disabler
    if buffer1.len() == buffer2.len() {
        2222
    } else {
        3333
    }
}

/// State with zero data
///
/// Good for benchmarking harness
#[derive(ReadWriteState, Eq, PartialEq, Debug)]
pub struct EmptyState {}

impl StateGenerator for EmptyState {
    fn generate(_state_size: usize) -> Self {
        Self {}
    }
}

/// State with BTreeMap
#[derive(ReadWriteState, Eq, PartialEq, Debug)]
pub struct StateWithBTreeMap {
    map: BTreeMap<u32, u32>,
}

impl StateGenerator for StateWithBTreeMap {
    fn generate(state_size: usize) -> Self {
        Self {
            map: get_vector_with_poor_random(state_size, state_size as u32)
                .iter()
                .map(|&k| (k, k.wrapping_add(7).wrapping_mul(k)))
                .collect(),
        }
    }
}

/// State with a VecMap
#[derive(ReadWriteState, Eq, PartialEq, Debug)]
pub struct StateWithVecMap {
    map: VecMap<u32, u32>,
}

impl StateGenerator for StateWithVecMap {
    fn generate(state_size: usize) -> Self {
        let mut map = VecMap::with_capacity(state_size);
        for k in get_vector_with_poor_random(state_size, state_size as u32) {
            map.insert(k, k.wrapping_add(7).wrapping_mul(k));
        }
        Self { map }
    }
}

/// State with a ListMap
#[derive(ReadWriteState, Eq, PartialEq, Debug)]
pub struct StateWithListMap {
    map: ListMap<u32, u32>,
}

impl StateGenerator for StateWithListMap {
    fn generate(state_size: usize) -> Self {
        let mut map = ListMap::new();
        for k in get_vector_with_poor_random(state_size, state_size as u32) {
            map.insert(k, k.wrapping_add(7).wrapping_mul(k));
        }
        Self { map }
    }
}

/// State with a byte vector.
#[derive(ReadWriteState, Eq, PartialEq, Debug)]
pub struct StateVecU8 {
    map: Vec<u8>,
}

impl StateGenerator for StateVecU8 {
    fn generate(state_size: usize) -> Self {
        Self {
            map: get_vector_with_poor_random(state_size, state_size as u32)
                .iter()
                .map(|&k| k.to_be_bytes()[0])
                .collect(),
        }
    }
}

/// State with a vector of u32
#[derive(ReadWriteState, Eq, PartialEq, Debug)]
pub struct StateVecU32 {
    map: Vec<u32>,
}

impl StateGenerator for StateVecU32 {
    fn generate(state_size: usize) -> Self {
        Self {
            map: get_vector_with_poor_random(state_size, state_size as u32),
        }
    }
}

/// State with a vecmap of little endian keys.
#[derive(ReadWriteState, Eq, PartialEq, Debug)]
pub struct StateVecTup {
    map: VecMap<u32, u8>,
}

impl StateGenerator for StateVecTup {
    fn generate(state_size: usize) -> Self {
        let mut map = VecMap::with_capacity(state_size);
        for k in get_vector_with_poor_random(state_size, state_size as u32) {
            map.insert(k, k.wrapping_add(7).wrapping_mul(k) as u8);
        }
        Self { map }
    }
}
