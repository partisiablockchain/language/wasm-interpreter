//! Provides two basic maps based on vectors.

use crate::map::GenericMap;
use pbc_traits::{ReadInt, ReadWriteState, WriteInt};
use std::collections::LinkedList;
use std::fmt::Debug;
use std::io::{Read, Write};

/// Container for key value pairs
///
#[derive(Debug, PartialEq, Eq)]
struct Entry<K, V> {
    key: K,
    val: V,
}

/// Simple map that wraps a vector. Stores entries as key value pairs. Note that keys are never
/// removed; they are just shadowed by other keys.
/// - Provides amortized O(1) insert, due to backing Vec.
/// - Provides O(n) lookup.
#[derive(Debug, PartialEq, Eq)]
pub struct VecMap<K, V> {
    entries: Vec<Entry<K, V>>,
}

impl<K, V> VecMap<K, V>
where
    K: Eq,
{
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            entries: Vec::with_capacity(capacity),
        }
    }

    pub fn insert(&mut self, key: K, val: V) {
        self.entries.push(Entry { key, val });
    }

    pub fn get(&self, key: &K) -> Option<&V> {
        self.entries
            .iter()
            .rev()
            .find(|entry| entry.key == *key)
            .map(|entry| &entry.val)
    }
}

impl<K, V> GenericMap<K, V> for VecMap<K, V>
where
    K: Eq,
{
    fn insert(&mut self, key: K, val: V) {
        self.insert(key, val)
    }

    fn get(&self, key: &K) -> Option<&V> {
        self.get(key)
    }
}

/// Simple map that wraps a sorted vector. Stores entries as key value pairs.
/// - Provides amortized O(n) insert.
/// - Provides O(log(n)) lookup.
#[derive(Debug, PartialEq, Eq)]
pub struct SortedVecMap<K, V> {
    entries: Vec<Entry<K, V>>,
}

impl<K, V> SortedVecMap<K, V>
where
    K: Ord,
    K: Copy,
    V: Clone,
{
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            entries: Vec::with_capacity(capacity),
        }
    }

    pub fn insert(&mut self, key: K, val: V) {
        match self.entries.binary_search_by_key(&key, |entry| entry.key) {
            Result::Ok(idx) => {
                self.entries.push(Entry { key, val });
                self.entries.swap_remove(idx);
            }
            Result::Err(idx) => self.entries.insert(idx, Entry { key, val }),
        }
    }

    pub fn get(&self, key: &K) -> Option<&V> {
        match self.entries.binary_search_by_key(key, |entry| entry.key) {
            Result::Ok(idx) => Some(&self.entries[idx].val),
            Result::Err(_) => None,
        }
    }
}

impl<K, V> GenericMap<K, V> for SortedVecMap<K, V>
where
    K: Ord,
    K: Copy,
    V: Clone,
{
    fn insert(&mut self, key: K, val: V) {
        self.insert(key, val)
    }

    fn get(&self, key: &K) -> Option<&V> {
        self.get(key)
    }
}

/// Simple map that wraps a list. Stores entries as key value pairs.
/// - Provides amortized O(1) insert.
/// - Provides O(n) lookup.
#[derive(Debug, PartialEq, Eq)]
pub struct ListMap<K, V> {
    entries: LinkedList<Entry<K, V>>,
}

impl<K, V> ListMap<K, V>
where
    K: Eq,
{
    pub fn new() -> Self {
        Self {
            entries: LinkedList::new(),
        }
    }

    pub fn insert(&mut self, key: K, val: V) {
        self.entries.push_front(Entry { key, val });
    }

    pub fn get(&self, key: &K) -> Option<&V> {
        self.entries
            .iter()
            .find(|entry| entry.key == *key)
            .map(|entry| &entry.val)
    }
}

impl<K, V> GenericMap<K, V> for ListMap<K, V>
where
    K: Eq,
{
    fn insert(&mut self, key: K, val: V) {
        self.insert(key, val)
    }

    fn get(&self, key: &K) -> Option<&V> {
        self.get(key)
    }
}

/// Serializer for Entry that all maps use.
///
/// Format: | key | value |
impl<K: ReadWriteState, V: ReadWriteState> ReadWriteState for Entry<K, V> {
    const SERIALIZABLE_BY_COPY: bool = K::SERIALIZABLE_BY_COPY &
            V::SERIALIZABLE_BY_COPY &
            (std::mem::size_of::<Self>().wrapping_rem(std::mem::align_of::<Self>()) == 0) &
            (std::mem::size_of::<Self>() == std::mem::size_of::<K>() + std::mem::size_of::<V>());

    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        let key = K::state_read_from(reader);
        let val = V::state_read_from(reader);
        Entry { key, val }
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        self.key.state_write_to(writer).unwrap();
        self.val.state_write_to(writer).unwrap();
        Ok(())
    }
}

/// Serializer for VecMap
///
/// Format: | n: len | e_1 | e_2 | ... | e_n |
///
/// Assumes list order is stable on-the-wire.
///
/// Delegates to Vec impl, in order to exploit optimizations there
impl<K: ReadWriteState, V: ReadWriteState> ReadWriteState for VecMap<K, V> {
    /// Never applicable, due to internal pointers.
    const SERIALIZABLE_BY_COPY: bool = false;

    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        Self {
            entries: <Vec<Entry<K, V>>>::state_read_from(reader),
        }
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        self.entries.state_write_to(writer)
    }
}

/// Serializer for SortedVecMap
///
/// Format: | n: len | e_1 | e_2 | ... | e_n |
///
/// Assumes list order is stable (and sorted) on-the-wire .
///
/// Delegates to Vec impl, in order to exploit optimizations there
impl<K: ReadWriteState, V: ReadWriteState> ReadWriteState for SortedVecMap<K, V> {
    /// Never applicable, due to internal pointers.
    const SERIALIZABLE_BY_COPY: bool = false;

    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        Self {
            entries: <Vec<Entry<K, V>>>::state_read_from(reader),
        }
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        self.entries.state_write_to(writer)
    }
}

/// Serializer for SortedVecMap
///
/// Format: | n: len | e_1 | e_2 | ... | e_n |
///
/// Assumes list order is stable on-the-wire.
impl<K: ReadWriteState, V: ReadWriteState> ReadWriteState for ListMap<K, V> {
    /// Never applicable, due to internal pointers.
    const SERIALIZABLE_BY_COPY: bool = false;

    fn state_read_from<R: Read>(reader: &mut R) -> Self {
        let len = reader.read_u32_be() as usize;
        let mut entries = LinkedList::new();
        for _ in 0..len {
            entries.push_back(<Entry<K, V>>::state_read_from(reader));
        }
        Self { entries }
    }

    fn state_write_to<W: Write>(&self, writer: &mut W) -> std::io::Result<()> {
        writer.write_u32_be(self.entries.len() as u32)?;
        for entry in &self.entries {
            entry.state_write_to(writer)?;
        }
        Ok(())
    }
}
