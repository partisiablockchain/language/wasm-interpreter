//! Benchmarks for maps, using the GenericMap trait to route to relevant methods.

mod generic_map;
mod vecmap;

use crate::rand;

pub use generic_map::*;
pub use vecmap::*;

/// Benchmarks time/cycles used to fill a map.
pub fn generate_and_lookup_first<M: generic_map::GenericMap<u32, u32>>(
    mut map: M,
    num_elements: usize,
) -> usize {
    // Setup
    let initial_elements = rand::get_vector_with_poor_random(num_elements, num_elements as u32);
    let initial_elements_2: Vec<_> = initial_elements.iter().map(|&x| (x, x)).collect();

    // Main benchmark focus
    for (k, v) in initial_elements_2 {
        map.insert(k, v);
    }

    // Optimization prevention
    return *map
        .get(initial_elements.first().unwrap_or(&0))
        .unwrap_or(&0) as usize;
}

/// Benchmarks time/cycles used to perform lookups in maps.
pub fn generate_and_insert_and_search<M: generic_map::GenericMap<u32, u32>>(
    mut map: M,
    num_elements: usize,
    search_multiplier: usize,
) -> usize {
    // Setup
    let initial_elements = rand::get_vector_with_poor_random(num_elements, num_elements as u32);
    for &e in &initial_elements {
        map.insert(e, e);
    }

    // Main benchmark focus
    let mut result: u32 = 0;
    for idx in 0..(search_multiplier as u32) {
        for k in &initial_elements {
            let e2: u32 = *map.get(k).unwrap_or(&0);
            // Optimization prevention
            result = result.wrapping_add(e2.wrapping_mul(if idx == *k { 1 } else { 2 }));
        }
    }

    // Optimization prevention
    result as usize
}
