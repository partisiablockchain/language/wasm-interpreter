//! Provides a generic way to interface with Rust's maps.

use std::collections::BTreeMap;
use std::collections::HashMap;
use std::hash::Hash;

/// Generic trait to interfae with Rust's maps.
pub trait GenericMap<K, V> {
    fn insert(&mut self, key: K, val: V);
    fn get(&self, key: &K) -> Option<&V>;
}

impl<K, V> GenericMap<K, V> for BTreeMap<K, V>
where
    K: Ord,
{
    fn insert(&mut self, key: K, val: V) {
        self.insert(key, val);
    }

    fn get(&self, key: &K) -> Option<&V> {
        self.get(key)
    }
}

impl<K: Eq + Hash, V> GenericMap<K, V> for HashMap<K, V> {
    fn insert(&mut self, key: K, val: V) {
        HashMap::insert(self, key, val);
    }

    fn get(&self, key: &K) -> Option<&V> {
        HashMap::get(self, key)
    }
}

/// Basic dumb map implementation; attempts to do as little as feasible. Useful for testing the
/// overhead of the benchmark harness.
pub struct NopMap {}

impl NopMap {
    pub fn new() -> NopMap {
        NopMap {}
    }
}

impl<K> GenericMap<K, u32> for NopMap {
    fn insert(&mut self, _key: K, _val: u32) {}

    fn get(&self, _key: &K) -> Option<&u32> {
        Some(&1)
    }
}
