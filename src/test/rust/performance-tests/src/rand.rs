//! Utility module for generating data to process.

const MODULO: u32 = 2147483648;
const MULTIPLIER: u32 = 1103515245;
const INCREMENT: u32 = 12345;

pub fn fill_vector_with_poor_random(vec: &mut Vec<u32>, seed: u32) {
    let mut x: u32 = seed;
    for _ in vec.len()..vec.capacity() {
        x = MULTIPLIER
            .wrapping_mul(x)
            .wrapping_add(INCREMENT)
            .wrapping_rem_euclid(MODULO);
        vec.push(x);
    }
}

pub fn get_vector_with_poor_random(size: usize, seed: u32) -> Vec<u32> {
    let mut vec = Vec::with_capacity(size);
    fill_vector_with_poor_random(&mut vec, seed);
    vec.to_vec()
}
