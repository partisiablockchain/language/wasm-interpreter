//! Benchmarks for sorting algorithms.

use super::rand;

/// Very simple benchmark for sorting numbers.
pub fn generate_and_sort(num_elements: usize) -> usize {
    // Setup
    let mut v = rand::get_vector_with_poor_random(num_elements, num_elements as u32);

    // Benchmark focus
    v.sort_unstable();

    // Optimization disabling
    let result: u32 = v[0]
        .wrapping_add(v[v.len() / 2])
        .wrapping_add(v[v.len() - 1]);
    result as usize
}
