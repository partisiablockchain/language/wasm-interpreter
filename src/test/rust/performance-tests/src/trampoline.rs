//! Benchmarks for function trampolines.
//!
//! A function trampoline is a workaround for implementing tail recursive functions in languages
//! with no built-in support for tail recursion.
//!
//! This allows us to benchmark function pointer functionality.

enum TrampolineData<S, R> {
    Continue {
        state: S,
        continuation: fn(S) -> TrampolineData<S, R>,
    },
    Return {
        result: R,
    },
}

fn odd(n: usize) -> TrampolineData<usize, bool> {
    if n == 0 {
        TrampolineData::Return { result: false }
    } else {
        TrampolineData::Continue {
            continuation: even,
            state: n - 1,
        }
    }
}

fn even(n: usize) -> TrampolineData<usize, bool> {
    if n == 0 {
        TrampolineData::Return { result: true }
    } else {
        TrampolineData::Continue {
            continuation: odd,
            state: n - 1,
        }
    }
}

fn accumulator_plus(n_and_result: (usize, usize)) -> TrampolineData<(usize, usize), usize> {
    let n = n_and_result.0;
    if n == 0 {
        TrampolineData::Return {
            result: n_and_result.1,
        }
    } else {
        TrampolineData::Continue {
            continuation: accumulator_plus,
            state: (n - 1, n_and_result.1 + 1),
        }
    }
}

fn trampoline<T, R>(func: fn(T) -> TrampolineData<T, R>, n: T) -> R {
    let mut trampoline_state = TrampolineData::Continue {
        continuation: func,
        state: n,
    };

    loop {
        match trampoline_state {
            TrampolineData::Continue {
                continuation,
                state,
            } => trampoline_state = continuation(state),
            TrampolineData::Return { result } => break result,
        };
    }
}

pub fn is_odd(n: usize) -> bool {
    trampoline(odd, n)
}

pub fn plus_zero(n: usize) -> usize {
    trampoline(accumulator_plus, (n, 0))
}
