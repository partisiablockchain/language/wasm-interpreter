//! Benchmark runner for native. Useful for testing the benchmarks themselves.

/// Runs a bunch of benchmarks
#[allow(clippy::inconsistent_digit_grouping)]
fn main() {
    println!("Sieve: {}", run_program(11_00, 1000));
    println!("Prime list: {}", run_program(11_01, 1000));
    println!("Is odd: {}", run_program(11_02, 1000001));
    println!("1000001: {}", run_program(11_03, 1000001));
    println!("sort: {}", run_program(11_04, 4000));

    let map_impls = ["NopMap", "BTreeMap", "VecMap", "SortedVecMap", "HashMap"];
    for (impl_idx, impl_name) in map_impls.iter().enumerate() {
        println!(
            "{:30} {:12}:  {}",
            impl_name,
            "insert",
            run_program(20_0_1 + 10 * (impl_idx as u32), 1000)
        );
        println!(
            "{:30} {:12}:  {}",
            impl_name,
            "search",
            run_program(20_0_2 + 10 * (impl_idx as u32), 100)
        );
    }

    let map_impls = [
        "BTreeMap<u32,u32>",
        "VecMap<u32,u8>",
        "Empty",
        "VecMap<u32,u32>",
        "ListMap<u32,u32>",
        "Vec<u8>",
        "Vec<u32>",
    ];
    for (impl_idx, impl_name) in map_impls.iter().enumerate() {
        println!(
            "{:30} {:12}:  {}",
            impl_name,
            "deserialize",
            run_program(30_0_0 + 10 * (impl_idx as u32), 1000)
        );
        println!(
            "{:30} {:12}:  {}",
            impl_name,
            "serialize",
            run_program(30_0_1 + 10 * (impl_idx as u32), 100)
        );
    }
}

fn run_program(program: u32, param: usize) -> usize {
    partisia_wasm_performance_tests::run_program(program, param, usize::MAX)
}
