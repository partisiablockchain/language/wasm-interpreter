#![no_std]
#![no_main]

use core::panic::PanicInfo;

#[panic_handler]
fn panic(_panic: &PanicInfo<'_>) -> ! {
    loop {}
}

#[no_mangle]
#[inline(never)]
pub extern "C" fn block(a: i32, b: i32) -> i32 {
    let mut x = 0i32;
    loop {
        if x & 10 != 0 { x += b; } else { x += a }

        if x & 0xF800 == 0 {
            break;
        }
    }
    b * a * x
}
