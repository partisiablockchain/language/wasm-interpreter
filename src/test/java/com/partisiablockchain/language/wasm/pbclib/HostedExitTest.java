package com.partisiablockchain.language.wasm.pbclib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.interpreter.WasmTest;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Basic tests for {@link HostedExit}. */
public final class HostedExitTest extends WasmTest {

  static final long CALL_OVERHEAD = 2L;

  private static final String LOREM_IPSUM =
      "euismod lacinia at quis risus sed vulputate odio ut enim blandit volutpat maecenas volutpat"
          + " blandit aliquam etiam erat velit scelerisque in dictum non consectetur a erat nam at"
          + " lectus urna duis convallis convallis tellus id interdum velit laoreet id donec"
          + " ultrices tincidunt arcu non sodales neque sodales ut etiam sit amet nisl purus in"
          + " mollis nunc sed id semper risus in hendrerit gravida rutrum quisque non tellus orci"
          + " ac auctor augue mauris augue neque gravida in fermentum et sollicitudin ac orci"
          + " phasellus egestas tellus rutrum tellus pellentesque eu tincidunt tortor aliquam nulla"
          + " facilisi cras fermentum odio eu feugiat pretium nibh ipsum consequat nisl vel pretium"
          + " lectus quam id leo in vitae turpis massa sed elementum tempus egestas sed sed risus"
          + " pretium quam vulputate dignissim suspendisse in est ante in nibh mauris cursus mattis"
          + " molestie a iaculis at erat pellentesque adipiscing commodo elit at imperdiet dui"
          + " accumsan sit amet nulla facilisi morbi tempus iaculis urna id volutpat lacus laoreet"
          + " non curabiturgravida arcu ac tortor dignissim convallis aenean et tortor at risus"
          + " viverra adipiscing at in tellus integer feugiat scelerisque varius morbi enim nunc"
          + " faucibus a pellentesque sit amet porttitor eget dolor morbi non arcu risus quis"
          + " varius quam quisque id";

  private void setupInstance() {
    // Setup
    parse(
        HostedExitTest.class,
        """
        (module
            (import "pbc" "exit" (func (param i32 i32)))
            (export "exit" (func 0))
            (memory 1)
            (data 0 (i32.const 0x00) "I am panicking")
            (data 0 (i32.const 0x1000)"""
            + " \""
            + LOREM_IPSUM
            + "\"))");

    instance.registerExternal("exit", new HostedExit());
  }

  @Test
  public void hostedExitTest() {
    setupInstance();

    Assertions.assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(0), new Uint31(14)))
        .containsExactly(73, 32, 97, 109, 32, 112, 97, 110, 105, 99, 107, 105, 110, 103);

    Assertions.assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(0), new Uint31(14)))
        .asString()
        .isEqualTo("I am panicking");

    Assertions.assertThatThrownBy(() -> runVoid("exit", new Int32(0), new Int32(14)))
        .hasMessage(trapMsg("I am panicking"));
  }

  private String trapMsg(final String msg) {
    return "Trap: Early exit: " + msg + "\n    at hosted:exit():0\n    at call_exit():1";
  }

  @Test
  public void truncation() {
    setupInstance();

    Assertions.assertThatThrownBy(() -> runFunction("exit", new Int32(0x1000), new Int32(1023)))
        .isInstanceOf(TrapException.class)
        .hasMessage(trapMsg(LOREM_IPSUM.substring(0, 1023)));

    Assertions.assertThatThrownBy(() -> runFunction("exit", new Int32(0x1000), new Int32(1024)))
        .isInstanceOf(TrapException.class)
        .hasMessage(trapMsg(LOREM_IPSUM.substring(0, 1024)));

    Assertions.assertThatThrownBy(() -> runFunction("exit", new Int32(0x1000), new Int32(1025)))
        .isInstanceOf(TrapException.class)
        .hasMessage(trapMsg(LOREM_IPSUM.substring(0, 1021) + "..."));

    Assertions.assertThatThrownBy(() -> runFunction("exit", new Int32(0x1000), new Int32(0x8000)))
        .isInstanceOf(TrapException.class)
        .hasMessage(trapMsg(LOREM_IPSUM.substring(0, 1021) + "..."));
  }

  @Test
  public void badMemoryAccess() {
    setupInstance();

    Assertions.assertThatThrownBy(() -> runFunction("exit", new Int32(-5), new Int32(14)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading error message in exit call 14 bytes at address -5, buffer len: 65536\n"
                + "    at hosted:exit():0\n"
                + "    at call_exit():1");

    Assertions.assertThatThrownBy(() -> runFunction("exit", new Int32(0), new Int32(-5)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading error message in exit call -5 bytes at address 0, buffer len: 65536\n"
                + "    at hosted:exit():0\n"
                + "    at call_exit():1");

    final int memorySize = instance.getMemory(Uint31.ZERO).size().asInt();

    Assertions.assertThatThrownBy(() -> runFunction("exit", new Int32(memorySize), new Int32(16)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. Reading error message in exit call 16 bytes at"
                + " address 65536, buffer len: 65536\n"
                + "    at hosted:exit():0\n"
                + "    at call_exit():1");

    Assertions.assertThatThrownBy(
            () -> runFunction("exit", new Int32(0), new Int32(memorySize + 1)))
        .isInstanceOf(TrapException.class)
        .hasMessage(
            "Trap: Out of bounds memory access. "
                + "Reading error message in exit call 65537 bytes at address 0, buffer len: 65536\n"
                + "    at hosted:exit():0\n"
                + "    at call_exit():1");
  }
}
