package com.partisiablockchain.language.wasm.pbclib;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmTest;
import org.junit.jupiter.api.Test;

/** Basic tests for {@link HostedMemmove}. */
public final class HostedMemmoveTest extends WasmTest {

  static final long CALL_OVERHEAD = 2L;

  private void setupInstance() {
    // Setup
    parse(
        HostedMemmoveTest.class,
        "(module "
            + " (import \"pbc\" \"memmove\" (func (param i32 i32 i32) (result i32))) "
            + " (export \"memmove\" (func 0)) "
            + " (memory 1) "
            + " (data 0 (i32.const 16) \"Hello, World!\") "
            + ")");

    instance.registerExternal("memmove", new HostedMemmove(new Uint31(20), Uint31.ONE));
  }

  @Test
  public void goodMemmove() {
    setupInstance();

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(0), new Uint31(5)))
        .containsExactly(0, 0, 0, 0, 0);

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(16), new Uint31(13)))
        .asString()
        .isEqualTo("Hello, World!");

    runFunctionWithPreciseCycles(
        "memmove", CALL_OVERHEAD + 20 + 5, new Int32(16), new Int32(23), new Int32(5));

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(16), new Uint31(13)))
        .asString()
        .isEqualTo("World, World!");

    runFunctionWithPreciseCycles(
        "memmove", CALL_OVERHEAD + 20 + 3, new Int32(16), new Int32(18), new Int32(3));
    runFunctionWithPreciseCycles(
        "memmove", CALL_OVERHEAD + 20 + 3, new Int32(25), new Int32(23), new Int32(3));

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(16), new Uint31(13)))
        .asString()
        .isEqualTo("rldld, WoWor!");
  }

  @Test
  public void badMemmove() {
    setupInstance();

    assertThatThrownBy(() -> runFunction("memmove", new Int32(-5), new Int32(0), new Int32(5)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Writing 5 bytes at address -5");

    assertThatThrownBy(() -> runFunction("memmove", new Int32(5), new Int32(-5), new Int32(6)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Copying 6 bytes at address -5");

    assertThatThrownBy(() -> runFunction("memmove", new Int32(5), new Int32(5), new Int32(-7)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Writing -7 bytes at address 5");

    final int memorySize = instance.getMemory(Uint31.ZERO).size().asInt();

    assertThatThrownBy(
            () -> runFunction("memmove", new Int32(memorySize), new Int32(0), new Int32(8)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Writing 8 bytes at address " + memorySize);

    assertThatThrownBy(
            () -> runFunction("memmove", new Int32(0), new Int32(memorySize), new Int32(9)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Copying 9 bytes at address " + memorySize);

    assertThatThrownBy(
            () -> runFunction("memmove", new Int32(0), new Int32(1), new Int32(memorySize)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Copying " + memorySize + " bytes at address 1");

    assertThatThrownBy(
            () -> runFunction("memmove", new Int32(1), new Int32(0), new Int32(memorySize)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Writing " + memorySize + " bytes at address 1");
  }

  @Test
  public void weirdGoodCornercaseMemmove() {
    setupInstance();

    final int memSize = instance.getMemory(Uint31.ZERO).size().asInt();

    runFunctionWithPreciseCycles(
        "memmove", CALL_OVERHEAD + 20 + memSize, new Int32(0), new Int32(0), new Int32(memSize));

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(0), new Uint31(5)))
        .containsExactly(0, 0, 0, 0, 0);

    assertThat(instance.getMemory(Uint31.ZERO).read(new Uint31(16), new Uint31(13)))
        .asString()
        .isEqualTo("Hello, World!");
  }

  @Test
  public void checkMemmoveCallableFromRust() throws java.io.IOException {
    load(HostedMemmoveTest.class, "memmove_test.wat");

    final boolean[] called = new boolean[] {false};
    instance.registerExternal(
        "memmove",
        ins -> {
          called[0] = true;
          return java.util.List.of(ins.getLocal(Uint31.ZERO));
        });

    runFunction("main", new Int32(1000));
    assertThat(called).containsExactly(true);
  }

  @Test
  public void rustMemmoveTest() throws java.io.IOException {
    load(HostedMemmoveTest.class, "memmove_test.wat");
    instance.registerExternal("memmove", new HostedMemmove(new Uint31(20), Uint31.ONE));

    instance.enableCyclesProfiling();
    final Literal result = runFunction("main", new Int32(1000));
    instance.disableCyclesProfiling();

    assertThat(result).isEqualTo(new Int32((500 + 599) * 100 / 2));
    assertThat(instance.getCyclesProfilingReport())
        .filteredOn(x -> x.frameName().equals("hosted:memmove"))
        .hasSize(1)
        .element(0)
        .extracting(x -> x.cyclesUsedSpecific())
        .isEqualTo(420L);
  }
}
