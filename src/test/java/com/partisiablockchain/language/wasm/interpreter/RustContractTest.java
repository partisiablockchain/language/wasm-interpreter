package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** Integration test using an actual Rust-as-WASM program. */
public final class RustContractTest extends WasmTest {

  /** Set up the test. */
  @BeforeEach
  public void setUp() throws Exception {
    load(WasmParser.class, "rust-contract.wat");
  }

  @Test
  public void unknownHostFunction() {
    assertThatThrownBy(() -> instance.runHostFunction("unknown"))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Cannot find external function");
  }

  @Test
  public void rawExecute() {
    instance.registerExternal(
        "log_external",
        o -> {
          Int64 ptr = (Int64) o.getLocal(new Uint31(0));
          Int32 len = (Int32) o.getLocal(new Uint31(1));

          byte[] bytes =
              o.getMemory(new Uint31(0)).read(new Uint31(ptr.toInt32()), new Uint31(len.value()));
          System.err.println(new String(bytes, StandardCharsets.UTF_8));
          instance.useCycles(new Uint31(1000));

          return List.of();
        });

    instance.registerExternal(
        "call_named",
        o -> {
          Int64 ptr = (Int64) o.getLocal(new Uint31(0));
          Int32 len = (Int32) o.getLocal(new Uint31(1));

          byte[] bytes =
              o.getMemory(new Uint31(0)).read(new Uint31(ptr.toInt32()), new Uint31(len.value()));
          System.out.println("calling: " + new String(bytes, StandardCharsets.UTF_8));
          instance.useCycles(new Uint31(1000));
          return List.of();
        });

    instance.registerExternal(
        "read_context_into_address",
        o -> {
          Int64 ptr = (Int64) instance.getLocal(new Uint31(0));
          byte[] decode = Hex.decode(RUST_PROGRAM_HEX);
          instance.getMemory(new Uint31(0)).write(new Uint31(ptr.toInt32()), decode);
          instance.useCycles(new Uint31(1000));
          return List.of(new Int32(decode.length));
        });

    runVoid("raw_execute");
    assertThat(instance.getCyclesUsed()).isEqualTo(1934402L);
  }

  private static String RUST_PROGRAM_HEX =
      "9e4dc14500fe3eb2d"
          + "b366fc770c522d4b4"
          + "2f0996b8fb83064a0"
          + "054ee8675498acc9f"
          + "4d22ffa1b1fe92046"
          + "1f54ebf005bd9e5f0"
          + "6334689b2c117b752"
          + "60fa511424e445300"
          + "00000000008608000"
          + "0017c0d55b8712ef9"
          + "52fbcfece19c6b66d"
          + "6953c48690f030127"
          + "cdc5b128adb61c17d"
          + "53e8b7aab2ef952fb"
          + "cfece19c6b66d6953"
          + "c48690f030127cdc5"
          + "b128adb61c17d53e8"
          + "b7aab000000000000"
          + "00000150696563654f"
          + "664569676874202020"
          + "200000000000bc4b210"
          + "0000000";
}
