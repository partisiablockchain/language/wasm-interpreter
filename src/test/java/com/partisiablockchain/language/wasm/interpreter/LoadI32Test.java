package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.MemoryAddress;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.LoadI32;
import com.partisiablockchain.language.wasm.interpreter.operator.i32.StoreI32;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link LoadI32}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.operator.i32.LoadI32
 */
public final class LoadI32Test extends WasmTest {

  @Test
  public void unknownBitSize() {
    assertThatThrownBy(() -> LoadI32.fromBytes(new byte[0], 63, false))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Unknown bit size")
        .hasMessageContaining("63");
  }

  @Test
  public void loadFromVeryLargePointer() throws IOException {
    load(WasmInstance.class, "memsize.wat");

    InstructionMemory insn =
        new InstructionMemory(OpCode.I_32_LOAD, new MemoryAddress(Uint31.ZERO, Uint31.ZERO));
    instance.newFrame(Uint31.ZERO, "test", Uint31.ZERO, List.of(insn));
    instance.stack().push(new Int32(Integer.MIN_VALUE));

    LoadI32 executor = new LoadI32(32, false);

    assertThatThrownBy(() -> executor.execute(instance, insn))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Trap")
        .hasMessageContaining("Out of bounds memory access");
  }

  @Test
  public void storeIntoVeryLargePointer() throws IOException {
    load(WasmInstance.class, "memsize.wat");

    InstructionMemory insn =
        new InstructionMemory(OpCode.I_32_STORE, new MemoryAddress(Uint31.ZERO, Uint31.ZERO));
    instance.newFrame(Uint31.ZERO, "test", Uint31.ZERO, List.of(insn));
    instance.stack().push(new Int32(Integer.MIN_VALUE));
    instance.stack().push(new Int32(Integer.MIN_VALUE));

    StoreI32 executor = new StoreI32(32);

    assertThatThrownBy(() -> executor.execute(instance, insn))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Trap")
        .hasMessageContaining("Out of bounds memory access");
  }
}
