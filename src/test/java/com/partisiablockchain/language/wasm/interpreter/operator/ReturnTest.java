package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.ArrayStack;
import com.partisiablockchain.language.wasm.interpreter.InstructionIfElse;
import com.partisiablockchain.language.wasm.interpreter.InstructionNoArgs;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmTest;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test for {@link InstructionIfElse}.
 *
 * @see InstructionIfElse
 */
public final class ReturnTest extends WasmTest {

  @Test
  public void mustNotBeAbleToPopAnyFromZeroArityFrame() throws IOException {
    load(WasmInstance.class, "memsize.wat");

    final InstructionNoArgs insn = new InstructionNoArgs(OpCode.RETURN);

    instance.newFrame(
        Uint31.ZERO, "test_outer", Uint31.ZERO, List.of(new InstructionNoArgs(OpCode.I_64_POPCNT)));
    instance.newFrame(Uint31.ZERO, "test_inner", Uint31.ZERO, List.of(insn));

    final Return executor = new Return();
    executor.execute(instance, insn);

    assertThat(instance.frame().getNextInstruction().getOpCode()).isEqualTo(OpCode.I_64_POPCNT);
    assertThatThrownBy(() -> instance.stack().pop())
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Empty stack");
  }

  @Test
  public void mustOnlyBeAbleToPopOneFromOneArityFrame() throws IOException {
    load(WasmInstance.class, "memsize.wat");

    final InstructionNoArgs insn = new InstructionNoArgs(OpCode.RETURN);

    instance.newFrame(
        Uint31.ZERO, "test_outer", Uint31.ZERO, List.of(new InstructionNoArgs(OpCode.I_64_POPCNT)));
    instance.newFrame(Uint31.ZERO, "test_inner", Uint31.ONE, List.of(insn));
    pushMany(instance.stack(), 4, 3, 2, 1);

    final Return executor = new Return();
    executor.execute(instance, insn);

    assertThat(instance.frame().getNextInstruction().getOpCode()).isEqualTo(OpCode.I_64_POPCNT);
    assertThat(instance.stack().pop()).isEqualTo(new Int32(1));
    assertThatThrownBy(() -> instance.stack().pop())
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Empty stack");
  }

  @Test
  public void mustOnlyBeAbleToPopThreeFromThreeArityFrameAndValuesMustBeInCorrectOrder()
      throws IOException {
    load(WasmInstance.class, "memsize.wat");

    final InstructionNoArgs insn = new InstructionNoArgs(OpCode.RETURN);

    instance.newFrame(
        Uint31.ZERO, "test_outer", Uint31.ZERO, List.of(new InstructionNoArgs(OpCode.I_64_POPCNT)));
    instance.newFrame(Uint31.ZERO, "test_inner", new Uint31(3), List.of(insn));
    pushMany(instance.stack(), 4, 3, 2, 1);

    final Return executor = new Return();
    executor.execute(instance, insn);

    assertThat(instance.frame().getNextInstruction().getOpCode()).isEqualTo(OpCode.I_64_POPCNT);

    // Check that values are in the correct order
    assertThat(instance.stack().pop()).isEqualTo(new Int32(1));
    assertThat(instance.stack().pop()).isEqualTo(new Int32(2));
    assertThat(instance.stack().pop()).isEqualTo(new Int32(3));

    // Check that we cannot pop more than three values from the stack.
    assertThatThrownBy(() -> instance.stack().pop())
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Empty stack");
  }

  /**
   * Pushes multiple integers to stack as Int32 literals.
   *
   * @param stack stack to push to
   * @param ints values to push, pushed in order specified, so first integer will be placed lowest
   *     on the stack.
   */
  private static void pushMany(final ArrayStack<Literal> stack, final int... ints) {
    for (final int x : ints) {
      stack.push(new Int32(x));
    }
  }
}
