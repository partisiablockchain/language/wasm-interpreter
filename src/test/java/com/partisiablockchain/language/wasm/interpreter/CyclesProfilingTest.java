package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmFrameCyclesProfile;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Integration test for running WASM programs. */
public final class CyclesProfilingTest extends WasmTest {

  @Test
  public void emptyReportBeforeStart() throws IOException {
    load(WasmInstance.class, "fib.wat");

    assertThat(instance.getCyclesProfilingReport()).isEmpty();
  }

  @Test
  public void simpleReport() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.setCyclesMax(10000000L);
    instance.enableCyclesProfiling();
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    instance.popFrame();

    final List<WasmFrameCyclesProfile> report = instance.getCyclesProfilingReport();
    assertThat(report).hasSize(1);
    assertThat(report.get(0).frameName()).isEqualTo("func1");
    assertThat(report.get(0).cyclesUsedSpecific()).isEqualTo(1000);
    assertThat(report.get(0).cyclesUsedClosure()).isEqualTo(1000);
  }

  @Test
  public void reportAfterPop() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.setCyclesMax(10000000L);
    instance.enableCyclesProfiling();
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));

    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(2000));
    instance.popFrame();

    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(4000));
    instance.popFrame();

    instance.useCycles(new Uint31(8000));
    instance.popFrame();

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(
            new WasmFrameCyclesProfile("func1", 9000, 15000),
            new WasmFrameCyclesProfile("func2", 6000, 6000));
  }

  @Test
  public void reportBeforePop() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.setCyclesMax(10000000L);
    instance.enableCyclesProfiling();
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(2000));

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(
            new WasmFrameCyclesProfile("func1", 1000, 3000),
            new WasmFrameCyclesProfile("func2", 2000, 2000));
  }

  @Test
  public void stopInTheMiddle() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.setCyclesMax(10000000L);
    instance.enableCyclesProfiling();
    instance.newFrame(Uint31.ZERO, "func_a", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));

    instance.newFrame(Uint31.ZERO, "func_b", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(2000));
    instance.popFrame();

    instance.newFrame(Uint31.ZERO, "func_b", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(4000));
    instance.disableCyclesProfiling();
    instance.popFrame();

    instance.newFrame(Uint31.ZERO, "func_b", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(8000));
    instance.popFrame();

    instance.useCycles(new Uint31(16000));
    instance.popFrame();

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(
            new WasmFrameCyclesProfile("func_a", 1000, 7000),
            new WasmFrameCyclesProfile("func_b", 6000, 6000));
  }

  @Test
  public void startInTheMiddle() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.setCyclesMax(10000000L);
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));

    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(2000));
    instance.enableCyclesProfiling();
    instance.useCycles(new Uint31(4000));
    instance.popFrame();
    instance.useCycles(new Uint31(8000));
    instance.popFrame();

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(
            new WasmFrameCyclesProfile("func1", 8000, 12000),
            new WasmFrameCyclesProfile("func2", 4000, 4000));
  }

  @Test
  public void doubleStartInTheMiddle() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.setCyclesMax(10000000L);
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    instance.enableCyclesProfiling();
    instance.useCycles(new Uint31(2000));
    instance.enableCyclesProfiling();
    instance.useCycles(new Uint31(4000));

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(new WasmFrameCyclesProfile("func1", 6000, 6000));
  }

  @Test
  public void doubleStopInTheMiddle() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.enableCyclesProfiling();
    instance.setCyclesMax(10000000L);
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    instance.disableCyclesProfiling();
    instance.useCycles(new Uint31(2000));
    instance.disableCyclesProfiling();
    instance.useCycles(new Uint31(4000));

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(new WasmFrameCyclesProfile("func1", 1000, 1000));
  }

  @Test
  public void reset() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.enableCyclesProfiling();
    instance.setCyclesMax(10000000L);
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    instance.resetCyclesProfiling();
    instance.enableCyclesProfiling();
    instance.useCycles(new Uint31(2000));

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(new WasmFrameCyclesProfile("func1", 2000, 2000));
  }

  @Test
  public void resetInTheMiddle() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.enableCyclesProfiling();
    instance.setCyclesMax(10000000L);
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    instance.resetCyclesProfiling();
    instance.useCycles(new Uint31(2000));
    instance.popFrame();
    instance.enableCyclesProfiling();
    instance.useCycles(new Uint31(4000));

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(new WasmFrameCyclesProfile("func1", 4000, 4000));
  }

  @Test
  public void pauseInTheMiddle() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.enableCyclesProfiling();
    instance.setCyclesMax(10000000L);
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    instance.disableCyclesProfiling();
    instance.useCycles(new Uint31(2000));
    instance.enableCyclesProfiling();
    instance.useCycles(new Uint31(4000));

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(new WasmFrameCyclesProfile("func1", 5000, 5000));
  }

  @Test
  public void recursiveFunction() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.enableCyclesProfiling();
    instance.setCyclesMax(10000000L);
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(2000));
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(4000));
    instance.popFrame();
    instance.useCycles(new Uint31(8000));
    instance.popFrame();
    instance.useCycles(new Uint31(16000));
    instance.popFrame();

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(
            new WasmFrameCyclesProfile("func1", 21000, 31000),
            new WasmFrameCyclesProfile("func2", 10000, 14000));
  }

  @Test
  public void startInTheMiddleOfRecursiveFunction() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.setCyclesMax(10000000L);
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(2000));
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.enableCyclesProfiling();
    instance.useCycles(new Uint31(4000));
    instance.popFrame();
    instance.useCycles(new Uint31(8000));
    instance.popFrame();
    instance.useCycles(new Uint31(16000));
    instance.popFrame();

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(
            new WasmFrameCyclesProfile("func1", 20000, 28000),
            new WasmFrameCyclesProfile("func2", 8000, 12000));
  }

  @Test
  public void confusionResistantRecursiveHandling() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.setCyclesMax(10000000L);
    instance.enableCyclesProfiling();
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.disableCyclesProfiling();
    instance.popFrame();
    instance.popFrame();
    instance.popFrame();

    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.newFrame(Uint31.ZERO, "func2", Uint31.ZERO, List.of());
    instance.enableCyclesProfiling();
    instance.useCycles(new Uint31(1000));
    instance.popFrame();
    instance.useCycles(new Uint31(2000));
    instance.popFrame();
    instance.useCycles(new Uint31(4000));
    instance.popFrame();

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(
            new WasmFrameCyclesProfile("func2", 5000, 7000),
            new WasmFrameCyclesProfile("func1", 2000, 3000));
  }

  @Test
  public void reportDoesNotDisableNorReset() throws IOException {
    load(WasmInstance.class, "fib.wat");

    instance.setCyclesMax(10000000L);
    instance.enableCyclesProfiling();
    instance.newFrame(Uint31.ZERO, "func1", Uint31.ZERO, List.of());
    instance.useCycles(new Uint31(1000));
    assertThat(instance.getCyclesProfilingReport()).isNotEmpty();
    instance.useCycles(new Uint31(2000));

    assertThat(instance.getCyclesProfilingReport())
        .containsExactlyInAnyOrder(new WasmFrameCyclesProfile("func1", 3000, 3000));
  }
}
