package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.partisiablockchain.language.wasm.common.BlockResultType;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.interpreter.Instruction;
import com.partisiablockchain.language.wasm.interpreter.InstructionIfElse;
import com.partisiablockchain.language.wasm.interpreter.InstructionNoArgs;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmTest;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test for {@link InstructionIfElse}.
 *
 * @see InstructionIfElse
 */
public final class IfTest extends WasmTest {

  @Test
  public void testExecute() throws IOException {
    load(WasmInstance.class, "memsize.wat");

    InstructionIfElse ifElse =
        new InstructionIfElse(
            BlockResultType.fromValue(0x7f),
            List.of(new InstructionNoArgs(OpCode.NOP)),
            List.of(new InstructionNoArgs(OpCode.UNREACHABLE)));

    instance.newFrame(Uint31.ZERO, "test", Uint31.ZERO, List.of(ifElse));
    instance.stack().push(new Int32(1));

    assertThat(instance.frame().hasLabel()).isFalse();

    If executor = new If();
    executor.execute(instance, ifElse);

    Instruction nextInstruction = instance.frame().getNextInstruction();
    assertThat(nextInstruction.getOpCode()).isEqualTo(OpCode.NOP);
  }
}
