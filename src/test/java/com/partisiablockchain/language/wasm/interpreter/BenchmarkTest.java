package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.google.errorprone.annotations.FormatMethod;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import com.partisiablockchain.language.wasm.pbclib.HostedMemmove;
import com.partisiablockchain.language.wasm.wabt.Wat2Wasm;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;

/**
 * Performs basic benchmarking, outputting both an html table to a file and some text output to the
 * terminal.
 */
public final class BenchmarkTest {

  private static final int NANOS_IN_MILLIS = 1_000_000;
  private static final int INPUT_SIZE_MULT = 1;
  private static final long MAX_GAS = Long.MAX_VALUE;

  private int numRuns;

  private byte[] bytes;
  private WasmModule module;
  private WasmInstance instance;

  private static PrintStream outputStream = null;

  private static void initSiteOutputFile() {
    final File file = new File(System.getProperty("basedir") + "/target/site/benchmark.html");
    try {
      file.getParentFile().mkdirs();
      outputStream = new PrintStream(file);
    } catch (final java.io.FileNotFoundException e) {
      System.out.println("Could not write to file: " + file);
      e.printStackTrace();
    }
  }

  /** Closes the output file if it were ever initialized. */
  @AfterAll
  public static void closeOutputFile() {
    if (outputStream != null) {
      outputStream.close();
    }
  }

  void loadBenchmarkSuite() throws IOException {
    final long time0 = System.nanoTime();
    bytes =
        Wat2Wasm.translate(
            BenchmarkTest.class.getResourceAsStream("benchmark.wat"), BenchmarkTest.class);
    final long time1 = System.nanoTime();
    module = new WasmParser(bytes).parse();
    final long time2 = System.nanoTime();
    instance = WasmInstance.forModule(module);
    instance.registerExternal("memmove", new HostedMemmove(new Uint31(100), Uint31.ZERO));
    final long time3 = System.nanoTime();

    System.out.println(
        String.format(
            "Loading times: %sms to load bytes\t%sms to parse\t%sms to initialize",
            (time1 - time0) / NANOS_IN_MILLIS,
            (time2 - time1) / NANOS_IN_MILLIS,
            (time3 - time2) / NANOS_IN_MILLIS));
  }

  private static final List<String> HEADERS_NAMES =
      List.of(
          "Benchmark",
          "Input Size",
          "#Runs",
          "Fastest",
          "Median",
          "95th%",
          "Slowest",
          "Most cycles used",
          "Least cycles used");

  private static final List<String> HEADERS_UNITS =
      List.of("", "", " runs", "ms", "ms", "ms", "ms", " cycles", " cycles");

  private static final List<Integer> HEADERS_WIDTH = List.of(-40, 7, 3, 5, 5, 5, 5, 10, 10);

  private static final List<Integer> WANTED_COLUMNS = null; // List.of(0, 1, 2, 7, 8);

  /**
   * Runs a light version of the benchmark suite in order to ensure that the WASM program can be
   * loaded, and that the benchmark suite calls with the correct protocol.
   */
  @Test
  @DisabledIfEnvironmentVariable(named = "WASM_BENCHMARK", matches = ".+")
  public void lightBenchmarkSuite() throws IOException {
    numRuns = 1;
    runBenchmarkSuite();
  }

  /** Runs the full version of the benchmark suite. */
  @Test
  @EnabledIfEnvironmentVariable(named = "WASM_BENCHMARK", matches = ".+")
  public void heavyBenchmarkSuite() throws IOException {
    numRuns = 100;
    initSiteOutputFile();
    runBenchmarkSuite();
  }

  void runBenchmarkSuite() throws IOException {

    // HTML Output
    outputStreamPrint(
        "%s",
        "<html><head><style>td { min-width: 80px; } tr td:nth-child(1) { text-align: left; width:"
            + " 400px; font-family: mono; } thead { font-weight: bold }</style></head><body><table"
            + " style=\"text-align: right\">");
    outputStreamPrint("%s", "<thead>");
    outputRow(HEADERS_NAMES);
    outputStreamPrint("%s", "</thead>");

    ///
    loadBenchmarkSuite();
    benchLoad(bytes);

    bench("Warmup", 11_00, INPUT_SIZE_MULT * 1000, 76127);

    bench("Prime Sieve", 11_00, INPUT_SIZE_MULT * 1000, 76127);
    bench("Prime List", 11_01, INPUT_SIZE_MULT * 1000, 76127);
    bench("Trampoline Odd", 11_02, INPUT_SIZE_MULT * 100000 + 1, 1);
    bench("Trampoline plusZero", 11_03, INPUT_SIZE_MULT * 100000 + 1, 100001);
    bench("Sort random list", 11_04, INPUT_SIZE_MULT * 4000, (int) 3233770092L);

    bench("NopMap" + " insert", 20_0_1 + 10 * 0, INPUT_SIZE_MULT * 1000, 1);
    bench("NopMap" + " search", 20_0_2 + 10 * 0, INPUT_SIZE_MULT * 100, (int) 20000L);

    // Map implementation benchmarking
    final String[] mapImpls = new String[] {"BtreeMap", "VecMap", "SortedVecMap", "HashMap"};
    int mapImplIdx = 1;
    for (final String implName : mapImpls) {
      bench(implName + " insert", 20_0_1 + 10 * mapImplIdx, INPUT_SIZE_MULT * 1000, 1856145921);
      bench(
          implName + " search", 20_0_2 + 10 * mapImplIdx, INPUT_SIZE_MULT * 100, (int) 1410005648L);
      mapImplIdx++;
    }

    // State serialization & deserialization benchmarking
    final String[] stateNames =
        new String[] {
          "BTreeMap<u32,u32>",
          "VecMap<u32,u8>",
          "Empty",
          "VecMap<u32,u32>",
          "ListMap<u32,u32>",
          "Vec<u8>",
          "Vec<u32>",
        };

    int stateImplIdx = 0;
    final int stateSize = INPUT_SIZE_MULT * 1000;
    for (final String implName : stateNames) {
      bench(implName + " deserialize    ", 30_0_0 + 10 * stateImplIdx, stateSize, 1, (int) 2222L);
      bench(implName + " deserialize (0)", 30_0_0 + 10 * stateImplIdx, stateSize, 0, null);
      bench(implName + "   serialize    ", 30_0_1 + 10 * stateImplIdx, stateSize, 1, (int) 2222L);
      bench(implName + "   serialize (0)", 30_0_1 + 10 * stateImplIdx, stateSize, 0, null);
      stateImplIdx++;
    }

    outputStreamPrint("%s", "</table></body></html>");
  }

  private void benchLoad(final byte[] bytes) {
    final List<Long> times = new ArrayList<>();

    for (int i = 0; i < numRuns; i++) {
      final long timeBefore = System.nanoTime();
      final WasmInstance myInstance = WasmInstance.forModule(new WasmParser(bytes).parse());
      final long timeAfter = System.nanoTime();
      times.add((timeAfter - timeBefore) / NANOS_IN_MILLIS);
      assertThat(myInstance).isNotNull();
    }
    formatTimes("Parse&Init", 0, times, 0, 0);
  }

  private void bench(
      final String name, final int benchId, final int param, final Integer expectedOutcome) {
    bench(name, benchId, param, -1, expectedOutcome);
  }

  private void bench(
      final String name,
      final int benchId,
      final int param,
      final int param2,
      final Integer expectedOutcome) {
    final List<Long> times = new ArrayList<>();
    long cyclesUsedMin = Long.MAX_VALUE;
    long cyclesUsedMax = 0;

    //
    for (int i = 0; i < numRuns; i++) {
      final long timeBefore = System.nanoTime();
      final List<Literal> results =
          instance.runFunction(
              "run_program",
              MAX_GAS,
              List.of(new Int32(benchId), new Int32(param), new Int32(param2)));
      final long timeAfter = System.nanoTime();
      times.add((timeAfter - timeBefore) / NANOS_IN_MILLIS);
      assertThat(results).isNotNull();
      if (INPUT_SIZE_MULT == 1 && expectedOutcome != null) {
        assertThat(results)
            .describedAs("%s/%d(%d)", name, benchId, param)
            .containsExactly(new Int32(expectedOutcome));
      }
      cyclesUsedMax = Long.max(cyclesUsedMax, instance.getCyclesUsed());
      cyclesUsedMin = Long.min(cyclesUsedMin, instance.getCyclesUsed());
    }

    // Format stuff
    formatTimes(name, param, times, cyclesUsedMin, cyclesUsedMax);
  }

  private static void formatTimes(
      final String name,
      final int argument,
      final List<Long> times,
      final long cyclesUsedMin,
      final long cyclesUsedMax) {
    times.sort(null);
    final long timeMin = times.get(0);
    final long timeMax = times.get(times.size() - 1);
    final long timeMedian = times.get(times.size() / 2);
    final long time95th = times.get((times.size() * 20 - times.size()) / 20);

    outputRow(
        List.of(
            name,
            argument,
            times.size(),
            timeMin,
            timeMedian,
            time95th,
            timeMax,
            cyclesUsedMin,
            cyclesUsedMax));
  }

  private static <T> void outputRow(final List<T> cellValues) {
    outputStreamPrint("%s", "<tr>");
    int cellIndex = 0;
    for (final T cellValue : cellValues) {
      if (WANTED_COLUMNS == null || WANTED_COLUMNS.contains(cellIndex)) {
        System.out.print(
            String.format(
                "%" + HEADERS_WIDTH.get(cellIndex) + "s%s ",
                cellValue,
                HEADERS_UNITS.get(cellIndex)));
        outputStreamPrint(
            "<td>%s</td>", cellValue.toString().replace("<", "&lt;").replace(">", "&gt;"));
      }
      cellIndex++;
    }
    System.out.println("");
    outputStreamPrint("%s", "</tr>");
  }

  @FormatMethod
  private static void outputStreamPrint(final String fmt, final Object... args) {
    if (outputStream == null) {
      return;
    }
    outputStream.println(String.format(fmt, args));
  }
}
