package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.interpreter.InstructionBrTable;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link BranchTable}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.operator.BranchTable
 */
public final class BranchTableTest {

  @Test
  public void determineBranch() {
    InstructionBrTable instr =
        new InstructionBrTable(
            OpCode.BR_TABLE, new Uint31[] {new Uint31(1), new Uint31(3)}, new Uint31(2));

    assertThat(BranchTable.determineBranch(instr, new Int32(0))).isEqualTo(new Uint31(1));
    assertThat(BranchTable.determineBranch(instr, new Int32(1))).isEqualTo(new Uint31(3));
    assertThat(BranchTable.determineBranch(instr, new Int32(2))).isEqualTo(new Uint31(2));
    assertThat(BranchTable.determineBranch(instr, new Int32(3))).isEqualTo(new Uint31(2));
    assertThat(BranchTable.determineBranch(instr, new Int32(4))).isEqualTo(new Uint31(2));
    assertThat(BranchTable.determineBranch(instr, new Int32(-1))).isEqualTo(new Uint31(2));
    assertThat(BranchTable.determineBranch(instr, new Int32(Integer.MIN_VALUE)))
        .isEqualTo(new Uint31(2));
    assertThat(BranchTable.determineBranch(instr, new Int32(Integer.MAX_VALUE)))
        .isEqualTo(new Uint31(2));
  }
}
