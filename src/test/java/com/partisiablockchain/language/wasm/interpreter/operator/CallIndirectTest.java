package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import com.partisiablockchain.language.wasm.interpreter.WasmTest;
import java.util.List;
import org.junit.jupiter.api.Test;

final class CallIndirectTest extends WasmTest {

  @Test
  public void incompatibleIndirectCallTypes() {
    parse(
        CallIndirectTest.class,
        "(module "
            + "(type $a_type (func (param i32) (result i32))) "
            + "(type $b_type (func (param i64) (result i64))) "
            + "(func $a (type $a_type) local.get 0 return) "
            + "(func $b (type $b_type) local.get 0 return) "
            + "(table funcref (elem $a $b))"
            + ")");

    final InstructionU31 callIndirect = new InstructionU31(OpCode.CALL_INDIRECT, Uint31.ZERO);

    instance.newFrame(Uint31.ZERO, "test", Uint31.ZERO, List.of(callIndirect));
    instance.stack().push(new Int32(1));

    final CallIndirect executor = new CallIndirect();
    assertThatThrownBy(() -> executor.execute(instance, callIndirect))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Indirect call type mismatch.")
        .hasMessageContaining("Instruction wanted [i32] -> [i32]")
        .hasMessageContaining("stack argument referenced [i64] -> [i64]");
  }
}
