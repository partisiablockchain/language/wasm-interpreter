package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.BlockResultType;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmIllegalBytecodeException;
import com.partisiablockchain.language.wasm.parser.WasmParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import org.junit.jupiter.api.Test;

/**
 * Tests of {@link InstructionParser}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.InstructionParser
 */
public final class InstructionParserTest {

  @Test
  public void callIndirectFollowedByInvalid() {
    InstructionParser parser = concatToParser(OpCode.CALL_INDIRECT, 0x0, 0x1);
    assertThatThrownBy(parser::parseFunction)
        .isInstanceOf(WasmParseException.class)
        .hasMessage("Unknown table index for CALL_INDIRECT. Expected '0', but was '1'");
  }

  @Test
  public void memorySizeMustHaveZeroAsArgument() {
    InstructionParser parser = concatToParser(OpCode.MEMORY_SIZE, 0x31);
    assertThatThrownBy(parser::parseFunction)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("zero flag expected")
        .hasMessageContaining("MEMORY_SIZE");
  }

  @Test
  public void growMemoryMustHaveZeroAsArgument() {
    InstructionParser parser = concatToParser(OpCode.GROW_MEMORY, 0x21);
    assertThatThrownBy(parser::parseFunction)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("zero flag expected")
        .hasMessageContaining("GROW_MEMORY");
  }

  @Test
  public void callIndirectFollowedByEof() {
    InstructionParser parser = concatToParser(OpCode.CALL_INDIRECT, 0x0);
    assertThatThrownBy(parser::parseFunction)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Unexpected end of section or function");
  }

  @Test
  public void eof() {
    InstructionParser parser = concatToParser(OpCode.NOP);
    List<Instruction> list = parser.parseFunction();
    assertThat(list).hasSize(1);
    assertThat(list.get(0).getOpCode()).isEqualTo(OpCode.NOP);
  }

  @Test
  public void readIf() {
    InstructionParser parser =
        concatToParser(OpCode.IF, 0x40, OpCode.DROP, OpCode.ELSE, OpCode.RETURN, OpCode.END);

    InstructionIfElse parsedOperator = (InstructionIfElse) parser.parseNextRecursive();
    assertThat(parsedOperator.getOpCode()).isEqualTo(OpCode.IF);
    assertThat(parsedOperator.getBlockResultType()).isEqualTo(BlockResultType.EMPTY);
    assertThat(parsedOperator.getIfClause()).hasSize(2);
    assertThat(parsedOperator.getElseClause()).hasSize(2);
  }

  @Test
  public void readBrTableBad1() {
    InstructionParser parser = concatToParser(OpCode.BR_TABLE, 1);

    assertThatThrownBy(parser::parseNextRecursive)
        .isInstanceOf(WasmIllegalBytecodeException.class)
        .hasMessageContaining("Unexpected end");
  }

  @Test
  public void readBrTableBad2() {
    InstructionParser parser = concatToParser(OpCode.BR_TABLE, 1, 2 | 0x80, 3);

    assertThatThrownBy(parser::parseNextRecursive)
        .isInstanceOf(WasmIllegalBytecodeException.class)
        .hasMessageContaining("Unexpected end");
  }

  @Test
  @SuppressWarnings("unchecked")
  public void readBrTableGood1() {
    InstructionParser parser = concatToParser(OpCode.BR_TABLE, 2, 3, 4, 5);

    InstructionBrTable args = (InstructionBrTable) parser.parseNextRecursive();

    Uint31[] valueBranches = args.getValueBranches();
    assertThat(valueBranches.length).isEqualTo(2);
    assertThat(valueBranches[0].asInt()).isEqualTo(3);
    assertThat(valueBranches[1].asInt()).isEqualTo(4);
    assertThat(args.getDefaultBranch().asInt()).isEqualTo(5);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void readBrTableGood2OnlyDefault() {
    InstructionParser parser = concatToParser(OpCode.BR_TABLE, 0, 5);

    InstructionBrTable args = (InstructionBrTable) parser.parseNextRecursive();

    Uint31[] valueBranches = args.getValueBranches();
    assertThat(valueBranches.length).isEqualTo(0);
    assertThat(args.getDefaultBranch()).isEqualTo(new Uint31(5));
  }

  @Test
  public void const64() {
    InstructionParser parser = concatToParser(OpCode.I_64_CONST, 42L);

    InstructionI64 parsedOperator = (InstructionI64) parser.parseNextRecursive();
    assertThat(parsedOperator.getOpCode()).isEqualTo(OpCode.I_64_CONST);
    assertThat(parsedOperator.getArgument().value()).isEqualTo(42L);
  }

  @Test
  public void unknownOpcode() {
    InstructionParser parser = concatToParser(0xFF, 42L);
    assertThatThrownBy(parser::parseNextRecursive)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("illegal opcode")
        .hasMessageContaining("FF");
  }

  @Test
  public void floatingPointOpcode() {
    InstructionParser parser = concatToParser(0x5B);
    assertThatThrownBy(parser::parseNextRecursive)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Floating point opcode not supported")
        .hasMessageContaining("0x5B");
  }

  @Test
  public void write() {
    Random random = new Random();
    for (int i = 0; i < 100; i++) {
      int expected = random.nextInt();

      byte[] bytes = writeVarUint32(expected);
      int actual = new WasmByteStream(bytes).readVarUint32();
      assertThat(actual).isEqualTo(expected);
    }
  }

  /** The table index argument of an indirect call can be a leb-encoded 0. */
  @Test
  public void parseIndirectLebTable() {
    InstructionParser parser =
        concatToParser(OpCode.CALL_INDIRECT, 0x00, 0x80, 0x80, 0x80, 0x80, 0x00, OpCode.END);

    InstructionU31 parsedOperator = (InstructionU31) parser.parseNextRecursive();
    assertThat(parsedOperator.getOpCode()).isEqualTo(OpCode.CALL_INDIRECT);
    assertThat(parsedOperator.getArgument()).isEqualTo(new Uint31(0));
  }

  /** Utility for concatting a lot of objects to an instruction parser. */
  public static InstructionParser concatToParser(Object... stuff) {
    return new InstructionParser(new WasmByteStream(concat(stuff)));
  }

  /** Utility for concatting a lot of objects. */
  public static byte[] concat(Object... stuff) {
    List<byte[]> arrays = new ArrayList<>();
    for (Object object : stuff) {
      if (object instanceof byte[]) {
        arrays.add((byte[]) object);
      } else if (object instanceof Integer) {
        arrays.add(new byte[] {((Integer) object).byteValue()});
      } else if (object instanceof Int32) {
        Int32 int32 = (Int32) object;
        arrays.add(int32.toBytes());
      } else if (object instanceof Long) {
        Int64 int64 = new Int64((Long) object);
        arrays.add(int64.toBytes());
      } else if (object instanceof OpCode) {
        OpCode opCode = (OpCode) object;
        int value = opCode.getValue();
        arrays.add(concat(value));
      }
    }
    return concatArrays(arrays);
  }

  /** Utility for concatting a lot of arrays. */
  public static byte[] concatArrays(List<byte[]> arrays) {
    int newLen = 0;
    for (byte[] array : arrays) {
      newLen += array.length;
    }

    int offset = 0;
    byte[] result = new byte[newLen];
    for (byte[] array : arrays) {
      System.arraycopy(array, 0, result, offset, array.length);
      offset += array.length;
    }
    return result;
  }

  /** Utility for writing leb128-encoded 32-bit unsigned integers. */
  public static byte[] writeVarUint32(int value) {
    byte[] bytes = new byte[16];
    int index = 0;

    int remaining = value >>> 7;

    while (remaining != 0) {
      bytes[index++] = (byte) ((value & 0x7f) | 0x80);
      value = remaining;
      remaining >>>= 7;
    }

    bytes[index++] = (byte) (value & 0x7f);
    return Arrays.copyOfRange(bytes, 0, index);
  }
}
