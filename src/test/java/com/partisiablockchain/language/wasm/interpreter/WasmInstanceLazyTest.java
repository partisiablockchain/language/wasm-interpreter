package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.CodeSegment;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmLimitations;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.parser.WasmParseException;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import java.util.List;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Integration test for running WASM programs. */
public final class WasmInstanceLazyTest {

  private static final byte[] moduleWithMalformedInstruction =
      Hex.decode(
          "00 61 73 6D " // Magic
              + "01 00 00 00 " // Version
              + "01 04 01 60 00 00 " // TYPE section with function [] -> []
              + "03 04 03 00 00 00" // FUNCTION section with 3 functions
              + "07 0D 03 01 41 00 00 01 42 00 01 01 43 00 02 " // EXPORT section with 3 functions
              + "0A 0D 03 " // CODE section with 3 functions
              + "03 00 FF 0B " // function 0: malformed instruction (FF)
              + "04 00 10 00 0B " // function 1: calling malformed function
              + "02 00 0B" // function 2: nop
          );

  @Test
  public void eagerError() {
    final WasmModule module = new WasmParser(moduleWithMalformedInstruction).parse();
    assertThatThrownBy(() -> WasmInstance.forModule(module, WasmLimitations.DEFAULT))
        .isInstanceOf(WasmParseException.class)
        .hasMessage("Unknown or illegal opcode: 0xFF");
  }

  @Test
  public void lazyErrorAngryCodeSegment() {
    final WasmModule module = new WasmParser(moduleWithMalformedInstruction).parse();
    final CodeSegment code = module.getFunctionSection().getEntryOrNull(Uint31.ZERO).codeSegment();
    assertThatThrownBy(() -> code.getCode())
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Unknown or illegal opcode: 0xFF");
  }

  @Test
  public void lazyError() {
    final WasmModule module = new WasmParser(moduleWithMalformedInstruction).parse();
    final WasmInstance instance =
        WasmInstance.forModule(module, WasmLimitations.DEFAULT.withLazyValidation());
    assertThatThrownBy(() -> instance.runFunction("A", 9999, List.of()))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Calling invalid function A")
        .cause()
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Unknown or illegal opcode: 0xFF");
  }

  @Test
  public void lazyErrorIndirect() {
    final WasmModule module = new WasmParser(moduleWithMalformedInstruction).parse();
    final WasmInstance instance =
        WasmInstance.forModule(module, WasmLimitations.DEFAULT.withLazyValidation());
    assertThatThrownBy(() -> instance.runFunction("B", 9999, List.of()))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Calling invalid function A")
        .cause()
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Unknown or illegal opcode: 0xFF");
  }

  @Test
  public void lazyIgnoresErrorsInUncalledFunction() {
    final WasmModule module = new WasmParser(moduleWithMalformedInstruction).parse();
    final WasmInstance instance =
        WasmInstance.forModule(module, WasmLimitations.DEFAULT.withLazyValidation());

    instance.runFunction("C", 9999, List.of()); // Just checking that it's run
  }
}
