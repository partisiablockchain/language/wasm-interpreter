package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.ValType;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link OperatorTypeCheck}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.operator.OperatorTypeCheck
 */
public final class OperatorTypeCheckTest {

  @Test
  public void testExpectInt32() {
    Int32 literal = new Int32(0);
    literal.expectInt32();
    Call.validateTypes(List.of(ValType.I32), 0, literal);

    Assertions.assertThatThrownBy(() -> Int64.ZERO.expectInt32())
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Expected i32 but got i64");
  }

  @Test
  public void testExpectInt64() {
    Int64 literal = new Int64(0);
    literal.expectInt64();
    Call.validateTypes(List.of(ValType.I64), 0, literal);

    Assertions.assertThatThrownBy(() -> Int32.ZERO.expectInt64())
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Expected i64 but got i32");
  }

  @Test
  public void callValidateTypes() {
    List<ValType> params = new ArrayList<>();
    params.add(null);
    Assertions.assertThatThrownBy(() -> Call.validateTypes(params, 0, null))
        .isInstanceOf(TrapException.class)
        .hasMessage("Trap: Invalid type: null");

    Assertions.assertThatThrownBy(() -> Call.validateTypes(List.of(ValType.I32), 0, Int64.ZERO))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Expected i32 but got i64");

    Assertions.assertThatThrownBy(() -> Call.validateTypes(List.of(ValType.I64), 0, Int32.ZERO))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Expected i64 but got i32");

    Assertions.assertThatThrownBy(() -> Call.validateTypes(List.of(ValType.F32), 0, Int32.ZERO))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Floating point type not supported: f32");

    Assertions.assertThatThrownBy(() -> Call.validateTypes(List.of(ValType.F64), 0, Int32.ZERO))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Floating point type not supported: f64");
  }
}
