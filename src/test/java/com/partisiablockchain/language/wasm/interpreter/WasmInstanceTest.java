package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.AddressKind;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.ResizableLimits;
import com.partisiablockchain.language.wasm.common.TableType;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.TypeIndex;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.sections.DataSection;
import com.partisiablockchain.language.wasm.common.sections.ElementSection;
import com.partisiablockchain.language.wasm.common.sections.ExportSection;
import com.partisiablockchain.language.wasm.common.sections.FunctionSection;
import com.partisiablockchain.language.wasm.common.sections.GlobalSection;
import com.partisiablockchain.language.wasm.common.sections.ImportSection;
import com.partisiablockchain.language.wasm.common.sections.MemorySection;
import com.partisiablockchain.language.wasm.common.sections.TableSection;
import com.partisiablockchain.language.wasm.common.sections.TypeSection;
import com.partisiablockchain.language.wasm.parser.WasmConsistencyException;
import com.partisiablockchain.language.wasm.parser.WasmInterpreterLimitationException;
import com.partisiablockchain.language.wasm.validator.WasmValidationException;
import java.io.IOException;
import java.util.List;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;

/** Integration test for running WASM programs. */
public final class WasmInstanceTest extends WasmTest {

  @Test
  public void lookupFunction() throws IOException {
    load(WasmInstance.class, "fib.wat");
    List<ExecutableFunction> functions = instance.getExportedFunctions();
    ExecutableFunction expected = functions.get(0);
    ExecutableFunction lookedUpFunction = instance.lookupFunction(expected.index());
    assertThat(lookedUpFunction).isEqualTo(expected);
  }

  @Test
  public void frameFunctionName() throws IOException {
    load(WasmInstance.class, "fib.wat");
    WasmFrame frame = instance.newFrame(new Uint31(1), "some_func", new Uint31(1), List.of());
    assertThat(frame.getFrameName()).isEqualTo("some_func");
  }

  @Test
  public void frameSetLocal() throws IOException {
    load(WasmInstance.class, "fib.wat");
    instance.newFrame(new Uint31(1), "name", new Uint31(1), List.of());

    Int32 one = new Int32(1);
    instance.setLocal(new Uint31(0), one);
    assertThat(instance.getLocal(new Uint31(0))).isEqualTo(one);
  }

  @Test
  public void frameCannotAccessPreviousFramesOperandStack() throws IOException {
    load(WasmInstance.class, "fib.wat");
    instance.newFrame(Uint31.ZERO, "test_outer", Uint31.ZERO, List.of());
    instance.stack().push(new Int32(4));

    instance.newFrame(Uint31.ZERO, "test_inner", Uint31.ZERO, List.of());
    assertThatThrownBy(() -> instance.stack().pop())
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Empty stack");
  }

  @Test
  public void frameCannotAccessLowerFramesOperandStack() throws IOException {
    load(WasmInstance.class, "fib.wat");
    instance.newFrame(Uint31.ZERO, "test_outer", Uint31.ZERO, List.of());
    instance.newFrame(Uint31.ZERO, "test_inner", Uint31.ZERO, List.of());
    instance.stack().push(new Int32(4));
    instance.popFrame();

    assertThatThrownBy(() -> instance.stack().pop())
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Empty stack");
  }

  @Test
  public void initTables() {
    WasmModule module =
        moduleWithDefaults(
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            new TableSection(
                List.of(
                    new TableType(
                        AddressKind.FUNCTION, new ResizableLimits(new Uint31(1), new Uint31(2))))),
            null);

    instance = WasmInstance.forModule(module);
    assertThat(instance.getTable(new Uint31(0))).isEmpty();
    assertThat(instance.getTable(new Uint31(1))).isNull();
  }

  @Test
  public void executeInitializer() {
    assertThat(
            WasmInstance.executeInitializer(
                List.of(
                    new InstructionI32(OpCode.I_32_CONST, new Int32(1)),
                    new InstructionNoArgs(OpCode.END))))
        .isEqualTo(new Int32(1));

    assertThat(
            ((Int64)
                    WasmInstance.executeInitializer(
                        List.of(
                            new InstructionI64(OpCode.I_64_CONST, new Int64(1)),
                            new InstructionNoArgs(OpCode.END))))
                .value())
        .isEqualTo(1L);
  }

  @Test
  public void outOfCycles() throws IOException {
    load(WasmInstance.class, "fib.wat");
    assertThat(runFunction("fib", 20713, new Int32(15))).isEqualTo(new Int32(610));
    assertThatThrownBy(() -> runFunction("fib", 20712, new Int32(15)))
        .hasMessage("Out of instruction cycles! 20713/20712 instructions run");
  }

  @Test
  public void loadNames() throws IOException {
    // Load a WASM file with a names custom section
    load(WasmInstance.class, "fib-names.wat");
    assertThat(module.getFunctionSection().getEntryOrNull(Uint31.ZERO).debugName())
        .isEqualTo("fib");
  }

  @Test
  public void loadDifferentNames() throws IOException {
    // Load a WASM file where the names custom section differs from the export name
    load(WasmInstance.class, "fib-different-names.wat");
    assertThatThrownBy(() -> runFunction("calc", 20713, new Int32(15)))
        .hasMessage("Trap: Cannot find function: calc");
    assertThatThrownBy(() -> runFunction("main", 20713))
        .hasMessage("Trap: Cannot find function: main");
  }

  @Test
  public void invalidType() throws IOException {
    load(WasmInstance.class, "fib.wat");
    assertThatThrownBy(() -> runFunction("fib", new Int64(2)))
        .hasStackTraceContaining("fib():0")
        .hasStackTraceContaining("Expected i32 but got i64");
  }

  @Test
  public void invalidTypeIf() throws IOException {
    // Send an illegal type (I64) to an if instruction to trigger a trap
    assertThatThrownBy(() -> load(WasmInstance.class, "invalid-stack-if.wat"))
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining("Expected i32, but got i64");
  }

  @Test
  public void runNonExistingFunction() throws IOException {
    load(WasmInstance.class, "fib.wat");
    assertThatThrownBy(() -> runFunction("fob"))
        .hasMessageContaining("Cannot find function")
        .hasMessageContaining("fob");
  }

  @Test
  public void importEntryWithoutTypeIndex() {
    assertThatThrownBy(
            () -> {
              parse(WasmInstanceTest.class, "(module (import \"pbc\" \"memory\" (memory 1 2)))");
            })
        .isInstanceOf(WasmInterpreterLimitationException.class)
        .hasMessageContaining(
            "Cannot import \"memory\" from module \"pbc\": Only function imports are supported.");

    assertThatThrownBy(
            () -> {
              parse(
                  WasmInstanceTest.class,
                  "(module (import \"pbc\" \"table\" (table 10 20 funcref)))");
            })
        .isInstanceOf(WasmInterpreterLimitationException.class)
        .hasMessageContaining(
            "Cannot import \"table\" from module \"pbc\": Only function imports are supported.");

    assertThatThrownBy(
            () -> {
              parse(WasmInstanceTest.class, "(module (import \"pbc\" \"global\" (global i32)))");
            })
        .isInstanceOf(WasmInterpreterLimitationException.class)
        .hasMessageContaining(
            "Cannot import \"global\" from module \"pbc\": Only function imports are supported.");
  }

  @Test
  public void maxStackDepth() throws IOException {

    // Recursive routine that exactly hits max stack depth (300) without exhausting it
    load(WasmInstance.class, "max-stack-depth.wat");
    assertThatThrownBy(() -> runFunction("recursive", Long.MAX_VALUE, new Int32(300)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("call stack exhausted");

    // Recursive routine that exactly misses max stack depth (300)
    load(WasmInstance.class, "max-stack-depth.wat");
    runFunction("recursive", Long.MAX_VALUE, new Int32(299));
  }

  @Test
  public void functionImportReferenceUnknownType() {
    assertThatThrownBy(
            () -> {
              parse(
                  WasmInstanceTest.class,
                  "(module (import \"pbc\" \"ext_func\" (func (;0;) (type 0))))");
            })
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining(
            "Function import 0 references unknown type 0 for module with only 0 types");
  }

  @Test
  public void functionReferenceUnknownType() {
    assertThatThrownBy(
            () -> {
              parse(
                  WasmInstanceTest.class,
                  "(module "
                      + "(type (func (param i32) (result i32))) "
                      + "(func $a (type 3) return) "
                      + ")");
            })
        .isInstanceOf(WasmConsistencyException.class)
        .hasMessageContaining("Function 0 references unknown type 3 for module with only 1 types");
  }

  @Test
  public void wasmInstanceWrapsArbitraryException() {
    parse(
        WasmInstanceTest.class,
        "(module "
            + " (type (;0;) (func)) "
            + " (import \"pbc\" \"ext_func\" (func (;0;) (type 0))) "
            + " (export \"ext_func\" (func 0)) "
            + ")");

    instance.registerExternal(
        "ext_func",
        o -> {
          throw new SomeArbitraryException("Hello exception");
        });

    AssertionsForClassTypes.assertThatThrownBy(() -> runFunction("ext_func"))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Hello exception")
        .hasStackTraceContaining("ext_func():0")
        .cause()
        .isInstanceOf(SomeArbitraryException.class)
        .hasMessageContaining("Hello exception");
  }

  private static final class SomeArbitraryException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public SomeArbitraryException(String message) {
      super(message);
    }
  }

  @Test
  public void wasmInstanceDoesNotWrapErrors() {
    parse(
        WasmInstanceTest.class,
        "(module "
            + " (type (;0;) (func)) "
            + " (import \"pbc\" \"ext_func\" (func (;0;) (type 0))) "
            + " (export \"ext_func\" (func 0)) "
            + ")");

    instance.registerExternal(
        "ext_func",
        o -> {
          throw new OutOfMemoryError("OOM");
        });

    AssertionsForClassTypes.assertThatThrownBy(() -> runFunction("ext_func"))
        .isInstanceOf(OutOfMemoryError.class)
        .hasMessageContaining("OOM");
  }

  @Test
  public void cannotImportNonPbcModule() {
    AssertionsForClassTypes.assertThatThrownBy(
            () -> {
              parse(
                  WasmInstanceTest.class,
                  "(module "
                      + " (type (;0;) (func)) "
                      + " (import \"hello\" \"world\" (func (;0;) (type 0))) "
                      + ")");
            })
        .isInstanceOf(WasmInterpreterLimitationException.class)
        .hasMessage(
            "Cannot import from module \"hello\": Arbitrarily imported modules are not supported.");
  }

  @Test
  public void canReuseInstance() throws IOException {
    parse(
        WasmInstanceTest.class,
        "(module (func (export \"a\") (param i32) (result i32) local.get 0 i32.const 5 i32.add"
            + " return) )");

    for (int i = 0; i < 10000; i++) {
      assertThat(runFunction("a", new Int32(i))).isEqualTo(new Int32(i + 5));
    }

    // No problem if no errors occurs
  }

  @Test
  public void getGlobalValueByName() {
    parse(WasmInstanceTest.class, "(module (global (export \"somebody\") i32 (i32.const 55)))");

    assertThat(instance.getGlobalValue("somebody")).isEqualTo(new Int32(55));
    assertThat(instance.getGlobalValue("nobody")).isNull();
  }

  @Test
  public void correctUnwindingForBranch() {
    parse(
        WasmInstanceTest.class,
        "(module "
            + " (func $a (export \"\") (param i32) (result i32) "
            + " i32.const 42 local.get 0 br 0) "
            + " (export \"a\" (func $a)) "
            + ")");
    assertThat(instance.runFunction("a", 100000000, List.of(new Int32(1337))))
        .containsExactly(new Int32(1337));
  }

  @Test
  public void correctUnwindingForLoop() {
    parse(
        WasmInstanceTest.class,
        "(module "
            + " (func $a (export \"\") (param i32) (result i32) "
            + " loop (result i32) local.get 0 end) "
            + " (export \"a\" (func $a)) "
            + ")");
    assertThat(instance.runFunction("a", 100000000, List.of(new Int32(1337))))
        .containsExactly(new Int32(1337));
  }

  @Test
  public void usedAllCycles() {
    parse(
        WasmInstanceTest.class,
        "(module "
            + " (type (;0;) (func)) "
            + " (import \"pbc\" \"ext_func1\" (func (type 0))) "
            + " (import \"pbc\" \"ext_func2\" (func (type 0))) "
            + " (import \"pbc\" \"ext_func3\" (func (type 0))) "
            + " (export \"ext_func1\" (func 0)) "
            + " (export \"ext_func2\" (func 1)) "
            + " (export \"ext_func3\" (func 2)) "
            + ")");

    instance.registerExternal(
        "ext_func1",
        ins -> {
          ins.useCycles(Uint31.MAX_VALUE); // Use 2^31
          return List.of();
        });

    instance.registerExternal(
        "ext_func2",
        ins -> {
          ins.useCycles(Uint31.MAX_VALUE, Uint31.MAX_VALUE); // Use (2^31-1)^2 = 2^62 - 2^32 + 1
          return List.of();
        });

    instance.registerExternal(
        "ext_func3",
        ins -> {
          ins.useCycles(Uint31.MAX_VALUE, Uint31.MAX_VALUE); // Use (2^31-1)^2 = 2^62 - 2^32 + 1
          ins.useCycles(Uint31.MAX_VALUE, Uint31.MAX_VALUE); // Now at: 2^63 - 2^33 + 2 < 2^63
          ins.useCycles(Uint31.MAX_VALUE, Uint31.MAX_VALUE); // This should overflow
          return List.of();
        });

    AssertionsForClassTypes.assertThatThrownBy(() -> runFunction("ext_func1", 500L))
        .isInstanceOf(TrapException.class)
        .cause()
        .isInstanceOf(OutOfCyclesException.class)
        .hasMessage("Out of instruction cycles! 2147483648/500 instructions run");

    AssertionsForClassTypes.assertThatThrownBy(() -> runFunction("ext_func2", 500L))
        .isInstanceOf(TrapException.class)
        .cause()
        .isInstanceOf(OutOfCyclesException.class)
        .hasMessage("Out of instruction cycles! 4611686014132420610/500 instructions run");

    AssertionsForClassTypes.assertThatThrownBy(() -> runFunction("ext_func3", 500L))
        .isInstanceOf(TrapException.class)
        .cause()
        .isInstanceOf(OutOfCyclesException.class)
        .hasMessage("Out of instruction cycles! 4611686014132420610/500 instructions run");

    AssertionsForClassTypes.assertThatThrownBy(() -> runFunction("ext_func3", Long.MAX_VALUE))
        .isInstanceOf(TrapException.class)
        .cause()
        .isInstanceOf(OutOfCyclesException.class)
        .hasMessage(
            "Out of instruction cycles! 13835058042397261828/9223372036854775807 instructions run");
  }

  static WasmModule moduleWithDefaults(
      FunctionSection codeSection,
      DataSection dataSection,
      ElementSection elementSection,
      ExportSection exportSection,
      GlobalSection globalSection,
      ImportSection<TypeIndex> importSection,
      MemorySection memorySection,
      TableSection tableSection,
      TypeSection typeSection) {
    return WasmModule.withSections(
        codeSection != null ? codeSection : new FunctionSection(List.of()),
        dataSection != null ? dataSection : new DataSection(List.of()),
        elementSection != null ? elementSection : new ElementSection(List.of()),
        exportSection != null ? exportSection : new ExportSection(List.of()),
        globalSection != null ? globalSection : new GlobalSection(List.of()),
        importSection != null ? importSection : new ImportSection<TypeIndex>(List.of()),
        memorySection != null ? memorySection : new MemorySection(List.of()),
        tableSection != null ? tableSection : new TableSection(List.of()),
        typeSection != null ? typeSection : new TypeSection(List.of()));
  }
}
