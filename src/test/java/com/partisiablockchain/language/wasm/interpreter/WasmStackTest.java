package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import org.junit.jupiter.api.Test;

/**
 * Test of WasmStack functionality. Most functionality is already covered by {@link ArrayStackTest}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.ArrayStack
 */
public final class WasmStackTest {

  @Test
  public void popNamed() {
    final ArrayStack<Literal> stack = new ArrayStack<Literal>();
    stack.push(Int32.ZERO);
    stack.push(Int64.ZERO);

    assertThat(stack.pop().expectInt64()).isEqualTo(Int64.ZERO);
    assertThat(stack.pop().expectInt32()).isEqualTo(Int32.ZERO);
  }

  @Test
  public void popInvalid() {
    final ArrayStack<Literal> stack = new ArrayStack<Literal>();
    stack.push(Int32.ZERO);
    stack.push(Int64.ZERO);

    assertThatThrownBy(() -> stack.pop().expectInt32()).isInstanceOf(TrapException.class);
    assertThatThrownBy(() -> stack.pop().expectInt64()).isInstanceOf(TrapException.class);
  }
}
