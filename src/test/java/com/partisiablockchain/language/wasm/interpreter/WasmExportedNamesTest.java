package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.parser.WasmConsistencyException;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import java.util.List;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Integration test for running WASM programs. */
public final class WasmExportedNamesTest {

  private static final byte[] moduleWithDuplicatedExportedNames =
      Hex.decode(
          "00 61 73 6D " // Magic
              + "01 00 00 00 " // Version
              + "01 04 01 60 00 00 " // TYPE section with function [] -> []
              + "03 03 02 00 00" // FUNCTION section with 2 functions
              + "07 09 02 01 41 00 00 01 41 00 01 " // EXPORT section with 2 functions: 0/A and 1/A
              + "0A 07 02 " // CODE section with 2 functions
              + "02 00 0B" // function 0: nop
              + "02 00 0B" // function 1: nop
          );

  @Test
  public void duplicatedExportName() {
    final WasmParser parser = new WasmParser(moduleWithDuplicatedExportedNames);
    assertThatThrownBy(() -> parser.parse())
        .isInstanceOf(WasmConsistencyException.class)
        .hasMessage("Duplicate export name: A");
  }

  private static final byte[] moduleWithMultipleExportedFunction =
      Hex.decode(
          "00 61 73 6D " // Magic
              + "01 00 00 00 " // Version
              + "01 04 01 60 00 00 " // TYPE section with function [] -> []
              + "03 02 01 00" // FUNCTION section with 1 functions
              + "07 09 02 01 41 00 00 01 42 00 00 " // EXPORT section with 2 functions: 0/A and 0/B
              + "0A 04 01 " // CODE section with 1 functions
              + "02 00 0B" // function 1: nop
          );

  @Test
  public void functionWithMultipleExportedNames() {
    final WasmModule module = new WasmParser(moduleWithMultipleExportedFunction).parse();
    final WasmInstance instance = WasmInstance.forModule(module);

    instance.runFunction("B", 9999, List.of()); // Just check that function can be found and run
    instance.runFunction("A", 9999, List.of()); // Just check that function can be found and run
  }

  private static final byte[] moduleWithExportedMemory =
      Hex.decode(
          "00 61 73 6D " // Magic
              + "01 00 00 00 " // Version
              + "01 04 01 60 00 00 " // TYPE section with function [] -> []
              + "03 02 01 00" // FUNCTION section with 1 functions
              + "05 03 01 00 00" // MEMORY section with 1 empty memory
              + "07 05 01 01 41 02 00 " // EXPORT section with 1 memory: 0/A
              + "0A 04 01 " // CODE section with 1 functions
              + "02 00 0B" // function 1: nop
          );

  @Test
  public void functionsAreNotExportedForMemoryExportKinds() {
    final WasmModule module = new WasmParser(moduleWithExportedMemory).parse();
    final WasmInstance instance = WasmInstance.forModule(module);

    assertThatThrownBy(() -> instance.runFunction("A", 9999, List.of()))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Cannot find function: A");
  }

  private static final byte[] moduleWithUnknownExportedFunction =
      Hex.decode(
          "00 61 73 6D " // Magic
              + "01 00 00 00 " // Version
              + "01 04 01 60 00 00 " // TYPE section with function [] -> []
              + "03 02 01 00" // FUNCTION section with 1 functions
              + "07 05 01 01 41 00 02 " // EXPORT section with 1 function: 2/A (unknown)
              + "0A 04 01 " // CODE section with 1 functions
              + "02 00 0B" // function 1: nop
          );

  @Test
  public void cannotParseModuleWithUnknownExportedFunction() {
    final WasmParser parser = new WasmParser(moduleWithUnknownExportedFunction);

    assertThatThrownBy(() -> parser.parse())
        .isInstanceOf(WasmConsistencyException.class)
        .hasMessageContaining("Cannot export \"A\": Unknown FUNCTION 2");
  }

  private static final byte[] moduleWithDuplicatedDebugNames =
      Hex.decode(
          "00 61 73 6D " // Magic
              + "01 00 00 00 " // Version
              + "01 04 01 60 00 00 " // TYPE section with function [] -> []
              + "03 03 02 00 00" // FUNCTION section with 2 functions
              + "00 0E " // CUSTOM section
              + "04 6E 61 6D 65 " // section name "name"
              + "01 " // function name subsection
              + "07 " // subsection data length
              + "02 " // number of function names
              + "00 01 41 " // function 0 has name "A"
              + "01 01 41 " // function 1 has name "A"
              + "0A 07 02 " // CODE section with 2 functions
              + "02 00 0B " // function 0: nop
              + "02 00 0B" // function 1: nop
          );

  @Test
  public void duplicatedDebugNames() {
    final WasmModule module = new WasmParser(moduleWithDuplicatedDebugNames).parse();
    assertThat(module.getFunctionSection().getEntryOrNull(Uint31.ZERO).debugName()).isEqualTo("A");
    assertThat(module.getFunctionSection().getEntryOrNull(Uint31.ONE).debugName()).isEqualTo("A");
    final WasmInstance instance = WasmInstance.forModule(module);
    assertThatThrownBy(() -> instance.runFunction("A", 9999, List.of()))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Cannot find function: A");
  }

  private static final byte[] moduleWithNonStandardCustomSection =
      Hex.decode(
          "00 61 73 6D " // Magic
              + "01 00 00 00 " // Version
              + "00 0A" // Custom section
              + "05 64 65 62 75 67 " // section name: "debug"
              + "DE AD BE EF " // Some Bytes
          );

  @Test
  public void nonStandardCustomSection() {
    new WasmParser(moduleWithNonStandardCustomSection).parse();
  }

  private static final byte[] moduleWithDoublyImportedFunction =
      Hex.decode(
          "00 61 73 6D " // Magic
              + "01 00 00 00 " // Version
              + "01 04 01 60 00 00 " // TYPE section with function [] -> []
              + "02 0D 02 " // IMPORT section with 2 functions:
              + "01 41 01 41 00 00 " // Function 0/A.A with type 0
              + "01 41 01 41 00 00 " // Function 1/A.A with type 0
          );

  @Test
  public void doublyImportedFunction() {
    final WasmModule module = new WasmParser(moduleWithDoublyImportedFunction).parse();
    assertThat(module.getFunctionImportSection().numEntries()).isEqualTo(new Uint31(2));
  }

  private static final byte[] moduleWithDoublyExportedImportedFunction =
      Hex.decode(
          "00 61 73 6D " // Magic
              + "01 00 00 00 " // Version
              + "01 04 01 60 00 00 " // TYPE section with function [] -> []
              + "02 0D 02 " // IMPORT section with 2 functions:
              + "01 41 01 41 00 00 " // Function 0/A.A with type 0
              + "01 41 01 41 00 00 " // Function 1/A.A with type 0
              + "07 09 02 01 41 00 00 01 41 00 01 " // EXPORT section with 2 functions: 0/A and 1/A
          );

  @Test
  public void doublyExportedImportedFunction() {
    final WasmParser parser = new WasmParser(moduleWithDoublyExportedImportedFunction);
    assertThatThrownBy(() -> parser.parse())
        .isInstanceOf(WasmConsistencyException.class)
        .hasMessage("Duplicate export name: A");
  }
}
