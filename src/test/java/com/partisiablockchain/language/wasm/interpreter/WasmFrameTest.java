package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.wasm.common.BlockResultType;
import com.partisiablockchain.language.wasm.common.Uint31;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link WasmFrame}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.WasmFrame
 */
public final class WasmFrameTest {

  @Test
  public void functionName() {
    WasmFrame frame = new WasmFrame(new Uint31(1), "a", 0);
    assertThat(frame.getFrameName()).isEqualTo("a");
  }

  @Test
  public void rootLabel() {
    WasmFrame frame = new WasmFrame(new Uint31(0), "test", 0);
    frame.resetPc();
    frame.incrementProgramCounter();
    assertThat(frame.calculateProgramCounterForStackTrace()).isEqualTo(new Uint31(1));

    frame.pushRootLabel(new Uint31(0), List.of());
    assertThat(frame.calculateProgramCounterForStackTrace()).isEqualTo(new Uint31(0));
  }

  @Test
  public void calculateProgramCounter() {
    WasmFrame frame = new WasmFrame(new Uint31(0), "test", 0);
    frame.pushRootLabel(new Uint31(0), List.of());

    frame.incrementProgramCounter();
    assertThat(frame.calculateProgramCounterForStackTrace()).isEqualTo(new Uint31(1));

    ArrayList<Instruction> instructions = new ArrayList<>();
    for (int i = 0; i < 10; i++) {
      instructions.add(null);
    }

    frame.pushLabel(new Uint31(0), BlockResultType.I32, false, instructions);
    frame.incrementProgramCounter();
    frame.pushLabel(new Uint31(0), BlockResultType.I32, false, instructions);

    assertThat(frame.calculateProgramCounterForStackTrace()).isEqualTo(new Uint31(2));
  }
}
