package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import java.io.IOException;
import java.util.List;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Miscellaneous Integration tests parsing and running different WASM programs. */
public final class MiscIntegrationTest extends WasmTest {

  @Test
  public void fib() throws IOException {
    load(WasmInstance.class, "fib.wat");
    assertThat(runI32("fib", new Int32(20))).isEqualTo(6765);
  }

  @Test
  public void testHostFunctions() throws IOException {
    load(WasmParser.class, "external-call.wat");
    byte[] decode = Hex.decode(RUST_PROGRAM_HEX);

    instance.registerExternal(
        "read_context_into_address",
        o -> {
          Int64 ptr = (Int64) instance.getLocal(new Uint31(0));
          instance.getMemory(new Uint31(0)).write(new Uint31(ptr.toInt32()), decode);
          instance.useCycles(new Uint31(1000));
          return List.of(new Int32(decode.length));
        });

    int sum = 0;
    for (byte b : decode) {
      sum = sum ^ (b & 0xFF);
    }

    int foo = runI32("foo");
    assertThat(foo).isEqualTo(sum);
    assertThat(instance.getCyclesUsed()).isEqualTo(1905025L);
  }

  @Test
  public void testBadCallToHostFunction() throws IOException {
    parse(
        MiscIntegrationTest.class,
        "(module "
            + "  (type (;0;) (func (param i64 i32) (result i32))) "
            + "  (import \"pbc\" \"ext_func\" (func (;0;) (type 0))) "
            + "  (export \"ext_func\" (func 0))"
            + ")");

    instance.registerExternal(
        "ext_func",
        o -> {
          return List.of(new Int32(0));
        });

    assertThatThrownBy(() -> runFunction("ext_func", new Int32(0), new Int32(0)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Expected i64 but got i32");
  }

  @Test
  public void formatAsHex() throws IOException {
    load(WasmParser.class, "external-call.wat");
    int foo = runI32("create_action_name", new Int32(0xBEEF));
    assertThat(foo).isEqualTo("action_beef".length());
  }

  @Test
  public void testVectors() throws IOException {
    load(WasmParser.class, "vector-magic.wat");

    instance.registerExternal(
        "read_context_into_address",
        o -> {
          Int64 ptr = (Int64) instance.getLocal(new Uint31(0));
          byte[] decode = Hex.decode(RUST_PROGRAM_HEX);
          instance.getMemory(new Uint31(0)).write(new Uint31(ptr.toInt32()), decode);
          return List.of(new Int32(decode.length));
        });

    int foo = runI32("foo");
    System.out.println("Cycles cost: " + instance.getCyclesUsed());

    assertThat(foo).isEqualTo(42);
  }

  private static String RUST_PROGRAM_HEX =
      "9e4dc14500fe3eb2d"
          + "b366fc770c522d4b4"
          + "2f0996b8fb83064a0"
          + "054ee8675498acc9f"
          + "4d22ffa1b1fe92046"
          + "1f54ebf005bd9e5f0"
          + "6334689b2c117b752"
          + "60fa511424e445300"
          + "00000000008608000"
          + "0017c0d55b8712ef9"
          + "52fbcfece19c6b66d"
          + "6953c48690f030127"
          + "cdc5b128adb61c17d"
          + "53e8b7aab2ef952fb"
          + "cfece19c6b66d6953"
          + "c48690f030127cdc5"
          + "b128adb61c17d53e8"
          + "b7aab000000000000"
          + "00000150696563654f"
          + "664569676874202020"
          + "200000000000bc4b210"
          + "0000000";
}
