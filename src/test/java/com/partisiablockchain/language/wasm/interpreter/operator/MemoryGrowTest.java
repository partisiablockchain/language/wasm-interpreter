package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmTest;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test for {@link InstructionMemoryGrow}.
 *
 * @see InstructionMemoryGrow
 */
public final class MemoryGrowTest extends WasmTest {

  @Test
  public void testVeryBigAdditional() throws IOException {
    load(WasmInstance.class, "memsize.wat");

    InstructionU31 insn = new InstructionU31(OpCode.GROW_MEMORY, Uint31.ZERO);
    instance.newFrame(Uint31.ZERO, "test", Uint31.ZERO, List.of(insn));
    instance.stack().push(new Int32(Integer.MIN_VALUE));

    MemoryGrow executor = new MemoryGrow();
    executor.execute(instance, insn);

    assertThat(instance.stack().pop().expectInt32()).isEqualTo(new Int32(-1));
  }
}
