package com.partisiablockchain.language.wasm.interpreter.operator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.interpreter.WasmTest;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link MemorySize}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.operator.MemorySize
 */
public final class MemorySizeTest extends WasmTest {

  @Test
  public void memgrow() throws IOException {
    load(WasmInstance.class, "memsize.wat");

    assertThat(instance.getMemory(new Uint31(0)).size()).isEqualTo(new Uint31(65536));

    assertValue(instance.runFunction("memgrow", 100000L, List.of(new Int32(1))), 1);
    assertValue(instance.runFunction("memgrow", 100000L, List.of(new Int32(20))), -1);
  }

  @Test
  public void memsize() throws IOException {
    load(WasmInstance.class, "memsize.wat");

    assertThat(instance.getMemory(new Uint31(0)).size()).isEqualTo(new Uint31(65536));

    assertValue(instance.runFunction("memsize", 100000L, List.of()), 1);

    instance.getMemory(new Uint31(0)).grow(new Uint31(1));
    assertValue(instance.runFunction("memsize", 100000L, List.of()), 2);
  }

  @Test
  public void memsizeInvalidArgument() {
    MemorySize executor = new MemorySize();
    assertThatThrownBy(
            () -> executor.execute(null, new InstructionU31(OpCode.MEMORY_SIZE, new Uint31(1))))
        .isInstanceOf(TrapException.class);
  }

  private void assertValue(List<Literal> list, int value) {
    assertThat(list).containsExactly(new Int32(value));
  }
}
