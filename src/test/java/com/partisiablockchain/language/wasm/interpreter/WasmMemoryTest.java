package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmLimitations;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link WasmMemory}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.WasmMemory
 */
public final class WasmMemoryTest {

  private WasmMemory memory;

  /** Sets up the test. */
  @BeforeEach
  public void setUp() {
    memory = new WasmMemory(new Uint31(1), new Uint31(2));
  }

  @Test
  public void constructorLimits() {
    assertThat(new WasmMemory(new Uint31(1), new Uint31(0)).getMaxPages()).isEqualTo(new Uint31(0));

    assertThat(new WasmMemory(new Uint31(1), new Uint31(2)).getMaxPages()).isEqualTo(new Uint31(2));
    assertThat(
            new WasmMemory(new Uint31(1), WasmLimitations.DEFAULT.maxMemoryPages().minusOne())
                .getMaxPages())
        .isEqualTo(WasmLimitations.DEFAULT.maxMemoryPages().minusOne());
    assertThat(
            new WasmMemory(new Uint31(1), WasmLimitations.DEFAULT.maxMemoryPages()).getMaxPages())
        .isEqualTo(WasmLimitations.DEFAULT.maxMemoryPages());

    assertThat(new WasmMemory(new Uint31(1), Uint31.MAX_VALUE).getMaxPages())
        .isEqualTo(Uint31.MAX_VALUE);
    assertThat(
            new WasmMemory(new Uint31(1), WasmLimitations.DEFAULT.maxMemoryPages().addOne())
                .getMaxPages())
        .isEqualTo(WasmLimitations.DEFAULT.maxMemoryPages().addOne());
  }

  @Test
  public void setGet() {
    Uint31 address = new Uint31(10);
    assertThat(memory.read(address, Uint31.ONE)[0]).isEqualTo((byte) 0);
    memory.write(address, new byte[] {2});
    assertThat(memory.read(address, Uint31.ONE)[0]).isEqualTo((byte) 2);
  }

  @Test
  public void boundsCheck() {
    Uint31 address = new Uint31(64).mult(new Uint31(1024)).minusOne();
    memory.write(address, new byte[] {1});

    assertThatThrownBy(() -> memory.write(address.addOne(), new byte[] {1}))
        .isInstanceOf(TrapException.class);

    assertThatThrownBy(() -> memory.read(address.addOne(), Uint31.ONE))
        .isInstanceOf(TrapException.class);

    assertThatThrownBy(() -> memory.write(Uint31.MAX_VALUE, new byte[] {1}))
        .isInstanceOf(TrapException.class);

    assertThatThrownBy(() -> memory.read(Uint31.ZERO, Uint31.MAX_VALUE))
        .isInstanceOf(TrapException.class);
  }

  @Test
  public void growOutOfBounds() {
    assertThat(memory.grow(new Uint31(1))).isTrue();
    assertThat(memory.grow(new Uint31(1))).isFalse();
  }

  @Test
  public void goodMemmove() {
    memory.write(new Uint31(16), "Hello, World!".getBytes(UTF_8));

    assertThat(memory.read(new Uint31(0), new Uint31(5))).containsExactly(0, 0, 0, 0, 0);

    assertThat(memory.read(new Uint31(16), new Uint31(13))).asString().isEqualTo("Hello, World!");

    memory.memmove(new Uint31(16), new Uint31(23), new Uint31(5));

    assertThat(memory.read(new Uint31(16), new Uint31(13))).asString().isEqualTo("World, World!");

    memory.memmove(new Uint31(16), new Uint31(18), new Uint31(3));
    memory.memmove(new Uint31(25), new Uint31(23), new Uint31(3));

    assertThat(memory.read(new Uint31(16), new Uint31(13))).asString().isEqualTo("rldld, WoWor!");
  }

  @Test
  public void badMemmove() {
    memory.write(new Uint31(16), "Hello, World!".getBytes(UTF_8));

    final int memorySize = memory.size().asInt();

    assertThatThrownBy(() -> memory.memmove(new Uint31(memorySize), new Uint31(0), new Uint31(8)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Reading 8 bytes at address " + memorySize);

    assertThatThrownBy(() -> memory.memmove(new Uint31(0), new Uint31(memorySize), new Uint31(9)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Reading 9 bytes at address " + memorySize);

    assertThatThrownBy(() -> memory.memmove(new Uint31(0), new Uint31(1), new Uint31(memorySize)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Reading " + memorySize + " bytes at address 1");

    assertThatThrownBy(() -> memory.memmove(new Uint31(1), new Uint31(0), new Uint31(memorySize)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Out of bounds memory access")
        .hasMessageContaining("Reading " + memorySize + " bytes at address 1");
  }

  @Test
  public void weirdGoodCornercaseMemmove() {
    memory.write(new Uint31(16), "Hello, World!".getBytes(UTF_8));

    final int memSize = memory.size().asInt();

    memory.memmove(new Uint31(0), new Uint31(0), new Uint31(memSize));

    assertThat(memory.read(new Uint31(0), new Uint31(5))).containsExactly(0, 0, 0, 0, 0);

    assertThat(memory.read(new Uint31(16), new Uint31(13))).asString().isEqualTo("Hello, World!");
  }
}
