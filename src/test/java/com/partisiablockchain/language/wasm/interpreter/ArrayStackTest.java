package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link ArrayStack}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.ArrayStack
 */
public final class ArrayStackTest {

  @Test
  public void peekEmpty() {
    final ArrayStack<Literal> stack = new ArrayStack<Literal>();
    assertThat(stack.peek()).isNull();
  }

  @Test
  public void popEmpty() {
    final ArrayStack<Literal> stack = new ArrayStack<Literal>();
    assertThatThrownBy(stack::pop)
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Empty stack");
  }

  @Test
  public void testToString() {
    final ArrayStack<Literal> stack = new ArrayStack<Literal>();
    stack.push(new Int32(3));
    stack.push(new Int64(1));

    assertThat(stack.toString()).isEqualTo("[3, 1]");
  }

  @Test
  public void stackFunctionality() {
    final ArrayStack<Literal> stack = new ArrayStack<Literal>();
    stack.push(Int32.ZERO);
    stack.push(Int32.ZERO);
    stack.push(Int64.ZERO);

    assertThat(stack.peek(0)).isSameAs(stack.peek());
    assertThat(stack.peek(0)).isEqualTo(Int64.ZERO);
    assertThat(stack.peek(1)).isEqualTo(Int32.ZERO);
    assertThat(stack.peek(2)).isEqualTo(Int32.ZERO);
  }

  @Test
  public void popMany() {
    final ArrayStack<Integer> stack = new ArrayStack<>();
    stack.push(1);
    stack.push(2);
    stack.push(3);
    stack.push(4);
    stack.push(5);
    assertThatThrownBy(() -> stack.popN(2147483647))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Empty stack");

    assertThat(stack.popN(2)).containsExactly(5, 4);
    assertThat(stack.popN(3)).containsExactly(3, 2, 1);
  }

  @Test
  public void pushPop() {
    final ArrayStack<Integer> stack = new ArrayStack<>();
    stack.pushReversed(List.of(1, 2, 3, 4, 5));
    assertThat(stack.popN(5)).containsExactly(1, 2, 3, 4, 5);
  }

  @Test
  public void peekOutOfBounds() {
    final ArrayStack<Literal> stack = new ArrayStack<Literal>();
    assertThat(stack.peek(-1)).isNull();
    assertThat(stack.peek(0)).isNull();
    assertThat(stack.peek(1)).isNull();
  }
}
