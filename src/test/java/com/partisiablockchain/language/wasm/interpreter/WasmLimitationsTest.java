package com.partisiablockchain.language.wasm.interpreter;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.WasmLimitations;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link WasmLimitations}.
 *
 * @see com.partisiablockchain.language.wasm.interpreter.WasmLimitations
 */
public final class WasmLimitationsTest {

  @Test
  public void constructorErrors() {
    assertThatThrownBy(() -> new WasmLimitations(-565, new Uint31(1), true))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("maxStackFrameDepth must be positive, was -565");

    assertThatThrownBy(() -> new WasmLimitations(300, null, true))
        .isInstanceOf(NullPointerException.class);
  }
}
