package com.partisiablockchain.language.wasm.interpreter.operator.i64;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.literal.Int64;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test for UnaryCalcOpI64. */
public final class UnaryCalcOpI64Test {

  @Test
  public void testExecuteWithZero() {
    UnaryCalcOpI64 unaryCalcOpI64 = new UnaryCalcOpI64(Function.identity());
    Int64 int64 = new Int64(0);
    Assertions.assertThat(unaryCalcOpI64.execute(null, int64)).isEqualTo(int64);
  }

  @Test
  public void testExecuteWithDump() {
    UnaryCalcOpI64 unaryCalcOpI64 = new UnaryCalcOpI64(i -> null);
    Int64 int64 = new Int64(0);
    Assertions.assertThat(unaryCalcOpI64.execute(null, int64)).isNull();
  }
}
