package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link Import}.
 *
 * @see com.partisiablockchain.language.wasm.common.Import
 */
public final class ImportTest {

  @Test
  public void values() {
    TypeIndex descriptor = new TypeIndex(new Uint31(2));
    Import<TypeIndex> anImport = create("import", descriptor);
    assertThat(anImport.getModuleName()).isEqualTo("pbc");
    assertThat(anImport.getImportedName()).isEqualTo("import");
    assertThat(anImport.getImportDescriptor()).isSameAs(descriptor);
  }

  @Test
  public void parseTypeIndex() {
    WasmByteStream stream = new WasmByteStream(new byte[] {0, 1, 1, 1, 1});

    ImportEntryType entry = Import.parseEntry(stream);
    assertThat(entry).isInstanceOf(TypeIndex.class);

    TypeIndex index = (TypeIndex) entry;
    assertThat(index.getValue()).isEqualTo(new Uint31(1));
  }

  @Test
  public void parseTableType() {
    WasmByteStream stream = new WasmByteStream(new byte[] {1, 0x70, 1, 1, 17});

    ImportEntryType entry = Import.parseEntry(stream);
    assertThat(entry).isInstanceOf(TableType.class);

    TableType tableType = (TableType) entry;
    assertThat(tableType.getAddressKind()).isEqualTo(AddressKind.FUNCTION);
    ResizableLimits limits = tableType.getLimits();
    assertThat(limits.getMinimum()).isEqualTo(new Uint31(1));
    assertThat(limits.getMaximum()).isEqualTo(new Uint31(17));
  }

  @Test
  public void parseMemoryType() {
    WasmByteStream stream = new WasmByteStream(new byte[] {2, 1, 1, 17});

    ImportEntryType entry = Import.parseEntry(stream);
    assertThat(entry).isInstanceOf(MemoryType.class);

    MemoryType memoryType = (MemoryType) entry;
    ResizableLimits limits = memoryType.getLimits();
    assertThat(limits.getMinimum()).isEqualTo(new Uint31(1));
    assertThat(limits.getMaximum()).isEqualTo(new Uint31(17));
  }

  @Test
  public void parseGlobalType() {
    WasmByteStream stream = new WasmByteStream(new byte[] {3, 0x7f, 1});

    ImportEntryType entry = Import.parseEntry(stream);
    assertThat(entry).isInstanceOf(GlobalType.class);

    GlobalType globalType = (GlobalType) entry;
    assertThat(globalType.getContentType()).isEqualTo(ValType.I32);
    assertThat(globalType.isMutable()).isTrue();
  }

  @Test
  public void invalidType() {
    WasmByteStream stream = new WasmByteStream(new byte[] {4});
    assertThatThrownBy(() -> Import.parseEntry(stream)).hasMessage("Malformed import kind: 0x04");
  }

  private static <T extends ImportEntryType> Import<T> create(String name, T descriptor) {
    return new Import<T>("pbc", name, descriptor);
  }
}
