package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link FunctionType}.
 *
 * <p>Simple test of struct.
 */
public final class FunctionTypeTest {

  @Test
  public void values() {
    FunctionType type = new FunctionType(new Uint31(1), List.of(ValType.I32), List.of(ValType.I64));
    assertThat(type.getIndex()).isEqualTo(new Uint31(1));
    assertThat(type.getParams()).containsExactly(ValType.I32);
    assertThat(type.getReturns()).containsExactly(ValType.I64);
  }

  @Test
  public void callCompatibleWith() {
    FunctionType type1 =
        new FunctionType(new Uint31(1), List.of(ValType.I32), List.of(ValType.I32));
    FunctionType type2 =
        new FunctionType(new Uint31(2), List.of(ValType.I64), List.of(ValType.I32));
    FunctionType type3 =
        new FunctionType(new Uint31(3), List.of(ValType.I64), List.of(ValType.I32));

    assertThat(type1.callCompatibleWith(type2)).isFalse();
    assertThat(type2.callCompatibleWith(type1)).isFalse();
    assertThat(type1.callCompatibleWith(type3)).isFalse();

    assertThat(type1.callCompatibleWith(type1)).isTrue();
    assertThat(type2.callCompatibleWith(type3)).isTrue();
  }

  @Test
  public void testToString() {
    FunctionType type = new FunctionType(new Uint31(1), List.of(ValType.I32), List.of(ValType.I64));
    assertThat(type.toString()).isEqualTo("FunctionType{index=1, params=[i32], returns=[i64]}");
  }

  @Test
  public void testToTypeString() {
    final FunctionType type =
        new FunctionType(new Uint31(1), List.of(ValType.I32), List.of(ValType.I64));
    assertThat(type.toTypeString()).isEqualTo("[i32] -> [i64]");
  }
}
