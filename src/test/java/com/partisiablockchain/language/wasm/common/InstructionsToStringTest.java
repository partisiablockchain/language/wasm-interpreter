package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.wasm.interpreter.Instruction;
import com.partisiablockchain.language.wasm.interpreter.InstructionParser;
import com.partisiablockchain.language.wasm.interpreter.InstructionParserTest;
import java.util.List;
import org.junit.jupiter.api.Test;

/** Test of toString of instructions. */
public final class InstructionsToStringTest {

  @Test
  public void name() {
    InstructionParser parser =
        InstructionParserTest.concatToParser(
            OpCode.I_32_CONST,
            InstructionParserTest.writeVarUint32(1),
            OpCode.I_32_EQZ,
            OpCode.IF,
            0x40,
            OpCode.NOP,
            OpCode.ELSE,
            OpCode.NOP,
            OpCode.NOP,
            OpCode.END,
            OpCode.BLOCK,
            0x40,
            OpCode.NOP,
            OpCode.END);

    List<Instruction> function = parser.parseFunction();
    assertThat(function.size()).isEqualTo(4);

    assertThat(function.get(0).toString()).isEqualTo("I_32_CONST 1");
    assertThat(function.get(1).toString()).isEqualTo("I_32_EQZ");
    assertThat(function.get(2).toString()).isEqualTo("IF EMPTY (if = 2, else = 3)");
    assertThat(function.get(3).toString()).isEqualTo("BLOCK EMPTY (len = 2)");
  }
}
