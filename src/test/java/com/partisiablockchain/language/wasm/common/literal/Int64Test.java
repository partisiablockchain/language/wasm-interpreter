package com.partisiablockchain.language.wasm.common.literal;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test of {@link Int64}.
 *
 * @see com.partisiablockchain.language.wasm.common.literal.Int64
 */
public final class Int64Test {
  private long leftValue;
  private long rightValue;

  /**
   * Tests conversions.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void conversions(LongPair longPair) {
    set(longPair);
    assertThat(left().toString()).isEqualTo(Long.toString(leftValue));
    assertThat(left().toInt32()).isEqualTo((int) leftValue);
  }

  private void set(LongPair longPair) {
    this.leftValue = longPair.lhs;
    this.rightValue = longPair.rhs;
  }

  private static final long[] essentialNumbers = {
    0L, 1L, -1L, Long.MAX_VALUE, Long.MIN_VALUE, 6131245312L, Integer.MIN_VALUE
  };

  /**
   * Creates the parameters for the test.
   *
   * @return the parameters
   */
  public static List<LongPair> getSeeds() {
    return Arrays.stream(essentialNumbers)
        .mapToObj(l -> Arrays.stream(essentialNumbers).mapToObj(r -> new LongPair(l, r)))
        .flatMap(list -> list)
        .collect(Collectors.toList());
  }

  private Int64 left() {
    return new Int64(leftValue);
  }

  private Int64 right() {
    return new Int64(rightValue);
  }

  /**
   * Tests greaterThanSigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void greaterThanSigned(LongPair longPair) {
    set(longPair);
    assertThat(left().greaterThanSigned(right())).isEqualTo(leftValue > rightValue);
  }

  /**
   * Tests greaterThanEqualSigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void greaterThanEqualSigned(LongPair longPair) {
    set(longPair);
    assertThat(left().greaterThanEqualSigned(right())).isEqualTo(leftValue >= rightValue);
  }

  /**
   * Tests greaterThanUnsigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void greaterThanUnsigned(LongPair longPair) {
    set(longPair);
    assertThat(left().greaterThanUnsigned(right()))
        .isEqualTo(Long.compareUnsigned(leftValue, rightValue) > 0);
  }

  /**
   * Tests greaterThanEqualUnsigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void greaterThanEqualUnsigned(LongPair longPair) {
    set(longPair);
    assertThat(left().greaterThanEqualUnsigned(right()))
        .isEqualTo(Long.compareUnsigned(leftValue, rightValue) >= 0);
  }

  /**
   * Tests lessThanSigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void lessThanSigned(LongPair longPair) {
    set(longPair);
    assertThat(left().lessThanSigned(right())).isEqualTo(leftValue < rightValue);
  }

  /**
   * Tests lessThanEqualSigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void lessThanEqualSigned(LongPair longPair) {
    set(longPair);
    assertThat(left().lessThanEqualSigned(right())).isEqualTo(leftValue <= rightValue);
  }

  /**
   * Tests lessThanUnsigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void lessThanUnsigned(LongPair longPair) {
    set(longPair);
    assertThat(left().lessThanUnsigned(right()))
        .isEqualTo(Long.compareUnsigned(leftValue, rightValue) < 0);
  }

  /**
   * Tests lessThanEqualUnsigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void lessThanEqualUnsigned(LongPair longPair) {
    set(longPair);
    assertThat(left().lessThanEqualUnsigned(right()))
        .isEqualTo(Long.compareUnsigned(leftValue, rightValue) <= 0);
  }

  /**
   * Tests equalsZero.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void equalsZero(LongPair longPair) {
    set(longPair);
    assertThat(left().equalsZero()).isEqualTo(leftValue == 0);
  }

  /**
   * Tests multiply.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void multiply(LongPair longPair) {
    set(longPair);
    assertThat(left().multiply(right())).isEqualTo(new Int64(leftValue * rightValue));
  }

  /**
   * Tests add.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void add(LongPair longPair) {
    set(longPair);
    assertThat(left().add(right())).isEqualTo(new Int64(leftValue + rightValue));
  }

  /**
   * Tests subtract.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void subtract(LongPair longPair) {
    set(longPair);
    assertThat(left().subtract(right())).isEqualTo(new Int64(leftValue - rightValue));
  }

  /**
   * Tests divideSigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void divideSigned(LongPair longPair) {
    set(longPair);
    if (rightValue == 0) {
      assertThatThrownBy(() -> left().divideSigned(right()))
          .hasMessage("Trap: 64-bit integer divide by zero");
    } else if (leftValue == Long.MIN_VALUE && rightValue == -1) {
      assertThatThrownBy(() -> left().divideSigned(right())).hasMessage("Trap: Integer overflow");
    } else {
      assertThat(left().divideSigned(right())).isEqualTo(new Int64(leftValue / rightValue));
    }
  }

  /**
   * Tests divideUnsigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void divideUnsigned(LongPair longPair) {
    set(longPair);
    if (rightValue == 0) {
      assertThatThrownBy(() -> left().divideUnsigned(right()))
          .hasMessage("Trap: 64-bit integer divide by zero");
    } else {
      assertThat(left().divideUnsigned(right()))
          .isEqualTo(new Int64(Long.divideUnsigned(leftValue, rightValue)));
    }
  }

  /**
   * Tests remainderSigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void remainderSigned(LongPair longPair) {
    set(longPair);
    if (rightValue == 0) {
      assertThatThrownBy(() -> left().remainderSigned(right()))
          .hasMessage("Trap: 64-bit integer divide by zero");
    } else {

      assertThat(left().remainderSigned(right())).isEqualTo(new Int64(leftValue % rightValue));
    }
  }

  /**
   * Tests remainderUnsigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void remainderUnsigned(LongPair longPair) {
    set(longPair);
    if (rightValue == 0) {
      assertThatThrownBy(() -> left().remainderUnsigned(right()))
          .hasMessage("Trap: 64-bit integer divide by zero");
    } else {

      assertThat(left().remainderUnsigned(right()))
          .isEqualTo(new Int64(Long.remainderUnsigned(leftValue, rightValue)));
    }
  }

  /**
   * Tests and.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void and(LongPair longPair) {
    set(longPair);
    assertThat(left().and(right())).isEqualTo(new Int64(leftValue & rightValue));
  }

  /**
   * Tests or.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void or(LongPair longPair) {
    set(longPair);
    assertThat(left().or(right())).isEqualTo(new Int64(leftValue | rightValue));
  }

  /**
   * Tests xor.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void xor(LongPair longPair) {
    set(longPair);
    assertThat(left().xor(right())).isEqualTo(new Int64(leftValue ^ rightValue));
  }

  /**
   * Tests shiftLeft.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void shiftLeft(LongPair longPair) {
    set(longPair);
    assertThat(left().shiftLeft(right())).isEqualTo(new Int64(leftValue << rightValue));
  }

  /**
   * Tests shiftRightSigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void shiftRightSigned(LongPair longPair) {
    set(longPair);
    assertThat(left().shiftRightSigned(right())).isEqualTo(new Int64(leftValue >> rightValue));
  }

  /**
   * Tests shiftRightUnsigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void shiftRightUnsigned(LongPair longPair) {
    set(longPair);
    assertThat(left().shiftRightUnsigned(right())).isEqualTo(new Int64(leftValue >>> rightValue));
  }

  /**
   * Tests bitwiseRotateLeft.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void bitwiseRotateLeft(LongPair longPair) {
    set(longPair);
    assertThat(left().bitwiseRotateLeft(right()))
        .isEqualTo(new Int64(Long.rotateLeft(leftValue, (int) rightValue)));
  }

  /**
   * Tests bitwiseRotateRight.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void bitwiseRotateRight(LongPair longPair) {
    set(longPair);
    assertThat(left().bitwiseRotateRight(right()))
        .isEqualTo(new Int64(Long.rotateRight(leftValue, (int) rightValue)));
  }

  /**
   * Tests countLeadingZeroes.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void countLeadingZeroes(LongPair longPair) {
    set(longPair);
    assertThat(left().countLeadingZeroes())
        .isEqualTo(new Int64(Long.numberOfLeadingZeros(leftValue)));
  }

  /**
   * Tests countTrailingZeroes.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void countTrailingZeroes(LongPair longPair) {
    set(longPair);
    assertThat(left().countTrailingZeroes())
        .isEqualTo(new Int64(Long.numberOfTrailingZeros(leftValue)));
  }

  /**
   * Tests populationCount.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void populationCount(LongPair longPair) {
    set(longPair);
    assertThat(left().populationCount()).isEqualTo(new Int64(Long.bitCount(leftValue)));
  }

  /**
   * Tests extendSigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void extendSigned(LongPair longPair) {
    set(longPair);
    assertThat(Int64.extendSigned(new Int32((int) leftValue)))
        .isEqualTo(new Int64((int) leftValue));
  }

  /**
   * Tests extendUnsigned.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void extendUnsigned(LongPair longPair) {
    set(longPair);
    assertThat(Int64.extendUnsigned(new Int32((int) leftValue)))
        .isEqualTo(new Int64(Integer.toUnsignedLong((int) leftValue)));
  }

  /**
   * Tests extend8Signed.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void extend8Signed(LongPair longPair) {
    set(longPair);
    assertThat(Int64.extend8Signed(new Int64(leftValue))).isEqualTo(new Int64((byte) leftValue));
  }

  /**
   * Tests extend16Signed.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void extend16Signed(LongPair longPair) {
    set(longPair);
    assertThat(Int64.extend16Signed(new Int64(leftValue))).isEqualTo(new Int64((short) leftValue));
  }

  /**
   * Tests extend32Signed.
   *
   * @param longPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void extend32Signed(LongPair longPair) {
    set(longPair);
    assertThat(Int64.extend32Signed(new Int64(leftValue))).isEqualTo(new Int64((int) leftValue));
  }

  private static final class LongPair {

    private final long lhs;
    private final long rhs;

    public LongPair(long lhs, long rhs) {
      this.lhs = lhs;
      this.rhs = rhs;
    }

    @Override
    public String toString() {
      return "LongPair{" + "l=" + lhs + ", r=" + rhs + '}';
    }
  }
}
