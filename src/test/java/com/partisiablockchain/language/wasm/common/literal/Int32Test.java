package com.partisiablockchain.language.wasm.common.literal;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test of {@link Int32}.
 *
 * @see com.partisiablockchain.language.wasm.common.literal.Int32
 */
public final class Int32Test {
  private int leftValue;
  private int rightValue;

  /**
   * tests conversions.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void conversions(IntPair intPair) {
    set(intPair);
    assertThat(left().toString()).isEqualTo(Integer.toString(leftValue));
  }

  private static final int[] essentialNumbers = {
    0, 1, -1, 131245312, Integer.MIN_VALUE, Integer.MAX_VALUE
  };

  /**
   * Creates the parameters for the test.
   *
   * @return the parameters
   */
  public static List<IntPair> getSeeds() {
    return Arrays.stream(essentialNumbers)
        .mapToObj(l -> Arrays.stream(essentialNumbers).mapToObj(r -> new IntPair(l, r)))
        .flatMap(list -> list)
        .collect(Collectors.toList());
  }

  private Int32 left() {
    return new Int32(leftValue);
  }

  private Int32 right() {
    return new Int32(rightValue);
  }

  /**
   * tests greaterThanSigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void greaterThanSigned(IntPair intPair) {
    set(intPair);
    assertThat(left().greaterThanSigned(right())).isEqualTo(leftValue > rightValue);
  }

  /**
   * tests greaterThanEqualSigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void greaterThanEqualSigned(IntPair intPair) {
    set(intPair);
    assertThat(left().greaterThanEqualSigned(right())).isEqualTo(leftValue >= rightValue);
  }

  /**
   * tests greaterThanUnsigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void greaterThanUnsigned(IntPair intPair) {
    set(intPair);
    assertThat(left().greaterThanUnsigned(right()))
        .isEqualTo(Integer.compareUnsigned(leftValue, rightValue) > 0);
  }

  /**
   * tests greaterThanEqualUnsigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void greaterThanEqualUnsigned(IntPair intPair) {
    set(intPair);
    assertThat(left().greaterThanEqualUnsigned(right()))
        .isEqualTo(Integer.compareUnsigned(leftValue, rightValue) >= 0);
  }

  /**
   * tests lessThanSigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void lessThanSigned(IntPair intPair) {
    set(intPair);
    assertThat(left().lessThanSigned(right())).isEqualTo(leftValue < rightValue);
  }

  /**
   * tests lessThanEqualSigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void lessThanEqualSigned(IntPair intPair) {
    set(intPair);
    assertThat(left().lessThanEqualSigned(right())).isEqualTo(leftValue <= rightValue);
  }

  /**
   * tests lessThanUnsigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void lessThanUnsigned(IntPair intPair) {
    set(intPair);
    assertThat(left().lessThanUnsigned(right()))
        .isEqualTo(Integer.compareUnsigned(leftValue, rightValue) < 0);
  }

  /**
   * tests lessThanEqualUnsigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void lessThanEqualUnsigned(IntPair intPair) {
    set(intPair);
    assertThat(left().lessThanEqualUnsigned(right()))
        .isEqualTo(Integer.compareUnsigned(leftValue, rightValue) <= 0);
  }

  /**
   * tests equalsZero.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void equalsZero(IntPair intPair) {
    set(intPair);
    assertThat(left().equalsZero()).isEqualTo(leftValue == 0);
  }

  /**
   * tests multiply.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void multiply(IntPair intPair) {
    set(intPair);
    int value = leftValue * rightValue;
    assertThat(left().multiply(right())).isEqualTo(new Int32(value));
  }

  /**
   * tests add.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void add(IntPair intPair) {
    set(intPair);
    assertThat(left().add(right())).isEqualTo(new Int32(leftValue + rightValue));
  }

  /**
   * tests subtract.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void subtract(IntPair intPair) {
    set(intPair);
    assertThat(left().subtract(right())).isEqualTo(new Int32(leftValue - rightValue));
  }

  /**
   * tests divideSigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void divideSigned(IntPair intPair) {
    set(intPair);
    if (rightValue == 0) {
      assertThatThrownBy(() -> left().divideSigned(right()))
          .hasMessage("Trap: 32-bit integer divide by zero");
    } else if (leftValue == Integer.MIN_VALUE && rightValue == -1) {
      assertThatThrownBy(() -> left().divideSigned(right())).hasMessage("Trap: Integer overflow");
    } else {
      assertThat(left().divideSigned(right())).isEqualTo(new Int32(leftValue / rightValue));
    }
  }

  /**
   * tests divideUnsigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void divideUnsigned(IntPair intPair) {
    set(intPair);
    if (rightValue == 0) {
      assertThatThrownBy(() -> left().divideUnsigned(right()))
          .hasMessage("Trap: 32-bit integer divide by zero");
    } else {
      assertThat(left().divideUnsigned(right()))
          .isEqualTo(new Int32(Integer.divideUnsigned(leftValue, rightValue)));
    }
  }

  /**
   * tests remainderSigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void remainderSigned(IntPair intPair) {
    set(intPair);
    if (rightValue == 0) {
      assertThatThrownBy(() -> left().remainderSigned(right()))
          .hasMessage("Trap: 32-bit integer divide by zero");
    } else {

      assertThat(left().remainderSigned(right())).isEqualTo(new Int32(leftValue % rightValue));
    }
  }

  /**
   * tests remainderUnsigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void remainderUnsigned(IntPair intPair) {
    set(intPair);
    if (rightValue == 0) {
      assertThatThrownBy(() -> left().remainderUnsigned(right()))
          .hasMessage("Trap: 32-bit integer divide by zero");
    } else {

      assertThat(left().remainderUnsigned(right()))
          .isEqualTo(new Int32(Integer.remainderUnsigned(leftValue, rightValue)));
    }
  }

  /**
   * tests and.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void and(IntPair intPair) {
    set(intPair);
    assertThat(left().and(right())).isEqualTo(new Int32(leftValue & rightValue));
  }

  /**
   * tests or.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void or(IntPair intPair) {
    set(intPair);
    assertThat(left().or(right())).isEqualTo(new Int32(leftValue | rightValue));
  }

  /**
   * tests xor.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void xor(IntPair intPair) {
    set(intPair);
    assertThat(left().xor(right())).isEqualTo(new Int32(leftValue ^ rightValue));
  }

  /**
   * tests shiftLeft.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void shiftLeft(IntPair intPair) {
    set(intPair);
    int value = leftValue << rightValue;
    assertThat(left().shiftLeft(right())).isEqualTo(new Int32(value));
  }

  /**
   * tests shiftRightSigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void shiftRightSigned(IntPair intPair) {
    set(intPair);
    assertThat(left().shiftRightSigned(right())).isEqualTo(new Int32(leftValue >> rightValue));
  }

  /**
   * tests shiftRightUnsigned.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void shiftRightUnsigned(IntPair intPair) {
    set(intPair);
    assertThat(left().shiftRightUnsigned(right())).isEqualTo(new Int32(leftValue >>> rightValue));
  }

  /**
   * tests bitwiseRotateLeft.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void bitwiseRotateLeft(IntPair intPair) {
    set(intPair);
    assertThat(left().bitwiseRotateLeft(right()))
        .isEqualTo(new Int32(Integer.rotateLeft(leftValue, rightValue)));
  }

  /**
   * tests bitwiseRotateRight.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void bitwiseRotateRight(IntPair intPair) {
    set(intPair);
    assertThat(left().bitwiseRotateRight(right()))
        .isEqualTo(new Int32(Integer.rotateRight(leftValue, rightValue)));
  }

  /**
   * tests countLeadingZeroes.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void countLeadingZeroes(IntPair intPair) {
    set(intPair);
    assertThat(left().countLeadingZeroes())
        .isEqualTo(new Int32(Integer.numberOfLeadingZeros(leftValue)));
  }

  /**
   * tests countTrailingZeroes.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void countTrailingZeroes(IntPair intPair) {
    set(intPair);
    assertThat(left().countTrailingZeroes())
        .isEqualTo(new Int32(Integer.numberOfTrailingZeros(leftValue)));
  }

  private void set(IntPair intPair) {
    this.leftValue = intPair.lhs;
    this.rightValue = intPair.rhs;
  }

  /**
   * tests populationCount.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void populationCount(IntPair intPair) {
    this.leftValue = intPair.lhs;
    assertThat(left().populationCount()).isEqualTo(new Int32(Integer.bitCount(leftValue)));
  }

  /**
   * Tests extend8Signed.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void extend8Signed(IntPair intPair) {
    set(intPair);
    assertThat(Int32.extend8Signed(new Int32(leftValue))).isEqualTo(new Int32((byte) leftValue));
  }

  /**
   * Tests extend16Signed.
   *
   * @param intPair input
   */
  @ParameterizedTest
  @MethodSource("getSeeds")
  public void extend16Signed(IntPair intPair) {
    set(intPair);
    assertThat(Int32.extend16Signed(new Int32(leftValue))).isEqualTo(new Int32((short) leftValue));
  }

  private static final class IntPair {

    private final int lhs;
    private final int rhs;

    public IntPair(int lhs, int rhs) {
      this.lhs = lhs;
      this.rhs = rhs;
    }

    @Override
    public String toString() {
      return "IntPair{" + "l=" + lhs + ", r=" + rhs + '}';
    }
  }
}
