package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.interpreter.InstructionParserTest;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmParseException;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link ResizableLimits}.
 *
 * @see com.partisiablockchain.language.wasm.common.ResizableLimits
 */
public final class ResizableLimitsTest {

  @Test
  public void noMax() {
    byte[] bytes =
        InstructionParserTest.concatArrays(
            List.of(InstructionParserTest.concat(0x0), InstructionParserTest.writeVarUint32(2)));

    ResizableLimits limits = ResizableLimits.read(new WasmByteStream(bytes));
    assertThat(limits.getMinimum()).isEqualTo(new Uint31(2));
    assertThat(limits.getMaximum()).isEqualTo(Uint31.MAX_VALUE);
  }

  @Test
  public void minAndMax() {
    byte[] bytes =
        InstructionParserTest.concatArrays(
            List.of(
                InstructionParserTest.concat(0x1),
                InstructionParserTest.writeVarUint32(2),
                InstructionParserTest.writeVarUint32(3)));

    ResizableLimits limits = ResizableLimits.read(new WasmByteStream(bytes));
    assertThat(limits.getMinimum()).isEqualTo(new Uint31(2));
    assertThat(limits.getMaximum()).isEqualTo(new Uint31(3));
  }

  @Test
  public void invalidFlag() {
    byte[] bytes =
        InstructionParserTest.concatArrays(
            List.of(InstructionParserTest.concat(0xFF), InstructionParserTest.writeVarUint32(3)));

    assertThatThrownBy(() -> ResizableLimits.read(new WasmByteStream(bytes)))
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Invalid start byte")
        .hasMessageContaining("FF");
  }
}
