package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmParseException;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link Export}.
 *
 * @see com.partisiablockchain.language.wasm.common.Export
 */
public final class ExportTest {

  @Test
  public void valueClass() {
    Export export = new Export("name", AddressKind.TABLE, new Uint31(2));
    assertThat(export.getName()).isEqualTo("name");
    assertThat(export.getKind()).isEqualTo(AddressKind.TABLE);
    assertThat(export.getIndex()).isEqualTo(new Uint31(2));
  }

  @Test
  public void parsing() {
    final WasmByteStream stream =
        new WasmByteStream(
            Hex.decode(
                "01 41 00 01" // Function a with index 1
                    + "01 42 01 02" // Table b with index 2
                    + "01 43 02 03" // Memory c with index 3
                    + "01 44 03 04" // Global d with index 4
                    + "01 45 04 05" // Unknown e with index 5
                ));

    assertThat(Export.read(stream).getKind()).isEqualTo(AddressKind.FUNCTION);
    assertThat(Export.read(stream).getKind()).isEqualTo(AddressKind.TABLE);
    assertThat(Export.read(stream).getKind()).isEqualTo(AddressKind.MEMORY);
    assertThat(Export.read(stream).getKind()).isEqualTo(AddressKind.GLOBAL);

    assertThatThrownBy(() -> Export.read(stream).getKind())
        .isInstanceOf(WasmParseException.class)
        .hasMessage("Unknown export kind id: 0x04");
  }
}
