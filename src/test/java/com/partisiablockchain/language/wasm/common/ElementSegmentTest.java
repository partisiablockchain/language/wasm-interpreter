package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.language.wasm.interpreter.InstructionNoArgs;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link ElementSegment}.
 *
 * @see com.partisiablockchain.language.wasm.common.ElementSegment
 */
public final class ElementSegmentTest {

  @Test
  public void values() {
    InstructionNoArgs instr = new InstructionNoArgs(OpCode.UNREACHABLE);

    ElementSegment segment =
        new ElementSegment(new Uint31(1), List.of(instr), List.of(new Uint31(2)));
    assertThat(segment.getTableIndex()).isEqualTo(new Uint31(1));
    assertThat(segment.getOffset()).containsExactly(instr);
    assertThat(segment.getInit()).containsExactly(new Uint31(2));
  }
}
