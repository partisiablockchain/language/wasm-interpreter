package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.literal.Int64;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link GlobalVariable}.
 *
 * @see com.partisiablockchain.language.wasm.common.GlobalVariable
 */
public final class GlobalVariableTest {

  private static final GlobalType mutable = new GlobalType(ValType.I32, true);
  private static final GlobalType immutable = new GlobalType(ValType.I32, false);

  @Test
  public void set() {
    GlobalVariable variable = new GlobalVariable(mutable, Int32.ZERO);
    variable.setValue(new Int32(1));
    assertThat(variable.getValue()).isEqualTo(new Int32(1));
  }

  @Test
  public void setNull() {
    GlobalVariable variable = new GlobalVariable(mutable, Int32.ZERO);
    assertThatThrownBy(() -> variable.setValue(null))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Cannot set value to null");

    assertThat(variable.getValue()).isEqualTo(Int32.ZERO);
  }

  @Test
  public void setImmutable() {
    GlobalVariable variable = new GlobalVariable(immutable, Int32.ZERO);
    assertThatThrownBy(() -> variable.setValue(new Int32(1)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("immutable global");

    assertThat(variable.getValue()).isEqualTo(Int32.ZERO);
  }

  @Test
  public void setInvalidType() {
    GlobalVariable variable = new GlobalVariable(mutable, Int32.ZERO);
    assertThatThrownBy(() -> variable.setValue(new Int64(1)))
        .isInstanceOf(TrapException.class)
        .hasMessageContaining("Invalid type")
        .hasMessageContaining(String.valueOf(ValType.I64));

    assertThat(variable.getValue()).isEqualTo(Int32.ZERO);
  }
}
