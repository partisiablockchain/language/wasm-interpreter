package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.interpreter.InstructionParserTest;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import java.util.List;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link TableType}.
 *
 * @see com.partisiablockchain.language.wasm.common.TableType
 */
public final class TableTypeTest {

  @Test
  public void invalidFlag() {
    assertThatThrownBy(() -> TableType.read(new WasmByteStream(new byte[] {0x71})))
        .hasMessageContaining("table type")
        .hasMessageContaining("71");
  }

  @Test
  public void valid() {
    byte[] bytes =
        InstructionParserTest.concatArrays(
            List.of(
                InstructionParserTest.concat(0x70, 0x1),
                InstructionParserTest.writeVarUint32(2),
                InstructionParserTest.writeVarUint32(3)));

    TableType type = TableType.read(new WasmByteStream(bytes));
    assertThat(type.getAddressKind()).isEqualTo(AddressKind.FUNCTION);

    ResizableLimits limits = type.getLimits();
    assertThat(limits.getMinimum().asInt()).isEqualTo(2);
    assertThat(limits.getMaximum().asInt()).isEqualTo(3);
  }
}
