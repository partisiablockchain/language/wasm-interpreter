package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link Uint31}.
 *
 * @see Uint31
 */
public final class Uint31Test {

  @Test
  public void constructor() {
    assertThat(new Uint31(40).asInt()).isEqualTo(40);
    assertThat(new Uint31(1213).asInt()).isEqualTo(1213);
    assertThat(new Uint31(231231).asInt()).isEqualTo(231231);
    assertThat(new Uint31(123456).asInt()).isEqualTo(123456);
    assertThat(new Uint31(456789).asInt()).isEqualTo(456789);
    assertThat(new Uint31(Integer.MAX_VALUE).asInt()).isEqualTo(Integer.MAX_VALUE);

    assertThatThrownBy(() -> new Uint31(Integer.MIN_VALUE))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Integer -2147483648 too large/small for Uint31");
  }

  @Test
  public void equalsAndHashcode() {
    EqualsVerifier.forClass(Uint31.class).verify();
  }
}
