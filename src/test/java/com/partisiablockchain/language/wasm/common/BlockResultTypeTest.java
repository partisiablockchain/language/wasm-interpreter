package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.parser.WasmParseException;
import java.util.List;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link BlockResultType}.
 *
 * @see BlockResultType
 */
public final class BlockResultTypeTest {

  @Test
  public void fromValue() {
    assertThat(BlockResultType.fromValue(0x40)).isEqualTo(BlockResultType.EMPTY);
    assertThat(BlockResultType.fromValue(0x7F)).isEqualTo(BlockResultType.I32);
    assertThat(BlockResultType.fromValue(0x7E)).isEqualTo(BlockResultType.I64);
    assertThat(BlockResultType.fromValue(0x7D)).isEqualTo(BlockResultType.of(List.of(ValType.F32)));
    assertThat(BlockResultType.fromValue(0x7C)).isEqualTo(BlockResultType.of(List.of(ValType.F64)));

    assertThatThrownBy(() -> BlockResultType.fromValue(0xFF))
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Invalid value type constant")
        .hasMessageContaining("0xFF");
  }

  private final List<BlockResultType> blockResultTypes =
      List.of(BlockResultType.EMPTY, BlockResultType.I32, BlockResultType.I64);

  @Test
  public void asTypeList() {
    for (BlockResultType blockType : blockResultTypes) {
      assertThat(BlockResultType.of(blockType.asTypeList())).isEqualTo(blockType);
    }

    assertThatThrownBy(() -> BlockResultType.of(List.of(ValType.I32, ValType.I64)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Generalized block result types not supported");
  }

  @Test
  public void toStringTest() {
    assertThat(BlockResultType.EMPTY.toString()).isEqualTo("EMPTY");
    assertThat(BlockResultType.I32.toString()).isEqualTo("i32");
    assertThat(BlockResultType.I64.toString()).isEqualTo("i64");
  }

  @Test
  public void equalsAndHashcode() {
    EqualsVerifier.forClass(BlockResultType.class).verify();

    assertThat(BlockResultType.I32).isNotEqualTo(BlockResultType.EMPTY);
    assertThat(BlockResultType.EMPTY).isNotEqualTo(BlockResultType.I32);
    assertThat(BlockResultType.I32).isNotEqualTo(BlockResultType.I64);
    assertThat(BlockResultType.I64).isNotEqualTo(BlockResultType.I32);
  }
}
