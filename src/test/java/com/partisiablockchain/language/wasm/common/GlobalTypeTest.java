package com.partisiablockchain.language.wasm.common;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static com.partisiablockchain.language.wasm.parser.WasmByteStreamTest.streamFromHex;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.parser.WasmIllegalBytecodeException;
import org.junit.jupiter.api.Test;

/** Tests that GlobalType can be parsed correctly. */
public final class GlobalTypeTest {

  @Test
  public void read() {

    assertThat(GlobalType.read(streamFromHex("7F 00")).isMutable()).isFalse();
    assertThat(GlobalType.read(streamFromHex("7F 01")).isMutable()).isTrue();
    assertThatThrownBy(() -> GlobalType.read(streamFromHex("7F 02")))
        .isInstanceOf(WasmIllegalBytecodeException.class)
        .hasMessageContaining("Invalid mutability indicator for GlobalType")
        .hasMessageContaining("0x02");
    assertThatThrownBy(() -> GlobalType.read(streamFromHex("7F FF")))
        .isInstanceOf(WasmIllegalBytecodeException.class)
        .hasMessageContaining("Invalid mutability indicator for GlobalType")
        .hasMessageContaining("0xFF");
  }
}
