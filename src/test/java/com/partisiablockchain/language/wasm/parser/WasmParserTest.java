package com.partisiablockchain.language.wasm.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.Export;
import com.partisiablockchain.language.wasm.common.SectionCode;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.ValType;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.sections.ExportSection;
import com.partisiablockchain.language.wasm.interpreter.ExecutableFunction;
import com.partisiablockchain.language.wasm.interpreter.FunctionCallable;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.wabt.Wat2Wasm;
import com.secata.tools.coverage.ExceptionConverter;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/**
 * Test for {@link WasmParser}.
 *
 * @see com.partisiablockchain.language.wasm.parser.WasmParser
 */
public final class WasmParserTest {

  private static byte[] loadWasmTestFile(String wasmTextFileName) {
    return ExceptionConverter.call(
        () -> {
          InputStream wasmTextStream = WasmParserTest.class.getResourceAsStream(wasmTextFileName);
          return Wat2Wasm.translate(wasmTextStream, WasmParserTest.class);
        },
        "Error reading WASM file.");
  }

  private void checkForDuplicatesInModule(WasmModule module, WasmInstance instance) {
    final Uint31 numFunctions = module.getFunctionSection().numEntries();

    // Ensure that no duplicate function indexes occur.
    final Set<Uint31> seenFunctionIndexes = new HashSet<>();
    for (int i = 0; i < numFunctions.asInt(); i++) {
      final Uint31 functionIndex = new Uint31(i);
      final ExecutableFunction function = instance.lookupFunction(functionIndex);
      assertThat(function.index()).isEqualTo(functionIndex);
      assertThat(seenFunctionIndexes).doesNotContain(function.index());

      seenFunctionIndexes.add(function.index());
    }

    // Ensure that no function names are duplicated
    final Set<String> seenFunctionNames = new HashSet<>();
    for (int i = 0; i < numFunctions.asInt(); i++) {
      final ExecutableFunction function = instance.lookupFunction(new Uint31(i));
      if (function.name() != null) {
        assertThat(seenFunctionNames).doesNotContain(function.name());
      }

      seenFunctionNames.add(function.name());
    }
  }

  /** Sanity checks our loading system. */
  @Test
  public void readContractAndCheckForDuplicates() {
    final WasmParser reader = new WasmParser(loadWasmTestFile("rust-contract.wat"));

    final WasmModule module = reader.parse();
    final WasmInstance instance = WasmInstance.forModule(module);
    checkForDuplicatesInModule(module, instance);
  }

  /** Parse a real WASM program compiled from a RUST smart contract. */
  @Test
  public void readContract() {
    WasmParser reader = new WasmParser(loadWasmTestFile("rust-contract.wat"));

    WasmModule module = reader.parse();

    // Can find exports in module
    final ExportSection exportSection = module.getExportSection();
    assertThat(exportSection.numEntries()).isEqualTo(new Uint31(8));
    final Export rawExecute = exportSection.getEntryOrNull(new Uint31(5));
    assertThat(rawExecute.getName()).isEqualTo("raw_execute");

    // Can find exported functions in module
    assertThat(module.exportedFunctionsByName())
        .containsKeys(
            "export_contract_schema_json",
            "action_9e4dc145",
            "action_bb176fdc",
            "action_ca76f527",
            "raw_execute");

    // Cannot change functionExportNamesByIndex values
    for (final List<String> functionString : module.functionExportNamesByIndex().values()) {
      assertThatThrownBy(() -> functionString.add("my_very_own_symbol"))
          .isInstanceOf(UnsupportedOperationException.class);
    }

    // Can find function
    WasmInstance instance = WasmInstance.forModule(module);
    FunctionCallable fn = (FunctionCallable) instance.lookupFunction(rawExecute.getIndex());

    assertThat(fn.name()).isEqualTo("raw_execute");
    assertThat(fn.getInstructions()).hasSize(149);
    assertThat(fn.type().getIndex().asInt()).isEqualTo(20);
    assertThat(fn.type().getParams()).isEmpty();

    List<ValType> expectedLocalTypes =
        List.of(
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I64,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I64,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I64,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32,
            ValType.I32);
    assertThat(fn.code().getLocalTypes()).containsExactlyElementsOf(expectedLocalTypes);
  }

  @Test
  public void invalidMagic() {
    WasmParser parser = new WasmParser(new byte[] {0x0, 0x0, 0x0, 0x0});
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Magic header not detected");
  }

  private static int IDX_VERSION = 4;
  private static int IDX_1ST_SECTION_ID = 8;
  private static int IDX_1ST_SECTION_LEN = 9;
  private static int IDX_1ST_SECTION_DATA = 10; // Assuming length < 128

  @Test
  public void invalidVersion() {
    byte[] data = loadWasmTestFile("rust-contract.wat");
    data[IDX_VERSION] = 2;
    WasmParser parser = new WasmParser(data);
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Unknown binary version");
  }

  @Test
  public void parseStartSection() {
    byte[] data = Arrays.copyOf(loadWasmTestFile("rust-contract.wat"), 4 + 4 + 3);
    data[IDX_1ST_SECTION_ID] = (byte) SectionCode.START.getValue();
    data[IDX_1ST_SECTION_LEN] = (byte) 0x1;
    data[IDX_1ST_SECTION_DATA] = (byte) 0x7;

    WasmByteStream stream = new WasmByteStream(data);
    WasmParser parser = new WasmParser(stream);
    parser.parse();
    assertThat(stream.isEof()).isTrue();
  }

  @Test
  public void parseUnknownSection() {
    byte[] data = Arrays.copyOf(loadWasmTestFile("rust-contract.wat"), 4 + 4 + 3);
    data[IDX_1ST_SECTION_ID] = (byte) SectionCode.UNKNOWN.getValue();

    WasmParser parser = new WasmParser(data);
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Unknown or malformed section id: 0xFF");
  }

  @Test
  public void parseEmptyCustomSection() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "00 " // Section header: Custom
                + "02 " // Section header: Size
                + "01 61" // Custom section name: "a"
            );
    WasmParser parser = new WasmParser(data);
    parser.parse(); // We ignore the section, so the test just checks that it doesn't crash.
  }

  @Test
  public void parseMismatchedCustomSection() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "00 " // Section header: Custom
                + "01 " // Section header: Size (!Shorter than actual section)
                + "01 61" // Custom section name: "a"
            );
    WasmParser parser = new WasmParser(data);

    // Header size mismatch
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessage("Section size mismatch for CUSTOM. Expected 1, but parsed 2 bytes");
  }

  @Test
  public void parseNameSectionBadSubsection() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "00 " // Section header: Custom
                + "0B " // Section header: Size
                + "04 6E 61 6D 65 " // Name section
                + "01 " // Function name section id
                + "03 " // Function name section length (!Shorter than data)
                + "01 01 01 61" // Function namemap: { 1 => "a" }
            );
    WasmParser parser = new WasmParser(data);

    // Header size mismatch
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessage("Name subsection 0x1 size mismatch. Expected 3, but parsed 4 bytes");
  }

  @Test
  public void complainAboutDuplicateSections() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "01 01 00 " // Empty TYPE section
                + "01 01 00" // Empty TYPE section (duplicate)
            );
    WasmParser parser = new WasmParser(data);

    // Header size mismatch
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Sections are duplicated")
        .hasMessageContaining("TYPE");
  }

  @Test
  public void complainAboutOutOfOrderSections() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "0A 01 00 " // Empty CODE section
                + "01 01 00" // Empty TYPE section (out of order)
            );
    WasmParser parser = new WasmParser(data);

    // Header size mismatch
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("out of order")
        .hasMessageContaining("CODE")
        .hasMessageContaining("TYPE");
  }

  @Test
  public void complainAboutInconsistentLengths() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "01 04 01 60 00 00 " // Singleton TYPE section with function: [] -> []
                + "03 02 01 01 " // Singleton FUNCTION section
                + "0A 01 00" // Empty CODE section header
            );
    WasmParser parser = new WasmParser(data);

    // Header size mismatch
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmConsistencyException.class)
        .hasMessageContaining("inconsistent lengths");
  }

  @Test
  public void complainAboutHugeSection() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "01 " // TYPE section
                + "FA FF FF FF 0F" // section length larger than UINT_MAX
            );
    WasmParser parser = new WasmParser(data);
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("TYPE")
        .hasMessageContaining("too long");
  }

  @Test
  public void complainAboutSectionCoveringMaxAllowedIndex() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "01 " // TYPE section
                + "FF FF FF FF 07" // section length UINT_MAX
            );
    WasmParser parser = new WasmParser(data);
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("TYPE")
        .hasMessageContaining("too long");
  }

  @Test
  public void elementComplainAboutUndefinedTable() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "09 06 01 00 41 00 0B 00" // ELEMENT section
            );
    WasmParser parser = new WasmParser(data);
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmConsistencyException.class)
        .hasMessageContaining("0")
        .hasMessageContaining("unknown table");
  }

  @Test
  public void dataComplainAboutUndefinedMemory() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "0b 06 01 00 41 00 0B 00" // DATA section
            );
    WasmParser parser = new WasmParser(data);
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmConsistencyException.class)
        .hasMessageContaining("0")
        .hasMessageContaining("unknown memory");
  }

  @Test
  public void complainAboutHugeCustomSubsection() {
    byte[] data =
        Hex.decode(
            "00 61 73 6D " // Magic
                + "01 00 00 00 " // Version
                + "00 " // Section header: Custom
                + "0B " // Section header: Size
                + "04 6E 61 6D 65 " // Name section
                + "02 " // Other name section id
                + "FA FF FF FF 0F" // section length very large UINT_MAX-6.
            // If uint32 is handled as signed the length will be negative (-6)
            // This can move the parser cursor backwards and cause an infinite loop in the parser.
            );
    WasmParser parser = new WasmParser(data);
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("too long");
  }

  @Test
  public void functionType() {
    byte[] data = Arrays.copyOf(loadWasmTestFile("rust-contract.wat"), 4 + 4 + 4);
    data[IDX_1ST_SECTION_ID] = (byte) SectionCode.TYPE.getValue();
    data[IDX_1ST_SECTION_LEN] = 0x01;
    data[IDX_1ST_SECTION_DATA] = 0x01;

    WasmParser parser = new WasmParser(data);
    assertThatThrownBy(parser::parse)
        .isInstanceOf(WasmParseException.class)
        .hasMessageContaining("Invalid function type");
  }

  @Test
  public void hasWasmHeader() {
    assertThat(WasmParser.hasWasmHeader(new byte[] {0})).isFalse();
    assertThat(WasmParser.hasWasmHeader(new byte[] {0, 0, 0, 0})).isFalse();
    assertThat(WasmParser.hasWasmHeader(new byte[] {0x00, 0x61, 0x73, 0x6d})).isTrue();
    assertThat(WasmParser.hasWasmHeader(new byte[] {0x00, 0x61, 0x73, 0x6d, 0x00})).isTrue();
  }
}
