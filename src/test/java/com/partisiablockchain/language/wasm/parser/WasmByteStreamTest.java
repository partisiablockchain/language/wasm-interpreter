package com.partisiablockchain.language.wasm.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.literal.MemoryAddress;
import java.util.ArrayList;
import java.util.List;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/**
 * Test of {@link WasmByteStream}.
 *
 * @see com.partisiablockchain.language.wasm.parser.WasmByteStream
 */
public final class WasmByteStreamTest {

  @Test
  public void uint8() {
    WasmByteStream stream = new WasmByteStream(new byte[] {1, 3, 3, 7});
    assertThat(stream.readUint8()).isEqualTo(1);
    assertThat(stream.readUint8()).isEqualTo(3);
    assertThat(stream.readUint8()).isEqualTo(3);
    assertThat(stream.readUint8()).isEqualTo(7);

    assertThatThrownBy(stream::readUint8)
        .isInstanceOf(WasmIllegalBytecodeException.class)
        .hasMessageContaining("Unexpected end");
  }

  @Test
  public void varUint32() {
    WasmByteStream stream = new WasmByteStream(new byte[] {(byte) 129, 3, 3, 7});
    assertThat(stream.peekInt32()).isEqualTo(117638017);
    assertThat(stream.readVarUint32()).isEqualTo(385);
    assertThat(stream.getPosition().asInt()).isEqualTo(2);
  }

  @Test
  public void uint32() {
    WasmByteStream stream = new WasmByteStream(new byte[] {1, 3, 3, 7});
    assertThat(stream.readVarUint32()).isEqualTo(1);
    assertThat(stream.readVarUint32()).isEqualTo(3);
    assertThat(stream.readVarUint32()).isEqualTo(3);
    assertThat(stream.readVarUint32()).isEqualTo(7);

    assertThatThrownBy(stream::readUint8).isInstanceOf(WasmIllegalBytecodeException.class);
  }

  @Test
  public void eof() {
    WasmByteStream stream = new WasmByteStream(new byte[] {1});
    assertThat(stream.isEof()).isFalse();
    stream.readUint8();

    assertThat(stream.isEof()).isTrue();
  }

  @Test
  public void readStringBytes() {
    assertThat(streamFromHex("01 61 03 07").readStringBytes()).isEqualTo("a");
    assertThat(streamFromHex("0D 48 65 6c 6c 6f 2c 20 77 6f 72 6c 64 21").readStringBytes())
        .isEqualTo("Hello, world!");

    final WasmByteStream stream1 = streamFromHex("81 81 81 81");
    assertThatThrownBy(() -> stream1.readStringBytes())
        .isInstanceOf(WasmIllegalBytecodeException.class);

    final WasmByteStream stream2 = streamFromHex("81 81 81 81");
    assertThatThrownBy(() -> stream2.readStringBytes())
        .isInstanceOf(WasmIllegalBytecodeException.class);
  }

  @Test
  public void readMemory() {
    WasmByteStream stream = new WasmByteStream(new byte[] {1, 3, 3, 7});
    MemoryAddress address = stream.readMemory();
    assertThat(address.getAlign().asInt()).isEqualTo(1);
    assertThat(address.getOffset().asInt()).isEqualTo(3);
  }

  @Test
  public void readInt64() {
    List<ParseCase<Long>> cases = new ArrayList<ParseCase<Long>>();
    cases.add(new ParseCase<Long>(-111724L, new byte[] {-108, -105, 121}));
    cases.add(new ParseCase<Long>(-62L, new byte[] {66}));
    cases.add(new ParseCase<Long>(52L, new byte[] {52}));
    cases.add(new ParseCase<Long>(-511510L, new byte[] {-22, -29, 96}));
    cases.add(new ParseCase<Long>(-3627L, new byte[] {-43, 99}));
    cases.add(new ParseCase<Long>(-62L, new byte[] {66}));
    cases.add(new ParseCase<Long>(449565L, new byte[] {-99, -72, 27}));
    cases.add(new ParseCase<Long>(-7454L, new byte[] {-30, 69}));
    cases.add(new ParseCase<Long>(50L, new byte[] {50}));
    cases.add(new ParseCase<Long>(800229L, new byte[] {-27, -21, 48}));
    cases.add(new ParseCase<Long>(0L, Hex.decode("00")));

    for (ParseCase<Long> testCase : cases) {
      WasmByteStream stream = new WasmByteStream(testCase.inputBytes);

      long value = stream.readInt64().value();
      assertThat(value).isEqualTo(testCase.expectedResult);
    }
  }

  private static final class ParseCase<T> {
    public final T expectedResult;
    public final byte[] inputBytes;

    public ParseCase(T expectedResult, byte[] inputBytes) {
      this.expectedResult = expectedResult;
      this.inputBytes = inputBytes;
    }
  }

  @Test
  public void readVarUint32() {
    List<ParseCase<Integer>> cases = new ArrayList<ParseCase<Integer>>();
    cases.addAll(intCasesUnsigned31);
    cases.addAll(intCasesUnsigned31ZeroExtend);
    cases.add(new ParseCase<Integer>((int) 4294967295L, Hex.decode("FF FF FF FF 0F")));

    for (ParseCase<Integer> testCase : cases) {
      final WasmByteStream stream = new WasmByteStream(testCase.inputBytes);

      int value = stream.readVarUint32();
      assertThat(value).isEqualTo(testCase.expectedResult);
    }
  }

  /** Cases that does not succeed with sign-extension. */
  static final List<ParseCase<Integer>> intCasesUnsigned31ZeroExtend =
      List.of(
          new ParseCase<Integer>(64, Hex.decode("40")),
          new ParseCase<Integer>(66, new byte[] {66}),
          new ParseCase<Integer>(107, new byte[] {107}),
          new ParseCase<Integer>(12757, new byte[] {-43, 99}),
          new ParseCase<Integer>(800229, new byte[] {-27, -21, 48}),
          new ParseCase<Integer>(1581806, new byte[] {-18, -59, 96}),
          new ParseCase<Integer>(1585642, new byte[] {-22, -29, 96}),
          new ParseCase<Integer>(1985428, new byte[] {-108, -105, 121}),
          new ParseCase<Integer>(21033196, new byte[] {-20, -31, -125, 10}));

  static final List<ParseCase<Integer>> intCasesUnsigned31 =
      List.of(
          new ParseCase<Integer>(8, new byte[] {8}),
          new ParseCase<Integer>(24, new byte[] {24}),
          new ParseCase<Integer>(28, new byte[] {28}),
          new ParseCase<Integer>(30, new byte[] {30}),
          new ParseCase<Integer>(51, new byte[] {51}),
          new ParseCase<Integer>(52, new byte[] {52}),
          new ParseCase<Integer>(449565, new byte[] {-99, -72, 27}),
          new ParseCase<Integer>(0, Hex.decode("00")),
          new ParseCase<Integer>(1, Hex.decode("01")),
          new ParseCase<Integer>(10, Hex.decode("0A")),
          new ParseCase<Integer>(32, Hex.decode("20")),
          new ParseCase<Integer>(62, Hex.decode("3e")),
          new ParseCase<Integer>(63, Hex.decode("3f")),
          new ParseCase<Integer>(64, Hex.decode("c0 00")),
          new ParseCase<Integer>(65, Hex.decode("c1 00")),
          new ParseCase<Integer>(66, Hex.decode("c2 00")),
          new ParseCase<Integer>(8190, Hex.decode("fe 3f")),
          new ParseCase<Integer>(8191, Hex.decode("ff 3f")),
          new ParseCase<Integer>(8192, Hex.decode("80 c0 00")),
          new ParseCase<Integer>(8193, Hex.decode("81 c0 00")),
          new ParseCase<Integer>(8194, Hex.decode("82 c0 00")),
          new ParseCase<Integer>(1048574, Hex.decode("fe ff 3f")),
          new ParseCase<Integer>(1048575, Hex.decode("ff ff 3f")),
          new ParseCase<Integer>(1048576, Hex.decode("80 80 c0 00")),
          new ParseCase<Integer>(1048577, Hex.decode("81 80 c0 00")),
          new ParseCase<Integer>(1048578, Hex.decode("82 80 c0 00")),
          new ParseCase<Integer>(134217726, Hex.decode("fe ff ff 3f")),
          new ParseCase<Integer>(134217727, Hex.decode("ff ff ff 3f")),
          new ParseCase<Integer>(134217728, Hex.decode("80 80 80 c0 00")),
          new ParseCase<Integer>(134217729, Hex.decode("81 80 80 c0 00")),
          new ParseCase<Integer>(134217730, Hex.decode("82 80 80 c0 00")),
          new ParseCase<Integer>(195940365, Hex.decode("8d a0 b7 dd 00")),
          new ParseCase<Integer>(1879048193, Hex.decode("81 80 80 80 07")),
          new ParseCase<Integer>(2147483647, Hex.decode("ff ff ff ff 07")));

  static final List<ParseCase<Integer>> intCasesSigned =
      List.of(
          new ParseCase<Integer>(-111724, new byte[] {-108, -105, 121}),
          new ParseCase<Integer>(-21, new byte[] {107}),
          new ParseCase<Integer>(-511510, new byte[] {-22, -29, 96}),
          new ParseCase<Integer>(-1, Hex.decode("7F FF FF FF FF")),
          new ParseCase<Integer>(-1, Hex.decode("FF 7F FF FF FF")),
          new ParseCase<Integer>(-1, Hex.decode("FF FF 7F FF FF")),
          new ParseCase<Integer>(-1, Hex.decode("FF FF FF 7F FF")),
          new ParseCase<Integer>(-1, Hex.decode("FF FF FF FF 7F")),
          new ParseCase<Integer>(-2147483648, Hex.decode("80 80 80 80 78")),
          new ParseCase<Integer>(-2147483647, Hex.decode("81 80 80 80 78")),
          new ParseCase<Integer>(-1879048193, Hex.decode("ff ff ff ff 78")),
          new ParseCase<Integer>(-134217730, Hex.decode("fe ff ff bf 7f")),
          new ParseCase<Integer>(-134217729, Hex.decode("ff ff ff bf 7f")),
          new ParseCase<Integer>(-134217728, Hex.decode("80 80 80 40")),
          new ParseCase<Integer>(-134217727, Hex.decode("81 80 80 40")),
          new ParseCase<Integer>(-134217726, Hex.decode("82 80 80 40")),
          new ParseCase<Integer>(-1048578, Hex.decode("fe ff bf 7f")),
          new ParseCase<Integer>(-1048577, Hex.decode("ff ff bf 7f")),
          new ParseCase<Integer>(-1048576, Hex.decode("80 80 40")),
          new ParseCase<Integer>(-1048575, Hex.decode("81 80 40")),
          new ParseCase<Integer>(-1048574, Hex.decode("82 80 40")),
          new ParseCase<Integer>(-8194, Hex.decode("fe bf 7f")),
          new ParseCase<Integer>(-8193, Hex.decode("ff bf 7f")),
          new ParseCase<Integer>(-8192, Hex.decode("80 40")),
          new ParseCase<Integer>(-8191, Hex.decode("81 40")),
          new ParseCase<Integer>(-8190, Hex.decode("82 40")),
          new ParseCase<Integer>(-66, Hex.decode("be 7f")),
          new ParseCase<Integer>(-65, Hex.decode("bf 7f")),
          new ParseCase<Integer>(-64, Hex.decode("40")),
          new ParseCase<Integer>(-63, Hex.decode("41")),
          new ParseCase<Integer>(-62, Hex.decode("42")));

  /**
   * Contains only cases too large to fit in u31. expectedResult field contains signed
   * representation of bits
   */
  static final List<ParseCase<Integer>> intCasesUnsigned32 =
      List.of(
          new ParseCase<Integer>((int) 2147483648L, Hex.decode("ff ff ff ff 08")),
          new ParseCase<Integer>((int) 4294967295L, Hex.decode("FF FF FF FF 0F")));

  @Test
  public void readVarInt32() {
    List<ParseCase<Integer>> cases = new ArrayList<ParseCase<Integer>>();
    cases.addAll(intCasesUnsigned31);
    cases.addAll(intCasesSigned);

    for (ParseCase<Integer> testCase : cases) {
      WasmByteStream stream = new WasmByteStream(testCase.inputBytes);

      int value = stream.readVarInt32().value();
      assertThat(value).isEqualTo(testCase.expectedResult);
    }
  }

  @Test
  public void readVarUint31() {
    List<ParseCase<Integer>> cases = new ArrayList<ParseCase<Integer>>();
    cases.addAll(intCasesUnsigned31);
    cases.addAll(intCasesUnsigned31ZeroExtend);

    for (ParseCase<Integer> testCase : cases) {
      WasmByteStream stream = new WasmByteStream(testCase.inputBytes);
      assertThat(stream.readVarUint31().asInt()).isEqualTo(testCase.expectedResult);
    }

    for (ParseCase<Integer> testCase : cases) {
      WasmByteStream stream = new WasmByteStream(testCase.inputBytes);
      assertThat(stream.readVarUint31OrNull().asInt()).isEqualTo(testCase.expectedResult);
    }
  }

  @Test
  public void readVarInt32Failing() {
    List<ParseCase<Integer>> cases = new ArrayList<ParseCase<Integer>>();
    cases.add(
        new ParseCase<Integer>(
            0, new byte[] {-69, -28, -12, -50, -5, -15, 83})); // Too long for i32
    cases.add(new ParseCase<Integer>(-1, Hex.decode("FF FF FF FF FF 7F"))); // Too long for i32

    // Incorrectly represented negative number (unneeded bits in number must be sign extended.)
    cases.add(new ParseCase<Integer>(-1, Hex.decode("FF FF FF FF 0F")));
    cases.add(new ParseCase<Integer>(-1, Hex.decode("FF FF FF FF 1F")));
    cases.add(new ParseCase<Integer>(-1, Hex.decode("FF FF FF FF 2F")));
    cases.add(new ParseCase<Integer>(-1, Hex.decode("FF FF FF FF 3F")));
    cases.add(new ParseCase<Integer>(-1, Hex.decode("FF FF FF FF 4F")));
    cases.add(new ParseCase<Integer>(-1, Hex.decode("FF FF FF FF 5F")));
    cases.add(new ParseCase<Integer>(-1, Hex.decode("FF FF FF FF 6F")));

    for (ParseCase<Integer> testCase : cases) {
      WasmByteStream stream = new WasmByteStream(testCase.inputBytes);
      assertThatThrownBy(() -> stream.readVarInt32()).isInstanceOf(Leb128Exception.class);
    }

    // Hits EOF without end of integer.
    WasmByteStream stream = new WasmByteStream(new byte[] {-69});
    assertThatThrownBy(stream::readVarInt32).isInstanceOf(WasmIllegalBytecodeException.class);
  }

  @Test
  public void readVarUint31Throw() {
    for (ParseCase<Integer> testCase : intCasesUnsigned32) {
      WasmByteStream stream = new WasmByteStream(testCase.inputBytes);
      assertThatThrownBy(() -> stream.readVarUint31())
          .isInstanceOf(WasmInterpreterLimitationException.class);
    }
  }

  @Test
  public void readVarUint31OrNull() {
    for (ParseCase<Integer> testCase : intCasesUnsigned32) {
      WasmByteStream stream = new WasmByteStream(testCase.inputBytes);
      assertThat(stream.readVarUint31OrNull()).isNull();
    }
  }

  @Test
  public void readBoxedUint() {
    WasmByteStream stream = new WasmByteStream(new byte[] {1, 3, 3, 7});
    assertThat(stream.readVarUint32()).isEqualTo(1);
  }

  @Test
  public void getSliceCheckBounds() {
    WasmByteStream stream = new WasmByteStream(new byte[] {1, 3, 3, 7});
    stream.getSlice(new Uint31(0), new Uint31(4));

    assertOutOfBoundsGetSlice(stream, new Uint31(1), new Uint31(4));
  }

  @Test
  public void byteStreamSkipping() {
    WasmByteStream stream = streamFromHex("00 11 22 33 44");
    assertThat(stream.getPosition().asInt()).isEqualTo(0);
    stream.skip(Uint31.ZERO);
    assertThat(stream.getPosition().asInt()).isEqualTo(0);
    stream.skip(new Uint31(3));
    assertThat(stream.getPosition().asInt()).isEqualTo(3);
    stream.skip(Uint31.ZERO);
    assertThat(stream.getPosition().asInt()).isEqualTo(3);
  }

  @Test
  public void byteStreamAttemptSkippingBackwards() {
    WasmByteStream stream = streamFromHex("00 11 22 33 44");
    Uint31 offset = new Uint31(3);
    stream.skip(offset);
    assertThatThrownBy(() -> stream.skip(Uint31.MAX_VALUE))
        .isInstanceOf(WasmIllegalBytecodeException.class);
    stream.skip(Uint31.MAX_VALUE.minus(offset));
    assertThatThrownBy(() -> stream.skip(Uint31.ONE))
        .isInstanceOf(WasmIllegalBytecodeException.class);
    assertThatThrownBy(() -> stream.skip(Uint31.MAX_VALUE))
        .isInstanceOf(WasmIllegalBytecodeException.class);
  }

  private void assertOutOfBoundsGetSlice(WasmByteStream stream, Uint31 index, Uint31 length) {
    assertThatThrownBy(() -> stream.getSlice(index, length))
        .isInstanceOf(ArrayIndexOutOfBoundsException.class);
  }

  /**
   * Utility to get a WasmByteStream directly from a hex string.
   *
   * @param hexString string to parse as hex
   * @return resulting byte stream
   */
  public static WasmByteStream streamFromHex(final String hexString) {
    return new WasmByteStream(Hex.decode(hexString));
  }
}
