package com.partisiablockchain.language.wasm.parser;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/** Test the LEB128 decoder. */
public final class Leb128Test {

  @Test
  public void leb128Unsigned() {
    Assertions.assertThat(Leb128.unsigned(1, wasmBytes(0x00))).isEqualTo(0L);
    Assertions.assertThat(Leb128.unsigned(8, wasmBytes(0x00))).isEqualTo(0L);
    Assertions.assertThat(Leb128.unsigned(16, wasmBytes(0x00))).isEqualTo(0L);
    Assertions.assertThat(Leb128.unsigned(32, wasmBytes(0x00))).isEqualTo(0L);
    Assertions.assertThat(Leb128.unsigned(62, wasmBytes(0x00))).isEqualTo(0L);
    Assertions.assertThat(Leb128.unsigned(63, wasmBytes(0x00))).isEqualTo(0L);

    Assertions.assertThat(Leb128.unsigned(32, wasmBytes(0xff, 0x7f))).isEqualTo(0x3fffL);
    Assertions.assertThat(Leb128.unsigned(32, wasmBytes(0xff, 0xff, 0x7f))).isEqualTo(0x1fffffL);
    Assertions.assertThat(Leb128.unsigned(32, wasmBytes(0xff, 0xff, 0xff, 0x7f)))
        .isEqualTo(0xfffffffL);
    Assertions.assertThat(Leb128.unsigned(32, wasmBytes(0xff, 0xff, 0xff, 0xff, 0x07)))
        .isEqualTo(Integer.MAX_VALUE);
    Assertions.assertThat(
            Leb128.unsigned(63, wasmBytes(0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f)))
        .isEqualTo(Long.MAX_VALUE);

    // Binary pattern matching LEB128 encoding
    Assertions.assertThat(Leb128.unsigned(32, wasmBytes(0b11010111, 0b11110101, 0b00001101)))
        .isEqualTo(0b000110111101011010111L);

    Assertions.assertThatThrownBy(() -> Leb128.unsigned(1, wasmBytes(0x02)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
    Assertions.assertThatThrownBy(
            () -> Leb128.unsigned(32, wasmBytes(0x80, 0x80, 0x80, 0x80, 0x10)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
    Assertions.assertThatThrownBy(() -> Leb128.unsigned(64, wasmBytes(0x00)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("LEB128 unsigned decoder only supports 63 bits.");
    Assertions.assertThatThrownBy(() -> Leb128.unsigned(65, wasmBytes(0x00)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("LEB128 unsigned decoder only supports 63 bits.");

    Assertions.assertThatThrownBy(
            () -> Leb128.unsigned(32, wasmBytes(0x80, 0x80, 0x80, 0x80, 0x80)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");

    Assertions.assertThatThrownBy(
            () -> Leb128.unsigned(32, wasmBytes(0x82, 0x80, 0x80, 0x80, 0x80, 0x00)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");

    Assertions.assertThatThrownBy(() -> Leb128.unsigned(13, wasmBytes(0xFF, 0x8F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");
    Assertions.assertThatThrownBy(() -> Leb128.unsigned(14, wasmBytes(0xFF, 0x8F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded unsigned number.");
    Assertions.assertThat(Leb128.unsigned(15, wasmBytes(0xFF, 0x8F, 0x01))).isEqualTo(18431L);

    Assertions.assertThat(Leb128.unsigned(14, wasmBytes(0x7F))).isEqualTo(0x7fL);
    Assertions.assertThat(Leb128.unsigned(15, wasmBytes(0x7F))).isEqualTo(0x7fL);
    Assertions.assertThat(Leb128.unsigned(16, wasmBytes(0x7F))).isEqualTo(0x7fL);
    Assertions.assertThat(Leb128.unsigned(17, wasmBytes(0x7F))).isEqualTo(0x7fL);

    Assertions.assertThatThrownBy(() -> Leb128.unsigned(6, wasmBytes(0x7F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
    Assertions.assertThat(Leb128.unsigned(7, wasmBytes(0x7F))).isEqualTo(0x7fL);
    Assertions.assertThatThrownBy(() -> Leb128.unsigned(8, wasmBytes(0xFF, 0x03)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
    Assertions.assertThatThrownBy(() -> Leb128.unsigned(9, wasmBytes(0xFF, 0x07)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded unsigned number.");
  }

  @Test
  public void leb128Signed() {
    Assertions.assertThat(Leb128.signed(1, wasmBytes(0x00))).isEqualTo(0L);
    Assertions.assertThat(Leb128.signed(8, wasmBytes(0x00))).isEqualTo(0L);
    Assertions.assertThat(Leb128.signed(16, wasmBytes(0x00))).isEqualTo(0L);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x00))).isEqualTo(0L);
    Assertions.assertThat(Leb128.signed(64, wasmBytes(0x00))).isEqualTo(0L);

    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x7f))).isEqualTo(-1L);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x41))).isEqualTo(-63L);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x40))).isEqualTo(-64L);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xbf, 0x7f))).isEqualTo(-65L);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xbe, 0x7f))).isEqualTo(-66L);

    Assertions.assertThat(Leb128.signed(32, wasmBytes(-108, -105, 121))).isEqualTo(-111724);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(107))).isEqualTo(-21);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(-22, -29, 96))).isEqualTo(-511510);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(24))).isEqualTo(24);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(-99, -72, 27))).isEqualTo(449565);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(51))).isEqualTo(51);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(-27, -21, 48))).isEqualTo(800229);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(28))).isEqualTo(28);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(30))).isEqualTo(30);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x7F, 0xFF, 0xFF, 0xFF, 0xFF))).isEqualTo(-1);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xFF, 0x7F, 0xFF, 0xFF, 0xFF))).isEqualTo(-1);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0x7F, 0xFF, 0xFF))).isEqualTo(-1);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0x7F, 0xFF))).isEqualTo(-1);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0xFF, 0x7F))).isEqualTo(-1);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x8d, 0xa0, 0xb7, 0xdd, 0x00)))
        .isEqualTo(195940365);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x80, 0x80, 0x80, 0x80, 0x78)))
        .isEqualTo(-2147483648);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x81, 0x80, 0x80, 0x80, 0x78)))
        .isEqualTo(-2147483647);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xfe, 0xff, 0xff, 0xbf, 0x7f)))
        .isEqualTo(-134217730);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xff, 0xff, 0xff, 0xbf, 0x7f)))
        .isEqualTo(-134217729);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x80, 0x80, 0x80, 0x40)))
        .isEqualTo(-134217728);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x81, 0x80, 0x80, 0x40)))
        .isEqualTo(-134217727);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x82, 0x80, 0x80, 0x40)))
        .isEqualTo(-134217726);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xfe, 0xff, 0xbf, 0x7f))).isEqualTo(-1048578);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xff, 0xff, 0xbf, 0x7f))).isEqualTo(-1048577);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x80, 0x80, 0x40))).isEqualTo(-1048576);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x81, 0x80, 0x40))).isEqualTo(-1048575);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x82, 0x80, 0x40))).isEqualTo(-1048574);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xfe, 0xbf, 0x7f))).isEqualTo(-8194);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xff, 0xbf, 0x7f))).isEqualTo(-8193);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x80, 0x40))).isEqualTo(-8192);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x81, 0x40))).isEqualTo(-8191);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x82, 0x40))).isEqualTo(-8190);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xbe, 0x7f))).isEqualTo(-66);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xbf, 0x7f))).isEqualTo(-65);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x40))).isEqualTo(-64);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x41))).isEqualTo(-63);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x42))).isEqualTo(-62);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x3e))).isEqualTo(62);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x3f))).isEqualTo(63);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xc0, 0x00))).isEqualTo(64);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xc1, 0x00))).isEqualTo(65);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xc2, 0x00))).isEqualTo(66);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xfe, 0x3f))).isEqualTo(8190);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xff, 0x3f))).isEqualTo(8191);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x80, 0xc0, 0x00))).isEqualTo(8192);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x81, 0xc0, 0x00))).isEqualTo(8193);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x82, 0xc0, 0x00))).isEqualTo(8194);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xfe, 0xff, 0x3f))).isEqualTo(1048574);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xff, 0xff, 0x3f))).isEqualTo(1048575);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x80, 0x80, 0xc0, 0x00))).isEqualTo(1048576);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x81, 0x80, 0xc0, 0x00))).isEqualTo(1048577);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x82, 0x80, 0xc0, 0x00))).isEqualTo(1048578);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xfe, 0xff, 0xff, 0x3f)))
        .isEqualTo(134217726);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xff, 0xff, 0xff, 0x3f)))
        .isEqualTo(134217727);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x80, 0x80, 0x80, 0xc0, 0x00)))
        .isEqualTo(134217728);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x81, 0x80, 0x80, 0xc0, 0x00)))
        .isEqualTo(134217729);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0x82, 0x80, 0x80, 0xc0, 0x00)))
        .isEqualTo(134217730);
    Assertions.assertThat(Leb128.signed(32, wasmBytes(0xff, 0xff, 0xff, 0xff, 0x07)))
        .isEqualTo(2147483647);

    Assertions.assertThatThrownBy(() -> Leb128.signed(32, wasmBytes(0xbe)))
        .isInstanceOf(WasmIllegalBytecodeException.class)
        .hasMessageContaining("Unexpected end");

    // Too long for i32
    Assertions.assertThatThrownBy(
            () -> Leb128.signed(32, wasmBytes(-69, -28, -12, -50, -5, -15, 83)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded signed number.");
    Assertions.assertThatThrownBy(
            () -> Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0x7F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded signed number.");

    Assertions.assertThatThrownBy(() -> Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0xFF, 0x0F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");
    Assertions.assertThatThrownBy(() -> Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0xFF, 0x1F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");
    Assertions.assertThatThrownBy(() -> Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0xFF, 0x2F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");
    Assertions.assertThatThrownBy(() -> Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0xFF, 0x3F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");
    Assertions.assertThatThrownBy(() -> Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0xFF, 0x4F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");
    Assertions.assertThatThrownBy(() -> Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0xFF, 0x5F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");
    Assertions.assertThatThrownBy(() -> Leb128.signed(32, wasmBytes(0xFF, 0xFF, 0xFF, 0xFF, 0x6F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");

    Assertions.assertThatThrownBy(() -> Leb128.signed(5, wasmBytes(0x3F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");
    Assertions.assertThatThrownBy(() -> Leb128.signed(6, wasmBytes(0x3F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");
    Assertions.assertThat(Leb128.signed(7, wasmBytes(0x3F))).isEqualTo(0x3FL);
    Assertions.assertThatThrownBy(() -> Leb128.signed(4, wasmBytes(0x08)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("Integer too large: Too many bits in LEB128 encoded signed number.");

    Assertions.assertThatThrownBy(() -> Leb128.signed(13, wasmBytes(0xFF, 0x8F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded signed number.");
    Assertions.assertThatThrownBy(() -> Leb128.signed(14, wasmBytes(0xFF, 0x8F)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage(
            "Integer representation too long: Too many bytes in LEB128 encoded signed number.");
    Assertions.assertThat(Leb128.signed(15, wasmBytes(0xFF, 0x8F, 0x7F))).isEqualTo(-14337L);

    Assertions.assertThatThrownBy(() -> Leb128.signed(65, wasmBytes(0x00)))
        .isInstanceOf(Leb128Exception.class)
        .hasMessage("LEB128 signed decoder only supports 64 bits.");
  }

  /**
   * Get a WASM stream containing the passed bytes.
   *
   * @param bytes The bytes of the stream
   * @return A WASM stream
   */
  private WasmByteStream wasmBytes(int... bytes) {
    byte[] bs = new byte[bytes.length];

    for (int i = 0; i < bytes.length; i++) {
      bs[i] = (byte) bytes[i];
    }
    return new WasmByteStream(bs);
  }
}
