package com.partisiablockchain.language.wasm.wabt;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.io.IOException;
import java.io.InputStream;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Hex;
import org.junit.jupiter.api.Test;

/** Test WASM to WAT. */
public final class Wat2WasmTest {

  /** Test wat2wasm on an empty WASM module. */
  @Test
  public void emptyModule() {
    Assertions.assertThat(Wat2Wasm.translate("(module )", Wat2WasmTest.class))
        .isEqualTo(
            Hex.decode(
                "00 61 73 6d " // WASM Magic Bytes
                    + "01 00 00 00 " // WASM Version
                ));
  }

  /** Test wat2wasm on a fibonacci program. */
  @Test
  public void fib() throws IOException {
    InputStream fibWatStream = Wat2WasmTest.class.getResourceAsStream("fib.wat");
    final byte[] fibWasm = Wat2WasmTest.class.getResourceAsStream("fib.wasm").readAllBytes();

    Assertions.assertThat(Wat2Wasm.translate(fibWatStream, Wat2WasmTest.class)).isEqualTo(fibWasm);
  }
}
