package com.partisiablockchain.language.wasm.spec;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.TrapException;
import com.partisiablockchain.language.wasm.common.WasmLimitations;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.parser.WasmByteStream;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import com.secata.tools.coverage.WithResource;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.assertj.core.api.Assertions;
import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Official WASM Spec Test Suite for Standardized W3C 1.0.
 * https://github.com/WebAssembly/spec/tree/w3c-1.0
 *
 * <p>The test format is described here https://github.com/WebAssembly/spec/tree/master/interpreter
 *
 * <p>The tests have been converted to JSON using the tool <code>wast2json</code>
 * https://github.com/WebAssembly/wabt/blob/main/docs/wast2json.md
 *
 * <p>Use the included bash-script to download and convert all the official test spec files.
 * /scripts/update-spec-tests.sh
 *
 * <p>The test uses Junit 4 Parametrized testing to iterate all test cases
 * https://www.tutorialspoint.com/junit/junit_parameterized_test.htm
 */
public final class WasmSpecTest {

  /** The test spec to run. */
  private TestSpec testSpec;

  private static final Logger logger = LoggerFactory.getLogger(WasmSpecTest.class);

  private WasmModule assertParseSuccess() {
    try {
      return loadAndParseModule(testSpec.suiteName, testSpec.fileName);
    } catch (Throwable e) {
      if (!IGNORE_TESTS.ignoreThrows(e)) {
        throw e;
      }
    }
    return null;
  }

  private void assertRunSuccess(ActionSpec action, List<Literal> expectedResult) {
    try {
      WasmModule parsedModule = loadAndParseModule(testSpec.suiteName, testSpec.fileName);
      List<Literal> actual = runAction(parsedModule, action);
      // Reverse the results from the stack before comparison
      Collections.reverse(actual);
      Assertions.assertThat(actual).containsExactlyElementsOf(expectedResult);
    } catch (Throwable e) {
      if (!IGNORE_TESTS.ignoreThrows(e)) {
        throw e;
      }
    }
  }

  private void assertParseThrowing(
      Class<? extends Throwable> throwClass, String messageContainsIgnoreCase) {
    assertThrowing(
        () -> {
          final WasmModule parsedModule = loadAndParseModule(testSpec.suiteName, testSpec.fileName);
          WasmInstance.forModule(parsedModule);
        },
        throwClass,
        messageContainsIgnoreCase);
  }

  private void assertRunThrowing(
      ActionSpec action, Class<? extends Throwable> throwClass, String messageContainsIgnoreCase) {
    WasmModule parsedModule = assertParseSuccess();
    if (parsedModule == null) {
      // The load&parse met an ignore condition
      return;
    }
    assertThrowing(
        () -> {
          runAction(parsedModule, action);
        },
        throwClass,
        messageContainsIgnoreCase);
  }

  private static void assertThrowing(
      ThrowableAssert.ThrowingCallable shouldThrow,
      Class<? extends Throwable> throwClass,
      String messageContainsIgnoreCase) {
    final Throwable thrown = Assertions.catchThrowable(shouldThrow);
    Assertions.assertThat(thrown).isNotNull();
    if (IGNORE_TESTS.ignoreThrows(thrown)) {
      return;
    }
    Assertions.assertThat(thrown).isInstanceOf(throwClass);
    if (!thrown
        .getMessage()
        .toLowerCase(Locale.ROOT)
        .contains(messageContainsIgnoreCase.toLowerCase(Locale.ROOT))) {
      Assertions.fail(
          "Expecting throwable with message containing '%s'.\n Actual thrown is %s.",
          messageContainsIgnoreCase, thrown);
    }
  }

  /**
   * Test of specification.
   *
   * @param testSpec the spec to test
   */
  @ParameterizedTest
  @MethodSource("parameters")
  public void testSpecCommand(TestSpec testSpec) {
    this.testSpec = testSpec;
    final ActionSpec action = testSpec.action;
    final String testType = testSpec.type;
    if (IGNORE_TESTS.ignoreTest(testSpec.suiteName, testSpec.line)) {
      return;
    }
    logger.info(
        "{}#{}: {} {}() in {}",
        testSpec.suiteName,
        testSpec.line,
        testType,
        (action == null) ? "" : action.field,
        testSpec.fileName);
    if ("module".equals(testType)) {
      // Parse the module expecting success
      assertParseSuccess();
    } else if ("assert_invalid".equals(testType) && action == null) {
      // Parse the module expecting failure
      assertParseThrowing(RuntimeException.class, testSpec.text);
    } else if ("assert_malformed".equals(testType)) {
      // Parse the module expecting failure
      assertParseThrowing(RuntimeException.class, testSpec.text);
    } else if ("assert_unlinkable".equals(testType)) {
      // Parse the module expecting failure
      assertParseThrowing(RuntimeException.class, testSpec.text);
    } else if ("assert_return".equals(testType)) {
      // Execute a function expecting a result
      assertRunSuccess(action, testSpec.getExpectedLiterals());
    } else if ("action".equals(testType)) {
      // Execute a function expecting no failure
      assertRunSuccess(action, new ArrayList<>());
    } else if ("assert_invalid".equals(testType)) { // implicit action != null
      // Execute a function expecting failure
      assertRunThrowing(action, RuntimeException.class, testSpec.text);
    } else if ("assert_exhaustion".equals(testType)) {
      // Execute a function expecting stack exhaustion failure
      assertRunThrowing(action, TrapException.class, "call stack exhausted");
    } else if ("assert_trap".equals(testType)) {
      // Execute a function expecting a trap
      assertRunThrowing(action, RuntimeException.class, testSpec.text);
    } else {
      // Unknown test type
      Assertions.fail("not implemented '%s'", testType);
    }
  }

  /**
   * Tests currently being ignored or forced to succeed. This list is used to make the SPEC suite
   * pass, and should be minimized through handling of more and more cases.
   */
  private static WasmSpecIgnores IGNORE_TESTS =
      new WasmSpecIgnores(
          Arrays.asList(
              new WasmSpecIgnores.ThrowsMessage("Unknown block result type: 0x7D"),
              new WasmSpecIgnores.ThrowsMessage("Unknown block result type: 0x7C"),
              new WasmSpecIgnores.ThrowsMessage("Floating point opcode not supported"),
              new WasmSpecIgnores.ThrowsMessage("WASM interpreter only supports 31-bits"),
              new WasmSpecIgnores.ThrowsMessage("Only function imports are supported"),
              new WasmSpecIgnores.ThrowsMessage("Arbitrarily imported modules are not supported"),
              new WasmSpecIgnores.ThrowsMessage(
                  "Trap: Calling invalid function"), // Alternatively let TrapException be
              // transparent to Throws Message.
              new WasmSpecIgnores.TestType(
                  "register", false), // Multi-module programs are unsupported
              new WasmSpecIgnores.TestType("assert_uninstantiable", false), // Start unsupported
              new WasmSpecIgnores.ModuleType(
                  "assert_malformed", "text"), // Text malformed tests are currently infeasible
              new WasmSpecIgnores.ModuleType(
                  "assert_invalid", "text"), // Text invalid tests are currently infeasible
              new WasmSpecIgnores.Test("address", 194), // Function not parsed due to large uint
              new WasmSpecIgnores.Test("address", 195), // ^^^
              new WasmSpecIgnores.Test("address", 196), // ^^^
              new WasmSpecIgnores.Test("address", 197), // ^^^
              new WasmSpecIgnores.Test("address", 198), // ^^^
              new WasmSpecIgnores.Test("address", 200), // ^^^
              new WasmSpecIgnores.Test("address", 201), // ^^^
              new WasmSpecIgnores.Test("address", 202), // ^^^
              new WasmSpecIgnores.Test("address", 203), // ^^^
              new WasmSpecIgnores.Test("address", 204), // ^^^
              new WasmSpecIgnores.Test("address", 481), // ^^^
              new WasmSpecIgnores.Test("address", 482), // ^^^
              new WasmSpecIgnores.Test("address", 483), // ^^^
              new WasmSpecIgnores.Test("address", 484), // ^^^
              new WasmSpecIgnores.Test("address", 485), // ^^^
              new WasmSpecIgnores.Test("address", 486), // ^^^
              new WasmSpecIgnores.Test("address", 487), // ^^^
              new WasmSpecIgnores.Test("address", 489), // ^^^
              new WasmSpecIgnores.Test("address", 490), // ^^^
              new WasmSpecIgnores.Test("address", 491), // ^^^
              new WasmSpecIgnores.Test("address", 492), // ^^^
              new WasmSpecIgnores.Test("address", 493), // ^^^
              new WasmSpecIgnores.Test("address", 494), // ^^^
              new WasmSpecIgnores.Test("address", 495), // ^^^
              new WasmSpecIgnores.Test(
                  "address", 539), // Function not parsed due to floating point opcodes
              new WasmSpecIgnores.Test("address", 541), // ^^^
              new WasmSpecIgnores.Test("address", 542), // ^^^
              new WasmSpecIgnores.Test("address", 586), // ^^^
              new WasmSpecIgnores.Test("address", 588), // ^^^
              new WasmSpecIgnores.Test("address", 589), // ^^^
              new WasmSpecIgnores.Test(
                  "br_if", 374), // Function not parsed due to floating point opcodes
              new WasmSpecIgnores.Test("br_if", 375), // ^^^
              new WasmSpecIgnores.Test("br_if", 379), // ^^^
              new WasmSpecIgnores.Test("br_if", 380), // ^^^
              new WasmSpecIgnores.Test("br_if", 459), // ^^^
              new WasmSpecIgnores.Test("binary", 50), // Spec impl finds other error
              new WasmSpecIgnores.Test("binary", 69), // ^^^
              new WasmSpecIgnores.Test("binary", 88), // ^^^
              new WasmSpecIgnores.Test("binary", 106), // ^^^
              new WasmSpecIgnores.Test("binary", 124), // ^^^
              new WasmSpecIgnores.Test("binary", 571), // ^^^
              new WasmSpecIgnores.Test("binary", 696), // ^^^
              new WasmSpecIgnores.Test("binary", 741), // ^^^
              new WasmSpecIgnores.Test("binary", 763), // ^^^
              new WasmSpecIgnores.Test("binary", 798), // ^^^
              new WasmSpecIgnores.Test("block", 291), // Uses module state (memory.load)
              new WasmSpecIgnores.Test("call", 304), // Uses module state (memory.grow)
              new WasmSpecIgnores.Test("call", 311), // Uses module state (memory.load)
              new WasmSpecIgnores.Test("call", 348), // Floating point
              new WasmSpecIgnores.Test("call", 384), // Floating point
              new WasmSpecIgnores.Test("call", 391), // Floating point
              new WasmSpecIgnores.Suite("conversions"), // Floating point
              new WasmSpecIgnores.Test("custom", 77), // Mismatched error messages
              new WasmSpecIgnores.Test("custom", 93), // Mismatched error messages
              new WasmSpecIgnores.Test("custom", 115), // Mismatched error messages
              new WasmSpecIgnores.Test("data", 308), // Different order of constant checks
              new WasmSpecIgnores.Test("elem", 274), // Different order of constant checks
              new WasmSpecIgnores.Test("elem", 353), // Missing uninitialized element trap
              new WasmSpecIgnores.Test("elem", 366), // Missing uninitialized element trap
              new WasmSpecIgnores.Test("elem", 367), // Multi-module program
              new WasmSpecIgnores.Test("elem", 379), // Missing uninitialized element trap
              new WasmSpecIgnores.Test("elem", 380), // Multi-module program
              new WasmSpecIgnores.Test("elem", 381), // Multi-module program
              new WasmSpecIgnores.Suite("f32"), // Floating point
              new WasmSpecIgnores.Suite("f32_bitwise"), // Floating point
              new WasmSpecIgnores.Suite("f32_cmp"), // Floating point
              new WasmSpecIgnores.Suite("f64"), // Floating point
              new WasmSpecIgnores.Suite("f64_bitwise"), // Floating point
              new WasmSpecIgnores.Suite("f64_cmp"), // Floating point
              new WasmSpecIgnores.Suite("float_exprs"), // Floating point
              new WasmSpecIgnores.Suite("float_literals"), // Floating point
              new WasmSpecIgnores.Suite("float_memory"), // Floating point
              new WasmSpecIgnores.Suite("float_misc"), // Floating point
              new WasmSpecIgnores.Test(
                  "func_ptrs", 27), // Uses external functions (spectest.print_i32)
              new WasmSpecIgnores.Test("func_ptrs", 28), // ^^^
              new WasmSpecIgnores.Test("func_ptrs", 29), // ^^^
              new WasmSpecIgnores.Test("func_ptrs", 30), // ^^^
              new WasmSpecIgnores.Test("func_ptrs", 44), // Spec impl finds other error.
              new WasmSpecIgnores.Test("global", 257), // Different order of constant checks
              new WasmSpecIgnores.Test("global", 272), // Different order of constant checks
              new WasmSpecIgnores.Test("global", 292), // Globals in initializers unsupported
              new WasmSpecIgnores.Test("global", 297), // Globals in initializers unsupported
              new WasmSpecIgnores.Test("linking", 80), // Uses module state (global.set)
              new WasmSpecIgnores.Test("linking", 82), // Uses module state (global.set)
              new WasmSpecIgnores.Test("linking", 172), // Multi-module programs unsupported
              new WasmSpecIgnores.Test("linking", 178), // Multi-module programs unsupported
              new WasmSpecIgnores.Test("linking", 288), // Multi-module programs unsupported
              new WasmSpecIgnores.Test("linking", 387), // Multi-module programs unsupported
              new WasmSpecIgnores.Test("linking", 388), // Multi-module programs unsupported
              new WasmSpecIgnores.Test("loop", 340), // Uses module state (memory.load)
              new WasmSpecIgnores.Test("memory", 9), // Missing consistency check
              new WasmSpecIgnores.Test("memory", 53), // Different approach for memory size checks
              new WasmSpecIgnores.Test("memory", 65), // Different approach for memory size checks
              new WasmSpecIgnores.Test("memory_grow", 20), // Uses module state (memory)
              new WasmSpecIgnores.Test("memory_grow", 21), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 22), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 23), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 26), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 27), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 28), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 29), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 30), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 31), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 32), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 33), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 43), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 44), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 45), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 48), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 57), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 58), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 59), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 60), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 61), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 87), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 89), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 90), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 91), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 92), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 93), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 94), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 95), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 96), // ^^^
              new WasmSpecIgnores.Test("memory_grow", 97), // ^^^
              new WasmSpecIgnores.Suite("memory_size"), // Entire suite uses module state (memory)
              new WasmSpecIgnores.Test("memory_trap", 22), // Uses module state (memory.load)
              new WasmSpecIgnores.Test("names", 1107), // Uses external function (print_i32)
              new WasmSpecIgnores.Test("nop", 382), // Uses module state (memory.load)
              new WasmSpecIgnores.Test("select", 257), // Uses module state (memory.grow)
              new WasmSpecIgnores.Test("select", 273), // Uses module state (memory.load)
              new WasmSpecIgnores.Test("select", 274), // Uses module state (memory.load)
              new WasmSpecIgnores.Suite("start") // Start expressions are unsupported
              ));

  private static final String[] SUITES =
      new String[] {
        "address",
        "align",
        "binary",
        "binary-leb128",
        "block",
        "br",
        "br_if",
        "br_table",
        "break-drop",
        "call",
        "call_indirect",
        "comments",
        "const",
        "conversions",
        "custom",
        "data",
        "elem",
        "endianness",
        "exports",
        "f32",
        "f32_bitwise",
        "f32_cmp",
        "f64",
        "f64_bitwise",
        "f64_cmp",
        "fac",
        "float_exprs",
        "float_literals",
        "float_memory",
        "float_misc",
        "forward",
        "func",
        "func_ptrs",
        "get_local",
        "global",
        "i32",
        "i64",
        "if",
        "imports",
        "inline-module",
        "int_exprs",
        "int_literals",
        "labels",
        "left-to-right",
        "linking",
        "load",
        "local_get",
        "local_set",
        "local_tee",
        "loop",
        "memory",
        "memory_grow",
        "memory_redundancy",
        "memory_size",
        "memory_trap",
        "names",
        "nop",
        "return",
        "select",
        "skip-stack-guard-page",
        "stack",
        "start",
        "store",
        "switch",
        "table",
        "token",
        "traps",
        "type",
        "unreachable",
        "unreached-invalid",
        "unwind",
        "utf8-custom-section-id",
        "utf8-import-field",
        "utf8-import-module",
        "utf8-invalid-encoding",
      };

  /**
   * Generate parameters for the parametrized test.
   *
   * <p>The parameters returned match the method parameters.
   *
   * @return List of parameters used to instantiate test cases.
   */
  public static List<TestSpec[]> parameters() {
    ArrayList<TestSpec[]> testSpecs = new ArrayList<>();
    for (String suiteName : SUITES) {
      if (IGNORE_TESTS.ignoreSuite(suiteName)) {
        continue;
      }
      addSuite(suiteName, testSpecs);
    }
    return testSpecs;
  }

  private static void addSuite(String suiteName, List<TestSpec[]> wasmSpecData) {
    TestSuiteSpec testSuiteSpec =
        TestSuiteSpec.deserializeJson(loadTestFileBytes(suiteName, suiteName + ".json"));
    String currentModuleFileName = "";
    final HashMap<String, String> registeredModuleFileNames = new HashMap<>();

    for (TestSpec testSpec : testSuiteSpec.commands) {
      if (IGNORE_TESTS.ignoreType(testSpec.type, testSpec.action)) {
        continue;
      }
      if (IGNORE_TESTS.ignoreModule(testSpec.type, testSpec.moduleType)) {
        continue;
      }
      // Inject the suite name into the test spec
      testSpec.suiteName = suiteName;
      // Update the current module filename if the test spec does not have a filename
      if (testSpec.fileName == null) {
        if (testSpec.action.withSpecificModule != null) {
          testSpec.fileName = registeredModuleFileNames.get(testSpec.action.withSpecificModule);
        } else {
          testSpec.fileName = currentModuleFileName;
        }
      }
      // Remember the module filename for the next test specs
      if (testSpec.type.equals("module")) {
        currentModuleFileName = testSpec.fileName;
        if (testSpec.name != null) {
          registeredModuleFileNames.put(testSpec.name, testSpec.fileName);
        }
      }
      wasmSpecData.add(new TestSpec[] {testSpec});
    }
  }

  /** Cycles passed to each call in the test suite. */
  public static final long GAS_PER_CALL = 1_000_000L;

  private static List<Literal> runAction(final WasmModule wasmModule, final ActionSpec action) {
    final WasmInstance instance =
        WasmInstance.forModule(wasmModule, WasmLimitations.DEFAULT.withLazyValidation());
    switch (action.type) {
      case "invoke":
        final List<Literal> executionResult =
            instance.runFunction(action.field, GAS_PER_CALL, action.getArgLiterals());
        logger.info("Cycles cost: {}", instance.getCyclesUsed());
        return executionResult;
      case "get":
        return List.of(instance.getGlobalValue(action.field));
      default:
        throw new IllegalArgumentException("Unknown action type: " + action.type);
    }
  }

  private static WasmModule loadAndParseModule(String suiteName, String fileName) {
    // Parse the module to make sure it does not produce a parse error
    byte[] wasmBytes = loadTestFileBytes(suiteName, fileName);
    WasmParser wasmParser = new WasmParser(new WasmByteStream(wasmBytes));
    return wasmParser.parse();
  }

  private static byte[] loadTestFileBytes(String baseFolder, String fileName) {
    String name = baseFolder + File.separator + fileName;
    return WithResource.apply(
        () -> WasmSpecTest.class.getResourceAsStream(name),
        InputStream::readAllBytes,
        "Cannot load bytes from file '" + name + "'");
  }
}
