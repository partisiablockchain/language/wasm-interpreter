package com.partisiablockchain.language.wasm.spec;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Specifies test cases or results to ignore during WASM spec testing.
 *
 * <p>The goal is to allow us to fix problems towards the spec test iteratively by first ignoring
 * large parts of the spec and then gradually improving the spec compliance.
 */
public final class WasmSpecIgnores {

  /** Marker interface for a type of test to ignore. */
  interface Ignore {}

  /** Ignores an entire spec test suite. */
  record Suite(String suiteName) implements Ignore {}

  /** Ignores a specific test in a suite. */
  record Test(String suiteName, Integer line) implements Ignore {}

  /** Ignores all tests with a specific test type and action spec. */
  record TestType(String type, boolean isActionPresent) implements Ignore {}

  /** Ignores all tests with a specific test type and module type. */
  record ModuleType(String type, String moduleType) implements Ignore {}

  /** Ignores all tests that throws a specific error message. */
  record ThrowsMessage(String throwsMessage) implements Ignore {}

  /** List of everything to ignore. */
  private final List<Ignore> ignores;

  WasmSpecIgnores(List<Ignore> ignores) {
    this.ignores = ignores;
  }

  /**
   * Determines if a test suite should be ignored.
   *
   * @param suiteName The name of the test suite
   * @return true if the suite should be ignored.
   */
  public boolean ignoreSuite(String suiteName) {
    return ignores.stream()
        .filter(ignore -> ignore instanceof Suite)
        .anyMatch(ignore -> ((Suite) ignore).suiteName.equals(suiteName));
  }

  /**
   * Determines if a test should be ignored.
   *
   * @param suiteName The name of the test suite
   * @param line The line of the test
   * @return true if the test should be ignored.
   */
  public boolean ignoreTest(String suiteName, Integer line) {
    return ignores.stream()
        .filter(ignore -> ignore instanceof Test)
        .anyMatch(
            ignore ->
                ((Test) ignore).suiteName.equals(suiteName)
                    && Objects.equals(((Test) ignore).line, line));
  }

  /**
   * Determines if a test type should be ignored.
   *
   * @param typeName The name of the test type
   * @return true if the type should be ignored.
   */
  public boolean ignoreType(String typeName, ActionSpec actionSpec) {
    return ignores.stream()
        .filter(ignore -> ignore instanceof TestType)
        .filter(ignore -> ((TestType) ignore).isActionPresent == (actionSpec != null))
        .anyMatch(ignore -> ((TestType) ignore).type.equals(typeName));
  }

  /**
   * Determines if a module type should be ignored.
   *
   * @param typeName The name of the module type
   * @return true if the type should be ignored.
   */
  public boolean ignoreModule(final String typeName, final String moduleType) {
    return ignores.stream()
        .filter(ignore -> ignore instanceof ModuleType)
        .filter(ignore -> ((ModuleType) ignore).type.equals(typeName))
        .anyMatch(ignore -> Objects.equals(((ModuleType) ignore).moduleType, moduleType));
  }

  /**
   * Determines if a test throwing an exception should succeed because the exception is currently
   * ignored.
   *
   * @param thrown The thrown exception
   * @return true if the thrown exception should be ignored.
   */
  public boolean ignoreThrows(Throwable thrown) {
    if (thrown.getMessage() == null) {
      return false;
    }
    return ignores.stream()
        .filter(ignore -> ignore instanceof ThrowsMessage)
        .anyMatch(
            ignore ->
                thrown
                    .getMessage()
                    .toLowerCase(Locale.ROOT)
                    .contains(((ThrowsMessage) ignore).throwsMessage.toLowerCase(Locale.ROOT)));
  }
}
