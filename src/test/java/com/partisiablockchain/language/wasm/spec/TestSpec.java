package com.partisiablockchain.language.wasm.spec;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.partisiablockchain.language.wasm.common.literal.Literal;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

/** A single test in the WASM spec test. */
@JsonIgnoreProperties(value = {"as"})
final class TestSpec {

  /**
   * Name of the test suite (The folder containing the JSON and WASM-files).
   *
   * <p>Set by {@link WasmSpecTest} during initialization.
   */
  public String suiteName;

  @JsonProperty("filename")
  public String fileName;

  @JsonProperty("module_type")
  public String moduleType;

  public String type;
  public ActionSpec action;
  public List<ValueSpec> expected;
  public String text;
  public Integer line;
  public String name;

  /**
   * Get the expected result of running the action as WASM Literals.
   *
   * @return The literal expected result
   */
  public List<Literal> getExpectedLiterals() {
    return expected.stream().map(ValueSpec::toLiteral).collect(Collectors.toList());
  }

  /**
   * Name used for the test spec in the JUnit UI.
   *
   * @return The name
   */
  @Override
  public String toString() {
    return MessageFormat.format("{0}:{1} {2}", suiteName, line, type);
  }
}
