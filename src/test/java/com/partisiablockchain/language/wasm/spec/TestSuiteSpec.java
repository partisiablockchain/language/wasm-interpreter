package com.partisiablockchain.language.wasm.spec;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import java.util.List;

/** A test suite of test commands in the WASM spec test. */
@JsonIgnoreProperties(value = {"source_filename"})
final class TestSuiteSpec {

  public List<TestSpec> commands;

  static TestSuiteSpec deserializeJson(byte[] read) {
    try {
      ObjectMapper result = new ObjectMapper();
      result
          .setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE)
          .setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.PUBLIC_ONLY);
      result.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, true);
      result.setSerializationInclusion(JsonInclude.Include.NON_NULL);
      result.registerModule(new JavaTimeModule());

      return result.readValue(read, TestSuiteSpec.class);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
