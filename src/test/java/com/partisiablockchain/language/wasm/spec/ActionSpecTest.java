package com.partisiablockchain.language.wasm.spec;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

import java.util.ArrayList;
import java.util.UUID;
import org.junit.jupiter.api.Test;

/** Dummy test to prevent unused fields SpotBugs. */
public final class ActionSpecTest {

  private static final String type = UUID.randomUUID().toString();
  private static final String field = UUID.randomUUID().toString();
  private static final String arg0Type = UUID.randomUUID().toString();
  private static final String arg0Value = UUID.randomUUID().toString();

  @Test
  public void test() {
    ActionSpec actionSpec = createActionSpec();
    assertActionSpec(actionSpec);
  }

  static ActionSpec createActionSpec() {
    ValueSpec arg0 = new ValueSpec();
    arg0.type = arg0Type;
    arg0.value = arg0Value;

    ActionSpec actionSpec = new ActionSpec();
    actionSpec.type = type;
    actionSpec.field = field;
    actionSpec.args = new ArrayList<ValueSpec>();
    actionSpec.args.add(arg0);
    return actionSpec;
  }

  static void assertActionSpec(ActionSpec dto) {
    assertThat(dto.type).isEqualTo(type);
    assertThat(dto.field).isEqualTo(field);
    assertThat(dto.args.get(0).type).isEqualTo(arg0Type);
    assertThat(dto.args.get(0).value).isEqualTo(arg0Value);
  }
}
