package com.partisiablockchain.language.wasm.validator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.AddressKind;
import com.partisiablockchain.language.wasm.common.FunctionType;
import com.partisiablockchain.language.wasm.common.Global;
import com.partisiablockchain.language.wasm.common.GlobalType;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.ResizableLimits;
import com.partisiablockchain.language.wasm.common.TableType;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.ValType;
import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.common.literal.Int32;
import com.partisiablockchain.language.wasm.common.sections.GlobalSection;
import com.partisiablockchain.language.wasm.common.sections.TableSection;
import com.partisiablockchain.language.wasm.common.sections.TypeSection;
import com.partisiablockchain.language.wasm.interpreter.Instruction;
import com.partisiablockchain.language.wasm.interpreter.InstructionI32;
import com.partisiablockchain.language.wasm.interpreter.InstructionNoArgs;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import com.partisiablockchain.language.wasm.validator.FunctionValidator.TypeContext;
import com.partisiablockchain.language.wasm.wabt.Wat2Wasm;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Tests that {@link FunctionValidator} can catch various type errors, and let non-problematic
 * programs through.
 */
public final class FunctionValidatorTest {

  private static final InstructionI32 CONST_I32 =
      new InstructionI32(OpCode.I_32_CONST, new Int32(2));
  private static final InstructionNoArgs END = new InstructionNoArgs(OpCode.END);

  record GoodCase(String name, ValType returnType, List<Instruction> instructions) {

    GoodCase(String name, ValType returnType, String wasmTextInstructions) {
      this(name, returnType, parseWatInstructions(wasmTextInstructions));
    }
  }

  record BadCase(String name, String expectedErrorMessage, List<Instruction> instructions) {
    BadCase(String name, String expectedErrorMessage, String wasmTextInstructions) {
      this(name, expectedErrorMessage, parseWatInstructions(wasmTextInstructions));
    }
  }

  static List<GoodCase> goodCases() {
    final List<GoodCase> cases =
        List.of(
            new GoodCase("BinaryCalc", ValType.I32, "i32.const 2 i32.const 2 i32.add"),
            new GoodCase("BinaryComparison", ValType.I32, "i32.const 2 i32.const 2 i32.eq"),
            new GoodCase("UnaryCalc", ValType.I32, "i32.const 2 i32.clz i32.eqz"),
            new GoodCase("BinaryComparisonI64", ValType.I32, "i64.const 2 i64.const 2 i64.eq"),
            new GoodCase(
                "Casts",
                ValType.I32,
                "i32.const 2 i64.extend_i32_s i32.wrap_i64 i64.extend_i32_u i32.wrap_i64"),
            new GoodCase("Unop64", ValType.I32, "i64.const 2 i64.popcnt i64.eqz"),
            new GoodCase("Binop64", ValType.I32, "i64.const 2 i64.const 2 i64.add i32.wrap_i64"),
            new GoodCase(
                "Nop", ValType.I32, "nop nop nop nop nop nop i32.const 2 nop nop nop nop nop"),
            new GoodCase("Local", ValType.I32, "i32.const 2 local.set 0 local.get 0 local.tee 0"),
            new GoodCase("Block", ValType.I32, "block (result i32) i32.const 2 end"),
            new GoodCase("EmptyBlock", ValType.I32, "i32.const 2 block nop end"),
            new GoodCase("BrIfEmpty", ValType.I32, "i32.const 2 block i32.const 2 br_if 0 end"),
            new GoodCase(
                "BrIf",
                ValType.I32,
                "block (result i32) i32.const 2 i32.const 2 br_if 0 i32.eqz end"),
            new GoodCase(
                "BrIfNested",
                ValType.I32,
                "block (result i32) "
                    + "block (result i64) i32.const 2 i32.const 2 br_if 1 i64.extend_i32_s end "
                    + "i64.eqz end"),
            new GoodCase(
                "BrIfNestedNested",
                ValType.I32,
                "block (result i32) "
                    + "block (result i32) "
                    + "block (result i64) i32.const 2 i32.const 2 br_if 2 i64.extend_i32_s end "
                    + "i64.eqz end "
                    + "end"),
            new GoodCase("EmptyExitedBlock", ValType.I32, "i32.const 2 block br 0 end"),
            new GoodCase("ExitedBlock", ValType.I32, "block (result i32) i32.const 2 br 0 end"),
            new GoodCase(
                "EmptyExitedBlockWithUnreachable",
                ValType.I32,
                "i32.const 2 block br 0 i32.const 2 drop end"),
            new GoodCase(
                "EmptyExitedBlockWithUnreachableBr",
                ValType.I32,
                "block (result i32) i32.const 2 br 0 br 0 br 0 br 0 br 0 end"),
            new GoodCase(
                "EmptyExitedBlockWithUnreachableUnop",
                ValType.I32,
                "block (result i32) i32.const 2 br 0 i64.eqz end"),
            new GoodCase(
                "BranchTable",
                ValType.I32,
                "block (result i32) block (result i32) i32.const 2 i32.const 2 (br_table 0 1) end"
                    + " end"),
            new GoodCase(
                "If", ValType.I32, "i32.const 2 if (result i32) i32.const 1 else i32.const 2 end"),
            new GoodCase(
                "IfEmpty", ValType.I32, "i32.const 2 i32.const 2 if (result) nop else nop end"),
            new GoodCase("Loop", ValType.I32, "loop (result i32) i32.const 2 end"),
            new GoodCase("LoopInfinite", ValType.I32, "loop (result i32) i64.const 2 br 0 end"),
            new GoodCase("Unreachable", ValType.I32, "unreachable"),
            new GoodCase("GlobalGet", ValType.I32, "global.get 0"),
            new GoodCase("GlobalSet", ValType.I32, "i32.const 1 i64.const 2 global.set 1"),
            new GoodCase("Drop", ValType.I32, "i32.const 1 i64.const 2 drop"),
            new GoodCase("DropI64", ValType.I64, "i64.const 1 i32.const 2 drop"),
            new GoodCase("Select", ValType.I32, "i32.const 1 i32.const 2 i32.const 3 select"),
            new GoodCase("SelectI64Eqz", ValType.I64, "i64.const 1 i64.const 2 i32.const 3 select"),
            new GoodCase(
                "DropALotInUnreachable",
                ValType.I32,
                "i32.const 1 unreachable drop drop drop drop"),
            new GoodCase(
                "SelectHalfKnown",
                ValType.I32,
                "i32.const 1 unreachable select i32.const 1 i32.const 1 select"),
            new GoodCase("DropUnknownType", ValType.I32, "i32.const 1 unreachable select drop"),
            new GoodCase(
                "SelectALotInUnreachable",
                ValType.I32,
                "i32.const 1 unreachable select select select select"),
            new GoodCase("CallUnary", ValType.I32, "i32.const 1 call 0"),
            new GoodCase("CallBinary", ValType.I32, "i32.const 1 i64.const 2 call 1"));

    return concat(
        cases, concat(goodCasesMemory(), concat(goodCasesReturn(), goodCasesCallIndirect())));
  }

  static List<BadCase> badCases() {
    return List.of(
        new BadCase(
            "ShallowStackBinary",
            "Value stack too shallow. Expected at least 2 elements, but had 1",
            "i32.const 2 i32.add"),
        new BadCase(
            "ShallowStackUnary",
            "Value stack too shallow. Expected at least 1 elements, but had 0",
            "i32.clz"),
        new BadCase(
            "WrongTypeUnary",
            "Type mismatch: Wrong stack type. Expected i32, but got i64.",
            "i64.const 2 i32.clz"),
        new BadCase(
            "WrongTypeBinary",
            "Type mismatch: Wrong stack type. Expected i32, but got i64.",
            "i64.const 2 i32.const 2 i32.add"),
        new BadCase(
            "MissingEnd", "Expression did not end with an END instruction", List.of(CONST_I32)),
        new BadCase(
            "UnexpectedEnd",
            "END instruction only allowed at end of expression",
            List.of(CONST_I32, END, new InstructionNoArgs(OpCode.I_32_CLZ), END)),
        new BadCase(
            "UnexpectedElse",
            "ELSE instruction only allowed at end of expression",
            List.of(
                CONST_I32,
                new InstructionNoArgs(OpCode.ELSE),
                new InstructionNoArgs(OpCode.I_32_CLZ),
                END)),
        new BadCase(
            "UnknownFloatingPoint",
            "Floating point opcode not supported",
            List.of(new InstructionNoArgs(OpCode.UNSUPPORTED_FLOATING_POINT), END)),
        new BadCase(
            "UnknownOpcode",
            "Unknown or illegal opcode",
            List.of(new InstructionNoArgs(OpCode.UNKNOWN), END)),
        new BadCase(
            "LocalSetWrongType",
            "Type mismatch: Wrong stack type. Expected i32, but got i64.",
            "i64.const 2 local.set 0"),
        new BadCase("UnknownLocalSet", "Unknown local 999", "i32.const 2 local.set 999"),
        new BadCase("UnknownLocalGet", "Unknown local 999", "local.get 999"),
        new BadCase("UnknownLocalTee", "Unknown local 999", "local.tee 999"),
        new BadCase(
            "ShouldBeEmptyBlock",
            "Stack contained more values than expected: [i32]",
            "i32.const 2 block i32.const 2 end"),
        new BadCase(
            "ShouldNotBeEmptyBlock",
            "Value stack too shallow. Expected at least 1 elements, but had 0",
            "i32.const 2 block (result i32) nop end"),
        new BadCase(
            "UnknownLabel",
            "Unknown label: Stack too shallow. Expected at least 1000 labels, but had only 0",
            "br_if 999"),
        new BadCase(
            "BrIfMissingResultOnTrueBranch",
            "Value stack too shallow. Expected at least 1 elements, but had 0",
            "block (result i32) i32.const 2 br_if 0 i32.const 2 end"),
        new BadCase(
            "BrIfWrongType",
            "Type mismatch: Wrong stack type. Expected i32, but got i64.",
            "block (result i32) i64.const 2 br_if 0 i32.const 2 end"),
        new BadCase(
            "NotAsNestedAsExpected",
            "Unknown label: Stack too shallow. Expected at least 4 labels, but had only 3",
            "block (result i32) "
                + "block (result i32) "
                + "block (result i64) i32.const 2 i32.const 2 br_if 3 i64.extend_i32_s end "
                + "i64.eqz end "
                + "end"),
        new BadCase(
            "BranchMismatches",
            "Branch to label 0 had type i32, whereas default branch had type i64",
            "block (result i64) "
                + "block (result i32) "
                + " i32.const 1 "
                + " i32.const 2 "
                + " (br_table 0 1) "
                + "end "
                + "end"),
        new BadCase(
            "ShallowLabelStack",
            "Unknown label: Stack too shallow. Expected at least 1 labels, but had only 0",
            "(br_table 0)"),
        new BadCase(
            "IfShouldNotBeEmpty",
            "Value stack too shallow. Expected at least 1 elements, but had 0",
            "i32.const 1 i32.const 2 if (result i32) nop else nop end"),
        new BadCase(
            "IfTooManyValuesIf",
            "Stack contained more values than expected: [i32]",
            "i32.const 1 if (result) i32.const 1 else nop end"),
        new BadCase(
            "IfTooManyValuesElse",
            "Stack contained more values than expected: [i32]",
            "i32.const 1 if (result) nop else i32.const 1 end"),
        new BadCase(
            "LoopMissingValue",
            "Value stack too shallow. Expected at least 1 elements, but had 0",
            "loop (result i32) nop end"),
        new BadCase(
            "LoopTooManyValues",
            "Stack contained more values than expected: [i32]",
            "loop (result i32) i32.const 1 i32.const 2 end"),
        new BadCase(
            "MemorySizeUnknown",
            "Unknown memory 1; context had 1 memories",
            List.of(new InstructionU31(OpCode.MEMORY_SIZE, Uint31.ONE), END)),
        new BadCase(
            "MemoryGrowUnknown",
            "Unknown memory 1; context had 1 memories",
            List.of(CONST_I32, new InstructionU31(OpCode.GROW_MEMORY, Uint31.ONE), END)),
        new BadCase(
            "LoadInvalidMem",
            "Malformed alignment: Alignment must not be larger than natural. Type is 32 bits wide,"
                + " but alignment specifies 64 bits",
            "i32.const 1 i32.load align=8"),
        new BadCase(
            "Load64InvalidMem",
            "Malformed alignment: Alignment must not be larger than natural. Type is 64 bits wide,"
                + " but alignment specifies 128 bits",
            "i32.const 1 i64.load align=16"),
        new BadCase(
            "StoreInvalidMem",
            "Malformed alignment: Alignment must not be larger than natural. Type is 32 bits wide,"
                + " but alignment specifies 64 bits",
            "i32.const 1 i32.const 2 i32.store align=8"),
        new BadCase(
            "Store64InvalidMem",
            "Malformed alignment: Alignment must not be larger than natural. Type is 64 bits wide,"
                + " but alignment specifies 128 bits",
            "i32.const 1 i64.const 2 i64.store align=16"),
        new BadCase("GetUnknown", "Unknown global 999; context had 3 globals", "global.get 999"),
        new BadCase(
            "SetUnknown",
            "Unknown global 999; context had 3 globals",
            "i32.const 42 global.set 999"),
        new BadCase("SetWrongType", "Expected i32, but got i64", "i64.const 42 global.set 0"),
        new BadCase(
            "SetImmutable",
            "OpCode GLOBAL_SET cannot set immutable global 2",
            "i32.const 42 global.set 2"),
        new BadCase(
            "ShallowStackDrop",
            "Value stack too shallow. Expected at least 1 elements, but had 0",
            "drop"),
        new BadCase(
            "ShallowStackSelect",
            "Value stack too shallow. Expected at least 2 elements, but had 1",
            "i32.const 1 i32.const 2 select"),
        new BadCase(
            "WrongTypeSelectValues",
            "Type mismatch: Wrong stack type. Expected i64, but got i32.",
            "i32.const 1 i64.const 2 i32.const 3 select"),
        new BadCase(
            "WrongTypeSelectCondition",
            "Type mismatch: Wrong stack type. Expected i32, but got i64.",
            "i32.const 1 i32.const 2 i64.const 3 select"),
        new BadCase(
            "ReturnWrongType",
            "Type mismatch: Wrong stack type. Expected i32, but got i64.",
            "i64.const 1 return"),
        new BadCase(
            "CallMissingArgument",
            "Value stack too shallow. Expected at least 1 elements, but had 0",
            "call 0"),
        new BadCase(
            "CallWrongArgument",
            "Type mismatch: Wrong stack type. Expected i32, but got i64.",
            "i64.const 231 call 0"),
        new BadCase(
            "CallUnknownFunction",
            "Unknown function 9999; context had 2 functions",
            "i32.const 231 call 9999"),
        new BadCase(
            "CallIndirectMissingFunctionIndex",
            "Value stack too shallow. Expected at least 1 elements, but had 0",
            "call_indirect (type 0)"),
        new BadCase(
            "CallIndirectMissingArgument",
            "Value stack too shallow. Expected at least 1 elements, but had 0",
            "i32.const 1 call_indirect (type 0)"),
        new BadCase(
            "CallIndirectUnknownType",
            "Unknown type 9999; context had 2 types",
            "i32.const 1 i32.const 1 call_indirect (type 9999)"),
        new BadCase(
            "CallIndirectWrongType",
            "Type mismatch: Wrong stack type. Expected i32, but got i64.",
            "i64.const 1 i32.const 1 call_indirect (type 0)"));
  }

  static List<GoodCase> goodCasesMemory() {
    return List.of(
        new GoodCase("MemorySize", ValType.I32, "memory.size"),
        new GoodCase("MemoryGrow", ValType.I32, "i32.const 1 memory.grow"),
        new GoodCase("Load", ValType.I32, "i32.const 1 i32.load"),
        new GoodCase("Load64", ValType.I64, "i32.const 1 i64.load"),
        new GoodCase("Store", ValType.I32, "i32.const 1 i32.const 2 i32.store align=4 i32.const 1"),
        new GoodCase("Store64", ValType.I32, "i32.const 1 i64.const 2 i64.store i32.const 1"),
        new GoodCase("LoadAligned", ValType.I32, "i32.const 1 i32.load offset=5 align=4"),
        new GoodCase("Load64Aligned", ValType.I64, "i32.const 1 i64.load offset=32 align=8"),
        new GoodCase(
            "StoreAligned", ValType.I32, "i32.const 1 i32.const 2 i32.store align=4 i32.const 4"),
        new GoodCase(
            "Store64Aligned",
            ValType.I32,
            "i32.const 1 i64.const 2 i64.store align=4 i32.const 4"));
  }

  static List<GoodCase> goodCasesReturn() {
    return List.of(
        new GoodCase("Return", ValType.I32, "i32.const 1 return"),
        new GoodCase("ReturnAndPopalot", ValType.I32, "i32.const 1 return drop drop drop drop"),
        new GoodCase(
            "ReturnThroughBlocks",
            ValType.I32,
            "block (result i32) block (result i32) i32.const 5 return end end"));
  }

  static List<GoodCase> goodCasesCallIndirect() {
    return List.of(
        new GoodCase("CallIndirect", ValType.I32, "i32.const 1 i32.const 2 call_indirect (type 0)"),
        new GoodCase(
            "CallIndirectBinop",
            ValType.I32,
            "i32.const 1 i64.const 2 i32.const 3 call_indirect (type 1)"));
  }

  private static final TypeContext FUNCTION_VALIDATOR_INITIAL_CONTEXT =
      TypeContext.EMPTY.withLocals(List.of(ValType.I32)).withReturnTypes(List.of(ValType.I32));

  private static final GlobalSection GLOBAL_SECTION_I32m_I64m_I32c =
      new GlobalSection(
          List.of(
              new Global(new GlobalType(ValType.I32, true), List.of()),
              new Global(new GlobalType(ValType.I64, true), List.of()),
              new Global(new GlobalType(ValType.I32, false), List.of())));

  private static final TypeSection TYPE_SECTION =
      new TypeSection(
          List.of(
              new FunctionType(Uint31.ZERO, List.of(ValType.I32), List.of(ValType.I32)),
              new FunctionType(
                  Uint31.ZERO, List.of(ValType.I32, ValType.I64), List.of(ValType.I32))));

  private static final Map<Uint31, FunctionType> TYPE_OF_FUNCTION_BY_INDEX =
      Map.of(
          Uint31.ZERO,
          TYPE_SECTION.getEntryOrNull(Uint31.ZERO),
          Uint31.ONE,
          TYPE_SECTION.getEntryOrNull(Uint31.ONE));

  private static final TableSection TABLE_SECTION =
      new TableSection(
          List.of(
              new TableType(
                  AddressKind.FUNCTION, new ResizableLimits(new Uint31(1), new Uint31(1)))));

  private static final FunctionValidator FUNCTION_VALIDATOR =
      new FunctionValidator(
          Uint31.ONE,
          GLOBAL_SECTION_I32m_I64m_I32c,
          TYPE_OF_FUNCTION_BY_INDEX,
          TYPE_SECTION,
          TABLE_SECTION);

  static final FunctionValidator FUNCTION_VALIDATOR_EMPTY =
      new FunctionValidator(
          Uint31.ZERO,
          new GlobalSection(List.of()),
          Map.of(),
          new TypeSection(List.of()),
          new TableSection(List.of()));

  /**
   * Parse WASM text format instructions to WASM instructions.
   *
   * @param wasmText The WASM instructions in text format.
   * @return List of WASM instructions
   */
  static List<Instruction> parseWatInstructions(String wasmText) {
    final String wasmTextModule = "(module (func " + wasmText + "))";
    final byte[] wasmBytes = Wat2Wasm.translate(wasmTextModule, FunctionValidatorTest.class);
    final WasmParser wasmParser = new WasmParser(wasmBytes);
    final WasmModule module = wasmParser.parse();
    return module.getFunctionSection().getEntryOrNull(Uint31.ZERO).codeSegment().getCode();
  }

  @ParameterizedTest
  @MethodSource("goodCases")
  void goodCaseTest(GoodCase testCase) {
    // No need to assertions, these are enough:
    final TypeContext returnContext =
        FUNCTION_VALIDATOR.validateExpression(
            testCase.instructions(), FUNCTION_VALIDATOR_INITIAL_CONTEXT);
    returnContext.withPopped(testCase.returnType()).mustBeEmpty();
  }

  @ParameterizedTest
  @MethodSource("badCases")
  void badCaseTest(BadCase testCase) {
    Assertions.assertThatThrownBy(
            () -> {
              FUNCTION_VALIDATOR.validateExpression(
                  testCase.instructions(), FUNCTION_VALIDATOR_INITIAL_CONTEXT);
            })
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining(testCase.expectedErrorMessage());
  }

  @ParameterizedTest
  @MethodSource("goodCasesMemory")
  void badCaseWhenMemoryIsMissing(GoodCase testCase) {
    Assertions.assertThatThrownBy(
            () -> {
              FUNCTION_VALIDATOR_EMPTY.validateExpression(
                  testCase.instructions(), TypeContext.EMPTY);
            })
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining("Unknown memory 0")
        .hasMessageContaining("context had 0 memories");
  }

  @ParameterizedTest
  @MethodSource("goodCasesReturn")
  void badCaseWhenReturnTypeIsMissing(GoodCase testCase) {
    Assertions.assertThatThrownBy(
            () -> {
              FUNCTION_VALIDATOR_EMPTY.validateExpression(
                  testCase.instructions(), TypeContext.EMPTY);
            })
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining("Cannot return from this context; no return types given.");
  }

  @Test
  public void withLocalsAndReturn() {
    final TypeContext context =
        TypeContext.EMPTY.withLocals(List.of(ValType.I32)).withReturnTypes(List.of(ValType.I32));
    Assertions.assertThat(context.getTypeOfLocal(Uint31.ZERO)).isEqualTo(ValType.I32);
    Assertions.assertThat(context.getReturnTypes()).containsExactly(ValType.I32);
    Assertions.assertThatThrownBy(() -> context.getTypeOfLocal(Uint31.ONE))
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining("Unknown local 1; context had 1 locals");
  }

  private static <T> List<T> concat(final List<T> l1, final List<T> l2) {
    final List<T> flat = new ArrayList<T>();
    flat.addAll(l1);
    flat.addAll(l2);
    return flat;
  }

  @ParameterizedTest
  @MethodSource("goodCasesCallIndirect")
  void badCaseWhenMissingTableSection(final GoodCase testCase) {
    Assertions.assertThatExceptionOfType(WasmValidationException.class)
        .describedAs(
            "Expected validation error for badCaseWhenMissingTableSection:%s", testCase.name())
        .isThrownBy(
            () -> {
              FUNCTION_VALIDATOR_EMPTY.validateExpression(
                  testCase.instructions(), TypeContext.EMPTY);
            })
        .withMessageContaining("Unknown table 0; context had 0 tables");
  }

  @ParameterizedTest
  @MethodSource("goodCasesCallIndirect")
  void badCaseWhenWeirdTableSection(final GoodCase testCase) {
    final TableSection weirdTableSection =
        new TableSection(
            List.of(
                new TableType(
                    AddressKind.GLOBAL, new ResizableLimits(new Uint31(1), new Uint31(1)))));
    final FunctionValidator weirdTableValidator =
        new FunctionValidator(
            Uint31.ZERO,
            new GlobalSection(List.of()),
            Map.of(),
            new TypeSection(List.of()),
            weirdTableSection);

    Assertions.assertThatExceptionOfType(WasmValidationException.class)
        .describedAs(
            "Expected validation error for badCaseWhenWeirdTableSection:%s", testCase.name())
        .isThrownBy(
            () -> {
              weirdTableValidator.validateExpression(testCase.instructions(), TypeContext.EMPTY);
            })
        .withMessageContaining("Opcode CALL_INDIRECT cannot call through table 0 of type GLOBAL");
  }

  @Test
  void typeCompatible() {
    Assertions.assertThat(FunctionValidator.compatible(ValType.I32, ValType.I32)).isTrue();
    Assertions.assertThat(FunctionValidator.compatible(ValType.I64, ValType.I32)).isFalse();
    Assertions.assertThat(FunctionValidator.compatible(ValType.I32, ValType.I64)).isFalse();
    Assertions.assertThat(FunctionValidator.compatible(ValType.I64, ValType.I64)).isTrue();
    Assertions.assertThat(FunctionValidator.compatible(ValType.UNKNOWN, ValType.I32)).isTrue();
    Assertions.assertThat(FunctionValidator.compatible(ValType.I32, ValType.UNKNOWN)).isTrue();
    Assertions.assertThat(FunctionValidator.compatible(ValType.UNKNOWN, ValType.UNKNOWN)).isTrue();
  }
}
