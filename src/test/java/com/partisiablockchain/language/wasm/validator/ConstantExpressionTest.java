package com.partisiablockchain.language.wasm.validator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.WasmModule;
import com.partisiablockchain.language.wasm.interpreter.WasmInstance;
import com.partisiablockchain.language.wasm.parser.WasmInterpreterLimitationException;
import com.partisiablockchain.language.wasm.parser.WasmParser;
import com.partisiablockchain.language.wasm.wabt.Wat2Wasm;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Tests that {@link FunctionValidator} can catch various type errors, and let non-problematic
 * programs through.
 */
public final class ConstantExpressionTest {

  record GoodCase(String name, WasmModule module) {

    GoodCase(String name, String watModule) {
      this(name, parseWatModule(watModule));
    }
  }

  record BadCase(String name, String expectedErrorMessage, WasmModule module) {
    BadCase(String name, String expectedErrorMessage, String watModule) {
      this(name, expectedErrorMessage, parseWatModule(watModule));
    }
  }

  static List<GoodCase> goodCases() {
    return List.of(
        new GoodCase("globals const i32", "(module (global i32 i32.const 42))"),
        new GoodCase("globals mutable i32", "(module (global (mut i32) i32.const 42))"),
        new GoodCase("globals const i64", "(module (global i64 i64.const 42))"),
        new GoodCase("globals mutable i64", "(module (global (mut i64) i64.const 42))"),
        new GoodCase(
            "elements const i32", "(module (table 1 funcref) (elem (offset i32.const 0)))"),
        new GoodCase(
            "data const i32", "(module (memory 1) (data 0 (offset i32.const 42) \"blah\"))"));
  }

  static List<BadCase> badCases() {
    return List.of(
        new BadCase(
            "Globals non-constant",
            "Malformed initializer: Constant expression required",
            "(module (global (mut i64) i64.const 42 i64.const 42 i64.add))"),
        new BadCase(
            "Global initializer longer than expected",
            "Stack contained more values than expected: [i64]",
            "(module (global (mut i64) i64.const 42 i64.const 42))"),
        new BadCase(
            "Global initializer shorter than expected",
            "Value stack too shallow",
            "(module (global (mut i64)))"),
        new BadCase(
            "Global initializer GLOBAL_GET, but forgot global decl",
            "Unknown global 1; context had 1 globals",
            "(module (global i64 global.get 1))"),
        new BadCase(
            "Global initializer wrong type",
            "Expected i32, but got i64",
            "(module (global i32 i64.const 1))"),
        new BadCase(
            "Elements non-constant",
            "Malformed initializer: Constant expression required",
            "(module (table 1 funcref) (elem 0 (offset i32.const 42 i32.const 42 i32.add)))"),
        new BadCase(
            "Element initializer longer than expected",
            "Stack contained more values than expected: [i32]",
            "(module (table 1 funcref) (elem 0 (offset i32.const 42 i32.const 42)))"),
        new BadCase(
            "Element initializer shorter than expected",
            "Value stack too shallow",
            "(module (table 1 funcref) (elem 0 (offset)))"),
        new BadCase(
            "Element initializer GLOBAL_GET, but forgot global decl",
            "Unknown global 0; context had 0 globals",
            "(module (table 1 funcref) (elem 0 (offset global.get 0)))"),
        new BadCase(
            "Element initializer GLOBAL_GET mutable not valid",
            "Malformed initializer: Constant expression required",
            "(module (table 1 funcref) (elem 0 (offset global.get 0)) (global (mut i32) global.get"
                + " 0))"),
        new BadCase(
            "Element initializer must be i32",
            "Expected i32, but got i64",
            "(module (table 1 funcref) (elem 0 (offset i64.const 1)))"),
        new BadCase(
            "Datas non-constant",
            "Malformed initializer: Constant expression required",
            "(module (memory 1) (data 0 (offset i32.const 42 i32.const 42 i32.add)))"),
        new BadCase(
            "Data initializer longer than expected",
            "Stack contained more values than expected: [i32]",
            "(module (memory 1) (data 0 (offset i32.const 42 i32.const 42)))"),
        new BadCase(
            "Data initializer shorter than expected",
            "Value stack too shallow",
            "(module (memory 1) (data 0 (offset)))"),
        new BadCase(
            "Data initializer GLOBAL_GET, but forgot global decl",
            "Unknown global 0; context had 0 globals",
            "(module (memory 1) (data 0 (offset global.get 0)))"),
        new BadCase(
            "Data initializer GLOBAL_GET mutable not valid",
            "Malformed initializer: Constant expression required",
            "(module (memory 1) (data 0 (offset global.get 0)) (global (mut i32) global.get 0))"),
        new BadCase(
            "Element initializer must be i32",
            "Expected i32, but got i64",
            "(module (memory 1) (data 0 (offset i64.const 1)))"));
  }

  static List<BadCase> limitationCases() {
    return List.of(
        new BadCase(
            "Global initializer GLOBAL_GET valid but not supported",
            "OpCode GLOBAL_GET not supported in initializer",
            "(module (global i64 global.get 0))"),
        new BadCase(
            "Element initializer GLOBAL_GET valid but not supported",
            "OpCode GLOBAL_GET not supported in initializer",
            "(module (table 1 funcref) (elem 0 (offset global.get 0)) (global i32 global.get 0))"),
        new BadCase(
            "Data initializer GLOBAL_GET valid but not supported",
            "OpCode GLOBAL_GET not supported in initializer",
            "(module (memory 1) (data 0 (offset global.get 0)) (global i32 global.get 0))"));
  }

  @ParameterizedTest
  @MethodSource("goodCases")
  void goodCaseTest(final GoodCase testCase) {
    // No need to assertions; just want it to succeed.
    WasmInstance.forModule(testCase.module());
  }

  @ParameterizedTest
  @MethodSource("badCases")
  void badCaseTest(final BadCase testCase) {
    Assertions.assertThatExceptionOfType(WasmValidationException.class)
        .describedAs("%s", testCase.name())
        .isThrownBy(() -> WasmInstance.forModule(testCase.module()))
        .withMessageContaining(testCase.expectedErrorMessage());
  }

  @ParameterizedTest
  @MethodSource("limitationCases")
  void limitationCaseTest(final BadCase testCase) {
    Assertions.assertThatExceptionOfType(WasmInterpreterLimitationException.class)
        .describedAs("%s", testCase.name())
        .isThrownBy(() -> WasmInstance.forModule(testCase.module()))
        .withMessageContaining(testCase.expectedErrorMessage());
  }

  private static WasmModule parseWatModule(final String watModule) {
    final byte[] wasmBytes = Wat2Wasm.translate(watModule, ConstantExpressionTest.class);
    return new WasmParser(wasmBytes).parse();
  }
}
