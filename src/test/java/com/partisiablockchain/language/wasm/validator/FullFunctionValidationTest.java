package com.partisiablockchain.language.wasm.validator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.wasm.common.FunctionType;
import com.partisiablockchain.language.wasm.common.OpCode;
import com.partisiablockchain.language.wasm.common.Uint31;
import com.partisiablockchain.language.wasm.common.ValType;
import com.partisiablockchain.language.wasm.interpreter.Instruction;
import com.partisiablockchain.language.wasm.interpreter.InstructionNoArgs;
import com.partisiablockchain.language.wasm.interpreter.InstructionU31;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests that {@link FunctionValidator} can catch various type errors, and let non-problematic
 * programs through.
 */
public final class FullFunctionValidationTest {

  private static final Instruction LOCAL_0 = new InstructionU31(OpCode.LOCAL_GET, Uint31.ZERO);
  private static final Instruction END = new InstructionNoArgs(OpCode.END);

  private static final FunctionType TYPE_UNARY_I32 =
      new FunctionType(Uint31.ZERO, List.of(ValType.I32), List.of(ValType.I32));

  private static final FunctionType TYPE_UNARY_F32 =
      new FunctionType(Uint31.ZERO, List.of(ValType.F32), List.of(ValType.F32));

  @Test
  public void badCaseShallowStackOnFunctionEnd() {
    Assertions.assertThatThrownBy(
            () ->
                FunctionValidatorTest.FUNCTION_VALIDATOR_EMPTY.validateFunction(
                    List.of(), List.of(END), TYPE_UNARY_I32))
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining("Value stack too shallow. Expected at least 1 elements, but had 0");
  }

  @Test
  public void badCaseDeepStackOnFunctionEnd() {
    Assertions.assertThatThrownBy(
            () ->
                FunctionValidatorTest.FUNCTION_VALIDATOR_EMPTY.validateFunction(
                    List.of(), List.of(LOCAL_0, LOCAL_0, END), TYPE_UNARY_I32))
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining("Stack contained more values than expected: [i32]");
  }

  @Test
  public void badCaseFloatParameters() {
    Assertions.assertThatThrownBy(
            () ->
                FunctionValidatorTest.FUNCTION_VALIDATOR_EMPTY.validateFunction(
                    List.of(), List.of(LOCAL_0, END), TYPE_UNARY_F32))
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining(
            "Function had type [f32] -> [f32], but floating point parameters and return types are"
                + " not supported");
  }

  @Test
  public void badCaseFloatLocals() {
    Assertions.assertThatThrownBy(
            () ->
                FunctionValidatorTest.FUNCTION_VALIDATOR_EMPTY.validateFunction(
                    List.of(ValType.I32, ValType.F32, ValType.I32),
                    List.of(LOCAL_0, END),
                    TYPE_UNARY_I32))
        .isInstanceOf(WasmValidationException.class)
        .hasMessageContaining(
            "Function had locals [i32, f32, i32], but floating point locals are not supported");
  }
}
